# MCLabFMI

MCLabFMI is a C library which implements missing [FMI](https://www.fmi-standard.org) functions of [JModelica.org v2.1](https://svn.jmodelica.org/tags/2.1/)
distribution in order to get/set/free and serialize the internal state of a FMU.

# Requirements

* [MCLabUtils]()
* [JModelica.org v2.1](https://bitbucket.org/mclab/jmodelica.org)

# Developers

* Agostina Calabrese
* Vadim Alimguzhin
* Stefano Sinisi
* Alessandro Steri

# License
MCLabFMI is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published
by the Free Software Foundation.

MCLabFMI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MCLabFMI.
If not, see [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).



