/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once


#define MCLABFMI_DEFAULT_LOGGING 0
/*! @file
   @brief
   Implementation of FMI API for set/get and free a FMU state
*/

/*! @brief
  Initialize log of MCLabFMI
  @details
  Logging levels are:
  - 0: ERROR
  - 1: WARNING, ERROR
  - 2: INFO, WARNING, ERROR
  - 3: VERBOSE, INFO, WARNING, ERROR
  - 4: DEBUG, VERBOSE, INFO, WARNING, ERROR
*/
void MCLabFMILog_init(int log_level);

/*! @brief
   Serializes the object fmi2_me into fmu_state.
   @details
   If on entry fmu_state == NULL, a new allocation is required. If fmu_state !=
   NULL,
   then fmu_state points to a previously returned fmu_state that has not been
   modified since.
   In particular, MCLabFMI_fmi2FreeFMUstate had not been called with this
   fmu_state as an argument.
   This function reuses the memory of this fmu_state in this case and returns
   the same pointer
   to it, but with the actual fmu_state.
*/
fmi2Status MCLabFMI_fmi2GetFMUstate(fmi2_me_t *fmi2_me, Properties **fmu_state);

/*! @brief
  Copies the content of the previously copied fmu_state back and uses it as
  actual new FMU state.
  @details
  The fmu_state copy does still exist.
*/
fmi2Status MCLabFMI_fmi2SetFMUstate(fmi2_me_t *fmi2_me, Properties *fmu_state);

/*! @brief
  Frees all memory and other resources allocated with the
  MCLabFMI_fmi2GetFMUstate
  call for this fmu_state.
  @details
  The input argument to this function is the fmu_state
  to be freed. If a null pointer is provided, the call is ignored. The function
  returns a null pointer in argument fmu_state.
*/
fmi2Status MCLabFMI_fmi2FreeFMUstate(fmi2_me_t *fmi2_me,
                                     Properties **fmu_state);
