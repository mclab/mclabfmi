/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __MCLABFMI_UTILS__
#define __MCLABFMI_UTILS__

#include <MCLabUtils.h>

#define MCLABFMI_LOG_INFO "MCLabFMILog INFO"
#define MCLABFMI_LOG_VERBOSE "MCLabFMILog VERBOSE"
#define MCLABFMI_LOG_WARNING "MCLabFMILog WARNING"
#define MCLABFMI_LOG_ERROR "MCLabFMILog ERROR"
#define MCLabFMILog_enableInfo() Debug_enableOut(MCLABFMI_LOG_INFO);
#define MCLabFMILog_enableVerbose() Debug_enableOut(MCLABFMI_LOG_VERBOSE);
#define MCLabFMILog_enableWarning() Debug_enableOut(MCLABFMI_LOG_WARNING);
#define MCLabFMILog_enableError() Debug_enableOut(MCLABFMI_LOG_ERROR);
#define MCLabFMILog_info(...)                                               \
  if (Debug_isEnabled(MCLABFMI_LOG_INFO, _OUT)) do {                        \
      fprintf(stdout, "[" MCLABFMI_LOG_INFO "]%s ", Debug_indent_string()); \
      fprintf(stdout, __VA_ARGS__);                                         \
      fprintf(stdout, "\n");                                                \
  } while (0)
#define MCLabFMILog_verbose(...)                                               \
  if (Debug_isEnabled(MCLABFMI_LOG_VERBOSE, _OUT)) do {                        \
      fprintf(stdout, "[" MCLABFMI_LOG_VERBOSE "]%s ", Debug_indent_string()); \
      fprintf(stdout, __VA_ARGS__);                                            \
      fprintf(stdout, "\n");                                                   \
  } while (0)
#define MCLabFMILog_warning(...)                            \
  if (Debug_isEnabled(MCLABFMI_LOG_WARNING, _OUT)) do {     \
      fprintf(stdout, "[" MCLABFMI_LOG_WARNING "]%s \"",    \
              Debug_indent_string());                       \
      fprintf(stdout, __VA_ARGS__);                         \
      fprintf(stdout, "\" at " __FILE__ ":%d\n", __LINE__); \
  } while (0)
#define MCLabFMILog_error(...)                                                 \
  if (Debug_isEnabled(MCLABFMI_LOG_ERROR, _OUT)) do {                          \
      fprintf(stderr, "[" MCLABFMI_LOG_ERROR "]%s \"", Debug_indent_string()); \
      fprintf(stdout, __VA_ARGS__);                                            \
      fprintf(stdout, "\" at " __FILE__ ":%d\n", __LINE__);                    \
  } while (0)
#define MCLabFMILog_nest() Debug_outNest_real_function();
#define MCLabFMILog_unnest() Debug_outUnnest_real_function();
#define MCLabFMI_error(...)                                        \
  do {                                                             \
    MCLabFMILog_error(__VA_ARGS__);                                \
    fprintf(stderr, "Fatal Error at " __FILE__ ":%d\n", __LINE__); \
    abort();                                                       \
  } while (0);

#define SAFETY_CHECK_STRING(ser, field, field_name, passport)                \
  do {                                                                       \
    if (StringUtils_equals(                                                  \
            field, (const char *)Serializable_getField(ser, field_name, passport)) == 0) { \
      MCLabFMILog_error(                                                     \
          "Value of deserialized %s->%s is '%s', should be '%s' "            \
          "instead. This could cause errors",                                \
          Serializable_className(ser), field_name,                           \
          (const char *)Serializable_getField(ser, field_name, passport), field);          \
    }                                                                        \
  } while (0);

#define SAFETY_CHECK_SIZE(ser, field, field_name, field_type, passport, spec) \
  do {                                                                        \
    if (field !=                                                              \
        *(field_type *) Serializable_getField(ser, field_name, passport)) {   \
      MCLabFMILog_error(                                                      \
          "Value of deserialized %s->%s is '" spec "', should be '" spec      \
          "' instead. This could cause errors.",                              \
          Serializable_className(ser), field_name,                            \
          *(field_type *) Serializable_getField(ser, field_name, passport),   \
          field);                                                             \
    }                                                                         \
  } while (0);

#define INTO_SIZE_T(obj, field) size_t field = (size_t) obj->field

#define NEW_SER_ARRAY(this_, field, sub_field, ser_field, ser_field_type, \
                      ser_field_constructor, it_var, it_type, env, op)    \
  do {                                                                    \
    it_type it_var = this_->field->it_var;                                \
    this_->ser_field = calloc(it_var, sizeof(ser_field_type));            \
    for (it_type i = 0; i < it_var; i++) {                                \
      this_->ser_field[i] =                                               \
          ser_field_constructor(env, op(field->sub_field[i]));            \
    }                                                                     \
  } while (0);

#endif
