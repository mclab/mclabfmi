/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_Z_OFFSETS_T.h"

#define DEBUG "SerializableJMI_Z_OFFSETS_T"

#define CLASS_NAME "jmi_z_offsets_t"

// defining constants for fields name
#define FIELD_O_CI "o_ci"
#define FIELD_O_CD "o_cd"
#define FIELD_O_PI "o_pi"
#define FIELD_O_PD "o_pd"
#define FIELD_O_PS "o_ps"
#define FIELD_O_PF "o_pf"
#define FIELD_O_PE "o_pe"
#define FIELD_N_CI "n_ci"
#define FIELD_N_CD "n_cd"
#define FIELD_N_PI "n_pi"
#define FIELD_N_PD "n_pd"
#define FIELD_N_PS "n_ps"
#define FIELD_N_PF "n_pf"
#define FIELD_N_PE "n_pe"
#define FIELD_N "n"

struct SerializableJMI_Z_OFFSETS_T {
  jmi_z_offsets_t *jmi_z_offsets;
  // Adding inner struct
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_Z_OFFSETS_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_Z_OFFSETS_T *ser_jmi_z_offsets = (SerializableJMI_Z_OFFSETS_T *)this_v;
  return ser_jmi_z_offsets->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_Z_OFFSETS_T *ser_jmi_z_offsets = (SerializableJMI_Z_OFFSETS_T *)this_v;
  jmi_z_offsets_t *jmi_z_offsets = ser_jmi_z_offsets->jmi_z_offsets;
  const void *passport = ser_jmi_z_offsets->passport;
  Serializable *serializable = ser_jmi_z_offsets->serializable;
  MCLabFMILog_verbose("o_ci: %d", jmi_z_offsets->o_ci);
  Serializable_setField(serializable, FIELD_O_CI, PROPERTIES_INT,
                       &jmi_z_offsets->o_ci, passport);
  MCLabFMILog_verbose("o_cd: %d", jmi_z_offsets->o_cd);
  Serializable_setField(serializable, FIELD_O_CD, PROPERTIES_INT,
                       &jmi_z_offsets->o_cd, passport);
  MCLabFMILog_verbose("o_pi: %d", jmi_z_offsets->o_pi);
  Serializable_setField(serializable, FIELD_O_PI, PROPERTIES_INT,
                       &jmi_z_offsets->o_pi, passport);
  MCLabFMILog_verbose("o_pd: %d", jmi_z_offsets->o_pd);
  Serializable_setField(serializable, FIELD_O_PD, PROPERTIES_INT,
                       &jmi_z_offsets->o_pd, passport);
  MCLabFMILog_verbose("o_ps: %d", jmi_z_offsets->o_ps);
  Serializable_setField(serializable, FIELD_O_PS, PROPERTIES_INT,
                       &jmi_z_offsets->o_ps, passport);
  MCLabFMILog_verbose("o_pf: %d", jmi_z_offsets->o_pf);
  Serializable_setField(serializable, FIELD_O_PF, PROPERTIES_INT,
                       &jmi_z_offsets->o_pf, passport);
  MCLabFMILog_verbose("o_pe: %d", jmi_z_offsets->o_pe);
  Serializable_setField(serializable, FIELD_O_PE, PROPERTIES_INT,
                       &jmi_z_offsets->o_pe, passport);
  MCLabFMILog_verbose("n_ci: %d", jmi_z_offsets->n_ci);
  Serializable_setField(serializable, FIELD_N_CI, PROPERTIES_INT,
                       &jmi_z_offsets->n_ci, passport);
  MCLabFMILog_verbose("n_cd: %d", jmi_z_offsets->n_cd);
  Serializable_setField(serializable, FIELD_N_CD, PROPERTIES_INT,
                       &jmi_z_offsets->n_cd, passport);
  MCLabFMILog_verbose("n_pi: %d", jmi_z_offsets->n_pi);
  Serializable_setField(serializable, FIELD_N_PI, PROPERTIES_INT,
                       &jmi_z_offsets->n_pi, passport);
  MCLabFMILog_verbose("n_pd: %d", jmi_z_offsets->n_pd);
  Serializable_setField(serializable, FIELD_N_PD, PROPERTIES_INT,
                       &jmi_z_offsets->n_pd, passport);
  MCLabFMILog_verbose("n_ps: %d", jmi_z_offsets->n_ps);
  Serializable_setField(serializable, FIELD_N_PS, PROPERTIES_INT,
                       &jmi_z_offsets->n_ps, passport);
  MCLabFMILog_verbose("n_pf: %d", jmi_z_offsets->n_pf);
  Serializable_setField(serializable, FIELD_N_PF, PROPERTIES_INT,
                       &jmi_z_offsets->n_pf, passport);
  MCLabFMILog_verbose("n_pe: %d", jmi_z_offsets->n_pe);
  Serializable_setField(serializable, FIELD_N_PE, PROPERTIES_INT,
                       &jmi_z_offsets->n_pe, passport);
  MCLabFMILog_verbose("n: %d", jmi_z_offsets->n);
  Serializable_setField(serializable, FIELD_N, PROPERTIES_INT,
                       &jmi_z_offsets->n, passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_Z_OFFSETS_T *ser_jmi_z_offsets = (SerializableJMI_Z_OFFSETS_T *)this_v;
  jmi_z_offsets_t *jmi_z_offsets = ser_jmi_z_offsets->jmi_z_offsets;
  const void *passport = ser_jmi_z_offsets->passport;
  Serializable *serializable = ser_jmi_z_offsets->serializable;
  // Deserializing simple fields via script
  jmi_z_offsets->o_ci =
    *(int *) Serializable_getField(serializable, FIELD_O_CI, passport);
  MCLabFMILog_verbose("o_ci: %d", jmi_z_offsets->o_ci);
  jmi_z_offsets->o_cd =
    *(int *) Serializable_getField(serializable, FIELD_O_CD, passport);
  MCLabFMILog_verbose("o_cd: %d", jmi_z_offsets->o_cd);
  jmi_z_offsets->o_pi =
    *(int *) Serializable_getField(serializable, FIELD_O_PI, passport);
  MCLabFMILog_verbose("o_pi: %d", jmi_z_offsets->o_pi);
  jmi_z_offsets->o_pd =
    *(int *) Serializable_getField(serializable, FIELD_O_PD, passport);
  MCLabFMILog_verbose("o_pd: %d", jmi_z_offsets->o_pd);
  jmi_z_offsets->o_ps =
    *(int *) Serializable_getField(serializable, FIELD_O_PS, passport);
  MCLabFMILog_verbose("o_ps: %d", jmi_z_offsets->o_ps);
  jmi_z_offsets->o_pf =
    *(int *) Serializable_getField(serializable, FIELD_O_PF, passport);
  MCLabFMILog_verbose("o_pf: %d", jmi_z_offsets->o_pf);
  jmi_z_offsets->o_pe =
    *(int *) Serializable_getField(serializable, FIELD_O_PE, passport);
  MCLabFMILog_verbose("o_pe: %d", jmi_z_offsets->o_pe);
  jmi_z_offsets->n_ci =
    *(int *) Serializable_getField(serializable, FIELD_N_CI, passport);
  MCLabFMILog_verbose("n_ci: %d", jmi_z_offsets->n_ci);
  jmi_z_offsets->n_cd =
    *(int *) Serializable_getField(serializable, FIELD_N_CD, passport);
  MCLabFMILog_verbose("n_cd: %d", jmi_z_offsets->n_cd);
  jmi_z_offsets->n_pi =
    *(int *) Serializable_getField(serializable, FIELD_N_PI, passport);
  MCLabFMILog_verbose("n_pi: %d", jmi_z_offsets->n_pi);
  jmi_z_offsets->n_pd =
    *(int *) Serializable_getField(serializable, FIELD_N_PD, passport);
  MCLabFMILog_verbose("n_pd: %d", jmi_z_offsets->n_pd);
  jmi_z_offsets->n_ps =
    *(int *) Serializable_getField(serializable, FIELD_N_PS, passport);
  MCLabFMILog_verbose("n_ps: %d", jmi_z_offsets->n_ps);
  jmi_z_offsets->n_pf =
    *(int *) Serializable_getField(serializable, FIELD_N_PF, passport);
  MCLabFMILog_verbose("n_pf: %d", jmi_z_offsets->n_pf);
  jmi_z_offsets->n_pe =
    *(int *) Serializable_getField(serializable, FIELD_N_PE, passport);
  MCLabFMILog_verbose("n_pe: %d", jmi_z_offsets->n_pe);
  jmi_z_offsets->n =
    *(int *) Serializable_getField(serializable, FIELD_N, passport);
  MCLabFMILog_verbose("n: %d", jmi_z_offsets->n);
  MCLabFMILog_unnest();
}

SerializableJMI_Z_OFFSETS_T *SerializableJMI_Z_OFFSETS_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_z_offsets_t *jmi_z_offsets) {
  Debug_assert(DEBUG_ALWAYS, jmi_z_offsets != NULL, "jmi_z_offsets == NULL");
  SerializableJMI_Z_OFFSETS_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_z_offsets)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_Z_OFFSETS_T));
    new->jmi_z_offsets = jmi_z_offsets;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_z_offsets, new,
                    SerializableJMI_Z_OFFSETS_T_free, new->passport);
    // Adding inner struct new
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_Z_OFFSETS_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_Z_OFFSETS_T *this_ = (SerializableJMI_Z_OFFSETS_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_Z_OFFSETS_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_Z_OFFSETS_T *this_ = (SerializableJMI_Z_OFFSETS_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
