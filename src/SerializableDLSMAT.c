/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include <sundials/sundials_direct.h>
#include "jmi.h"
#include "SerializableDLSMAT.h"

#include "MCLabFMIEnv.h"

#define DEBUG "SerializableDLSMAT"

#define CLASS_NAME "DlsMat"

// defining constants for fields name
#define FIELD_TYPE "type"
#define FIELD_M "M"
#define FIELD_N "N"
#define FIELD_LDIM "ldim"
#define FIELD_MU "mu"
#define FIELD_ML "ml"
#define FIELD_S_MU "s_mu"
#define FIELD_DATA "data"
#define FIELD_LDATA "ldata"
#define FIELD_COLS "cols"


struct SerializableDLSMAT {
  DlsMat m;
  // Adding inner struct
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableDLSMAT_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableDLSMAT *ser_m = (SerializableDLSMAT *)this_v;
  return ser_m->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableDLSMAT *ser_m = (SerializableDLSMAT *)this_v;
  DlsMat m = ser_m->m;
  const void *passport = ser_m->passport;
  Serializable *serializable = ser_m->serializable;
  MCLabFMILog_verbose("type: %d", m->type);
  Serializable_setField(serializable, FIELD_TYPE, PROPERTIES_INT, &m->type, passport);
  MCLabFMILog_verbose("M: %ld", m->M);
  Serializable_setField(serializable, FIELD_M, PROPERTIES_LONG, &m->M, passport);
  MCLabFMILog_verbose("N: %ld", m->N);
  Serializable_setField(serializable, FIELD_N, PROPERTIES_LONG, &m->N, passport);
  MCLabFMILog_verbose("ldim: %ld", m->ldim);
  Serializable_setField(serializable, FIELD_LDIM, PROPERTIES_LONG, &m->ldim, passport);
  MCLabFMILog_verbose("mu: %ld", m->mu);
  Serializable_setField(serializable, FIELD_MU, PROPERTIES_LONG, &m->mu, passport);
  MCLabFMILog_verbose("ml: %ld", m->ml);
  Serializable_setField(serializable, FIELD_ML, PROPERTIES_LONG, &m->ml, passport);
  MCLabFMILog_verbose("s_mu: %ld", m->s_mu);
  Serializable_setField(serializable, FIELD_S_MU, PROPERTIES_LONG, &m->s_mu, passport);
  MCLabFMILog_verbose("ldata: %ld", m->ldata);
  Serializable_setField(serializable, FIELD_LDATA, PROPERTIES_LONG, &m->ldata, passport);
  size_t data_size = (size_t) m->ldata;
  MCLabFMILog_verbose("data[%zu]", data_size);
  Serializable_setArrayField(serializable, FIELD_DATA, PROPERTIES_DOUBLE, (void **)m->data, sizeof(double), &data_size, NULL, 0, false, NULL, passport);
  // size_t cols_size = (size_t) m->N;
  // MCLabFMILog_verbose("cols[%zu]", cols_size);
  // Serializable_setArrayField(serializable, FIELD_COLS, PROPERTIES_POINTER, (void **)m->cols, sizeof(double *), &cols_size, NULL, 0, false, NULL, passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableDLSMAT *ser_m = (SerializableDLSMAT *)this_v;
  DlsMat m = ser_m->m;
  const void *passport = ser_m->passport;
  Serializable *serializable = ser_m->serializable;
  // Deserializing simple fields via script
  m->type = *(int *) Serializable_getField(serializable, FIELD_TYPE, passport);
  m->M = *(long *) Serializable_getField(serializable, FIELD_M, passport);
  m->N = *(long *) Serializable_getField(serializable, FIELD_N, passport);
  m->ldim = *(long *) Serializable_getField(serializable, FIELD_LDIM, passport);
  m->mu = *(long *) Serializable_getField(serializable, FIELD_MU, passport);
  m->ml = *(long *) Serializable_getField(serializable, FIELD_ML, passport);
  m->s_mu = *(long *) Serializable_getField(serializable, FIELD_S_MU, passport);
  size_t data_size = (size_t) m->ldata;
  Serializable_getArrayField(serializable, FIELD_DATA, (void **)m->data, sizeof(double), &data_size, 0, passport);
  // size_t cols_size = (size_t) m->N;
  // Serializable_getArrayField(serializable, FIELD_COLS, (void **)m->cols, sizeof(double *), &cols_size, 0, passport);
  MCLabFMILog_unnest();
}

SerializableDLSMAT *SerializableDLSMAT_new(MCLabFMIEnv *fmi_env, DlsMat m) {
  Debug_assert(DEBUG_ALWAYS, m != NULL, "m == NULL");
  SerializableDLSMAT *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, m)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableDLSMAT));
    new->m = m;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, m, new,
                    SerializableDLSMAT_free, new->passport);
    // Adding inner struct new
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableDLSMAT_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableDLSMAT *this_ = (SerializableDLSMAT *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableDLSMAT_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableDLSMAT *this_ = (SerializableDLSMAT *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
