/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once
// TODO insert includes for used struct, and check if this two are ok
#include <MCLabUtils.h>
#include "MCLabFMIEnv.h"
#include "jmi_types.h"

typedef struct SerializableJMI_MATRIX_SPARSE_CSC_T SerializableJMI_MATRIX_SPARSE_CSC_T;

SerializableJMI_MATRIX_SPARSE_CSC_T *SerializableJMI_MATRIX_SPARSE_CSC_T_new(MCLabFMIEnv *fmi_env, jmi_matrix_sparse_csc_t *matrix);

Serializable *SerializableJMI_MATRIX_SPARSE_CSC_T_asSerializable(void *this_v);
