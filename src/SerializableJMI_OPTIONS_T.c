/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi_util.h"
#include "SerializableJMI_BLOCK_SOLVER_OPTIONS_T.h"
#include "SerializableJMI_LOG_OPTIONS_T.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_OPTIONS_T.h"

#define DEBUG "SerializableJMI_OPTIONS_T"

#define CLASS_NAME "jmi_options_t"

// defining constants for fields name
#define FIELD_BLOCK_SOLVER_OPTIONS "block_solver_options"
#define FIELD_LOG_OPTIONS "log_options"
#define FIELD_NLE_SOLVER_DEFAULT_TOL "nle_solver_default_tol"
#define FIELD_NLE_SOLVER_TOL_FACTOR "nle_solver_tol_factor"
#define FIELD_EVENTS_DEFAULT_TOL "events_default_tol"
#define FIELD_TIME_EVENTS_DEFAULT_TOL "time_events_default_tol"
#define FIELD_EVENTS_TOL_FACTOR "events_tol_factor"
#define FIELD_CS_SOLVER "cs_solver"
#define FIELD_CS_REL_TOL "cs_rel_tol"
#define FIELD_CS_STEP_SIZE "cs_step_size"
#define FIELD_CS_EXPERIMENTAL_MODE "cs_experimental_mode"

struct SerializableJMI_OPTIONS_T {
  jmi_options_t *jmi_options;
  // Adding inner struct
  SerializableJMI_BLOCK_SOLVER_OPTIONS_T *ser_block_solver_options;
  SerializableJMI_LOG_OPTIONS_T *ser_log_options;
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_OPTIONS_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_OPTIONS_T *ser_jmi_options = (SerializableJMI_OPTIONS_T *)this_v;
  return ser_jmi_options->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_OPTIONS_T *ser_jmi_options = (SerializableJMI_OPTIONS_T *)this_v;
  jmi_options_t *jmi_options = ser_jmi_options->jmi_options;
  const void *passport = ser_jmi_options->passport;
  Serializable *serializable = ser_jmi_options->serializable;
  Serializable_setField(serializable, FIELD_BLOCK_SOLVER_OPTIONS, PROPERTIES_POINTER,
                       SerializableJMI_BLOCK_SOLVER_OPTIONS_T_asSerializable(ser_jmi_options->ser_block_solver_options), passport);
  Serializable_setField(serializable, FIELD_LOG_OPTIONS, PROPERTIES_POINTER,
                       SerializableJMI_LOG_OPTIONS_T_asSerializable(ser_jmi_options->ser_log_options), passport);
  MCLabFMILog_verbose("nle_solver_default_tol: %g", jmi_options->nle_solver_default_tol);
  Serializable_setField(serializable, FIELD_NLE_SOLVER_DEFAULT_TOL, PROPERTIES_DOUBLE,
                       &jmi_options->nle_solver_default_tol, passport);
  MCLabFMILog_verbose("nle_solver_tol_factor: %g", jmi_options->nle_solver_tol_factor);
  Serializable_setField(serializable, FIELD_NLE_SOLVER_TOL_FACTOR, PROPERTIES_DOUBLE,
                       &jmi_options->nle_solver_tol_factor, passport);
  MCLabFMILog_verbose("events_default_tol: %g", jmi_options->events_default_tol);
  Serializable_setField(serializable, FIELD_EVENTS_DEFAULT_TOL, PROPERTIES_DOUBLE,
                       &jmi_options->events_default_tol, passport);
  MCLabFMILog_verbose("time_events_default_tol: %g", jmi_options->time_events_default_tol);
  Serializable_setField(serializable, FIELD_TIME_EVENTS_DEFAULT_TOL, PROPERTIES_DOUBLE,
                       &jmi_options->time_events_default_tol, passport);
  MCLabFMILog_verbose("events_tol_factor: %g", jmi_options->events_tol_factor);
  Serializable_setField(serializable, FIELD_EVENTS_TOL_FACTOR, PROPERTIES_DOUBLE,
                       &jmi_options->events_tol_factor, passport);
  MCLabFMILog_verbose("cs_solver: %d", jmi_options->cs_solver);
  Serializable_setField(serializable, FIELD_CS_SOLVER, PROPERTIES_INT,
                       &jmi_options->cs_solver, passport);
  MCLabFMILog_verbose("cs_rel_tol: %g", jmi_options->cs_rel_tol);
  Serializable_setField(serializable, FIELD_CS_REL_TOL, PROPERTIES_DOUBLE,
                       &jmi_options->cs_rel_tol, passport);
  MCLabFMILog_verbose("cs_step_size: %g", jmi_options->cs_step_size);
  Serializable_setField(serializable, FIELD_CS_STEP_SIZE, PROPERTIES_DOUBLE,
                       &jmi_options->cs_step_size, passport);
  MCLabFMILog_verbose("cs_experimental_mode: %d", jmi_options->cs_experimental_mode);
  Serializable_setField(serializable, FIELD_CS_EXPERIMENTAL_MODE, PROPERTIES_INT,
                       &jmi_options->cs_experimental_mode, passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_OPTIONS_T *ser_jmi_options = (SerializableJMI_OPTIONS_T *)this_v;
  jmi_options_t *jmi_options = ser_jmi_options->jmi_options;
  const void *passport = ser_jmi_options->passport;
  Serializable *serializable = ser_jmi_options->serializable;
  // Deserializing simple fields via script
  ser_jmi_options->ser_block_solver_options =
        *(SerializableJMI_BLOCK_SOLVER_OPTIONS_T **) Serializable_getField(serializable, FIELD_BLOCK_SOLVER_OPTIONS, passport);
  ser_jmi_options->ser_log_options =
        *(SerializableJMI_LOG_OPTIONS_T **) Serializable_getField(serializable, FIELD_LOG_OPTIONS, passport);
  jmi_options->nle_solver_default_tol =
    *(double *) Serializable_getField(serializable, FIELD_NLE_SOLVER_DEFAULT_TOL, passport);
  MCLabFMILog_verbose("nle_solver_default_tol: %g", jmi_options->nle_solver_default_tol);
  jmi_options->nle_solver_tol_factor =
    *(double *) Serializable_getField(serializable, FIELD_NLE_SOLVER_TOL_FACTOR, passport);
  MCLabFMILog_verbose("nle_solver_tol_factor: %g", jmi_options->nle_solver_tol_factor);
  jmi_options->events_default_tol =
    *(double *) Serializable_getField(serializable, FIELD_EVENTS_DEFAULT_TOL, passport);
  MCLabFMILog_verbose("events_default_tol: %g", jmi_options->events_default_tol);
  jmi_options->time_events_default_tol =
    *(double *) Serializable_getField(serializable, FIELD_TIME_EVENTS_DEFAULT_TOL, passport);
  MCLabFMILog_verbose("time_events_default_tol: %g", jmi_options->time_events_default_tol);
  jmi_options->events_tol_factor =
    *(double *) Serializable_getField(serializable, FIELD_EVENTS_TOL_FACTOR, passport);
  MCLabFMILog_verbose("events_tol_factor: %g", jmi_options->events_tol_factor);
  jmi_options->cs_solver =
    *(int *) Serializable_getField(serializable, FIELD_CS_SOLVER, passport);
  MCLabFMILog_verbose("cs_solver: %d", jmi_options->cs_solver);
  jmi_options->cs_rel_tol =
    *(double *) Serializable_getField(serializable, FIELD_CS_REL_TOL, passport);
  MCLabFMILog_verbose("cs_rel_tol: %g", jmi_options->cs_rel_tol);
  jmi_options->cs_step_size =
    *(double *) Serializable_getField(serializable, FIELD_CS_STEP_SIZE, passport);
  MCLabFMILog_verbose("cs_step_size: %g", jmi_options->cs_step_size);
  jmi_options->cs_experimental_mode =
    *(int *) Serializable_getField(serializable, FIELD_CS_EXPERIMENTAL_MODE, passport);
  MCLabFMILog_verbose("cs_experimental_mode: %d", jmi_options->cs_experimental_mode);
  MCLabFMILog_unnest();
}

SerializableJMI_OPTIONS_T *SerializableJMI_OPTIONS_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_options_t *jmi_options) {
  Debug_assert(DEBUG_ALWAYS, jmi_options != NULL, "jmi_options == NULL");
  SerializableJMI_OPTIONS_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_options)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_OPTIONS_T));
    new->jmi_options = jmi_options;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_options, new,
                    SerializableJMI_OPTIONS_T_free, new->passport);
    // Adding inner struct new
    new->ser_block_solver_options = SerializableJMI_BLOCK_SOLVER_OPTIONS_T_new(fmi_env, &jmi_options->block_solver_options);
    new->ser_log_options = SerializableJMI_LOG_OPTIONS_T_new(fmi_env, jmi_options->log_options);
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_OPTIONS_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_OPTIONS_T *this_ = (SerializableJMI_OPTIONS_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_OPTIONS_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_OPTIONS_T *this_ = (SerializableJMI_OPTIONS_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
