/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "jmi_dynamic_state.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_DYNAMIC_STATE_SET_T.h"

#define DEBUG "SerializableJMI_DYNAMIC_STATE_SET_T"

#define CLASS_NAME "jmi_dynamic_state_set_t"

// defining constants for fields name
#define FIELD_N_VARIABLES "n_variables"
#define FIELD_N_STATES "n_states"
#define FIELD_N_ALGEBRAICS "n_algebraics"
#define FIELD_VARIABLES_VALUE_REFERENCES "variables_value_references"
#define FIELD_STATE_VALUE_REFERENCES "state_value_references"
#define FIELD_DS_STATE_VALUE_REFERENCES "ds_state_value_references"
#define FIELD_DS_STATE_VALUE_LOCAL_INDEX "ds_state_value_local_index"
#define FIELD_ALGEBRAIC_VALUE_REFERENCES "algebraic_value_references"
#define FIELD_DS_ALGEBRAIC_VALUE_REFERENCES "ds_algebraic_value_references"
#define FIELD_DS_ALGEBRAIC_VALUE_LOCAL_INDEX "ds_algebraic_value_local_index"
#define FIELD_TEMP_ALGEBRAIC "temp_algebraic"
#define FIELD_COEFFICENT_MATRIX "coefficent_matrix"
#define FIELD_SUB_COEFFICENT_MATRIX "sub_coefficent_matrix"
#define FIELD_DGEQP3_WORK "dgeqp3_work"
#define FIELD_DGEQP3_TAU "dgeqp3_tau"
#define FIELD_DGEQP3_JPVT "dgeqp3_jpvt"
#define FIELD_DGEQP3_LWORK "dgeqp3_lwork"


struct SerializableJMI_DYNAMIC_STATE_SET_T {
  jmi_dynamic_state_set_t *jmi_dynamic_state_set;
  // Adding inner struct
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_DYNAMIC_STATE_SET_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_DYNAMIC_STATE_SET_T *ser_jmi_dynamic_state_set = (SerializableJMI_DYNAMIC_STATE_SET_T *)this_v;
  return ser_jmi_dynamic_state_set->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_DYNAMIC_STATE_SET_T *ser_jmi_dynamic_state_set = (SerializableJMI_DYNAMIC_STATE_SET_T *)this_v;
  jmi_dynamic_state_set_t *jmi_dynamic_state_set = ser_jmi_dynamic_state_set->jmi_dynamic_state_set;
  const void *passport = ser_jmi_dynamic_state_set->passport;
  Serializable *serializable = ser_jmi_dynamic_state_set->serializable;
  MCLabFMILog_verbose("n_variables: %d", jmi_dynamic_state_set->n_variables);
  Serializable_setField(serializable, FIELD_N_VARIABLES, PROPERTIES_INT,
                       &jmi_dynamic_state_set->n_variables, passport);
  MCLabFMILog_verbose("n_states: %d", jmi_dynamic_state_set->n_states);
  Serializable_setField(serializable, FIELD_N_STATES, PROPERTIES_INT,
                       &jmi_dynamic_state_set->n_states, passport);
  MCLabFMILog_verbose("n_algebraics: %d", jmi_dynamic_state_set->n_algebraics);
  Serializable_setField(serializable, FIELD_N_ALGEBRAICS, PROPERTIES_INT,
                       &jmi_dynamic_state_set->n_algebraics, passport);
  MCLabFMILog_verbose("variables_value_references[%d]", jmi_dynamic_state_set->n_variables);
  size_t variables_value_references_size_t = (size_t) jmi_dynamic_state_set->n_variables;
  Serializable_setArrayField(serializable, FIELD_VARIABLES_VALUE_REFERENCES, PROPERTIES_INT,
  	                        (void **)jmi_dynamic_state_set->variables_value_references, sizeof(int), &variables_value_references_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("state_value_references[%d]", jmi_dynamic_state_set->n_states);
  size_t state_value_references_size_t = (size_t) jmi_dynamic_state_set->n_states;
  Serializable_setArrayField(serializable, FIELD_STATE_VALUE_REFERENCES, PROPERTIES_INT,
  	                        (void **)jmi_dynamic_state_set->state_value_references, sizeof(int), &state_value_references_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("ds_state_value_references[%d]", jmi_dynamic_state_set->n_states);
  size_t ds_state_value_references_size_t = (size_t) jmi_dynamic_state_set->n_states;
  Serializable_setArrayField(serializable, FIELD_DS_STATE_VALUE_REFERENCES, PROPERTIES_INT,
  	                        (void **)jmi_dynamic_state_set->ds_state_value_references, sizeof(int), &ds_state_value_references_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("ds_state_value_local_index[%d]", jmi_dynamic_state_set->n_states);
  size_t ds_state_value_local_index_size_t = (size_t) jmi_dynamic_state_set->n_states;
  Serializable_setArrayField(serializable, FIELD_DS_STATE_VALUE_LOCAL_INDEX, PROPERTIES_INT,
  	                        (void **)jmi_dynamic_state_set->ds_state_value_local_index, sizeof(int), &ds_state_value_local_index_size_t,
  	                        NULL, 0, false, NULL, passport);
  size_t algebraic_value_references_size = (size_t) ((jmi_dynamic_state_set->n_variables-jmi_dynamic_state_set->n_states));
  MCLabFMILog_verbose("algebraic_value_references[%zu]", algebraic_value_references_size);
  Serializable_setArrayField(serializable, FIELD_ALGEBRAIC_VALUE_REFERENCES, PROPERTIES_INT,
  	                        (void **)jmi_dynamic_state_set->algebraic_value_references, sizeof(int), &algebraic_value_references_size,
  	                        NULL, 0, false, NULL, passport);
  size_t ds_algebraic_value_references_size = (size_t) ((jmi_dynamic_state_set->n_variables-jmi_dynamic_state_set->n_states));
  MCLabFMILog_verbose("ds_algebraic_value_references[%zu]", ds_algebraic_value_references_size);
  Serializable_setArrayField(serializable, FIELD_DS_ALGEBRAIC_VALUE_REFERENCES, PROPERTIES_INT,
  	                        (void **)jmi_dynamic_state_set->ds_algebraic_value_references, sizeof(int), &ds_algebraic_value_references_size,
  	                        NULL, 0, false, NULL, passport);
  size_t ds_algebraic_value_local_index_size = (size_t) ((jmi_dynamic_state_set->n_variables-jmi_dynamic_state_set->n_states));
  MCLabFMILog_verbose("ds_algebraic_value_local_index[%zu]", ds_algebraic_value_local_index_size);
  Serializable_setArrayField(serializable, FIELD_DS_ALGEBRAIC_VALUE_LOCAL_INDEX, PROPERTIES_INT,
  	                        (void **)jmi_dynamic_state_set->ds_algebraic_value_local_index, sizeof(int), &ds_algebraic_value_local_index_size,
  	                        NULL, 0, false, NULL, passport);
  size_t temp_algebraic_size = (size_t) ((jmi_dynamic_state_set->n_variables-jmi_dynamic_state_set->n_states));
  MCLabFMILog_verbose("temp_algebraic[%zu]", temp_algebraic_size);
  Serializable_setArrayField(serializable, FIELD_TEMP_ALGEBRAIC, PROPERTIES_INT,
  	                        (void **)jmi_dynamic_state_set->temp_algebraic, sizeof(int), &temp_algebraic_size,
  	                        NULL, 0, false, NULL, passport);
  size_t coefficent_matrix_size = (size_t) (((jmi_dynamic_state_set->n_variables-jmi_dynamic_state_set->n_states)*jmi_dynamic_state_set->n_variables));
  MCLabFMILog_verbose("coefficent_matrix[%zu]", coefficent_matrix_size);
  Serializable_setArrayField(serializable, FIELD_COEFFICENT_MATRIX, PROPERTIES_DOUBLE,
  	                        (void **)jmi_dynamic_state_set->coefficent_matrix, sizeof(double), &coefficent_matrix_size,
  	                        NULL, 0, false, NULL, passport);
  size_t sub_coefficent_matrix_size = (size_t) (((jmi_dynamic_state_set->n_variables-jmi_dynamic_state_set->n_states)*(jmi_dynamic_state_set->n_variables-jmi_dynamic_state_set->n_states)));
  MCLabFMILog_verbose("sub_coefficent_matrix[%zu]", sub_coefficent_matrix_size);
  Serializable_setArrayField(serializable, FIELD_SUB_COEFFICENT_MATRIX, PROPERTIES_DOUBLE,
  	                        (void **)jmi_dynamic_state_set->sub_coefficent_matrix, sizeof(double), &sub_coefficent_matrix_size,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("dgeqp3_work[%d]", jmi_dynamic_state_set->dgeqp3_lwork);
  size_t dgeqp3_work_size_t = (size_t) jmi_dynamic_state_set->dgeqp3_lwork;
  Serializable_setArrayField(serializable, FIELD_DGEQP3_WORK, PROPERTIES_DOUBLE,
  	                        (void **)jmi_dynamic_state_set->dgeqp3_work, sizeof(double), &dgeqp3_work_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("dgeqp3_tau[%d]", jmi_dynamic_state_set->n_algebraics);
  size_t dgeqp3_tau_size_t = (size_t) jmi_dynamic_state_set->n_algebraics;
  Serializable_setArrayField(serializable, FIELD_DGEQP3_TAU, PROPERTIES_DOUBLE,
  	                        (void **)jmi_dynamic_state_set->dgeqp3_tau, sizeof(double), &dgeqp3_tau_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("dgeqp3_jpvt[%d]", jmi_dynamic_state_set->n_variables);
  size_t dgeqp3_jpvt_size_t = (size_t) jmi_dynamic_state_set->n_variables;
  Serializable_setArrayField(serializable, FIELD_DGEQP3_JPVT, PROPERTIES_INT,
  	                        (void **)jmi_dynamic_state_set->dgeqp3_jpvt, sizeof(int), &dgeqp3_jpvt_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("dgeqp3_lwork: %d", jmi_dynamic_state_set->dgeqp3_lwork);
  Serializable_setField(serializable, FIELD_DGEQP3_LWORK, PROPERTIES_INT,
                       &jmi_dynamic_state_set->dgeqp3_lwork, passport);
  //TO_BE_SERIALIZED FIELD_COEFFICENTS as type jmi_dynamic_state_coefficents_func_t
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_DYNAMIC_STATE_SET_T *ser_jmi_dynamic_state_set = (SerializableJMI_DYNAMIC_STATE_SET_T *)this_v;
  jmi_dynamic_state_set_t *jmi_dynamic_state_set = ser_jmi_dynamic_state_set->jmi_dynamic_state_set;
  const void *passport = ser_jmi_dynamic_state_set->passport;
  Serializable *serializable = ser_jmi_dynamic_state_set->serializable;
  // Deserializing simple fields via script
  jmi_dynamic_state_set->n_variables =
    *(int *) Serializable_getField(serializable, FIELD_N_VARIABLES, passport);
  MCLabFMILog_verbose("n_variables: %d", jmi_dynamic_state_set->n_variables);
  jmi_dynamic_state_set->n_states =
    *(int *) Serializable_getField(serializable, FIELD_N_STATES, passport);
  MCLabFMILog_verbose("n_states: %d", jmi_dynamic_state_set->n_states);
  jmi_dynamic_state_set->n_algebraics =
    *(int *) Serializable_getField(serializable, FIELD_N_ALGEBRAICS, passport);
  MCLabFMILog_verbose("n_algebraics: %d", jmi_dynamic_state_set->n_algebraics);
  size_t variables_value_references_size_t = (size_t) jmi_dynamic_state_set->n_variables;
  Serializable_getArrayField(serializable, FIELD_VARIABLES_VALUE_REFERENCES, (void **)jmi_dynamic_state_set->variables_value_references,
  	                        sizeof(int), &variables_value_references_size_t, 0, passport);
  MCLabFMILog_verbose("variables_value_references[%d]", jmi_dynamic_state_set->n_variables);
  size_t state_value_references_size_t = (size_t) jmi_dynamic_state_set->n_states;
  Serializable_getArrayField(serializable, FIELD_STATE_VALUE_REFERENCES, (void **)jmi_dynamic_state_set->state_value_references,
  	                        sizeof(int), &state_value_references_size_t, 0, passport);
  MCLabFMILog_verbose("state_value_references[%d]", jmi_dynamic_state_set->n_states);
  size_t ds_state_value_references_size_t = (size_t) jmi_dynamic_state_set->n_states;
  Serializable_getArrayField(serializable, FIELD_DS_STATE_VALUE_REFERENCES, (void **)jmi_dynamic_state_set->ds_state_value_references,
  	                        sizeof(int), &ds_state_value_references_size_t, 0, passport);
  MCLabFMILog_verbose("ds_state_value_references[%d]", jmi_dynamic_state_set->n_states);
  size_t ds_state_value_local_index_size_t = (size_t) jmi_dynamic_state_set->n_states;
  Serializable_getArrayField(serializable, FIELD_DS_STATE_VALUE_LOCAL_INDEX, (void **)jmi_dynamic_state_set->ds_state_value_local_index,
  	                        sizeof(int), &ds_state_value_local_index_size_t, 0, passport);
  MCLabFMILog_verbose("ds_state_value_local_index[%d]", jmi_dynamic_state_set->n_states);
  size_t algebraic_value_references_size = (size_t) ((jmi_dynamic_state_set->n_variables-jmi_dynamic_state_set->n_states));
  Serializable_getArrayField(serializable, FIELD_ALGEBRAIC_VALUE_REFERENCES, (void **)jmi_dynamic_state_set->algebraic_value_references,
  	                        sizeof(int), &algebraic_value_references_size, 0, passport);
  MCLabFMILog_verbose("algebraic_value_references[%zu]", algebraic_value_references_size);
  size_t ds_algebraic_value_references_size = (size_t) ((jmi_dynamic_state_set->n_variables-jmi_dynamic_state_set->n_states));
  Serializable_getArrayField(serializable, FIELD_DS_ALGEBRAIC_VALUE_REFERENCES, (void **)jmi_dynamic_state_set->ds_algebraic_value_references,
  	                        sizeof(int), &ds_algebraic_value_references_size, 0, passport);
  MCLabFMILog_verbose("ds_algebraic_value_references[%zu]", ds_algebraic_value_references_size);
  size_t ds_algebraic_value_local_index_size = (size_t) ((jmi_dynamic_state_set->n_variables-jmi_dynamic_state_set->n_states));
  Serializable_getArrayField(serializable, FIELD_DS_ALGEBRAIC_VALUE_LOCAL_INDEX, (void **)jmi_dynamic_state_set->ds_algebraic_value_local_index,
  	                        sizeof(int), &ds_algebraic_value_local_index_size, 0, passport);
  MCLabFMILog_verbose("ds_algebraic_value_local_index[%zu]", ds_algebraic_value_local_index_size);
  size_t temp_algebraic_size = (size_t) ((jmi_dynamic_state_set->n_variables-jmi_dynamic_state_set->n_states));
  Serializable_getArrayField(serializable, FIELD_TEMP_ALGEBRAIC, (void **)jmi_dynamic_state_set->temp_algebraic,
  	                        sizeof(int), &temp_algebraic_size, 0, passport);
  MCLabFMILog_verbose("temp_algebraic[%zu]", temp_algebraic_size);
  size_t coefficent_matrix_size = (size_t) (((jmi_dynamic_state_set->n_variables-jmi_dynamic_state_set->n_states)*jmi_dynamic_state_set->n_variables));
  Serializable_getArrayField(serializable, FIELD_COEFFICENT_MATRIX, (void **)jmi_dynamic_state_set->coefficent_matrix,
  	                        sizeof(double), &coefficent_matrix_size, 0, passport);
  MCLabFMILog_verbose("coefficent_matrix[%zu]", coefficent_matrix_size);
  size_t sub_coefficent_matrix_size = (size_t) (((jmi_dynamic_state_set->n_variables-jmi_dynamic_state_set->n_states)*(jmi_dynamic_state_set->n_variables-jmi_dynamic_state_set->n_states)));
  Serializable_getArrayField(serializable, FIELD_SUB_COEFFICENT_MATRIX, (void **)jmi_dynamic_state_set->sub_coefficent_matrix,
  	                        sizeof(double), &sub_coefficent_matrix_size, 0, passport);
  MCLabFMILog_verbose("sub_coefficent_matrix[%zu]", sub_coefficent_matrix_size);
  size_t dgeqp3_work_size_t = (size_t) jmi_dynamic_state_set->dgeqp3_lwork;
  Serializable_getArrayField(serializable, FIELD_DGEQP3_WORK, (void **)jmi_dynamic_state_set->dgeqp3_work,
  	                        sizeof(double), &dgeqp3_work_size_t, 0, passport);
  MCLabFMILog_verbose("dgeqp3_work[%d]", jmi_dynamic_state_set->dgeqp3_lwork);
  size_t dgeqp3_tau_size_t = (size_t) jmi_dynamic_state_set->n_algebraics;
  Serializable_getArrayField(serializable, FIELD_DGEQP3_TAU, (void **)jmi_dynamic_state_set->dgeqp3_tau,
  	                        sizeof(double), &dgeqp3_tau_size_t, 0, passport);
  MCLabFMILog_verbose("dgeqp3_tau[%d]", jmi_dynamic_state_set->n_algebraics);
  size_t dgeqp3_jpvt_size_t = (size_t) jmi_dynamic_state_set->n_variables;
  Serializable_getArrayField(serializable, FIELD_DGEQP3_JPVT, (void **)jmi_dynamic_state_set->dgeqp3_jpvt,
  	                        sizeof(int), &dgeqp3_jpvt_size_t, 0, passport);
  MCLabFMILog_verbose("dgeqp3_jpvt[%d]", jmi_dynamic_state_set->n_variables);
  jmi_dynamic_state_set->dgeqp3_lwork =
    *(int *) Serializable_getField(serializable, FIELD_DGEQP3_LWORK, passport);
  MCLabFMILog_verbose("dgeqp3_lwork: %d", jmi_dynamic_state_set->dgeqp3_lwork);
  //TO_BE_SERIALIZED FIELD_COEFFICENTS as type jmi_dynamic_state_coefficents_func_t
  MCLabFMILog_unnest();
}

SerializableJMI_DYNAMIC_STATE_SET_T *SerializableJMI_DYNAMIC_STATE_SET_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_dynamic_state_set_t *jmi_dynamic_state_set) {
  Debug_assert(DEBUG_ALWAYS, jmi_dynamic_state_set != NULL, "jmi_dynamic_state_set == NULL");
  SerializableJMI_DYNAMIC_STATE_SET_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_dynamic_state_set)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_DYNAMIC_STATE_SET_T));
    new->jmi_dynamic_state_set = jmi_dynamic_state_set;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_dynamic_state_set, new,
                    SerializableJMI_DYNAMIC_STATE_SET_T_free, new->passport);
    // Adding inner struct new
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_DYNAMIC_STATE_SET_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_DYNAMIC_STATE_SET_T *this_ = (SerializableJMI_DYNAMIC_STATE_SET_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_DYNAMIC_STATE_SET_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_DYNAMIC_STATE_SET_T *this_ = (SerializableJMI_DYNAMIC_STATE_SET_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
