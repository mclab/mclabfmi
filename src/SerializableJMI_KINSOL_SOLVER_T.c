/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "jmi_block_solver.h"
#include "jmi_block_solver_impl.h"
#include "SerializableJMI_KINSOL_SOLVER_T.h"
#include "SerializableJMI_KINSOL_SOLVER_RESET_T.h"
#include "SerializableDLSMAT.h"

#include "MCLabFMIEnv.h"

#define DEBUG "SerializableJMI_KINSOL_SOLVER_T"

#define CLASS_NAME "jmi_kinsol_solver"

#define FIELD_EXTERNALBRENT "externalBrent"
#define FIELD_SAVED_STATE "saved_state"
#define FIELD_KIN_MEM "kin_mem"
#define FIELD_KIN_Y "kin_y"
#define FIELD_LAST_RESIDUAL "last_residual"
#define FIELD_KIN_Y_SCALE "kin_y_scale"
#define FIELD_GRADIENT "gradient"
#define FIELD_KIN_JAC_UPDATE_TIME "kin_jac_update_time"
#define FIELD_KIN_FTOL "kin_ftol"
#define FIELD_KIN_STOL "kin_stol"
#define FIELD_KIN_REG_TOL "kin_reg_tol"
#define FIELD_JTJ "JTJ"
#define FIELD_J_IS_SINGULAR_FLAG "J_is_singular_flag"
#define FIELD_USE_STEEPEST_DESCENT_FLAG "use_steepest_descent_flag"
#define FIELD_FORCE_NEW_J_FLAG "force_new_J_flag"
#define FIELD_UPDATED_JACOBIAN_FLAG "updated_jacobian_flag"
#define FIELD_HANDLING_OF_SINGULAR_JACOBIAN_FLAG "handling_of_singular_jacobian_flag"
#define FIELD_J_LU "J_LU"
#define FIELD_J_SING "J_sing"
#define FIELD_J_DEPENDENCY "J_Dependency"
#define FIELD_IS_FIRST_NEWTON_SOLVE_FLAG "is_first_newton_solve_flag"
#define FIELD_EQUED "equed"
#define FIELD_RSCALE "rScale"
#define FIELD_CSCALE "cScale"
#define FIELD_WORK_VECTOR "work_vector"
#define FIELD_WORK_VECTOR2 "work_vector2"
#define FIELD_WORK_VECTOR3 "work_vector3"
#define FIELD_LAPACK_WORK "lapack_work"
#define FIELD_LAPACK_IWORK "lapack_iwork"
#define FIELD_LAPACK_IPIV "lapack_ipiv"
#define FIELD_DGESDD_WORK "dgesdd_work"
#define FIELD_DGESDD_LWORK "dgesdd_lwork"
#define FIELD_DGESDD_IWORK "dgesdd_iwork"
#define FIELD_SUNDIALS_PERMUTATION_WORK "sundials_permutationwork"
#define FIELD_DGELSS_RWORK "dgelss_rwork"
#define FIELD_SINGULAR_VALUES "singular_values"
#define FIELD_NUM_BOUNDS "num_bounds"
#define FIELD_RANGE_MOST_LIMITING "range_most_limiting"
#define FIELD_BOUND_VINDEX "bound_vindex"
#define FIELD_BOUND_KIND "bound_kind"
#define FIELD_BOUND_LIMITING "bound_limiting"
#define FIELD_BOUNDS "bounds"
#define FIELD_ACTIVE_BOUNDS "active_bounds"
#define FIELD_MAX_NW_STEP "max_nw_step"
#define FIELD_RANGE_LIMITS "range_limits"
#define FIELD_RANGE_LIMITED "range_limited"
#define FIELD_JAC_COMPRESSION_GROUPS "jac_compression_groups"
#define FIELD_JAC_COMPRESSION_GROUP_INDEX "jac_compression_group_index"
#define FIELD_HAS_COMPRESSION_SETUP_FLAG "has_compression_setup_flag"
#define FIELD_Y_POS_MIN_1D "y_pos_min_1d"
#define FIELD_F_POS_MIN_1D "f_pos_min_1d"
#define FIELD_Y_NEG_MAX_1D "y_neg_max_1d"
#define FIELD_F_NEG_MAX_1D "f_neg_max_1d"
#define FIELD_LAST_XNORM "last_xnorm"
#define FIELD_LAST_FNORM "last_fnorm"
#define FIELD_LAST_MAX_RESIDUAL "last_max_residual"
#define FIELD_LAST_MAX_RESIDUAL_INDEX "last_max_residual_index"
#define FIELD_SJPNORM "sJpnorm"
#define FIELD_LAST_BOUNDING_INDEX "last_bounding_index"
#define FIELD_LAST_NUM_LIMITING_BOUNDS "last_num_limiting_bounds"
#define FIELD_LAST_NUM_ACTIVE_BOUNDS "last_num_active_bounds"
#define FIELD_LAMBDA "lambda"
#define FIELD_LAMBDA_MAX "lambda_max"
#define FIELD_ITERATIONPROGRESSFLAG "iterationProgressFlag"
#define FIELD_CURRENT_NNI "current_nni"
#define FIELD_MAX_STEP_RATIO "max_step_ratio"
#define FIELD_CHAR_LOG_LENGTH "char_log_length"
#define FIELD_CHAR_LOG "char_log"


struct SerializableJMI_KINSOL_SOLVER_T {
  jmi_kinsol_solver_t *solver;
  jmi_block_solver_t *block_solver;
  // Adding inner struct
  SerializableJMI_KINSOL_SOLVER_RESET_T *ser_saved_state;
  SerializableDLSMAT *ser_jtj;
  SerializableDLSMAT *ser_j_lu;
  SerializableDLSMAT *ser_j_sing;
  SerializableDLSMAT *ser_j_dependency;
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_KINSOL_SOLVER_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_KINSOL_SOLVER_T *ser_solver = (SerializableJMI_KINSOL_SOLVER_T *)this_v;
  return ser_solver->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_KINSOL_SOLVER_T *ser_solver = (SerializableJMI_KINSOL_SOLVER_T *)this_v;
  jmi_kinsol_solver_t *solver = ser_solver->solver;
  jmi_block_solver_t *block_solver = ser_solver->block_solver;
  const void *passport = ser_solver->passport;
  Serializable *serializable = ser_solver->serializable;
  // if (solver->saved_state != NULL) {
  Serializable_setField(serializable, FIELD_SAVED_STATE, PROPERTIES_POINTER,SerializableJMI_KINSOL_SOLVER_RESET_T_asSerializable(ser_solver->ser_saved_state), passport);
  // }
  MCLabFMILog_verbose("kin_y[%d]", block_solver->n);
  size_t kin_y_size = (size_t) block_solver->n;
  Serializable_setArrayField(serializable, FIELD_KIN_Y, PROPERTIES_DOUBLE, N_VGetArrayPointer(solver->kin_y), sizeof(double), &kin_y_size, NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("last_residual[%d]", block_solver->n);
  size_t last_residual_size = (size_t) block_solver->n;
  Serializable_setArrayField(serializable, FIELD_LAST_RESIDUAL, PROPERTIES_DOUBLE, N_VGetArrayPointer(solver->last_residual), sizeof(double), &last_residual_size, NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("kin_y_scale[%d]", block_solver->n);
  size_t kin_y_scale_size = (size_t) block_solver->n;
  Serializable_setArrayField(serializable, FIELD_KIN_Y_SCALE, PROPERTIES_DOUBLE, N_VGetArrayPointer(solver->kin_y_scale), sizeof(double), &kin_y_scale_size, NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("gradient[%d]", block_solver->n);
  size_t gradient_size = (size_t) block_solver->n;
  Serializable_setArrayField(serializable, FIELD_GRADIENT, PROPERTIES_DOUBLE, N_VGetArrayPointer(solver->gradient), sizeof(double), &gradient_size, NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("kin_jac_update_time = %g", solver->kin_jac_update_time);
  Serializable_setField(serializable, FIELD_KIN_JAC_UPDATE_TIME, PROPERTIES_DOUBLE, &(solver->kin_jac_update_time), passport);
  MCLabFMILog_verbose("kin_ftol = %g", solver->kin_ftol);
  Serializable_setField(serializable, FIELD_KIN_FTOL, PROPERTIES_DOUBLE, &(solver->kin_ftol), passport);
  MCLabFMILog_verbose("kin_stol = %g", solver->kin_stol);
  Serializable_setField(serializable, FIELD_KIN_STOL, PROPERTIES_DOUBLE, &(solver->kin_stol), passport);
  MCLabFMILog_verbose("kin_reg_tol = %g", solver->kin_reg_tol);
  Serializable_setField(serializable, FIELD_KIN_REG_TOL, PROPERTIES_DOUBLE, &(solver->kin_reg_tol), passport);

  Serializable_setField(serializable, FIELD_JTJ, PROPERTIES_POINTER, SerializableDLSMAT_asSerializable(ser_solver->ser_jtj), passport);

  MCLabFMILog_verbose("J_is_singular_flag = %d", solver->J_is_singular_flag);
  Serializable_setField(serializable, FIELD_J_IS_SINGULAR_FLAG, PROPERTIES_INT, &(solver->J_is_singular_flag), passport);
  MCLabFMILog_verbose("use_steepest_descent_flag= %d", solver->use_steepest_descent_flag);
  Serializable_setField(serializable, FIELD_USE_STEEPEST_DESCENT_FLAG, PROPERTIES_INT, &(solver->use_steepest_descent_flag), passport);
  MCLabFMILog_verbose("force_new_J_flag= %d", solver->force_new_J_flag);
  Serializable_setField(serializable, FIELD_FORCE_NEW_J_FLAG, PROPERTIES_INT, &(solver->force_new_J_flag), passport);
  MCLabFMILog_verbose("updated_jacobian_flag= %d", solver->updated_jacobian_flag);
  Serializable_setField(serializable, FIELD_UPDATED_JACOBIAN_FLAG, PROPERTIES_INT, &(solver->updated_jacobian_flag), passport);
  MCLabFMILog_verbose("handling_of_singular_jacobian_flag= %d", solver->handling_of_singular_jacobian_flag);
  Serializable_setField(serializable, FIELD_HANDLING_OF_SINGULAR_JACOBIAN_FLAG, PROPERTIES_INT, &(solver->handling_of_singular_jacobian_flag), passport);

  Serializable_setField(serializable, FIELD_J_LU, PROPERTIES_POINTER, SerializableDLSMAT_asSerializable(ser_solver->ser_j_lu), passport);
  Serializable_setField(serializable, FIELD_J_SING, PROPERTIES_POINTER, SerializableDLSMAT_asSerializable(ser_solver->ser_j_sing), passport);
  Serializable_setField(serializable, FIELD_J_DEPENDENCY, PROPERTIES_POINTER, SerializableDLSMAT_asSerializable(ser_solver->ser_j_dependency), passport);

  MCLabFMILog_verbose("is_first_newton_solve_flag = %d", solver->is_first_newton_solve_flag);
  Serializable_setField(serializable, FIELD_IS_FIRST_NEWTON_SOLVE_FLAG, PROPERTIES_INT, &(solver->is_first_newton_solve_flag), passport);

  int equed = (int) solver->equed;
  MCLabFMILog_verbose("equed = %c", solver->equed);
  Serializable_setField(serializable, FIELD_EQUED, PROPERTIES_INT, &equed, passport);

  size_t rscale_size = (size_t) block_solver->n + 1;
  MCLabFMILog_verbose("rScale[%zu]", rscale_size);
  Serializable_setArrayField(serializable, FIELD_RSCALE, PROPERTIES_DOUBLE, solver->rScale, sizeof(double), &rscale_size, NULL, 0, false, NULL, passport);
  size_t cscale_size = (size_t) block_solver->n + 1;
  MCLabFMILog_verbose("cScale[%zu]", cscale_size);
  Serializable_setArrayField(serializable, FIELD_RSCALE, PROPERTIES_DOUBLE, solver->cScale, sizeof(double), &cscale_size, NULL, 0, false, NULL, passport);

  MCLabFMILog_verbose("work_vector[%d]", block_solver->n);
  size_t work_vector_size = (size_t) block_solver->n;
  Serializable_setArrayField(serializable, FIELD_WORK_VECTOR, PROPERTIES_DOUBLE, N_VGetArrayPointer(solver->work_vector), sizeof(double), &work_vector_size, NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("work_vector2[%d]", block_solver->n);
  Serializable_setArrayField(serializable, FIELD_WORK_VECTOR2, PROPERTIES_DOUBLE, N_VGetArrayPointer(solver->work_vector2), sizeof(double), &work_vector_size, NULL, 0, false, NULL, passport);
  if (N_VGetArrayPointer(solver->work_vector3) != NULL) {
    MCLabFMILog_verbose("work_vector3[%d]", block_solver->n);
    Serializable_setArrayField(serializable, FIELD_WORK_VECTOR3, PROPERTIES_DOUBLE, N_VGetArrayPointer(solver->work_vector3), sizeof(double), &work_vector_size, NULL, 0, false, NULL, passport);
  }
  size_t lapack_work_size = 4 * ( (size_t) block_solver->n + 1);
  MCLabFMILog_verbose("lapack_work_size[%zu]", lapack_work_size);
  Serializable_setArrayField(serializable, FIELD_LAPACK_WORK, PROPERTIES_DOUBLE, solver->lapack_work, sizeof(double), &lapack_work_size, NULL, 0, false, NULL, passport);
  size_t lapack_iwork_size = (size_t) (block_solver->n + 2);
  MCLabFMILog_verbose("lapack_iwork_size[%zu]", lapack_iwork_size);
  Serializable_setArrayField(serializable, FIELD_LAPACK_IWORK, PROPERTIES_INT, solver->lapack_iwork, sizeof(int), &lapack_iwork_size, NULL, 0, false, NULL, passport);
  size_t lapack_ipiv_size = (size_t) (block_solver->n + 2);
  MCLabFMILog_verbose("lapack_ipiv_size[%zu]", lapack_ipiv_size);
  Serializable_setArrayField(serializable, FIELD_LAPACK_IPIV, PROPERTIES_INT, solver->lapack_ipiv, sizeof(int), &lapack_ipiv_size, NULL, 0, false, NULL, passport);

  size_t dgesdd_work_size = (size_t) solver->dgesdd_lwork;
  MCLabFMILog_verbose("dgesdd_work[%zu]", dgesdd_work_size);
  Serializable_setArrayField(serializable, FIELD_DGESDD_WORK, PROPERTIES_DOUBLE, solver->dgesdd_work, sizeof(double), &dgesdd_work_size, NULL, 0, false, NULL, passport);
  Serializable_setField(serializable, FIELD_DGESDD_LWORK, PROPERTIES_INT, &(solver->dgesdd_lwork), passport);
  size_t dgesdd_iwork_size = (size_t) (8 * block_solver->n);
  MCLabFMILog_verbose("dgesdd_iwork[%zu]", dgesdd_iwork_size);
  Serializable_setArrayField(serializable, FIELD_DGESDD_IWORK, PROPERTIES_INT, solver->dgesdd_iwork, sizeof(int), &dgesdd_iwork_size, NULL, 0, false, NULL, passport);

  size_t sundials_permutationwork_size = (size_t) (block_solver->n + 1);
  MCLabFMILog_verbose("sundials_permutationwork_size[%zu]", sundials_permutationwork_size);
  Serializable_setArrayField(serializable, FIELD_SUNDIALS_PERMUTATION_WORK, PROPERTIES_LONG, solver->sundials_permutationwork, sizeof(long), &sundials_permutationwork_size, NULL, 0, false, NULL, passport);

  size_t dgelss_rwork_size = (size_t) (5 *block_solver->n);
  MCLabFMILog_verbose("dgelss_rwork[%zu]", dgelss_rwork_size);
  Serializable_setArrayField(serializable, FIELD_DGELSS_RWORK, PROPERTIES_DOUBLE, solver->dgelss_rwork, sizeof(double), &dgelss_rwork_size, NULL, 0, false, NULL, passport);

  size_t singular_values_size = (size_t) (block_solver->n);
  MCLabFMILog_verbose("singular_values[%zu]", singular_values_size);
  Serializable_setArrayField(serializable,  FIELD_SINGULAR_VALUES, PROPERTIES_DOUBLE, solver->singular_values, sizeof(double), &singular_values_size, NULL, 0, false, NULL, passport);

  MCLabFMILog_verbose("num_bounds = %d", solver->num_bounds);
  Serializable_setField(serializable, FIELD_NUM_BOUNDS, PROPERTIES_INT, &(solver->num_bounds), passport);
  MCLabFMILog_verbose("range_most_limiting = %d", solver->range_most_limiting);
  Serializable_setField(serializable, FIELD_RANGE_MOST_LIMITING, PROPERTIES_INT, &(solver->range_most_limiting), passport);

  size_t bound_vindex_size = (size_t) (solver->num_bounds);
  MCLabFMILog_verbose("bound_vindex[%zu]", bound_vindex_size);
  Serializable_setArrayField(serializable,  FIELD_BOUND_VINDEX, PROPERTIES_INT, solver->bound_vindex, sizeof(int), &bound_vindex_size, NULL, 0, false, NULL, passport);
  size_t bound_kind_size = (size_t) (solver->num_bounds);
  MCLabFMILog_verbose("bound_kind[%zu]", bound_kind_size);
  Serializable_setArrayField(serializable,  FIELD_BOUND_KIND, PROPERTIES_INT, solver->bound_kind, sizeof(int), &bound_kind_size, NULL, 0, false, NULL, passport);
  size_t bound_limiting_size = (size_t) (solver->num_bounds);
  MCLabFMILog_verbose("bound_limiting[%zu]", bound_limiting_size);
  Serializable_setArrayField(serializable,  FIELD_BOUND_LIMITING, PROPERTIES_INT, solver->bound_limiting, sizeof(int), &bound_limiting_size, NULL, 0, false, NULL, passport);
  size_t bounds_size = (size_t) (solver->num_bounds);
  MCLabFMILog_verbose("bounds[%zu]", bounds_size);
  Serializable_setArrayField(serializable,  FIELD_BOUNDS, PROPERTIES_DOUBLE, solver->bounds, sizeof(double), &bounds_size, NULL, 0, false, NULL, passport);
  size_t active_bounds_size = (size_t) (block_solver->n);
  MCLabFMILog_verbose("active_bounds[%zu]", active_bounds_size);
  if (solver->active_bounds != NULL) {
    Serializable_setArrayField(serializable,  FIELD_ACTIVE_BOUNDS, PROPERTIES_DOUBLE, solver->active_bounds, sizeof(double), &active_bounds_size, NULL, 0, false, NULL, passport);
  }

  MCLabFMILog_verbose("max_nw_step = %g", solver->max_nw_step);
  Serializable_setField(serializable, FIELD_MAX_NW_STEP, PROPERTIES_DOUBLE, &(solver->max_nw_step), passport);

  size_t range_limits_size = (size_t) (block_solver->n + 1);
  MCLabFMILog_verbose("range_limits[%zu]", range_limits_size);
  Serializable_setArrayField(serializable,  FIELD_RANGE_LIMITS, PROPERTIES_DOUBLE, solver->range_limits, sizeof(double), &range_limits_size, NULL, 0, false, NULL, passport);

  size_t range_limited_size = (size_t) (block_solver->n + 1);
  MCLabFMILog_verbose("range_limited[%zu]", range_limited_size);
  Serializable_setArrayField(serializable,  FIELD_RANGE_LIMITED, PROPERTIES_INT, solver->range_limited, sizeof(int), &range_limited_size, NULL, 0, false, NULL, passport);

  size_t jac_compression_groups_size = (size_t) (block_solver->n + 1);
  MCLabFMILog_verbose("jac_compression_groups[%zu]", jac_compression_groups_size);
  Serializable_setArrayField(serializable,  FIELD_JAC_COMPRESSION_GROUPS, PROPERTIES_INT, solver->jac_compression_groups, sizeof(int), &jac_compression_groups_size, NULL, 0, false, NULL, passport);
  size_t jac_compression_group_index_size = (size_t) (block_solver->n + 1);
  MCLabFMILog_verbose("jac_compression_group_index[%zu]", jac_compression_group_index_size);
  Serializable_setArrayField(serializable,  FIELD_JAC_COMPRESSION_GROUPS, PROPERTIES_INT, solver->jac_compression_group_index, sizeof(int), &jac_compression_group_index_size, NULL, 0, false, NULL, passport);

  MCLabFMILog_verbose("has_compression_setup_flag = %d", solver->has_compression_setup_flag);
  Serializable_setField(serializable, FIELD_HAS_COMPRESSION_SETUP_FLAG, PROPERTIES_INT, &(solver->has_compression_setup_flag), passport);

  MCLabFMILog_verbose("y_pos_min_1d = %g", solver->y_pos_min_1d);
  Serializable_setField(serializable, FIELD_Y_POS_MIN_1D, PROPERTIES_DOUBLE, &(solver->y_pos_min_1d), passport);
  MCLabFMILog_verbose("f_pos_min_1d = %g", solver->f_pos_min_1d);
  Serializable_setField(serializable, FIELD_F_POS_MIN_1D, PROPERTIES_DOUBLE, &(solver->f_pos_min_1d), passport);
  MCLabFMILog_verbose("y_neg_max_1d = %g", solver->y_neg_max_1d);
  Serializable_setField(serializable, FIELD_Y_NEG_MAX_1D, PROPERTIES_DOUBLE, &(solver->y_neg_max_1d), passport);
  MCLabFMILog_verbose("f_neg_max_1d = %g", solver->f_neg_max_1d);
  Serializable_setField(serializable, FIELD_F_NEG_MAX_1D, PROPERTIES_DOUBLE, &(solver->f_neg_max_1d), passport);
  MCLabFMILog_verbose("last_xnorm = %g", solver->last_xnorm);
  Serializable_setField(serializable, FIELD_LAST_XNORM, PROPERTIES_DOUBLE, &(solver->last_xnorm), passport);
  MCLabFMILog_verbose("last_fnorm = %g", solver->last_fnorm);
  Serializable_setField(serializable, FIELD_LAST_FNORM, PROPERTIES_DOUBLE, &(solver->last_fnorm), passport);
  MCLabFMILog_verbose("last_max_residual = %g", solver->last_max_residual);
  Serializable_setField(serializable, FIELD_LAST_MAX_RESIDUAL, PROPERTIES_DOUBLE, &(solver->last_max_residual), passport);
  MCLabFMILog_verbose("last_max_residual_index = %d", solver->last_max_residual_index);
  Serializable_setField(serializable, FIELD_LAST_MAX_RESIDUAL_INDEX, PROPERTIES_INT, &(solver->last_max_residual_index), passport);
  MCLabFMILog_verbose("sJpnorm = %g", solver->sJpnorm);
  Serializable_setField(serializable, FIELD_SJPNORM, PROPERTIES_DOUBLE, &(solver->sJpnorm), passport);
  MCLabFMILog_verbose("last_bounding_index = %d", solver->last_bounding_index);
  Serializable_setField(serializable, FIELD_LAST_BOUNDING_INDEX, PROPERTIES_INT, &(solver->last_bounding_index), passport);
  MCLabFMILog_verbose("last_num_limiting_bounds = %d", solver->last_num_limiting_bounds);
  Serializable_setField(serializable, FIELD_LAST_NUM_LIMITING_BOUNDS, PROPERTIES_INT, &(solver->last_num_limiting_bounds), passport);
  MCLabFMILog_verbose("last_num_active_bounds = %d", solver->last_num_active_bounds);
  Serializable_setField(serializable, FIELD_LAST_NUM_ACTIVE_BOUNDS, PROPERTIES_INT, &(solver->last_num_active_bounds), passport);
  MCLabFMILog_verbose("lambda = %g", solver->lambda);
  Serializable_setField(serializable, FIELD_LAMBDA, PROPERTIES_DOUBLE, &(solver->lambda), passport);
  MCLabFMILog_verbose("lambda_max = %g", solver->lambda_max);
  Serializable_setField(serializable, FIELD_LAMBDA_MAX, PROPERTIES_DOUBLE, &(solver->lambda_max), passport);
  MCLabFMILog_verbose("iterationProgressFlag = %d", solver->iterationProgressFlag);
  Serializable_setField(serializable, FIELD_ITERATIONPROGRESSFLAG, PROPERTIES_INT, &(solver->iterationProgressFlag), passport);
  MCLabFMILog_verbose("current_nni = %ld", solver->current_nni);
  Serializable_setField(serializable, FIELD_CURRENT_NNI, PROPERTIES_LONG, &(solver->current_nni), passport);
  MCLabFMILog_verbose("char_log_length = %d", solver->char_log_length);
  Serializable_setField(serializable, FIELD_CHAR_LOG_LENGTH, PROPERTIES_INT, &(solver->char_log_length), passport);

  // size_t char_log_size = (size_t) (JMI_KINSOL_SOLVER_MAX_CHAR_LOG_LENGTH + 1);
  // MCLabFMILog_verbose("char_log[%zu]", char_log_size);
  // Serializable_setArrayField(serializable, FIELD_CHAR_LOG, PROPERTIES_STRING, solver->char_log, sizeof(char), &char_log_size, NULL, 0, false, NULL, passport);

  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_KINSOL_SOLVER_T *ser_solver = (SerializableJMI_KINSOL_SOLVER_T *)this_v;
  jmi_kinsol_solver_t *solver = ser_solver->solver;
  jmi_block_solver_t *block_solver = ser_solver->block_solver;
  const void *passport = ser_solver->passport;
  Serializable *serializable = ser_solver->serializable;
  ser_solver->ser_saved_state= *(SerializableJMI_KINSOL_SOLVER_RESET_T **) Serializable_getField(serializable, FIELD_SAVED_STATE, passport);
  size_t kin_y_size = (size_t) block_solver->n;
  Serializable_getArrayField(serializable, FIELD_KIN_Y, N_VGetArrayPointer(solver->kin_y), sizeof(double), &kin_y_size, 0, passport);
  MCLabFMILog_verbose("kin_y[%d]", block_solver->n);
  size_t last_residual_size = (size_t) block_solver->n;
  Serializable_getArrayField(serializable, FIELD_LAST_RESIDUAL, N_VGetArrayPointer(solver->last_residual), sizeof(double), &last_residual_size, 0, passport);
  MCLabFMILog_verbose("last_residual[%d]", block_solver->n);
  size_t kin_y_scale_size = (size_t) block_solver->n;
  Serializable_getArrayField(serializable, FIELD_KIN_Y_SCALE, N_VGetArrayPointer(solver->kin_y_scale), sizeof(double), &kin_y_scale_size, 0, passport);
  MCLabFMILog_verbose("kin_y_scale[%d]", block_solver->n);
  size_t gradient_size = (size_t) block_solver->n;
  Serializable_getArrayField(serializable, FIELD_GRADIENT, N_VGetArrayPointer(solver->gradient), sizeof(double), &gradient_size, 0, passport);
  MCLabFMILog_verbose("gradient[%d]", block_solver->n);
  solver->kin_jac_update_time = *(double *) Serializable_getField(serializable, FIELD_KIN_JAC_UPDATE_TIME, passport);
  MCLabFMILog_verbose("kin_jac_update_time = %g", solver->kin_jac_update_time);
  solver->kin_ftol = *(double *) Serializable_getField(serializable, FIELD_KIN_FTOL, passport);
  MCLabFMILog_verbose("kin_ftol = %g", solver->kin_ftol);
  solver->kin_stol = *(double *) Serializable_getField(serializable, FIELD_KIN_STOL, passport);
  MCLabFMILog_verbose("kin_stol = %g", solver->kin_stol);
  solver->kin_reg_tol = *(double *) Serializable_getField(serializable, FIELD_KIN_REG_TOL, passport);
  MCLabFMILog_verbose("kin_reg_tol = %g", solver->kin_reg_tol);

  ser_solver->ser_jtj = *(SerializableDLSMAT **) Serializable_getField(serializable, FIELD_JTJ, passport);

  solver->J_is_singular_flag = *(int *) Serializable_getField(serializable, FIELD_J_IS_SINGULAR_FLAG, passport);
  MCLabFMILog_verbose("J_is_singular_flag = %d", solver->J_is_singular_flag);
  solver->use_steepest_descent_flag = *(int *) Serializable_getField(serializable, FIELD_USE_STEEPEST_DESCENT_FLAG, passport);
  MCLabFMILog_verbose("use_steepest_descent_flag= %d", solver->use_steepest_descent_flag);
  solver->force_new_J_flag  = *(int *) Serializable_getField(serializable, FIELD_FORCE_NEW_J_FLAG, passport);
  MCLabFMILog_verbose("force_new_J_flag= %d", solver->force_new_J_flag);
  solver->updated_jacobian_flag = *(int *) Serializable_getField(serializable, FIELD_UPDATED_JACOBIAN_FLAG, passport);
  MCLabFMILog_verbose("updated_jacobian_flag= %d", solver->updated_jacobian_flag);
  solver->handling_of_singular_jacobian_flag = *(int *) Serializable_getField(serializable, FIELD_HANDLING_OF_SINGULAR_JACOBIAN_FLAG, passport);
  MCLabFMILog_verbose("handling_of_singular_jacobian_flag= %d", solver->handling_of_singular_jacobian_flag);

  ser_solver->ser_j_lu = *(SerializableDLSMAT **) Serializable_getField(serializable, FIELD_J_LU, passport);
  ser_solver->ser_j_sing = *(SerializableDLSMAT **) Serializable_getField(serializable, FIELD_J_SING, passport);
  ser_solver->ser_j_dependency = *(SerializableDLSMAT **) Serializable_getField(serializable, FIELD_J_DEPENDENCY, passport);

  solver->is_first_newton_solve_flag = *(int *) Serializable_getField(serializable, FIELD_IS_FIRST_NEWTON_SOLVE_FLAG, passport);
  MCLabFMILog_verbose("is_first_newton_solve_flag = %d", solver->is_first_newton_solve_flag);

  int equed = *(int *) Serializable_getField(serializable, FIELD_EQUED, passport);
  solver->equed = (char) equed;
  MCLabFMILog_verbose("equed = %c", solver->equed);

  size_t rscale_size = (size_t) block_solver->n + 1;
  MCLabFMILog_verbose("rScale[%zu]", rscale_size);
  Serializable_getArrayField(serializable, FIELD_RSCALE, solver->rScale, sizeof(double), &rscale_size, 0, passport);
  size_t cscale_size = (size_t) block_solver->n + 1;
  MCLabFMILog_verbose("cScale[%zu]", cscale_size);
  Serializable_getArrayField(serializable, FIELD_RSCALE, solver->cScale, sizeof(double), &cscale_size, 0, passport);

  MCLabFMILog_verbose("work_vector[%d]", block_solver->n);
  size_t work_vector_size = (size_t) block_solver->n;
  Serializable_getArrayField(serializable, FIELD_WORK_VECTOR, N_VGetArrayPointer(solver->work_vector), sizeof(double), &work_vector_size, 0, passport);
  MCLabFMILog_verbose("work_vector2[%d]", block_solver->n);
  Serializable_getArrayField(serializable, FIELD_WORK_VECTOR2, N_VGetArrayPointer(solver->work_vector2), sizeof(double), &work_vector_size, 0, passport);
  if (N_VGetArrayPointer(solver->work_vector3) != NULL) {
    MCLabFMILog_verbose("work_vector3[%d]", block_solver->n);
    Serializable_getArrayField(serializable, FIELD_WORK_VECTOR3, N_VGetArrayPointer(solver->work_vector3), sizeof(double), &work_vector_size, 0, passport);
  }
  size_t lapack_work_size = 4 * ( (size_t) block_solver->n + 1);
  MCLabFMILog_verbose("lapack_work_size[%zu]", lapack_work_size);
  Serializable_getArrayField(serializable, FIELD_LAPACK_WORK, solver->lapack_work, sizeof(double), &lapack_work_size, 0, passport);
  size_t lapack_iwork_size = (size_t) (block_solver->n + 2);
  MCLabFMILog_verbose("lapack_iwork_size[%zu]", lapack_iwork_size);
  Serializable_getArrayField(serializable, FIELD_LAPACK_IWORK, solver->lapack_iwork, sizeof(int), &lapack_iwork_size, 0, passport);
  size_t lapack_ipiv_size = (size_t) (block_solver->n + 2);
  MCLabFMILog_verbose("lapack_ipiv_size[%zu]", lapack_ipiv_size);
  Serializable_getArrayField(serializable, FIELD_LAPACK_IPIV, solver->lapack_ipiv, sizeof(int), &lapack_ipiv_size, 0, passport);

  size_t dgesdd_work_size = (size_t) solver->dgesdd_lwork;
  MCLabFMILog_verbose("dgesdd_work[%zu]", dgesdd_work_size);
  Serializable_getArrayField(serializable, FIELD_DGESDD_WORK, solver->dgesdd_work, sizeof(double), &dgesdd_work_size, 0, passport);
  solver->dgesdd_lwork = *(int *) Serializable_getField(serializable, FIELD_DGESDD_LWORK, passport);
  size_t dgesdd_iwork_size = (size_t) (8 * block_solver->n);
  MCLabFMILog_verbose("dgesdd_iwork[%zu]", dgesdd_iwork_size);
  Serializable_getArrayField(serializable, FIELD_DGESDD_IWORK, solver->dgesdd_iwork, sizeof(int), &dgesdd_iwork_size, 0, passport);

  size_t sundials_permutationwork_size = (size_t) (block_solver->n + 1);
  MCLabFMILog_verbose("sundials_permutationwork_size[%zu]", sundials_permutationwork_size);
  Serializable_getArrayField(serializable, FIELD_SUNDIALS_PERMUTATION_WORK, solver->sundials_permutationwork, sizeof(long), &sundials_permutationwork_size, 0, passport);

  size_t dgelss_rwork_size = (size_t) (5 *block_solver->n);
  MCLabFMILog_verbose("dgelss_rwork[%zu]", dgelss_rwork_size);
  Serializable_getArrayField(serializable, FIELD_DGELSS_RWORK, solver->dgelss_rwork, sizeof(double), &dgelss_rwork_size, 0, passport);

  size_t singular_values_size = (size_t) (block_solver->n);
  MCLabFMILog_verbose("singular_values[%zu]", singular_values_size);
  Serializable_getArrayField(serializable,  FIELD_SINGULAR_VALUES, solver->singular_values, sizeof(double), &singular_values_size, 0, passport);

  solver->num_bounds = *(int *) Serializable_getField(serializable, FIELD_NUM_BOUNDS, passport);
  MCLabFMILog_verbose("num_bounds = %d", solver->num_bounds);
  solver->range_most_limiting = *(int *) Serializable_getField(serializable, FIELD_RANGE_MOST_LIMITING, passport);
  MCLabFMILog_verbose("range_most_limiting = %d", solver->range_most_limiting);

  size_t bound_vindex_size = (size_t) (solver->num_bounds);
  MCLabFMILog_verbose("bound_vindex[%zu]", bound_vindex_size);
  Serializable_getArrayField(serializable, FIELD_BOUND_VINDEX, solver->bound_vindex, sizeof(int), &bound_vindex_size, 0, passport);
  size_t bound_kind_size = (size_t) (solver->num_bounds);
  MCLabFMILog_verbose("bound_kind[%zu]", bound_kind_size);
  Serializable_getArrayField(serializable,  FIELD_BOUND_KIND, solver->bound_kind, sizeof(int), &bound_kind_size, 0, passport);
  size_t bound_limiting_size = (size_t) (solver->num_bounds);
  MCLabFMILog_verbose("bound_limiting[%zu]", bound_limiting_size);
  Serializable_getArrayField(serializable,  FIELD_BOUND_LIMITING, solver->bound_limiting, sizeof(int), &bound_limiting_size, 0, passport);
  size_t bounds_size = (size_t) (solver->num_bounds);
  MCLabFMILog_verbose("bounds[%zu]", bounds_size);
  Serializable_getArrayField(serializable,  FIELD_BOUNDS, solver->bounds, sizeof(double), &bounds_size, 0, passport);
  size_t active_bounds_size = (size_t) (block_solver->n);
  MCLabFMILog_verbose("active_bounds[%zu]", active_bounds_size);
  if (solver->active_bounds != NULL) {
    Serializable_getArrayField(serializable,  FIELD_ACTIVE_BOUNDS, solver->active_bounds, sizeof(double), &active_bounds_size, 0, passport);
  }

  solver->max_nw_step = *(double *) Serializable_getField(serializable, FIELD_MAX_NW_STEP, passport);
  MCLabFMILog_verbose("max_nw_step = %g", solver->max_nw_step);

  size_t range_limits_size = (size_t) (block_solver->n + 1);
  MCLabFMILog_verbose("range_limits[%zu]", range_limits_size);
  Serializable_getArrayField(serializable,  FIELD_RANGE_LIMITS, solver->range_limits, sizeof(double), &range_limits_size, 0, passport);

  size_t range_limited_size = (size_t) (block_solver->n + 1);
  MCLabFMILog_verbose("range_limited[%zu]", range_limited_size);
  Serializable_getArrayField(serializable,  FIELD_RANGE_LIMITED, solver->range_limited, sizeof(int), &range_limited_size, 0, passport);

  size_t jac_compression_groups_size = (size_t) (block_solver->n + 1);
  MCLabFMILog_verbose("jac_compression_groups[%zu]", jac_compression_groups_size);
  Serializable_getArrayField(serializable,  FIELD_JAC_COMPRESSION_GROUPS, solver->jac_compression_groups, sizeof(int), &jac_compression_groups_size, 0, passport);
  size_t jac_compression_group_index_size = (size_t) (block_solver->n + 1);
  MCLabFMILog_verbose("jac_compression_group_index[%zu]", jac_compression_group_index_size);
  Serializable_getArrayField(serializable,  FIELD_JAC_COMPRESSION_GROUPS, solver->jac_compression_group_index, sizeof(int), &jac_compression_group_index_size, 0, passport);

  solver->has_compression_setup_flag = *(int *) Serializable_getField(serializable, FIELD_HAS_COMPRESSION_SETUP_FLAG, passport);
  MCLabFMILog_verbose("has_compression_setup_flag = %d", solver->has_compression_setup_flag);

  solver->y_pos_min_1d = *(double *) Serializable_getField(serializable, FIELD_Y_POS_MIN_1D, passport);
  MCLabFMILog_verbose("y_pos_min_1d = %g", solver->y_pos_min_1d);
  solver->f_pos_min_1d = *(double *) Serializable_getField(serializable, FIELD_F_POS_MIN_1D, passport);
  MCLabFMILog_verbose("f_pos_min_1d = %g", solver->f_pos_min_1d);
  solver->y_neg_max_1d = *(double *) Serializable_getField(serializable, FIELD_Y_NEG_MAX_1D, passport);
  MCLabFMILog_verbose("y_neg_max_1d = %g", solver->y_neg_max_1d);
  solver->f_neg_max_1d = *(double *) Serializable_getField(serializable, FIELD_F_NEG_MAX_1D, passport);
  MCLabFMILog_verbose("f_neg_max_1d = %g", solver->f_neg_max_1d);
  solver->last_xnorm = *(double *) Serializable_getField(serializable, FIELD_LAST_XNORM, passport);
  MCLabFMILog_verbose("last_xnorm = %g", solver->last_xnorm);
  solver->last_fnorm = *(double *) Serializable_getField(serializable, FIELD_LAST_FNORM, passport);
  MCLabFMILog_verbose("last_fnorm = %g", solver->last_fnorm);
  solver->last_max_residual = *(double *) Serializable_getField(serializable, FIELD_LAST_MAX_RESIDUAL, passport);
  MCLabFMILog_verbose("last_max_residual = %g", solver->last_max_residual);
  solver->last_max_residual_index = *(int *) Serializable_getField(serializable, FIELD_LAST_MAX_RESIDUAL_INDEX, passport);
  MCLabFMILog_verbose("last_max_residual_index = %d", solver->last_max_residual_index);
  solver->sJpnorm = *(double *) Serializable_getField(serializable, FIELD_SJPNORM, passport);
  MCLabFMILog_verbose("sJpnorm = %g", solver->sJpnorm);
  solver->last_bounding_index = *(int *) Serializable_getField(serializable, FIELD_LAST_BOUNDING_INDEX, passport);
  MCLabFMILog_verbose("last_bounding_index = %d", solver->last_bounding_index);
 solver->last_num_limiting_bounds = *(int *) Serializable_getField(serializable, FIELD_LAST_NUM_LIMITING_BOUNDS, passport);
  MCLabFMILog_verbose("last_num_limiting_bounds = %d", solver->last_num_limiting_bounds);
  solver->last_num_active_bounds = *(int *) Serializable_getField(serializable, FIELD_LAST_NUM_ACTIVE_BOUNDS, passport);
  MCLabFMILog_verbose("last_num_active_bounds = %d", solver->last_num_active_bounds);
  solver->lambda = *(double *) Serializable_getField(serializable, FIELD_LAMBDA,  passport);
  MCLabFMILog_verbose("lambda = %g", solver->lambda);
  solver->lambda_max = *(double *) Serializable_getField(serializable, FIELD_LAMBDA_MAX, passport);
  MCLabFMILog_verbose("lambda_max = %g", solver->lambda_max);
  solver->iterationProgressFlag = *(int *) Serializable_getField(serializable, FIELD_ITERATIONPROGRESSFLAG, passport);
  MCLabFMILog_verbose("iterationProgressFlag = %d", solver->iterationProgressFlag);
  solver->current_nni = *(long *) Serializable_getField(serializable, FIELD_CURRENT_NNI, passport);
  MCLabFMILog_verbose("current_nni = %ld", solver->current_nni);
  solver->char_log_length = *(int *) Serializable_getField(serializable, FIELD_CHAR_LOG_LENGTH, passport);
  MCLabFMILog_verbose("char_log_length = %d", solver->char_log_length);
  MCLabFMILog_unnest();
}

SerializableJMI_KINSOL_SOLVER_T *SerializableJMI_KINSOL_SOLVER_T_new(MCLabFMIEnv *fmi_env, jmi_kinsol_solver_t *solver, jmi_block_solver_t *block_solver) {
  Debug_assert(DEBUG_ALWAYS, solver != NULL, "solver == NULL");
  SerializableJMI_KINSOL_SOLVER_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, solver)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_KINSOL_SOLVER_T));
    new->solver = solver;
    new->block_solver = block_solver;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, solver, new,
                    SerializableJMI_KINSOL_SOLVER_T_free, new->passport);
    // Adding inner struct new
    // if (solver->saved_state != NULL) {
      new->ser_saved_state = SerializableJMI_KINSOL_SOLVER_RESET_T_new(fmi_env, solver->saved_state, block_solver);
    // }
     new->ser_jtj = SerializableDLSMAT_new(fmi_env, solver->JTJ);
     new->ser_j_lu = SerializableDLSMAT_new(fmi_env, solver->J_LU);
     new->ser_j_sing = SerializableDLSMAT_new(fmi_env, solver->J_sing);
     new->ser_j_dependency = SerializableDLSMAT_new(fmi_env, solver->J_Dependency);
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_KINSOL_SOLVER_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_KINSOL_SOLVER_T *this_ = (SerializableJMI_KINSOL_SOLVER_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_KINSOL_SOLVER_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_KINSOL_SOLVER_T *this_ = (SerializableJMI_KINSOL_SOLVER_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {
      .className = className,
      .passport = this_->passport,
      .serializeFields = serializeFields,
      .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
