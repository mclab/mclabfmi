/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "jmi_block_solver.h"
#include "jmi_block_solver_impl.h"
#include "SerializableJMI_LINEAR_SOLVER_T.h"
#include "SerializableJMI_LINEAR_SOLVER_SPARSE_T.h"

#include "MCLabFMIEnv.h"

#define DEBUG "SerializableJMI_LINEAR_SOLVER_T"

#define CLASS_NAME "jmi_linear_solver"

// defining constants for fields name

#define FIELD_IPIV "ipiv"
#define FIELD_FACTORIZATION "factorization"
#define FIELD_DEPENDENT_SET "dependent_set"
#define FIELD_JACOBIAN_TEMP "jacobian_temp"
#define FIELD_JACOBIAN_EXTENSION "jacobian_extension"
#define FIELD_RHS "rhs"
#define FIELD_SINGULAR_VALUES "singular_values"
#define FIELD_SINGULAR_VECTORS "singular_vectors"
#define FIELD_RSCALE "singular_rscale"
#define FIELD_CSCALE "singular_cscale"
#define FIELD_EQUED "equed"
#define FIELD_CACHED_JACOBIAN "cached_jacobian"
#define FIELD_SINGULAR_JACOBIAN "singular_jacobian"
#define FIELD_IWORK "iwork"
#define FIELD_UPDATE_ACTIVE_SET "update_active_set"
#define FIELD_N_EXTRA_ROWS "n_extra_rows"
#define FIELD_ZERO_VECTOR "zero_vector"
#define FIELD_RWORK "rwork"
#define FIELD_DGESDD_WORK "dgesdd_work"
#define FIELD_DGESDD_LWORK "dgesdd_lwork"
#define FIELD_DGESDD_IWORK "dgesdd_iwork"
#define FIELD_JSP "jsp"

struct SerializableJMI_LINEAR_SOLVER_T {
  jmi_linear_solver_t *solver;
  jmi_block_solver_t *block_solver;
  // Adding inner struct
  SerializableJMI_LINEAR_SOLVER_SPARSE_T *ser_Jsp;
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_LINEAR_SOLVER_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_LINEAR_SOLVER_T *ser_solver = (SerializableJMI_LINEAR_SOLVER_T *)this_v;
  return ser_solver->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_LINEAR_SOLVER_T *ser_solver = (SerializableJMI_LINEAR_SOLVER_T *)this_v;
  jmi_linear_solver_t *solver = ser_solver->solver;
  jmi_block_solver_t *block_solver = ser_solver->block_solver;
  const void *passport = ser_solver->passport;
  Serializable *serializable = ser_solver->serializable;
  if (solver->Jsp != NULL) {
    Serializable_setField(serializable, FIELD_JSP, PROPERTIES_POINTER, SerializableJMI_LINEAR_SOLVER_SPARSE_T_asSerializable(ser_solver->ser_Jsp), passport);
  }

  size_t ipiv_size = (size_t) block_solver->n;
  MCLabFMILog_verbose("ipiv[%zu]", ipiv_size);
  Serializable_setArrayField(serializable, FIELD_IPIV, PROPERTIES_INT, solver->ipiv, sizeof(int), &ipiv_size, NULL, 0, false, NULL, passport);

  size_t factorization_size = (size_t) (block_solver->n * block_solver->n);
  MCLabFMILog_verbose("factorization[%zu]", factorization_size);
  Serializable_setArrayField(serializable, FIELD_FACTORIZATION, PROPERTIES_DOUBLE, solver->factorization, sizeof(double), &factorization_size, NULL, 0, false, NULL, passport);

  size_t dependent_set_size = (size_t) (block_solver->n * block_solver->n);
  MCLabFMILog_verbose("dependent_set[%zu]", dependent_set_size);
  Serializable_setArrayField(serializable, FIELD_DEPENDENT_SET, PROPERTIES_DOUBLE, solver->dependent_set, sizeof(double), &dependent_set_size, NULL, 0, false, NULL, passport);

  size_t jacobian_temp_size = (size_t) (2 * block_solver->n * block_solver->n);
  MCLabFMILog_verbose("jacobian_temp[%zu]", jacobian_temp_size);
  Serializable_setArrayField(serializable, FIELD_JACOBIAN_TEMP, PROPERTIES_DOUBLE, solver->jacobian_temp, sizeof(double), &jacobian_temp_size, NULL, 0, false, NULL, passport);

  size_t jacobian_extension_size = (size_t) (block_solver->n * block_solver->n);
  MCLabFMILog_verbose("jacobian_extension[%zu]", jacobian_extension_size);
  Serializable_setArrayField(serializable, FIELD_JACOBIAN_EXTENSION, PROPERTIES_DOUBLE, solver->jacobian_extension, sizeof(double), &jacobian_extension_size, NULL, 0, false, NULL, passport);

  size_t rhs_size = (size_t) (2 * block_solver->n);
  MCLabFMILog_verbose("rhs[%zu]", rhs_size);
  Serializable_setArrayField(serializable, FIELD_RHS, PROPERTIES_DOUBLE, solver->rhs, sizeof(double), &rhs_size, NULL, 0, false, NULL, passport);

  size_t singular_values_size = (size_t) (2 * block_solver->n);
  MCLabFMILog_verbose("singular_values[%zu]", singular_values_size);
  Serializable_setArrayField(serializable, FIELD_SINGULAR_VALUES, PROPERTIES_DOUBLE, solver->singular_values, sizeof(double), &singular_values_size, NULL, 0, false, NULL, passport);

  size_t singular_vectors_size = (size_t) (block_solver->n * block_solver->n);
  MCLabFMILog_verbose("singular_vectors[%zu]", singular_vectors_size);
  Serializable_setArrayField(serializable, FIELD_SINGULAR_VECTORS, PROPERTIES_DOUBLE, solver->singular_vectors, sizeof(double), &singular_vectors_size, NULL, 0, false, NULL, passport);

  size_t rScale_size = (size_t) block_solver->n;
  MCLabFMILog_verbose("rScale[%zu]", rScale_size);
  Serializable_setArrayField(serializable, FIELD_RSCALE, PROPERTIES_DOUBLE, solver->rScale, sizeof(double), &rScale_size, NULL, 0, false, NULL, passport);

  size_t cScale_size = (size_t) block_solver->n;
  MCLabFMILog_verbose("cScale[%zu]", cScale_size);
  Serializable_setArrayField(serializable, FIELD_CSCALE, PROPERTIES_DOUBLE, solver->cScale, sizeof(double), &cScale_size, NULL, 0, false, NULL, passport);

  int equed = (int) solver->equed;
  MCLabFMILog_verbose("equed = %c", solver->equed);
  Serializable_setField(serializable, FIELD_EQUED, PROPERTIES_INT, &equed, passport);

  MCLabFMILog_verbose("cached_jacobian = %d", solver->cached_jacobian);
  Serializable_setField(serializable, FIELD_CACHED_JACOBIAN, PROPERTIES_INT, &solver->cached_jacobian, passport);

  MCLabFMILog_verbose("singular_jacobian = %d", solver->singular_jacobian);
  Serializable_setField(serializable, FIELD_SINGULAR_JACOBIAN, PROPERTIES_INT, &solver->singular_jacobian, passport);

  MCLabFMILog_verbose("iwork = %d", solver->iwork);
  Serializable_setField(serializable, FIELD_IWORK, PROPERTIES_INT, &solver->iwork, passport);

  MCLabFMILog_verbose("update_active_set = %d", solver->update_active_set);
  Serializable_setField(serializable, FIELD_UPDATE_ACTIVE_SET, PROPERTIES_INT, &solver->update_active_set, passport);

  MCLabFMILog_verbose("n_extra_rows = %d", solver->n_extra_rows);
  Serializable_setField(serializable, FIELD_N_EXTRA_ROWS, PROPERTIES_INT, &solver->n_extra_rows, passport);

  size_t zero_vector_size = (size_t) block_solver->n;
  MCLabFMILog_verbose("zero_vector[%zu]", zero_vector_size);
  Serializable_setArrayField(serializable, FIELD_ZERO_VECTOR, PROPERTIES_DOUBLE, solver->zero_vector, sizeof(double), &zero_vector_size, NULL, 0, false, NULL, passport);

  size_t dgesdd_work_size = (size_t) solver->dgesdd_lwork;
  MCLabFMILog_verbose("dgesdd_work[%zu]", dgesdd_work_size);
  Serializable_setArrayField(serializable, FIELD_DGESDD_WORK, PROPERTIES_DOUBLE, solver->dgesdd_work, sizeof(double), &dgesdd_work_size, NULL, 0, false, NULL, passport);

  MCLabFMILog_verbose("dgesdd_lwork = %d", solver->dgesdd_lwork);
  Serializable_setField(serializable, FIELD_DGESDD_LWORK, PROPERTIES_INT, &solver->dgesdd_lwork, passport);

  size_t dgesdd_iwork_size = (size_t) (8 * block_solver->n);
  MCLabFMILog_verbose("dgesdd_iwork[%zu]", dgesdd_iwork_size);
  Serializable_setArrayField(serializable, FIELD_DGESDD_IWORK, PROPERTIES_INT, solver->dgesdd_iwork, sizeof(int), &dgesdd_iwork_size, NULL, 0, false, NULL, passport);

  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_LINEAR_SOLVER_T *ser_solver = (SerializableJMI_LINEAR_SOLVER_T *)this_v;
  jmi_linear_solver_t *solver = ser_solver->solver;
  jmi_block_solver_t *block_solver = ser_solver->block_solver;
  const void *passport = ser_solver->passport;
  Serializable *serializable = ser_solver->serializable;
  if (solver->Jsp != NULL) {
    ser_solver->ser_Jsp= *(SerializableJMI_LINEAR_SOLVER_SPARSE_T **) Serializable_getField(serializable, FIELD_JSP, passport);
  }
  // Deserializing simple fields via script

  size_t ipiv_size = (size_t) block_solver->n;
  MCLabFMILog_verbose("ipiv[%zu]", ipiv_size);
  Serializable_getArrayField(serializable, FIELD_IPIV, solver->ipiv, sizeof(int), &ipiv_size, 0, passport);

  size_t factorization_size = (size_t) (block_solver->n * block_solver->n);
  MCLabFMILog_verbose("factorization[%zu]", factorization_size);
  Serializable_getArrayField(serializable, FIELD_FACTORIZATION, solver->factorization, sizeof(double), &factorization_size, 0, passport);

  size_t dependent_set_size = (size_t) (block_solver->n * block_solver->n);
  MCLabFMILog_verbose("dependent_set[%zu]", dependent_set_size);
  Serializable_getArrayField(serializable, FIELD_DEPENDENT_SET, solver->dependent_set, sizeof(double), &dependent_set_size, 0, passport);

  size_t jacobian_temp_size = (size_t) (2 * block_solver->n * block_solver->n);
  MCLabFMILog_verbose("jacobian_temp[%zu]", jacobian_temp_size);
  Serializable_getArrayField(serializable, FIELD_JACOBIAN_TEMP, solver->jacobian_temp, sizeof(double), &jacobian_temp_size, 0, passport);

  size_t jacobian_extension_size = (size_t) (block_solver->n * block_solver->n);
  MCLabFMILog_verbose("jacobian_extension[%zu]", jacobian_extension_size);
  Serializable_getArrayField(serializable, FIELD_JACOBIAN_EXTENSION, solver->jacobian_extension, sizeof(double), &jacobian_extension_size, 0, passport);

  size_t rhs_size = (size_t) (2 * block_solver->n);
  MCLabFMILog_verbose("rhs[%zu]", rhs_size);
  Serializable_getArrayField(serializable, FIELD_RHS, solver->rhs, sizeof(double), &rhs_size, 0, passport);

  size_t singular_values_size = (size_t) (2 * block_solver->n);
  MCLabFMILog_verbose("singular_values[%zu]", singular_values_size);
  Serializable_getArrayField(serializable, FIELD_SINGULAR_VALUES, solver->singular_values, sizeof(double), &singular_values_size, 0, passport);

  size_t singular_vectors_size = (size_t) (block_solver->n * block_solver->n);
  MCLabFMILog_verbose("singular_vectors[%zu]", singular_vectors_size);
  Serializable_getArrayField(serializable, FIELD_SINGULAR_VECTORS, solver->singular_vectors, sizeof(double), &singular_vectors_size, 0, passport);

  size_t rScale_size = (size_t) block_solver->n;
  MCLabFMILog_verbose("rScale[%zu]", rScale_size);
  Serializable_getArrayField(serializable, FIELD_RSCALE, solver->rScale, sizeof(double), &rScale_size, 0, passport);

  size_t cScale_size = (size_t) block_solver->n;
  MCLabFMILog_verbose("cScale[%zu]", cScale_size);
  Serializable_getArrayField(serializable, FIELD_RSCALE, solver->cScale, sizeof(double), &cScale_size, 0, passport);

  int equed = *(int *) Serializable_getField(serializable, FIELD_EQUED, passport);
  solver->equed = (char) equed;
  MCLabFMILog_verbose("equed = %c", solver->equed);

  solver->cached_jacobian= *(int *) Serializable_getField(serializable, FIELD_CACHED_JACOBIAN, passport);
  MCLabFMILog_verbose("cached_jacobian = %d", solver->cached_jacobian);

  solver->singular_jacobian= *(int *) Serializable_getField(serializable, FIELD_SINGULAR_JACOBIAN, passport);
  MCLabFMILog_verbose("singular_jacobian = %d", solver->singular_jacobian);

  solver->iwork= *(int *) Serializable_getField(serializable, FIELD_SINGULAR_JACOBIAN, passport);
  MCLabFMILog_verbose("iwork = %d", solver->iwork);

  solver->update_active_set= *(int *) Serializable_getField(serializable, FIELD_UPDATE_ACTIVE_SET, passport);
  MCLabFMILog_verbose("update_active_set = %d", solver->update_active_set);

  solver->n_extra_rows= *(int *) Serializable_getField(serializable, FIELD_N_EXTRA_ROWS, passport);
  MCLabFMILog_verbose("n_extra_rows = %d", solver->n_extra_rows);

  size_t zero_vector_size = (size_t) block_solver->n;
  MCLabFMILog_verbose("zero_vector[%zu]", zero_vector_size);
  Serializable_getArrayField(serializable, FIELD_ZERO_VECTOR, solver->zero_vector, sizeof(double), &zero_vector_size, 0, passport);

  size_t dgesdd_work_size = (size_t) solver->dgesdd_lwork;
  MCLabFMILog_verbose("dgesdd_work[%zu]", dgesdd_work_size);
  Serializable_getArrayField(serializable, FIELD_DGESDD_WORK, solver->dgesdd_work, sizeof(double), &dgesdd_work_size, 0, passport);

  solver->dgesdd_lwork= *(int *) Serializable_getField(serializable, FIELD_DGESDD_LWORK, passport);
  MCLabFMILog_verbose("dgesdd_lwork = %d", solver->dgesdd_lwork);

  size_t dgesdd_iwork_size = (size_t) (8 * block_solver->n);
  MCLabFMILog_verbose("dgesdd_iwork[%zu]", dgesdd_iwork_size);
  Serializable_getArrayField(serializable, FIELD_DGESDD_IWORK, solver->dgesdd_iwork, sizeof(int), &dgesdd_iwork_size, 0, passport);

  MCLabFMILog_unnest();
}

SerializableJMI_LINEAR_SOLVER_T *SerializableJMI_LINEAR_SOLVER_T_new(MCLabFMIEnv *fmi_env, jmi_linear_solver_t *solver, jmi_block_solver_t *block_solver) {
  Debug_assert(DEBUG_ALWAYS, solver != NULL, "solver == NULL");
  SerializableJMI_LINEAR_SOLVER_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, solver)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_LINEAR_SOLVER_T));
    new->solver = solver;
    new->block_solver = block_solver;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, solver, new,
                    SerializableJMI_LINEAR_SOLVER_T_free, new->passport);
    // Adding inner struct new
    if (solver->Jsp != NULL) {
      new->ser_Jsp = SerializableJMI_LINEAR_SOLVER_SPARSE_T_new(fmi_env, solver->Jsp);
    }
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_LINEAR_SOLVER_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_LINEAR_SOLVER_T *this_ = (SerializableJMI_LINEAR_SOLVER_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_LINEAR_SOLVER_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_LINEAR_SOLVER_T *this_ = (SerializableJMI_LINEAR_SOLVER_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {
      .className = className,
      .passport = this_->passport,
      .serializeFields = serializeFields,
      .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
