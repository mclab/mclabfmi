/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "jmi_delay_impl.h"
#include "SerializableJMI_DELAY_POINT_T.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_DELAYBUFFER_T.h"

#define DEBUG "SerializableJMI_DELAYBUFFER_T"

#define CLASS_NAME "jmi_delaybuffer_t"

// defining constants for fields name
#define FIELD_CAPACITY "capacity"
#define FIELD_SIZE "size"
#define FIELD_HEAD_INDEX "head_index"
#define FIELD_BUF "buf"
#define FIELD_EVENT_CAPACITY "event_capacity"
#define FIELD_EVENT_BUF "event_buf"
#define FIELD_MAX_DELAY "max_delay"

struct SerializableJMI_DELAYBUFFER_T {
  jmi_delaybuffer_t *jmi_delaybuffer;
  // Adding inner struct
  SerializableJMI_DELAY_POINT_T **ser_buf;
  MCLabFMIEnv *fmi_env;
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_DELAYBUFFER_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_DELAYBUFFER_T *ser_jmi_delaybuffer = (SerializableJMI_DELAYBUFFER_T *)this_v;
  return ser_jmi_delaybuffer->class_name;
}

static void *extend_ser_buf(void *obj, size_t size) {
  MCLabFMILog_info("Extending buf");
  SerializableJMI_DELAYBUFFER_T *ser_jmi_delaybuffer = (SerializableJMI_DELAYBUFFER_T *) obj;
  ser_jmi_delaybuffer->ser_buf =
    realloc(ser_jmi_delaybuffer->ser_buf, size * sizeof(SerializableJMI_DELAY_POINT_T *));
  jmi_delaybuffer_t *jmi_delaybuffer = ser_jmi_delaybuffer->jmi_delaybuffer;
  SerializableJMI_DELAY_POINT_T ** ser_pointer_buf =
    (SerializableJMI_DELAY_POINT_T **) ser_jmi_delaybuffer->ser_buf;
  jmi_delaybuffer->buf =
    realloc(jmi_delaybuffer->buf, size * sizeof(jmi_delay_point_t));
  for (size_t i = 0; i < jmi_delaybuffer->capacity; i++) {
    SerializableJMI_DELAY_POINT_T_update(ser_pointer_buf[i], ser_jmi_delaybuffer->fmi_env,
                                       &(((jmi_delay_point_t *) jmi_delaybuffer->buf)[i]));
  }
  for (size_t i = jmi_delaybuffer->capacity; i < size; i++) {
    ser_pointer_buf[i] = SerializableJMI_DELAY_POINT_T_new(
    ser_jmi_delaybuffer->fmi_env, &(((jmi_delay_point_t *) jmi_delaybuffer->buf)[i]));
  }
  jmi_delaybuffer->capacity = size;
  return ser_jmi_delaybuffer->ser_buf;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_DELAYBUFFER_T *ser_jmi_delaybuffer = (SerializableJMI_DELAYBUFFER_T *)this_v;
  jmi_delaybuffer_t *jmi_delaybuffer = ser_jmi_delaybuffer->jmi_delaybuffer;
  const void *passport = ser_jmi_delaybuffer->passport;
  Serializable *serializable = ser_jmi_delaybuffer->serializable;
  MCLabFMILog_verbose("capacity: %d", jmi_delaybuffer->capacity);
  Serializable_setField(serializable, FIELD_CAPACITY, PROPERTIES_INT,
                       &jmi_delaybuffer->capacity, passport);
  MCLabFMILog_verbose("size: %d", jmi_delaybuffer->size);
  Serializable_setField(serializable, FIELD_SIZE, PROPERTIES_INT,
                       &jmi_delaybuffer->size, passport);
  MCLabFMILog_verbose("head_index: %d", jmi_delaybuffer->head_index);
  Serializable_setField(serializable, FIELD_HEAD_INDEX, PROPERTIES_INT,
                       &jmi_delaybuffer->head_index, passport);
  MCLabFMILog_verbose("buf[%d]", jmi_delaybuffer->capacity);
  size_t buf_size = (size_t) jmi_delaybuffer->capacity;
  Serializable_setArrayField(serializable, FIELD_BUF, PROPERTIES_POINTER, (void *)ser_jmi_delaybuffer->ser_buf,
  	                        sizeof(SerializableJMI_DELAY_POINT_T*), &buf_size,
  	                        SerializableJMI_DELAY_POINT_T_asSerializable, 0, false, extend_ser_buf, passport);
  MCLabFMILog_verbose("event_capacity: %d", jmi_delaybuffer->event_capacity);
  Serializable_setField(serializable, FIELD_EVENT_CAPACITY, PROPERTIES_INT,
                       &jmi_delaybuffer->event_capacity, passport);
  MCLabFMILog_verbose("event_buf[%d]", jmi_delaybuffer->event_capacity);
  size_t event_buf_size_t = (size_t) jmi_delaybuffer->event_capacity;
  Serializable_setArrayField(serializable, FIELD_EVENT_BUF, PROPERTIES_INT,
  	                        (void **)jmi_delaybuffer->event_buf, sizeof(int), &event_buf_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("max_delay: %g", jmi_delaybuffer->max_delay);
  Serializable_setField(serializable, FIELD_MAX_DELAY, PROPERTIES_DOUBLE,
                       &jmi_delaybuffer->max_delay, passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_DELAYBUFFER_T *ser_jmi_delaybuffer = (SerializableJMI_DELAYBUFFER_T *)this_v;
  jmi_delaybuffer_t *jmi_delaybuffer = ser_jmi_delaybuffer->jmi_delaybuffer;
  const void *passport = ser_jmi_delaybuffer->passport;
  Serializable *serializable = ser_jmi_delaybuffer->serializable;
  // Deserializing simple fields via script
  jmi_delaybuffer->capacity =
    *(int *) Serializable_getField(serializable, FIELD_CAPACITY, passport);
  MCLabFMILog_verbose("capacity: %d", jmi_delaybuffer->capacity);
  jmi_delaybuffer->size =
    *(int *) Serializable_getField(serializable, FIELD_SIZE, passport);
  MCLabFMILog_verbose("size: %d", jmi_delaybuffer->size);
  jmi_delaybuffer->head_index =
    *(int *) Serializable_getField(serializable, FIELD_HEAD_INDEX, passport);
  MCLabFMILog_verbose("head_index: %d", jmi_delaybuffer->head_index);
  size_t buf_size = (size_t) jmi_delaybuffer->capacity;
  Serializable_getArrayField(serializable, FIELD_BUF, (void **)ser_jmi_delaybuffer->ser_buf,
  	                        sizeof(SerializableJMI_DELAY_POINT_T*), &buf_size, 0, passport);
  MCLabFMILog_verbose("buf[%d]", jmi_delaybuffer->capacity);
  jmi_delaybuffer->event_capacity =
    *(int *) Serializable_getField(serializable, FIELD_EVENT_CAPACITY, passport);
  MCLabFMILog_verbose("event_capacity: %d", jmi_delaybuffer->event_capacity);
  size_t event_buf_size_t = (size_t) jmi_delaybuffer->event_capacity;
  Serializable_getArrayField(serializable, FIELD_EVENT_BUF, (void **)jmi_delaybuffer->event_buf,
  	                        sizeof(int), &event_buf_size_t, 0, passport);
  MCLabFMILog_verbose("event_buf[%d]", jmi_delaybuffer->event_capacity);
  jmi_delaybuffer->max_delay =
    *(double *) Serializable_getField(serializable, FIELD_MAX_DELAY, passport);
  MCLabFMILog_verbose("max_delay: %g", jmi_delaybuffer->max_delay);
  MCLabFMILog_unnest();
}

SerializableJMI_DELAYBUFFER_T *SerializableJMI_DELAYBUFFER_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_delaybuffer_t *jmi_delaybuffer) {
  Debug_assert(DEBUG_ALWAYS, jmi_delaybuffer != NULL, "jmi_delaybuffer == NULL");
  SerializableJMI_DELAYBUFFER_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_delaybuffer)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_DELAYBUFFER_T));
    new->jmi_delaybuffer = jmi_delaybuffer;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    new->fmi_env = fmi_env;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_delaybuffer, new,
                    SerializableJMI_DELAYBUFFER_T_free, new->passport);
    // Adding inner struct new
    size_t buf_size = (size_t) jmi_delaybuffer->capacity;
    new->ser_buf = calloc(buf_size, sizeof(SerializableJMI_DELAY_POINT_T *));
      for (long i = 0; i < buf_size; i++) {
        new->ser_buf[i] =
            SerializableJMI_DELAY_POINT_T_new(fmi_env, &jmi_delaybuffer->buf[i]);
      }
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_DELAYBUFFER_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_DELAYBUFFER_T *this_ = (SerializableJMI_DELAYBUFFER_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->ser_buf);
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_DELAYBUFFER_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_DELAYBUFFER_T *this_ = (SerializableJMI_DELAYBUFFER_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
