/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "utils.h"

int
vsprintf_smart(char **str, char const *fmt, va_list args) {
  *str = NULL;
  va_list args_copy;
  va_copy(args_copy, args);
  int count = vsnprintf(NULL, 0, fmt, args_copy);
  va_end(args_copy);
  va_copy(args_copy, args);
  if (count >= 0) {
    char *buffer = malloc((count + 1) * sizeof(char));
    count = vsnprintf(buffer, count + 1, fmt, args_copy);
    if (count < 0) {
      free(buffer);
      return count;
    }
    *str = buffer;
  }
  va_end(args_copy);
  return count;
}

int
sprintf_smart(char **str, char const *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  int count = vsprintf_smart(str, fmt, args);
  va_end(args);
  return count;
}
