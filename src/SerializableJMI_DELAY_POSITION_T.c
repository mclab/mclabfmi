/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "jmi_delay_impl.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_DELAY_POSITION_T.h"

#define DEBUG "SerializableJMI_DELAY_POSITION_T"

#define CLASS_NAME "jmi_delay_position_t"

// defining constants for fields name
#define FIELD_CURR_INTERVAL "curr_interval"

struct SerializableJMI_DELAY_POSITION_T {
  jmi_delay_position_t *jmi_delay_position;
  // Adding inner struct
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_DELAY_POSITION_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_DELAY_POSITION_T *ser_jmi_delay_position = (SerializableJMI_DELAY_POSITION_T *)this_v;
  return ser_jmi_delay_position->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_DELAY_POSITION_T *ser_jmi_delay_position = (SerializableJMI_DELAY_POSITION_T *)this_v;
  jmi_delay_position_t *jmi_delay_position = ser_jmi_delay_position->jmi_delay_position;
  const void *passport = ser_jmi_delay_position->passport;
  Serializable *serializable = ser_jmi_delay_position->serializable;
  MCLabFMILog_verbose("curr_interval: %d", jmi_delay_position->curr_interval);
  Serializable_setField(serializable, FIELD_CURR_INTERVAL, PROPERTIES_INT,
                       &jmi_delay_position->curr_interval, passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_DELAY_POSITION_T *ser_jmi_delay_position = (SerializableJMI_DELAY_POSITION_T *)this_v;
  jmi_delay_position_t *jmi_delay_position = ser_jmi_delay_position->jmi_delay_position;
  const void *passport = ser_jmi_delay_position->passport;
  Serializable *serializable = ser_jmi_delay_position->serializable;
  // Deserializing simple fields via script
  jmi_delay_position->curr_interval =
    *(int *) Serializable_getField(serializable, FIELD_CURR_INTERVAL, passport);
  MCLabFMILog_verbose("curr_interval: %d", jmi_delay_position->curr_interval);
  MCLabFMILog_unnest();
}

SerializableJMI_DELAY_POSITION_T *SerializableJMI_DELAY_POSITION_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_delay_position_t *jmi_delay_position) {
  Debug_assert(DEBUG_ALWAYS, jmi_delay_position != NULL, "jmi_delay_position == NULL");
  SerializableJMI_DELAY_POSITION_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_delay_position)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_DELAY_POSITION_T));
    new->jmi_delay_position = jmi_delay_position;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_delay_position, new,
                    SerializableJMI_DELAY_POSITION_T_free, new->passport);
    // Adding inner struct new
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_DELAY_POSITION_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_DELAY_POSITION_T *this_ = (SerializableJMI_DELAY_POSITION_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_DELAY_POSITION_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_DELAY_POSITION_T *this_ = (SerializableJMI_DELAY_POSITION_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
