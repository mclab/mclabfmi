/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi_callbacks.h"
#include "SerializableJMI_LOG_OPTIONS_T.h"
#include "SerializableFMI2_ME_T.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_CALLBACKS_T.h"

#define DEBUG "SerializableJMI_CALLBACKS_T"

#define CLASS_NAME "jmi_callbacks_t"

// defining constants for fields name
#define FIELD_LOG_OPTIONS "log_options"




#define FIELD_MODEL_NAME "model_name"
#define FIELD_INSTANCE_NAME "instance_name"
#define FIELD_MODEL_DATA "model_data"

struct SerializableJMI_CALLBACKS_T {
  jmi_callbacks_t *jmi_callbacks;
  // Adding inner struct
  SerializableJMI_LOG_OPTIONS_T *ser_log_options;
  SerializableFMI2_ME_T *ser_model_data;
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_CALLBACKS_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_CALLBACKS_T *ser_jmi_callbacks = (SerializableJMI_CALLBACKS_T *)this_v;
  return ser_jmi_callbacks->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_CALLBACKS_T *ser_jmi_callbacks = (SerializableJMI_CALLBACKS_T *)this_v;
  jmi_callbacks_t *jmi_callbacks = ser_jmi_callbacks->jmi_callbacks;
  const void *passport = ser_jmi_callbacks->passport;
  Serializable *serializable = ser_jmi_callbacks->serializable;
  Serializable_setField(serializable, FIELD_LOG_OPTIONS, PROPERTIES_POINTER,
                       SerializableJMI_LOG_OPTIONS_T_asSerializable(ser_jmi_callbacks->ser_log_options), passport);
  //TO_BE_SERIALIZED FIELD_EMIT_LOG as type jmi_callback_emit_log_ft
  //TO_BE_SERIALIZED FIELD_IS_LOG_CATEGORY_EMITTED as type jmi_callback_is_log_category_emitted_ft
  //TO_BE_SERIALIZED FIELD_ALLOCATE_MEMORY as type jmi_callback_allocate_memory_ft
  //TO_BE_SERIALIZED FIELD_FREE_MEMORY as type jmi_callback_free_memory_ft
  MCLabFMILog_verbose("model_name: %s", jmi_callbacks->model_name);
  Serializable_setField(serializable, FIELD_MODEL_NAME, PROPERTIES_STRING,
                       (void *)jmi_callbacks->model_name, passport);
  MCLabFMILog_verbose("instance_name: %s", jmi_callbacks->instance_name);
  Serializable_setField(serializable, FIELD_INSTANCE_NAME, PROPERTIES_STRING,
                       (void *)jmi_callbacks->instance_name, passport);
  Serializable_setField(serializable, FIELD_MODEL_DATA, PROPERTIES_POINTER,
                       SerializableFMI2_ME_T_asSerializable(ser_jmi_callbacks->ser_model_data), passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_CALLBACKS_T *ser_jmi_callbacks = (SerializableJMI_CALLBACKS_T *)this_v;
  jmi_callbacks_t *jmi_callbacks = ser_jmi_callbacks->jmi_callbacks;
  const void *passport = ser_jmi_callbacks->passport;
  Serializable *serializable = ser_jmi_callbacks->serializable;
  // Deserializing simple fields via script
  ser_jmi_callbacks->ser_log_options =
        *(SerializableJMI_LOG_OPTIONS_T **) Serializable_getField(serializable, FIELD_LOG_OPTIONS, passport);
  //TO_BE_SERIALIZED FIELD_EMIT_LOG as type jmi_callback_emit_log_ft
  //TO_BE_SERIALIZED FIELD_IS_LOG_CATEGORY_EMITTED as type jmi_callback_is_log_category_emitted_ft
  //TO_BE_SERIALIZED FIELD_ALLOCATE_MEMORY as type jmi_callback_allocate_memory_ft
  //TO_BE_SERIALIZED FIELD_FREE_MEMORY as type jmi_callback_free_memory_ft
  SAFETY_CHECK_STRING(serializable, jmi_callbacks->model_name, FIELD_MODEL_NAME, passport);
  MCLabFMILog_verbose("model_name: %s", jmi_callbacks->model_name);
  SAFETY_CHECK_STRING(serializable, jmi_callbacks->instance_name, FIELD_INSTANCE_NAME, passport);
  MCLabFMILog_verbose("instance_name: %s", jmi_callbacks->instance_name);
  ser_jmi_callbacks->ser_model_data =
        *(SerializableFMI2_ME_T **) Serializable_getField(serializable, FIELD_MODEL_DATA, passport);
  MCLabFMILog_unnest();
}

SerializableJMI_CALLBACKS_T *SerializableJMI_CALLBACKS_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_callbacks_t *jmi_callbacks) {
  Debug_assert(DEBUG_ALWAYS, jmi_callbacks != NULL, "jmi_callbacks == NULL");
  SerializableJMI_CALLBACKS_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_callbacks)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_CALLBACKS_T));
    new->jmi_callbacks = jmi_callbacks;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_callbacks, new,
                    SerializableJMI_CALLBACKS_T_free, new->passport);
    // Adding inner struct new
    new->ser_log_options = SerializableJMI_LOG_OPTIONS_T_new(fmi_env, &jmi_callbacks->log_options);
    new->ser_model_data = SerializableFMI2_ME_T_new(fmi_env, jmi_callbacks->model_data);
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_CALLBACKS_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_CALLBACKS_T *this_ = (SerializableJMI_CALLBACKS_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_CALLBACKS_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_CALLBACKS_T *this_ = (SerializableJMI_CALLBACKS_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
