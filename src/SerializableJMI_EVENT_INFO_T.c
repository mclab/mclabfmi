/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi_me.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_EVENT_INFO_T.h"

#define DEBUG "SerializableJMI_EVENT_INFO_T"

#define CLASS_NAME "jmi_event_info_t"

// defining constants for fields name
#define FIELD_ITERATION_CONVERGED "iteration_converged"
#define FIELD_STATE_VALUE_REFERENCES_CHANGED "state_value_references_changed"
#define FIELD_STATE_VALUES_CHANGED "state_values_changed"
#define FIELD_NOMINALS_OF_STATES_CHANGED "nominals_of_states_changed"
#define FIELD_TERMINATE_SIMULATION "terminate_simulation"
#define FIELD_NEXT_EVENT_TIME_DEFINED "next_event_time_defined"
#define FIELD_NEXT_EVENT_TIME "next_event_time"

struct SerializableJMI_EVENT_INFO_T {
  jmi_event_info_t *jmi_event_info;
  // Adding inner struct
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_EVENT_INFO_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_EVENT_INFO_T *ser_jmi_event_info = (SerializableJMI_EVENT_INFO_T *)this_v;
  return ser_jmi_event_info->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_EVENT_INFO_T *ser_jmi_event_info = (SerializableJMI_EVENT_INFO_T *)this_v;
  jmi_event_info_t *jmi_event_info = ser_jmi_event_info->jmi_event_info;
  const void *passport = ser_jmi_event_info->passport;
  Serializable *serializable = ser_jmi_event_info->serializable;
  MCLabFMILog_verbose("iteration_converged: %d", jmi_event_info->iteration_converged);
  Serializable_setField(serializable, FIELD_ITERATION_CONVERGED, PROPERTIES_BOOLEAN,
                       &jmi_event_info->iteration_converged, passport);
  MCLabFMILog_verbose("state_value_references_changed: %d", jmi_event_info->state_value_references_changed);
  Serializable_setField(serializable, FIELD_STATE_VALUE_REFERENCES_CHANGED, PROPERTIES_BOOLEAN,
                       &jmi_event_info->state_value_references_changed, passport);
  MCLabFMILog_verbose("state_values_changed: %d", jmi_event_info->state_values_changed);
  Serializable_setField(serializable, FIELD_STATE_VALUES_CHANGED, PROPERTIES_BOOLEAN,
                       &jmi_event_info->state_values_changed, passport);
  MCLabFMILog_verbose("nominals_of_states_changed: %d", jmi_event_info->nominals_of_states_changed);
  Serializable_setField(serializable, FIELD_NOMINALS_OF_STATES_CHANGED, PROPERTIES_BOOLEAN,
                       &jmi_event_info->nominals_of_states_changed, passport);
  MCLabFMILog_verbose("terminate_simulation: %d", jmi_event_info->terminate_simulation);
  Serializable_setField(serializable, FIELD_TERMINATE_SIMULATION, PROPERTIES_BOOLEAN,
                       &jmi_event_info->terminate_simulation, passport);
  MCLabFMILog_verbose("next_event_time_defined: %d", jmi_event_info->next_event_time_defined);
  Serializable_setField(serializable, FIELD_NEXT_EVENT_TIME_DEFINED, PROPERTIES_BOOLEAN,
                       &jmi_event_info->next_event_time_defined, passport);
  MCLabFMILog_verbose("next_event_time: %g", jmi_event_info->next_event_time);
  Serializable_setField(serializable, FIELD_NEXT_EVENT_TIME, PROPERTIES_DOUBLE,
                       &jmi_event_info->next_event_time, passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_EVENT_INFO_T *ser_jmi_event_info = (SerializableJMI_EVENT_INFO_T *)this_v;
  jmi_event_info_t *jmi_event_info = ser_jmi_event_info->jmi_event_info;
  const void *passport = ser_jmi_event_info->passport;
  Serializable *serializable = ser_jmi_event_info->serializable;
  // Deserializing simple fields via script
  jmi_event_info->iteration_converged =
    *(char *) Serializable_getField(serializable, FIELD_ITERATION_CONVERGED, passport);
  MCLabFMILog_verbose("iteration_converged: %c", jmi_event_info->iteration_converged);
  jmi_event_info->state_value_references_changed =
    *(char *) Serializable_getField(serializable, FIELD_STATE_VALUE_REFERENCES_CHANGED, passport);
  MCLabFMILog_verbose("state_value_references_changed: %c", jmi_event_info->state_value_references_changed);
  jmi_event_info->state_values_changed =
    *(char *) Serializable_getField(serializable, FIELD_STATE_VALUES_CHANGED, passport);
  MCLabFMILog_verbose("state_values_changed: %c", jmi_event_info->state_values_changed);
  jmi_event_info->nominals_of_states_changed =
    *(char *) Serializable_getField(serializable, FIELD_NOMINALS_OF_STATES_CHANGED, passport);
  MCLabFMILog_verbose("nominals_of_states_changed: %c", jmi_event_info->nominals_of_states_changed);
  jmi_event_info->terminate_simulation =
    *(char *) Serializable_getField(serializable, FIELD_TERMINATE_SIMULATION, passport);
  MCLabFMILog_verbose("terminate_simulation: %c", jmi_event_info->terminate_simulation);
  jmi_event_info->next_event_time_defined =
    *(char *) Serializable_getField(serializable, FIELD_NEXT_EVENT_TIME_DEFINED, passport);
  MCLabFMILog_verbose("next_event_time_defined: %c", jmi_event_info->next_event_time_defined);
  jmi_event_info->next_event_time =
    *(double *) Serializable_getField(serializable, FIELD_NEXT_EVENT_TIME, passport);
  MCLabFMILog_verbose("next_event_time: %g", jmi_event_info->next_event_time);
  MCLabFMILog_unnest();
}

SerializableJMI_EVENT_INFO_T *SerializableJMI_EVENT_INFO_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_event_info_t *jmi_event_info) {
  Debug_assert(DEBUG_ALWAYS, jmi_event_info != NULL, "jmi_event_info == NULL");
  SerializableJMI_EVENT_INFO_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_event_info)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_EVENT_INFO_T));
    new->jmi_event_info = jmi_event_info;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_event_info, new,
                    SerializableJMI_EVENT_INFO_T_free, new->passport);
    // Adding inner struct new
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_EVENT_INFO_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_EVENT_INFO_T *this_ = (SerializableJMI_EVENT_INFO_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_EVENT_INFO_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_EVENT_INFO_T *this_ = (SerializableJMI_EVENT_INFO_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
