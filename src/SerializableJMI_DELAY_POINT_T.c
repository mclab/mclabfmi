/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "jmi_delay_impl.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_DELAY_POINT_T.h"

#define DEBUG "SerializableJMI_DELAY_POINT_T"

#define CLASS_NAME "jmi_delay_point_t"

// defining constants for fields name
#define FIELD_T "t"
#define FIELD_Y "y"
#define FIELD_SEGMENT "segment"

struct SerializableJMI_DELAY_POINT_T {
  jmi_delay_point_t *jmi_delay_point;
  // Adding inner struct
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_DELAY_POINT_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_DELAY_POINT_T *ser_jmi_delay_point = (SerializableJMI_DELAY_POINT_T *)this_v;
  return ser_jmi_delay_point->class_name;
}

void SerializableJMI_DELAY_POINT_T_update(SerializableJMI_DELAY_POINT_T *this_,
                                        MCLabFMIEnv *fmi_env,
                                        jmi_delay_point_t *jmi_delay_point) {
  Debug_assert(DEBUG_ALWAYS, this_ != NULL, "Error: this_ == NULL\n");
  Debug_assert(DEBUG_ALWAYS, fmi_env != NULL, "Error: fmi_env == NULL\n");
  Debug_assert(DEBUG_ALWAYS, jmi_delay_point != NULL, "Error: jmi_delay_point == NULL\n");
  MCLabFMIEnv_replace(fmi_env, CLASS_NAME, this_->jmi_delay_point,
                     jmi_delay_point, this_, this_->passport);
  this_->jmi_delay_point = jmi_delay_point;
  Serializable_changeSubInstance(this_->serializable, this_, this_->passport);
}
static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_DELAY_POINT_T *ser_jmi_delay_point = (SerializableJMI_DELAY_POINT_T *)this_v;
  jmi_delay_point_t *jmi_delay_point = ser_jmi_delay_point->jmi_delay_point;
  const void *passport = ser_jmi_delay_point->passport;
  Serializable *serializable = ser_jmi_delay_point->serializable;
  MCLabFMILog_verbose("t: %g", jmi_delay_point->t);
  Serializable_setField(serializable, FIELD_T, PROPERTIES_DOUBLE,
                       &jmi_delay_point->t, passport);
  MCLabFMILog_verbose("y: %g", jmi_delay_point->y);
  Serializable_setField(serializable, FIELD_Y, PROPERTIES_DOUBLE,
                       &jmi_delay_point->y, passport);
  MCLabFMILog_verbose("segment: %d", jmi_delay_point->segment);
  Serializable_setField(serializable, FIELD_SEGMENT, PROPERTIES_INT,
                       &jmi_delay_point->segment, passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_DELAY_POINT_T *ser_jmi_delay_point = (SerializableJMI_DELAY_POINT_T *)this_v;
  jmi_delay_point_t *jmi_delay_point = ser_jmi_delay_point->jmi_delay_point;
  const void *passport = ser_jmi_delay_point->passport;
  Serializable *serializable = ser_jmi_delay_point->serializable;
  // Deserializing simple fields via script
  jmi_delay_point->t =
    *(double *) Serializable_getField(serializable, FIELD_T, passport);
  MCLabFMILog_verbose("t: %g", jmi_delay_point->t);
  jmi_delay_point->y =
    *(double *) Serializable_getField(serializable, FIELD_Y, passport);
  MCLabFMILog_verbose("y: %g", jmi_delay_point->y);
  jmi_delay_point->segment =
    *(int *) Serializable_getField(serializable, FIELD_SEGMENT, passport);
  MCLabFMILog_verbose("segment: %d", jmi_delay_point->segment);
  MCLabFMILog_unnest();
}

SerializableJMI_DELAY_POINT_T *SerializableJMI_DELAY_POINT_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_delay_point_t *jmi_delay_point) {
  Debug_assert(DEBUG_ALWAYS, jmi_delay_point != NULL, "jmi_delay_point == NULL");
  SerializableJMI_DELAY_POINT_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_delay_point)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_DELAY_POINT_T));
    new->jmi_delay_point = jmi_delay_point;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_delay_point, new,
                    SerializableJMI_DELAY_POINT_T_free, new->passport);
    // Adding inner struct new
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_DELAY_POINT_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_DELAY_POINT_T *this_ = (SerializableJMI_DELAY_POINT_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_DELAY_POINT_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_DELAY_POINT_T *this_ = (SerializableJMI_DELAY_POINT_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
