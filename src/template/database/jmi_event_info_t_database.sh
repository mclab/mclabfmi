#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.

FIELDS=(              iteration_converged state_value_references_changed state_values_changed nominals_of_states_changed terminate_simulation next_event_time_defined next_event_time )
SER_AS=(              boolean             boolean                        boolean              boolean                    boolean              boolean                 double          )
TYPES=(               jmi_boolean         jmi_boolean                    jmi_boolean          jmi_boolean                jmi_boolean          jmi_boolean             jmi_real_t      )
TYPES_NO_MODIFIER=(   jmi_boolean         jmi_boolean                    jmi_boolean          jmi_boolean                jmi_boolean          jmi_boolean             jmi_real_t      )
SIZE_1D=(             -                   -                              -                    -                          -                    -                       -               )
field_num=${#FIELDS[@]}
