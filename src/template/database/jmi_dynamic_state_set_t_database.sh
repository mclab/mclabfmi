#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.

FIELDS=(            n_variables    n_states        n_algebraics   variables_value_references    state_value_references     ds_state_value_references     ds_state_value_local_index    algebraic_value_references                                    ds_algebraic_value_references                                      ds_algebraic_value_local_index                                     temp_algebraic                                            coefficent_matrix                                                                             sub_coefficent_matrix                                                                                             dgeqp3_work         dgeqp3_tau           dgeqp3_jpvt       dgeqp3_lwork    coefficents                            )
SER_AS=(            int            int             int            1d_array_int                  1d_array_int               1d_array_int                  1d_array_int                  1d_array_int_size_exp                                         1d_array_int_size_exp                                              1d_array_int_size_exp                                              1d_array_int_size_exp                                     1d_array_double_size_exp                                                                      1d_array_double_size_exp                                                                                          1d_array_double     1d_array_double      1d_array_int      int             funptr                                 )
TYPES=(             jmi_int_t      jmi_int_t       jmi_int_t      jmi_int_t*                    jmi_int_t*                 jmi_int_t*                    jmi_int_t*                    jmi_int_t*                                                    jmi_int_t*                                                         jmi_int_t*                                                         jmi_int_t*                                                jmi_real_t*                                                                                   jmi_real_t*                                                                                                       double*             double*              int*              int             jmi_dynamic_state_coefficents_func_t   )
TYPES_NO_MODIFIER=( jmi_int_t      jmi_int_t       jmi_int_t      jmi_int_t                     jmi_int_t                  jmi_int_t                     jmi_int_t                     jmi_int_t                                                     jmi_int_t                                                          jmi_int_t                                                          jmi_int_t                                                 jmi_real_t                                                                                    jmi_real_t                                                                                                        double              double               int               int             jmi_dynamic_state_coefficents_func_t   )
SIZE_1D=(           -              -               -              n_variables                   n_states                   n_states                      n_states                      "($JM_FIELDNAME->n_variables-$JM_FIELDNAME->n_states)"        "($JM_FIELDNAME->n_variables-$JM_FIELDNAME->n_states)"             "($JM_FIELDNAME->n_variables-$JM_FIELDNAME->n_states)"             "($JM_FIELDNAME->n_variables-$JM_FIELDNAME->n_states)"    "(($JM_FIELDNAME->n_variables-$JM_FIELDNAME->n_states)*$JM_FIELDNAME->n_variables)"          "(($JM_FIELDNAME->n_variables-$JM_FIELDNAME->n_states)*($JM_FIELDNAME->n_variables-$JM_FIELDNAME->n_states))"      dgeqp3_lwork        n_algebraics         n_variables       -               -                                      )
SIZE_K=()
field_num=${#FIELDS[@]}
