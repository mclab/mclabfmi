#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.

FIELDS=(            block_solver_options             log_options           nle_solver_default_tol    nle_solver_tol_factor    events_default_tol     time_events_default_tol     events_tol_factor     cs_solver     cs_rel_tol     cs_step_size     cs_experimental_mode    )
SER_AS=(            str_no_ptr                       str_ptr               double                    double                   double                 double                      double                int           double         double           int                     )
TYPES=(             jmi_block_solver_options_t       jmi_log_options_t*    double                    double                   double                 double                      double                int           double         double           int                     )
TYPES_NO_MODIFIER=( jmi_block_solver_options_t       jmi_log_options_t     double                    double                   double                 double                      double                int           double         double           int                     )
SIZE_1D=(           -                                -                     -                         -                        -                      -                           -                     -             -              -                -                                                                       )
field_num=${#FIELDS[@]}
