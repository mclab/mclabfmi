#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.

FIELDS=(            capacity     size     head_index    buf                                   event_capacity     event_buf           max_delay       )
SER_AS=(            int          int      int           expandable_array_of_struct_no_ptr     int                1d_array_int        double          )
TYPES=(             int          int      int           jmi_delay_point_t*                    int                int*                jmi_real_t      )
TYPES_NO_MODIFIER=( int          int      int           jmi_delay_point_t                     int                int                 jmi_real_t      )
SIZE_1D=(           -            -        -             capacity                              -                  event_capacity      -               )
SIZE_K=()
field_num=${#FIELDS[@]}
