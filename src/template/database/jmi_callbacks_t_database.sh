#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.

FIELDS=(            log_options        emit_log                    is_log_category_emitted                   allocate_memory                    free_memory                  model_name             instance_name         model_data )
SER_AS=(            str_no_ptr         funptr                      funptr                                    funptr                             funptr                       other_const_string     other_const_string    str_ptr    )
TYPES=(             jmi_log_options_t  jmi_callback_emit_log_ft    jmi_callback_is_log_category_emitted_ft   jmi_callback_allocate_memory_ft    jmi_callback_free_memory_ft  "const char*"          "const char*"         fmi2_me_t* )
TYPES_NO_MODIFIER=( jmi_log_options_t  jmi_callback_emit_log_ft    jmi_callback_is_log_category_emitted_ft   jmi_callback_allocate_memory_ft    jmi_callback_free_memory_ft  "const char*"          "const char*"         fmi2_me_t )
SIZE_1D=(           -                  -                           -                                         -                                  -                            -                      -                     -          )
field_num=${#FIELDS[@]}
