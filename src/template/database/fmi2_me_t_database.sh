#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.

FIELDS=(             jmi         fmu_mode       fmu_type   fmu_instance_name  fmu_GUID       initial_logging_on   fmi_functions                     event_info            work_real_array    work_int_array )
SER_AS=(             str_no_ptr  enum           enum       const_string       const_string   int                  funptr                            str_ptr               1d_array_double    1d_array_int   )
TYPES=(              jmi_t       fmi2_mode_t    fmi2Type   fmi2String         fmi2String     fmi2Boolean          "const fmi2CallbackFunctions*"    jmi_event_info_t*     fmi2Real*          fmi2Integer*   )
TYPES_NO_MODIFIER=(  jmi_t       fmi2_mode_t    fmi2Type   fmi2String         fmi2String     fmi2Boolean          fmi2CallbackFunctions             jmi_event_info_t      fmi2Real           fmi2Integer    )
SIZE_1D=(            -           -              -          -                  -              -                    -                                 -                     jmi.n_z            jmi.n_z        )
field_num=${#FIELDS[@]}
