#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.

FIELDS=(            chattering_detection_mode   clear_counter   logging_counter  pre_switches                             chattering                    max_chattering   )
SER_AS=(            int                         int             int              1d_array_double_size_exp                 1d_array_int_size_exp                    int              )
TYPES=(             int                         int             int              jmi_real_t*                              jmi_int_t*                    jmi_int_t        )
TYPES_NO_MODIFIER=( int                         int             int              jmi_real_t                               jmi_int_t                     jmi_int_t        )
SIZE_1D=(           -                           -                -               "ser_$JM_FIELDNAME->n_sw"                "ser_$JM_FIELDNAME->n_sw"     -                )
field_num=${#FIELDS[@]}
