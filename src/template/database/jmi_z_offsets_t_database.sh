#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.

FIELDS=(            o_ci      o_cd      o_pi      o_pd      o_ps      o_pf      o_pe      n_ci      n_cd      n_pi      n_pd      n_ps      n_pf      n_pe     n    )
SER_AS=(            int       int       int       int       int       int       int       int       int       int       int       int       int       int      int  )
TYPES=(             int       int       int       int       int       int       int       int       int       int       int       int       int       int      int  )
TYPES_NO_MODIFIER=( int       int       int       int       int       int       int       int       int       int       int       int       int       int      int  )
SIZE_1D=(           -         -         -         -         -         -         -         -         -         -         -         -         -         -        -    )
field_num=${#FIELDS[@]}
