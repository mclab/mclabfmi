#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.

FIELDS=(            buffer                 no_event         lposition                  rposition                 last_x      )
SER_AS=(            str_no_ptr             boolean          str_no_ptr                 str_no_ptr                double      )
TYPES=(             jmi_delaybuffer_t      jmi_boolean      jmi_delay_position_t       jmi_delay_position_t      jmi_real_t  )
TYPES_NO_MODIFIER=( jmi_delaybuffer_t      jmi_boolean      jmi_delay_position_t       jmi_delay_position_t      jmi_real_t  )
SIZE_1D=(           -                      -                -                          -                         -           )
SIZE_K=()
field_num=${#FIELDS[@]}
