#!/usr/bin/env bash
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.


main() {
  #build .c file
  C_FILENAME="$SER_CLASSNAME.c"
  touch $C_FILENAME
  #include
  generate_includes
  #define
  generate_defines
  #struct
  generate_serializable_struct
  #prototypes
  generate_function_prototypes
  #fun CLASSNAME
  generate_function_CLASSNAME
  #generate function for memory reallocation
  generate_function_EXTEND
  #update function for things that appear inside an expandable array
  if [ "$EXPANDED" = "yes" ]; then generate_function_UPDATE; fi
  # generate_function_UPDATE
  #fun SERIALIZEFIELDS
  generate_function_SERIALIZEFIELDS
  #fun SERIALIZEFIELDS
  generate_function_DESERIALIZEFIELDS
  #fun NEW
  generate_function_NEW
  #fun FREE
  generate_function_FREE
  #fun ASSERIALIZABLE
  generate_function_ASSERIALIZABLE
}

generate_includes(){
  echo \#include \<stdlib.h\> > $C_FILENAME
  echo >> $C_FILENAME
  echo \#include \<MCLabUtils.h\> >> $C_FILENAME
  echo \#include \"MCLabFMIUtils.h\" >> $C_FILENAME
  echo >> $C_FILENAME
  echo //Adding needed include >> $C_FILENAME
  for (( i=0; i<${field_num}; i++ ));
  do
    if [ "${SER_AS[$i]}" = "str_ptr" ] ||  [ "${SER_AS[$i]}" = "str_no_ptr" ] ||  [ "${SER_AS[$i]}" = "array_of_struct_no_ptr" ] ||  [ "${SER_AS[$i]}" = "array_of_struct_ptr" ] ||  [ "${SER_AS[$i]}" = "str_ptr_param" ] ||  [ "${SER_AS[$i]}" = "expandable_array_of_struct_no_ptr" ]
    then
      INNER_STRUCT="Serializable$( echo ${TYPES_NO_MODIFIER[$i]} | tr "[:lower:]" "[:upper:]" ).h"
      TO_INCLUDE+=($INNER_STRUCT)
    fi
  done
  include_num=${#TO_INCLUDE[@]}
  for (( i=0; i<${include_num}; i++ ));
  do
    echo \#include \"${TO_INCLUDE[$i]}\" >> $C_FILENAME
  done
  echo >> $C_FILENAME

  echo \#include \"MCLabFMIEnv.h\" >> $C_FILENAME
  echo \#include \"$H_FILENAME\" >> $C_FILENAME
}

generate_defines(){
  echo >> $C_FILENAME
  echo \#define DEBUG \"$SER_CLASSNAME\" >> $C_FILENAME
  echo >> $C_FILENAME
  echo \#define CLASS_NAME \"$JM_CLASSNAME\" >> $C_FILENAME

  echo >> $C_FILENAME
  echo // defining constants for fields name >> $C_FILENAME

  for (( i=0; i<${field_num}; i++ ));
  do
    TOTAL_SERIALIZED_FIELDS=$((TOTAL_SERIALIZED_FIELDS + 1))
    FIELD_NAME_UPPERCASE=$( echo ${FIELDS[$i]} | tr "[:lower:]" "[:upper:]" )
    if [ "${SER_AS[$i]}" != funptr_str ] && [ "${SER_AS[$i]}" != funptr ] && [ "${SER_AS[$i]}" != preproc_directive ] && [ "${SER_AS[$i]}" != not_yet_implemented ]
      then
        echo -n \#define FIELD_$FIELD_NAME_UPPERCASE \"${FIELDS[$i]}\" >> $C_FILENAME
      fi
    if [ "${SER_AS[$i]}" != int ] && [ "${SER_AS[$i]}" != "long int" ] && [ "${SER_AS[$i]}" != enum ] && [ "${SER_AS[$i]}" != double ] && [ "${SER_AS[$i]}" != const_string ] && [ "${SER_AS[$i]}" != string ] && [ "${SER_AS[$i]}" != 1d_array_double ] && [ "${SER_AS[$i]}" != 1d_array_int ] && [ "${SER_AS[$i]}" != boolean ] && [ "${SER_AS[$i]}" != str_ptr ] && [ "${SER_AS[$i]}" != str_no_ptr ] && [ "${SER_AS[$i]}" != ptr_k_1d_array_double ] && [ "${SER_AS[$i]}" != other_const_string ] && [ "${SER_AS[$i]}" != funptr_str ] && [ "${SER_AS[$i]}" != funptr ] && [ "${SER_AS[$i]}" != 1d_array_char_ptr ] && [ "${SER_AS[$i]}" != preproc_directive ] && [ "${SER_AS[$i]}" != 1d_array_double_size_exp ] && [ "${SER_AS[$i]}" != 1d_array_int_size_exp ] && [ "${SER_AS[$i]}" != array_of_struct_ptr ] && [ "${SER_AS[$i]}" != array_of_struct_no_ptr ] && [ "${SER_AS[$i]}" != preproc_directive ] && [ "${SER_AS[$i]}" != not_yet_implemented ] && [ "${SER_AS[$i]}" != "str_ptr_param" ] && [ "${SER_AS[$i]}" != "expandable_array_of_struct_no_ptr" ]
    then
      echo -n "//TO_BE_SERIALIZED" >> $C_FILENAME
      echo [\"${SER_AS[$i]}\"] TO SERIALIZE FIELD: \"${FIELDS[$i]}\", OF TYPE : \"${TYPES[$i]}\" IN CLASS: \"$SER_CLASSNAME\".
    fi
    echo >> $C_FILENAME
  done
}

generate_serializable_struct(){
  #STRUCT definition
  echo >> $C_FILENAME
  echo struct $SER_CLASSNAME \{ >> $C_FILENAME
  echo ' ' $JM_CLASSNAME \*$JM_FIELDNAME\;  >> $C_FILENAME
  echo ' ' // Adding inner struct >> $C_FILENAME
  #take in account if preproc_directive involve any inner struct in such a casa must be handled
  for (( i=0; i<${field_num}; i++ ));
  do
    TYPE_UPPERCASE=$( echo ${TYPES_NO_MODIFIER[$i]} | tr "[:lower:]" "[:upper:]" )
    # FIELD_CONSTANT=FIELD_$FIELD_NAME_UPPERCASE
    case "${SER_AS[$i]}" in
    "str_ptr" | "str_no_ptr" | "str_ptr_param")
        echo ' ' Serializable$TYPE_UPPERCASE \*ser_${FIELDS[$i]}\; >> $C_FILENAME
        ;;
    "array_of_struct_ptr" | "array_of_struct_no_ptr" | "expandable_array_of_struct_no_ptr")
        echo ' ' Serializable$TYPE_UPPERCASE \*\*ser_${FIELDS[$i]}\; >> $C_FILENAME
        ;;
    "1d_array_int")
        ;;
    *)
        ;;
    esac
  done
  # EXTRA_PARAM_FROM_OUTSIDE=$(echo $EXTRA_PARAM | tr "," "\n")
  for PARAM in $(echo $EXTRA_PARAM| tr " " "^" | tr "," "\n")
  do
    echo "> [$PARAM]"
    echo '' $(echo $PARAM | tr "^" " ")\; >> $C_FILENAME
  done
  #if has an expandable field than needs the env
  if [ ${#TO_EXPAND[@]} != 0 ]
  then
    echo ' ' MCLabFMIEnv \*fmi_env\;  >> $C_FILENAME
  fi
  echo ' ' const char \*class_name\; >> $C_FILENAME
  echo ' ' void *passport\;  >> $C_FILENAME
  echo ' ' Serializable *serializable\; >> $C_FILENAME
  echo }\; >> $C_FILENAME
}

generate_function_prototypes(){
  #prototipo free
  echo >> $C_FILENAME
  echo static void $SER_CLASSNAME\_free\(void **thisP\)\; >> $C_FILENAME
  echo >> $C_FILENAME
}

generate_function_EXTEND(){
  for (( i=0; i<${#TO_EXPAND[@]}; i++ ));
  do
    TYPE_UPPERCASE=$( echo ${TO_EXPAND_TYPE[$i]} | tr "[:lower:]" "[:upper:]" )

      echo static void \*extend_ser_${TO_EXPAND[$i]}\(void \*obj, size_t size\) \{ >> $C_FILENAME
    echo ' ' MCLabFMILog_info\(\"Extending ${TO_EXPAND[$i]}\"\)\; >> $C_FILENAME
    echo ' ' $SER_CLASSNAME \*ser_$JM_FIELDNAME = \($SER_CLASSNAME \*\) obj\; >> $C_FILENAME
    echo ' '  ser_$JM_FIELDNAME-\>ser_${TO_EXPAND[$i]} = >> $C_FILENAME
    echo '   '  realloc\(ser_$JM_FIELDNAME-\>ser_${TO_EXPAND[$i]}, size \* sizeof\(Serializable$TYPE_UPPERCASE \*\)\)\; >> $C_FILENAME
    echo ' ' $JM_CLASSNAME \*$JM_FIELDNAME = ser_$JM_FIELDNAME-\>$JM_FIELDNAME\; >> $C_FILENAME
    echo ' ' Serializable$TYPE_UPPERCASE \*\* ser_pointer_${TO_EXPAND[$i]} = >> $C_FILENAME
    echo '   ' \(Serializable$TYPE_UPPERCASE \*\*\) ser_$JM_FIELDNAME-\>ser_${TO_EXPAND[$i]}\; >> $C_FILENAME
    echo ' ' $JM_FIELDNAME-\>${TO_EXPAND[$i]} = >> $C_FILENAME
    echo '   ' realloc\($JM_FIELDNAME-\>${TO_EXPAND[$i]}, size \* sizeof\(${TO_EXPAND_TYPE[$i]}\)\)\; >> $C_FILENAME
    echo ' '  for \(size_t i = 0\; i \< $JM_FIELDNAME-\>${TO_EXPAND_SIZE[$i]}\; i++\) \{>> $C_FILENAME
    echo '   ' Serializable$TYPE_UPPERCASE\_update\(ser_pointer_${TO_EXPAND[$i]}\[i\], ser_$JM_FIELDNAME-\>fmi_env, >> $C_FILENAME
    echo '                                      '  \&\(\(\(${TO_EXPAND_TYPE[$i]} \*\) $JM_FIELDNAME-\>${TO_EXPAND[$i]}\)\[i\]\)\)\; >> $C_FILENAME
    echo ' ' \} >> $C_FILENAME
    echo ' ' for \(size_t i = $JM_FIELDNAME-\>${TO_EXPAND_SIZE[$i]}\; i \< size\; i++\) \{ >> $C_FILENAME
    echo '   ' ser_pointer_${TO_EXPAND[$i]}\[i\] = Serializable$TYPE_UPPERCASE\_new\(  >> $C_FILENAME
    echo '   ' ser_$JM_FIELDNAME-\>fmi_env, \&\(\(\(${TO_EXPAND_TYPE[$i]} \*\) $JM_FIELDNAME-\>${TO_EXPAND[$i]}\)\[i\]\)\)\; >> $C_FILENAME
    echo ' ' \} >> $C_FILENAME
    echo ' ' $JM_FIELDNAME-\>${TO_EXPAND_SIZE[$i]} = size\; >> $C_FILENAME
    echo ' ' return ser_$JM_FIELDNAME-\>ser_${TO_EXPAND[$i]}\; >> $C_FILENAME
    echo \} >> $C_FILENAME
    echo >> $C_FILENAME
  done
}

generate_function_UPDATE(){
  echo void $SER_CLASSNAME\_update\($SER_CLASSNAME \*this_, >> $C_FILENAME
  echo '                                       ' MCLabFMIEnv \*fmi_env, >> $C_FILENAME
  echo '                                       ' $JM_CLASSNAME \*$JM_FIELDNAME\) \{ >> $C_FILENAME
  echo ' ' 'Debug_assert(DEBUG_ALWAYS, this_ != NULL, "Error: this_ == NULL\n");' >> $C_FILENAME
  echo ' ' 'Debug_assert(DEBUG_ALWAYS, fmi_env != NULL, "Error: fmi_env == NULL\n");' >> $C_FILENAME
  echo ' ' Debug_assert\(DEBUG_ALWAYS, $JM_FIELDNAME != NULL, \"Error: $JM_FIELDNAME == NULL\\n\"\)\; >> $C_FILENAME
  echo ' ' MCLabFMIEnv_replace\(fmi_env, CLASS_NAME, this_-\>$JM_FIELDNAME, >> $C_FILENAME
  echo '                    ' $JM_FIELDNAME, this_, this_-\>passport\)\; >> $C_FILENAME
  echo ' ' this_-\>$JM_FIELDNAME = $JM_FIELDNAME\;  >> $C_FILENAME
  echo ' ' Serializable_changeSubInstance\(this_-\>serializable, this_, this_-\>passport\)\; >> $C_FILENAME
  echo \} >> $C_FILENAME
    # echo >> $C_FILENAME
}

generate_function_CLASSNAME(){
  echo static const char *className\(void *this_v\) \{ >> $C_FILENAME
  echo ' ' 'Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");' >> $C_FILENAME
  echo ' ' $SER_CLASSNAME *ser_$JM_FIELDNAME = \($SER_CLASSNAME *\)this_v\; >> $C_FILENAME
  echo ' ' return ser_$JM_FIELDNAME-\>class_name\;  >> $C_FILENAME
  echo }>> $C_FILENAME
  echo >> $C_FILENAME
}

generate_function_SERIALIZEFIELDS(){
  echo static void serializeFields\(void *this_v\) \{ >> $C_FILENAME
  echo ' ' 'Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");' >> $C_FILENAME
  echo ' ' MCLabFMILog_info\(\"Serialize \" CLASS_NAME \" fields\"\)\; >> $C_FILENAME
  echo ' ' MCLabFMILog_nest\(\)\; >> $C_FILENAME
  echo ' ' $SER_CLASSNAME \*ser_$JM_FIELDNAME = \($SER_CLASSNAME \*\)this_v\; >> $C_FILENAME
  echo ' ' $JM_CLASSNAME \*$JM_FIELDNAME = ser_$JM_FIELDNAME-\>$JM_FIELDNAME\; >> $C_FILENAME
  echo ' ' const void *passport = ser_$JM_FIELDNAME-\>passport\; >> $C_FILENAME
  echo ' ' Serializable \*serializable = ser_$JM_FIELDNAME-\>serializable\; >> $C_FILENAME
  #serializing some easy fields
  for (( i=0; i<${field_num}; i++ ));
  do
    FIELD_NAME_UPPERCASE=$( echo ${FIELDS[$i]} | tr "[:lower:]" "[:upper:]" )
    FIELD_CONSTANT=FIELD_$FIELD_NAME_UPPERCASE
    case "${SER_AS[$i]}" in
    "int" | "enum")
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}: %d\", $JM_FIELDNAME-\>${FIELDS[$i]}\)\; >> $C_FILENAME
        echo ' ' Serializable_setField\(serializable, $FIELD_CONSTANT, PROPERTIES_INT, >> $C_FILENAME
        echo '                      ' \&$JM_FIELDNAME-\>${FIELDS[$i]}, passport\)\;  >> $C_FILENAME
        ;;
    "long int")
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}: %lu\", $JM_FIELDNAME-\>${FIELDS[$i]}\)\; >> $C_FILENAME
        echo ' ' Serializable_setField\(serializable, $FIELD_CONSTANT, PROPERTIES_ULONG, >> $C_FILENAME
        echo '                      ' \&$JM_FIELDNAME-\>${FIELDS[$i]}, passport\)\;  >> $C_FILENAME
        ;;
    "double")
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}: %g\", $JM_FIELDNAME-\>${FIELDS[$i]}\)\; >> $C_FILENAME
        echo ' ' Serializable_setField\(serializable, $FIELD_CONSTANT, PROPERTIES_DOUBLE, >> $C_FILENAME
        echo '                      ' \&$JM_FIELDNAME-\>${FIELDS[$i]}, passport\)\;  >> $C_FILENAME
        ;;
    "const_string" | "other_const_string" | "string")
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}: %s\", $JM_FIELDNAME-\>${FIELDS[$i]}\)\; >> $C_FILENAME
        echo ' ' Serializable_setField\(serializable, $FIELD_CONSTANT, PROPERTIES_STRING, >> $C_FILENAME
        echo '                      ' \(void \*\)$JM_FIELDNAME-\>${FIELDS[$i]}, passport\)\;  >> $C_FILENAME
        ;;
    "boolean")
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}: %d\", $JM_FIELDNAME-\>${FIELDS[$i]}\)\; >> $C_FILENAME
        echo ' ' Serializable_setField\(serializable, $FIELD_CONSTANT, PROPERTIES_BOOLEAN, >> $C_FILENAME
        echo '                      ' \&$JM_FIELDNAME-\>${FIELDS[$i]}, passport\)\;  >> $C_FILENAME
        ;;
    "1d_array_double")
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%d]\", $JM_FIELDNAME-\>${SIZE_1D[$i]}\)\; >> $C_FILENAME
        echo ' ' size_t ${FIELDS[$i]}_size_t = \(size_t\) $JM_FIELDNAME-\>${SIZE_1D[$i]}\; >> $C_FILENAME
        echo ' ' Serializable_setArrayField\(serializable, $FIELD_CONSTANT, PROPERTIES_DOUBLE, >> $C_FILENAME
        echo '  	                       ' \(void \*\*\)$JM_FIELDNAME-\>${FIELDS[$i]}, sizeof\(double\), \&${FIELDS[$i]}_size_t, >> $C_FILENAME
        echo '  	                       ' NULL, 0, false, NULL, passport\)\; >> $C_FILENAME
        ;;
    "1d_array_double_size_exp")
        echo ' ' size_t ${FIELDS[$i]}_size = \(size_t\) \(${SIZE_1D[$i]}\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%zu]\", ${FIELDS[$i]}_size\)\; >> $C_FILENAME
        echo ' ' Serializable_setArrayField\(serializable, $FIELD_CONSTANT, PROPERTIES_DOUBLE, >> $C_FILENAME
        echo '  	                       ' \(void \*\*\)$JM_FIELDNAME-\>${FIELDS[$i]}, sizeof\(double\), \&${FIELDS[$i]}_size, >> $C_FILENAME
        echo '  	                       ' NULL, 0, false, NULL, passport\)\; >> $C_FILENAME
        ;;
    "1d_array_int")
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%d]\", $JM_FIELDNAME-\>${SIZE_1D[$i]}\)\; >> $C_FILENAME
        echo ' ' size_t ${FIELDS[$i]}_size_t = \(size_t\) $JM_FIELDNAME-\>${SIZE_1D[$i]}\; >> $C_FILENAME
        echo ' ' Serializable_setArrayField\(serializable, $FIELD_CONSTANT, PROPERTIES_INT, >> $C_FILENAME
        echo '  	                       ' \(void \*\*\)$JM_FIELDNAME-\>${FIELDS[$i]}, sizeof\(int\), \&${FIELDS[$i]}_size_t, >> $C_FILENAME
        echo '  	                       ' NULL, 0, false, NULL, passport\)\; >> $C_FILENAME
        ;;
    "1d_array_int_size_exp")
        echo ' ' size_t ${FIELDS[$i]}_size = \(size_t\) \(${SIZE_1D[$i]}\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%zu]\", ${FIELDS[$i]}_size\)\; >> $C_FILENAME
        echo ' ' Serializable_setArrayField\(serializable, $FIELD_CONSTANT, PROPERTIES_INT, >> $C_FILENAME
        echo '  	                       ' \(void \*\*\)$JM_FIELDNAME-\>${FIELDS[$i]}, sizeof\(int\), \&${FIELDS[$i]}_size, >> $C_FILENAME
        echo '  	                       ' NULL, 0, false, NULL, passport\)\; >> $C_FILENAME
        ;;

    "ptr_k_1d_array_double")
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%d][%d]\", ${SIZE_K[$i]}, $JM_FIELDNAME-\>${SIZE_1D[$i]}\)\; >> $C_FILENAME
        echo ' ' size_t ${FIELDS[$i]}_sizes\[2\]\; >> $C_FILENAME
        echo ' ' ${FIELDS[$i]}_sizes\[0\] = ${SIZE_K[$i]}\; >> $C_FILENAME
        echo ' ' ${FIELDS[$i]}_sizes\[1\] = \(size_t\) $JM_FIELDNAME-\>${SIZE_1D[$i]}\; >> $C_FILENAME
        echo ' ' Serializable_setArrayField\(serializable, $FIELD_CONSTANT, PROPERTIES_DOUBLE, >> $C_FILENAME
        echo '  	                       ' \(void \*\*\)$JM_FIELDNAME-\>${FIELDS[$i]}, sizeof\(double\), ${FIELDS[$i]}_sizes, >> $C_FILENAME
        echo '  	                       ' NULL, 1, false, NULL, passport\)\; >> $C_FILENAME
        ;;
      "1d_array_char_ptr")
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%d]\", $JM_FIELDNAME-\>${SIZE_1D[$i]}\)\; >> $C_FILENAME
        echo ' ' size_t ${FIELDS[$i]}_size = $JM_FIELDNAME-\>${SIZE_1D[$i]}\; >> $C_FILENAME
	      echo ' ' Serializable_setArrayField\(serializable, $FIELD_CONSTANT, PROPERTIES_POINTER, \(void \*\)$JM_FIELDNAME-\>${FIELDS[$i]}, >> $C_FILENAME
	      echo '  	                       ' sizeof\(char \*\), \&${FIELDS[$i]}_size, NULL, 0, false, NULL, passport\)\; >> $C_FILENAME
	      ;;
    "str_no_ptr" | "str_ptr" | "str_ptr_param")
        # echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}: %p\", $JM_FIELDNAME-\>${FIELDS[$i]}\)\; >> $C_FILENAME
        TYPE_UPPERCASE=$( echo ${TYPES_NO_MODIFIER[$i]} | tr "[:lower:]" "[:upper:]" )
        echo ' ' Serializable_setField\(serializable, $FIELD_CONSTANT, PROPERTIES_POINTER, >> $C_FILENAME
        echo '                      ' Serializable$TYPE_UPPERCASE\_asSerializable\(ser_$JM_FIELDNAME-\>ser_${FIELDS[$i]}\), passport\)\; >> $C_FILENAME
        ;;
    "array_of_struct_no_ptrOLD")
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%d]\", $JM_FIELDNAME-\>${SIZE_1D[$i]}\)\; >> $C_FILENAME
        TYPE_UPPERCASE=$( echo ${TYPES_NO_MODIFIER[$i]} | tr "[:lower:]" "[:upper:]" )
        echo header mod needed to serve $C_FILENAME in Serializable$TYPE_UPPERCASE
        echo ' ' size_t ${FIELDS[$i]}_size = \(size_t\) $JM_FIELDNAME-\>${SIZE_1D[$i]}\; >> $C_FILENAME
        echo ' ' Serializable_setArrayField\(serializable, $FIELD_CONSTANT, PROPERTIES_POINTER, \(void \*\)$JM_FIELDNAME-\>${FIELDS[$i]}, >> $C_FILENAME
        echo '  	                     ' sizeof\(Serializable$TYPE_UPPERCASE\), \&${FIELDS[$i]}_size, >> $C_FILENAME
        echo '                           ' Serializable$TYPE_UPPERCASE\_asSerializable, 0, true, NULL, passport\)\; >> $C_FILENAME
	      ;;
    "array_of_struct_ptr" | "array_of_struct_no_ptr")
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%d]\", $JM_FIELDNAME-\>${SIZE_1D[$i]}\)\; >> $C_FILENAME
        TYPE_UPPERCASE=$( echo ${TYPES_NO_MODIFIER[$i]} | tr "[:lower:]" "[:upper:]" )
        echo ' ' size_t ${FIELDS[$i]}_size = \(size_t\) $JM_FIELDNAME-\>${SIZE_1D[$i]}\; >> $C_FILENAME
        echo ' ' Serializable_setArrayField\(serializable, $FIELD_CONSTANT, PROPERTIES_POINTER, \(void \*\)ser_$JM_FIELDNAME-\>ser_${FIELDS[$i]}, >> $C_FILENAME
        echo '  	                       ' sizeof\(Serializable$TYPE_UPPERCASE\*\), \&${FIELDS[$i]}_size, >> $C_FILENAME
        echo '  	                       ' Serializable$TYPE_UPPERCASE\_asSerializable, 0, false, NULL, passport\)\; >> $C_FILENAME
        ;;
     "expandable_array_of_struct_no_ptr")
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%d]\", $JM_FIELDNAME-\>${SIZE_1D[$i]}\)\; >> $C_FILENAME
        TYPE_UPPERCASE=$( echo ${TYPES_NO_MODIFIER[$i]} | tr "[:lower:]" "[:upper:]" )
        echo ' ' size_t ${FIELDS[$i]}_size = \(size_t\) $JM_FIELDNAME-\>${SIZE_1D[$i]}\; >> $C_FILENAME
        echo ' ' Serializable_setArrayField\(serializable, $FIELD_CONSTANT, PROPERTIES_POINTER, \(void \*\)ser_$JM_FIELDNAME-\>ser_${FIELDS[$i]}, >> $C_FILENAME
        echo '  	                       ' sizeof\(Serializable$TYPE_UPPERCASE\*\), \&${FIELDS[$i]}_size, >> $C_FILENAME
        echo '  	                       ' Serializable$TYPE_UPPERCASE\_asSerializable, 0, false, extend_ser_${FIELDS[$i]}, passport\)\; >> $C_FILENAME
        ;;
    "preproc_directive")
        echo ${FIELDS[$i]} >> $C_FILENAME
	      ;;
    "not_yet_implemented")
        echo ' ' MCLabFMILog_warning\(\"Serialization of ${FIELDS[$i]} is not implemented yet.\"\)\; >> $C_FILENAME
	      ;;
    *)
        echo ' ' //TO_BE_SERIALIZED $FIELD_CONSTANT as type ${TYPES[$i]} >> $C_FILENAME
        ;;
    esac
  done
  echo ' ' MCLabFMILog_unnest\(\)\; >> $C_FILENAME
  #done serializing easy fields
  echo }>> $C_FILENAME
  echo >> $C_FILENAME
}

generate_function_DESERIALIZEFIELDS(){
 #fun DESERIALIZEFIELDS
  echo static void deserializeFields\(void *this_v\) \{ >> $C_FILENAME
  echo ' ' 'Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");'  >> $C_FILENAME
  echo ' ' MCLabFMILog_info\(\"Deserialize \" CLASS_NAME \" fields\"\)\; >> $C_FILENAME
  echo ' ' MCLabFMILog_nest\(\)\; >> $C_FILENAME
  echo ' ' $SER_CLASSNAME \*ser_$JM_FIELDNAME = \($SER_CLASSNAME \*\)this_v\; >> $C_FILENAME
  echo ' ' $JM_CLASSNAME \*$JM_FIELDNAME = ser_$JM_FIELDNAME-\>$JM_FIELDNAME\; >> $C_FILENAME
  echo ' ' const void *passport = ser_$JM_FIELDNAME-\>passport\; >> $C_FILENAME
  echo ' ' Serializable \*serializable = ser_$JM_FIELDNAME-\>serializable\; >> $C_FILENAME
  echo ' ' // Deserializing simple fields via script >> $C_FILENAME
  #deserializing some easy fields
  for (( i=0; i<${field_num}; i++ ));
  do
    FIELD_NAME_UPPERCASE=$( echo ${FIELDS[$i]} | tr "[:lower:]" "[:upper:]" )
    FIELD_CONSTANT=FIELD_$FIELD_NAME_UPPERCASE
    case "${SER_AS[$i]}" in
    "int" | "enum")
        echo ' ' $JM_FIELDNAME-\>${FIELDS[$i]} = >> $C_FILENAME
        echo '   ' \*\(int \*\) Serializable_getField\(serializable, $FIELD_CONSTANT, passport\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}: %d\", $JM_FIELDNAME-\>${FIELDS[$i]}\)\; >> $C_FILENAME
        ;;
    "long int")
        echo ' ' $JM_FIELDNAME-\>${FIELDS[$i]} = >> $C_FILENAME
        echo '   ' \*\(unsigned long \*\) Serializable_getField\(serializable, $FIELD_CONSTANT, passport\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}: %lu\", $JM_FIELDNAME-\>${FIELDS[$i]}\)\; >> $C_FILENAME
        ;;
    "double")
        echo ' ' $JM_FIELDNAME-\>${FIELDS[$i]} = >> $C_FILENAME
        echo '   ' \*\(double \*\) Serializable_getField\(serializable, $FIELD_CONSTANT, passport\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}: %g\", $JM_FIELDNAME-\>${FIELDS[$i]}\)\; >> $C_FILENAME
        ;;
    "const_string")
        # echo ' ' $JM_FIELDNAME-\>${FIELDS[$i]} = >> $C_FILENAME
        # echo '   ' \(char \*\) Serializable_getField\(serializable, $FIELD_CONSTANT, passport\)\; >> $C_FILENAME
        echo ' ' Serializable_deserializeString\( >> $C_FILENAME
        echo '   ' \(char \*\) $JM_FIELDNAME-\>${FIELDS[$i]}, >> $C_FILENAME
        echo '   ' \(char \*\) Serializable_getField\(serializable, $FIELD_CONSTANT, passport\)\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}: %s\", $JM_FIELDNAME-\>${FIELDS[$i]}\)\; >> $C_FILENAME
        ;;
    "other_const_string" | "string")
        echo ' ' SAFETY_CHECK_STRING\(serializable, $JM_FIELDNAME-\>${FIELDS[$i]}, $FIELD_CONSTANT, passport\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}: %s\", $JM_FIELDNAME-\>${FIELDS[$i]}\)\; >> $C_FILENAME
        ;;
    "boolean")
        echo ' ' $JM_FIELDNAME-\>${FIELDS[$i]} = >> $C_FILENAME
        echo '   ' \*\(char \*\) Serializable_getField\(serializable, $FIELD_CONSTANT, passport\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}: %c\", $JM_FIELDNAME-\>${FIELDS[$i]}\)\; >> $C_FILENAME
          # this_->iteration_converged = *(char *) Serializable_getField(this_->serializable, "iteration_converged", this_->passport);

        ;;
    "1d_array_double")
        echo ' ' size_t ${FIELDS[$i]}_size_t = \(size_t\) $JM_FIELDNAME-\>${SIZE_1D[$i]}\; >> $C_FILENAME
        echo ' ' Serializable_getArrayField\(serializable, $FIELD_CONSTANT, \(void \*\*\)$JM_FIELDNAME-\>${FIELDS[$i]}, >> $C_FILENAME
        echo '  	                       ' sizeof\(double\), \&${FIELDS[$i]}_size_t, 0, passport\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%d]\", $JM_FIELDNAME-\>${SIZE_1D[$i]}\)\; >> $C_FILENAME
        ;;
    "1d_array_double_size_exp")
        echo ' ' size_t ${FIELDS[$i]}_size = \(size_t\) \(${SIZE_1D[$i]}\)\; >> $C_FILENAME
        echo ' ' Serializable_getArrayField\(serializable, $FIELD_CONSTANT, \(void \*\*\)$JM_FIELDNAME-\>${FIELDS[$i]}, >> $C_FILENAME
        echo '  	                       ' sizeof\(double\), \&${FIELDS[$i]}_size, 0, passport\)\; >> $C_FILENAME
        # echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%d]\", $JM_FIELDNAME-\>${SIZE_1D[$i]}\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%zu]\", ${FIELDS[$i]}_size\)\; >> $C_FILENAME
        ;;
    "1d_array_int")
        echo ' ' size_t ${FIELDS[$i]}_size_t = \(size_t\) $JM_FIELDNAME-\>${SIZE_1D[$i]}\; >> $C_FILENAME
        echo ' ' Serializable_getArrayField\(serializable, $FIELD_CONSTANT, \(void \*\*\)$JM_FIELDNAME-\>${FIELDS[$i]}, >> $C_FILENAME
        echo '  	                       ' sizeof\(int\), \&${FIELDS[$i]}_size_t, 0, passport\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%d]\", $JM_FIELDNAME-\>${SIZE_1D[$i]}\)\; >> $C_FILENAME
        ;;
    "1d_array_int_size_exp")
        echo ' ' size_t ${FIELDS[$i]}_size = \(size_t\) \(${SIZE_1D[$i]}\)\; >> $C_FILENAME
        echo ' ' Serializable_getArrayField\(serializable, $FIELD_CONSTANT, \(void \*\*\)$JM_FIELDNAME-\>${FIELDS[$i]}, >> $C_FILENAME
        echo '  	                       ' sizeof\(int\), \&${FIELDS[$i]}_size, 0, passport\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%zu]\", ${FIELDS[$i]}_size\)\; >> $C_FILENAME
        ;;

    "ptr_k_1d_array_double")
        echo ' ' size_t ${FIELDS[$i]}_sizes\[2\]\; >> $C_FILENAME
        echo ' ' ${FIELDS[$i]}_sizes\[0\] = ${SIZE_K[$i]}\; >> $C_FILENAME
        echo ' ' ${FIELDS[$i]}_sizes\[1\] = \(size_t\) $JM_FIELDNAME-\>${SIZE_1D[$i]}\; >> $C_FILENAME
        echo ' ' Serializable_getArrayField\(serializable, $FIELD_CONSTANT, \(void \*\*\)$JM_FIELDNAME-\>${FIELDS[$i]}, >> $C_FILENAME
        echo '  	                       ' sizeof\(double\), ${FIELDS[$i]}_sizes, 1, passport\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%d][%d]\", ${SIZE_K[$i]}, $JM_FIELDNAME-\>${SIZE_1D[$i]}\)\; >> $C_FILENAME
        ;;

    "1d_array_char_ptr")
        echo ' ' size_t ${FIELDS[$i]}_size = $JM_FIELDNAME-\>${SIZE_1D[$i]}\; >> $C_FILENAME
        # echo ' ' $JM_FIELDNAME-\>${FIELDS[$i]} = >> $C_FILENAME
        # echo '   ' \(char \*\*\) Serializable_getArrayField\(serializable, $FIELD_CONSTANT, \(void \*\*\)$JM_FIELDNAME-\>${FIELDS[$i]}, >> $C_FILENAME
        # echo '  	                       ' sizeof\(char \*\), \&${FIELDS[$i]}_size, 0, passport\)\; >> $C_FILENAME
        echo ' ' Serializable_getArrayField\(serializable, $FIELD_CONSTANT, \(void \*\*\)$JM_FIELDNAME-\>${FIELDS[$i]}, >> $C_FILENAME
        echo '                            ' sizeof\(char \*\), \&${FIELDS[$i]}_size, 0, passport\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%d]\", $JM_FIELDNAME-\>${SIZE_1D[$i]}\)\; >> $C_FILENAME
	      ;;
    "str_no_ptr" | "str_ptr" | "str_ptr_param")
        TYPE_UPPERCASE=$( echo ${TYPES_NO_MODIFIER[$i]} | tr "[:lower:]" "[:upper:]" )
        echo ' ' ser_$JM_FIELDNAME-\>ser_${FIELDS[$i]} = >> $C_FILENAME
        echo '       ' \*\(Serializable$TYPE_UPPERCASE \*\*\) Serializable_getField\(serializable, $FIELD_CONSTANT, passport\)\; >> $C_FILENAME
        # echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}: %p\", $JM_FIELDNAME-\>${FIELDS[$i]}\)\; >> $C_FILENAME
        ;;
    "array_of_struct_no_ptrOLD")
        TYPE_UPPERCASE=$( echo ${TYPES_NO_MODIFIER[$i]} | tr "[:lower:]" "[:upper:]" )
        echo ' ' size_t ${FIELDS[$i]}_size = \(size_t\) $JM_FIELDNAME-\>${SIZE_1D[$i]}\; >> $C_FILENAME
        echo ' ' Serializable_getArrayField\(serializable, $FIELD_CONSTANT, \(void \*\*\)$JM_FIELDNAME-\>${FIELDS[$i]}, >> $C_FILENAME
        echo '  	                     ' sizeof\(Serializable$TYPE_UPPERCASE\), \&${FIELDS[$i]}_size, 0, passport\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%d]\", $JM_FIELDNAME-\>${SIZE_1D[$i]}\)\; >> $C_FILENAME
        ;;
    "not_yet_impl")
      	echo ' ' MCLabFMILog_warning\( >> $C_FILENAME
	      echo '     ' \"Serialization of $JM_FIELDNAME-\>${FIELDS[$i]} is not implemented yet\"\)\; >> $C_FILENAME
        ;;
"array_of_struct_ptr" | "array_of_struct_no_ptr" | "expandable_array_of_struct_no_ptr")
        TYPE_UPPERCASE=$( echo ${TYPES_NO_MODIFIER[$i]} | tr "[:lower:]" "[:upper:]" )
        echo ' ' size_t ${FIELDS[$i]}_size = \(size_t\) $JM_FIELDNAME-\>${SIZE_1D[$i]}\; >> $C_FILENAME
        echo ' ' Serializable_getArrayField\(serializable, $FIELD_CONSTANT, \(void \*\*\)ser_$JM_FIELDNAME-\>ser_${FIELDS[$i]}, >> $C_FILENAME
        echo '  	                       ' sizeof\(Serializable$TYPE_UPPERCASE\*\), \&${FIELDS[$i]}_size, 0, passport\)\; >> $C_FILENAME
        echo ' ' MCLabFMILog_verbose\(\"${FIELDS[$i]}[%d]\", $JM_FIELDNAME-\>${SIZE_1D[$i]}\)\; >> $C_FILENAME
        ;;

    "preproc_directive")
        echo ${FIELDS[$i]} >> $C_FILENAME
	      ;;
    "not_yet_implemented")
        echo ' ' MCLabFMILog_warning\(\"Deserialization of ${FIELDS[$i]} is not implemented yet.\"\)\; >> $C_FILENAME
	      ;;

    *)
        echo ' ' //TO_BE_SERIALIZED $FIELD_CONSTANT as type ${TYPES[$i]} >> $C_FILENAME
        ;;
    esac
  done
  echo ' ' MCLabFMILog_unnest\(\)\; >> $C_FILENAME
  #done deserializing easy fields
  echo }>> $C_FILENAME
  echo >> $C_FILENAME
}

generate_function_NEW(){
  echo $SER_CLASSNAME \*$SER_CLASSNAME\_new\(MCLabFMIEnv *fmi_env,  >> $C_FILENAME
  echo '                                                ' $JM_CLASSNAME *$JM_FIELDNAME$EXTRA_PARAM\) \{ >> $C_FILENAME # TODO indentare as serclassname
  echo ' ' Debug_assert\(DEBUG_ALWAYS, $JM_FIELDNAME != NULL, \"$JM_FIELDNAME == NULL\"\)\;>> $C_FILENAME
  echo ' ' $SER_CLASSNAME *new = NULL\; >> $C_FILENAME
  echo ' ' if \(\(new = MCLabFMIEnv_get\(fmi_env, CLASS_NAME, $JM_FIELDNAME\)\) == NULL\) \{ >> $C_FILENAME
  echo '   ' MCLabFMILog_nest\(\)\; >> $C_FILENAME
  echo '   ' new = calloc\(1, sizeof\($SER_CLASSNAME\)\)\; >> $C_FILENAME
  echo '   ' new-\>$JM_FIELDNAME = $JM_FIELDNAME\; >> $C_FILENAME
  echo '   ' new-\>class_name = CLASS_NAME\; >> $C_FILENAME
  echo '   ' new-\>passport = malloc\(sizeof\(char\)\)\; >> $C_FILENAME
  echo '   ' new-\>serializable = NULL\; >> $C_FILENAME

  #if has an expandable field than needs the env
  if [ ${#TO_EXPAND[@]} != 0 ]
  then
    echo '   ' new-\>fmi_env = fmi_env\;  >> $C_FILENAME
  fi


  echo '   ' MCLabFMIEnv_add\(fmi_env, CLASS_NAME, $JM_FIELDNAME, new, >> $C_FILENAME
  echo '                   ' $SER_CLASSNAME\_free, new-\>passport\)\; >> $C_FILENAME
  echo '   ' // Adding inner struct new >> $C_FILENAME
  for (( i=0; i<${field_num}; i++ ));
  do
    TYPE_UPPERCASE=$( echo ${TYPES_NO_MODIFIER[$i]} | tr "[:lower:]" "[:upper:]" )
    case "${SER_AS[$i]}" in
    "str_no_ptr")
      echo '   ' new-\>ser_${FIELDS[$i]} = Serializable$TYPE_UPPERCASE\_new\(fmi_env, \&$JM_FIELDNAME-\>${FIELDS[$i]}\)\; >> $C_FILENAME
        ;;
    "str_ptr")
      echo '   ' new-\>ser_${FIELDS[$i]} = Serializable$TYPE_UPPERCASE\_new\(fmi_env, $JM_FIELDNAME-\>${FIELDS[$i]}\)\; >> $C_FILENAME
        ;;
    "str_ptr_param")
      echo '   ' new-\>ser_${FIELDS[$i]} = Serializable$TYPE_UPPERCASE\_new\(fmi_env, $JM_FIELDNAME-\>${FIELDS[$i]}${SIZE_1D[$i]}\)\; >> $C_FILENAME
        ;;
    "array_of_struct_ptr")
      echo '   ' size_t ${FIELDS[$i]}_size = \(size_t\) $JM_FIELDNAME-\>${SIZE_1D[$i]}\; >> $C_FILENAME
      echo '   ' new-\>ser_${FIELDS[$i]} = calloc\(${FIELDS[$i]}_size, sizeof\(Serializable$TYPE_UPPERCASE \*\)\)\;>> $C_FILENAME
      echo '     ' for \(long i = 0\; i \< ${FIELDS[$i]}_size\; i++\) \{ >> $C_FILENAME
      echo '       ' new-\>ser_${FIELDS[$i]}\[i\] = >> $C_FILENAME
      echo '           ' Serializable$TYPE_UPPERCASE\_new\(fmi_env, $JM_FIELDNAME-\>${FIELDS[$i]}\[i\]\)\; >> $C_FILENAME
    	echo '     ' \} >> $C_FILENAME
        ;;
    "array_of_struct_no_ptr" | "expandable_array_of_struct_no_ptr")
      echo '   ' size_t ${FIELDS[$i]}_size = \(size_t\) $JM_FIELDNAME-\>${SIZE_1D[$i]}\; >> $C_FILENAME
      echo '   ' new-\>ser_${FIELDS[$i]} = calloc\(${FIELDS[$i]}_size, sizeof\(Serializable$TYPE_UPPERCASE \*\)\)\;>> $C_FILENAME
      echo '     ' for \(long i = 0\; i \< ${FIELDS[$i]}_size\; i++\) \{ >> $C_FILENAME
      echo '       ' new-\>ser_${FIELDS[$i]}\[i\] = >> $C_FILENAME
      echo '           ' Serializable$TYPE_UPPERCASE\_new\(fmi_env, \&$JM_FIELDNAME-\>${FIELDS[$i]}\[i\]\)\; >> $C_FILENAME
    	echo '     ' \} >> $C_FILENAME
        ;;
    *)
        ;;
    esac
  done

  # echo '   ' new-\>class_name = CLASS_NAME\; >> $C_FILENAME
  # echo '   ' new-\>passport = malloc\(sizeof\(char\)\)\; >> $C_FILENAME
  # echo '   ' new-\>serializable = NULL\; >> $C_FILENAME
  # echo '   ' MCLabFMIEnv_add\(fmi_env, CLASS_NAME, $JM_FIELDNAME, new, >> $C_FILENAME
  # echo '                   ' $SER_CLASSNAME\_free, new-\>passport\)\; >> $C_FILENAME
  echo '   ' MCLabFMILog_unnest\(\)\; >> $C_FILENAME
  echo ' ' }>> $C_FILENAME
  echo ' ' return new\; >> $C_FILENAME
  echo }>> $C_FILENAME
}

generate_function_FREE(){
  echo static void $SER_CLASSNAME\_free\(void **thisP\) \{ >> $C_FILENAME
  echo ' ' 'Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");' >> $C_FILENAME
  echo ' '  $SER_CLASSNAME \*this_ = \($SER_CLASSNAME \*\)\*thisP\;>> $C_FILENAME
  echo ' '  // TODO free of calloc-ated stuff and free of inner structure  >> $C_FILENAME
  for (( i=0; i<${field_num}; i++ ));
  do
    TYPE_UPPERCASE=$( echo ${TYPES_NO_MODIFIER[$i]} | tr "[:lower:]" "[:upper:]" )
    case "${SER_AS[$i]}" in
    "str_no_ptr")
        ;;
    "str_ptr")
        ;;
    "array_of_struct_ptr" | "array_of_struct_no_ptr" | "expandable_array_of_struct_no_ptr")
      echo ' ' free\(this_-\>ser_${FIELDS[$i]}\)\; >> $C_FILENAME
        ;;
    *)
        ;;
    esac
  done
  echo ' ' free\(this_-\>passport\)\; >> $C_FILENAME
  echo ' ' free\(*thisP\)\; >> $C_FILENAME
  echo }>> $C_FILENAME
}

generate_function_ASSERIALIZABLE(){
  echo Serializable \*$SER_CLASSNAME\_asSerializable\(void \*this_v\) \{ >> $C_FILENAME
  echo ' ' 'Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");' >> $C_FILENAME
  echo ' ' $SER_CLASSNAME \*this_ = \($SER_CLASSNAME \*\)this_v\;  >> $C_FILENAME
  echo ' ' if \(this_-\>serializable == NULL\) \{ >> $C_FILENAME
  echo '   ' SerializableMethods methods = \{.className = className, >> $C_FILENAME
  echo '                                  ' .passport = this_-\>passport, >> $C_FILENAME
  echo '                                  ' .serializeFields = serializeFields, >> $C_FILENAME
  echo '                                  ' .deserializeFields = deserializeFields\}\; >> $C_FILENAME
  echo '   ' this_-\>serializable = Serializable_new\(this_, methods\)\;  >> $C_FILENAME
  echo ' ' }>> $C_FILENAME
  echo ' ' return this_-\>serializable\; >> $C_FILENAME
  echo }>> $C_FILENAME
}
main "$@"
