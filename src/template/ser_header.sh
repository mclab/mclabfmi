#!/usr/bin/env bash
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.


# EXTRA_PARAM=", size_t n_sw"

#build .h file
H_FILENAME="$SER_CLASSNAME.h"
touch $H_FILENAME
echo \#pragma once > $H_FILENAME #check if this or ifdef
# TODO includes?
echo // TODO insert includes for used struct, and check if this two are ok >> $H_FILENAME
echo \#include \<MCLabUtils.h\> >> $H_FILENAME #TODO included cause make complains about it, check if is ok
echo \#include \"MCLabFMIEnv.h\" >> $H_FILENAME
echo >> $H_FILENAME
echo >> $H_FILENAME
echo typedef struct $SER_CLASSNAME $SER_CLASSNAME\; >> $H_FILENAME
echo >> $H_FILENAME
echo $SER_CLASSNAME *$SER_CLASSNAME\_new\(MCLabFMIEnv *fmi_env, $JM_CLASSNAME *$JM_FIELDNAME$EXTRA_PARAM\)\; >> $H_FILENAME
echo >> $H_FILENAME

echo Serializable *$SER_CLASSNAME\_asSerializable\(void *this_v\)\; >> $H_FILENAME

if [ "$EXPANDED" = "yes" ]
  then
    echo void $SER_CLASSNAME\_update\($SER_CLASSNAME \*this_, >> $H_FILENAME
    echo '                                       ' MCLabFMIEnv \*fmi_env, >> $H_FILENAME
    echo '                                       ' $JM_CLASSNAME \*$JM_FIELDNAME\)\; >> $H_FILENAME

fi


