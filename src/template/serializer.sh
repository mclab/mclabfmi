#!/usr/bin/env bash
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.


#clean any file generated before
rm -f generated_file/Serializable*

TOTAL_SERIALIZED_FIELDS=0
#serializing fmi2_me_t
JM_CLASSNAME="fmi2_me_t"
JM_FIELDNAME="fmi2_me"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "fmi2_me.h" "jmi_me.h")
. ./database/fmi2_me_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_t
JM_CLASSNAME="jmi_t"
JM_FIELDNAME="jmi"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi.h" "jmi_dynamic_state.h" "jmi_delay_impl.h")
. ./database/jmi_me_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_event_info_t
JM_CLASSNAME="jmi_event_info_t"
JM_FIELDNAME="jmi_event_info"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi_me.h")
. ./database/jmi_event_info_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_time_event_t
JM_CLASSNAME="jmi_time_event_t"
JM_FIELDNAME="jmi_time_event"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi_util.h")
. ./database/jmi_time_event_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_chattering_t
JM_CLASSNAME="jmi_chattering_t"
JM_FIELDNAME="jmi_chattering"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=", size_t n_sw"
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi_chattering.h")
. ./database/jmi_chattering_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_callbacks_t
JM_CLASSNAME="jmi_callbacks_t"
JM_FIELDNAME="jmi_callbacks"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi_callbacks.h")
. ./database/jmi_callbacks_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_log_options_t
JM_CLASSNAME="jmi_log_options_t"
JM_FIELDNAME="jmi_log_options"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi_callbacks.h")
. ./database/jmi_log_options_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_options_t
JM_CLASSNAME="jmi_options_t"
JM_FIELDNAME="jmi_options"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi_util.h")
. ./database/jmi_options_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_block_solver_options_t
JM_CLASSNAME="jmi_block_solver_options_t"
JM_FIELDNAME="jmi_block_solver_options"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi_block_solver.h")
. ./database/jmi_block_solver_options_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_z_t
JM_CLASSNAME="jmi_z_t"
JM_FIELDNAME="jmi_z"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi.h")
. ./database/jmi_z_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_z_strings_t
JM_CLASSNAME="jmi_z_strings_t"
JM_FIELDNAME="jmi_z_strings"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi.h")
. ./database/jmi_z_strings_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_z_offsets_t
JM_CLASSNAME="jmi_z_offsets_t"
JM_FIELDNAME="jmi_z_offsets"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi.h")
. ./database/jmi_z_offsets_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_delay_t
JM_CLASSNAME="jmi_delay_t"
JM_FIELDNAME="jmi_delay"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi.h" "jmi_delay_impl.h")
. ./database/jmi_delay_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_delaybuffer_t
JM_CLASSNAME="jmi_delaybuffer_t"
JM_FIELDNAME="jmi_delaybuffer"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=( "buf")
TO_EXPAND_TYPE=( "jmi_delay_point_t")
TO_EXPAND_SIZE=( "capacity")
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi.h" "jmi_delay_impl.h")
. ./database/jmi_delaybuffer_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_delay_point_t
JM_CLASSNAME="jmi_delay_point_t"
JM_FIELDNAME="jmi_delay_point"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED="yes"
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi.h" "jmi_delay_impl.h")
. ./database/jmi_delay_point_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_delay_position_t
JM_CLASSNAME="jmi_delay_position_t"
JM_FIELDNAME="jmi_delay_position"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi.h" "jmi_delay_impl.h")
. ./database/jmi_delay_position_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_spatialdist_t
JM_CLASSNAME="jmi_spatialdist_t"
JM_FIELDNAME="jmi_spatialdist"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi.h" "jmi_delay_impl.h")
. ./database/jmi_spatialdist_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_dynamic_state_set_t
JM_CLASSNAME="jmi_dynamic_state_set_t"
JM_FIELDNAME="jmi_dynamic_state_set"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi.h" "jmi_dynamic_state.h")
. ./database/jmi_dynamic_state_set_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh


#serializing jmi_block_residual_t
JM_CLASSNAME="jmi_block_residual_t"
JM_FIELDNAME="jmi_block_residual"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi.h" "jmi_block_residual.h")
. ./database/jmi_block_residual_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh

#serializing jmi_block_solver_t
JM_CLASSNAME="jmi_block_solver_t"
JM_FIELDNAME="jmi_block_solver"
JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
EXTRA_PARAM=""
TO_EXPAND=()
EXPANDED=""
SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
echo serializing $SER_CLASSNAME

TO_INCLUDE=( "jmi.h" "jmi_block_residual.h" "jmi_block_solver.h" "jmi_block_solver_impl.h")
. ./database/jmi_block_solver_t_database.sh

echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

. ./ser_header.sh
. ./ser_src.sh


#serializing DlsMat
#JM_CLASSNAME="_DlsMat"
#JM_FIELDNAME="_dls_mat"
#JM_CLASSNAME_UPPERCASE=$( echo $JM_CLASSNAME | tr "[:lower:]" "[:upper:]" )
#SER_CLASSNAME="Serializable$JM_CLASSNAME_UPPERCASE"
# EXTRA_PARAM=""
# TO_EXPAND=()
EXPANDED=""
#echo serializing $SER_CLASSNAME

#TO_INCLUDE=( "jmi.h" "jmi_block_solver_impl.h") # "jmi_block_residual.h" "jmi_block_solver.h" "jmi_block_solver_impl.h")
#. ./database/_DlsMat_database.sh

#echo $field_num = ${#SER_AS[@]} = ${#TYPES[@]} = ${#TYPES_NO_MODIFIER[@]} = ${#SIZE_1D[@]}

#. ./ser_header.sh
#. ./ser_src.sh

# check what changed from latest working set of files
mv Serializable* generated_file/
echo DIFF LATEST WORKING vs JUST GENERATED
diff latest_working_set_of_files generated_file

echo SERIALIZED $TOTAL_SERIALIZED_FIELDS FIELD

#############################################################################################################

# TODO src usa H_filename

    # jmi_real_t *dz_active_variables[1];
## define JMI_ACTIVE_VAR_BUFS_NUM 3
    # jmi_real_t *dz_active_variables_buf[JMI_ACTIVE_VAR_BUFS_NUM];
    # jmp_buf try_location[JMI_MAX_EXCEPTION_DEPTH+1];

# echo field_num=${#FIELDS[@]}
# echo field_num=${#SER_AS[@]}
# echo field_num=${#SIZE_1D[@]}
