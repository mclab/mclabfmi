/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

typedef void (*serObj_free)(void **ptr);
typedef struct MCLabFMIEnv MCLabFMIEnv;

MCLabFMIEnv *MCLabFMIEnv_new();
void MCLabFMIEnv_free(MCLabFMIEnv **thisPtr);
void MCLabFMIEnv_add(MCLabFMIEnv *this_, const char *class_name, void *objPtr,
                     void *serObj, serObj_free free, const void *passport);
void *MCLabFMIEnv_get(MCLabFMIEnv *this_, const char *class_name, void *objPtr);
void MCLabFMIEnv_replace(MCLabFMIEnv *this_, const char *class_name,
                         void *oldObjPtr, void *newObjPtr, void *newSerObj,
                         const void *passport);
