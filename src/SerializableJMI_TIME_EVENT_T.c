/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi_util.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_TIME_EVENT_T.h"

#define DEBUG "SerializableJMI_TIME_EVENT_T"

#define CLASS_NAME "jmi_time_event_t"

// defining constants for fields name
#define FIELD_DEFINED "defined"
#define FIELD_PHASE "phase"
#define FIELD_TIME "time"

struct SerializableJMI_TIME_EVENT_T {
  jmi_time_event_t *jmi_time_event;
  // Adding inner struct
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_TIME_EVENT_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_TIME_EVENT_T *ser_jmi_time_event = (SerializableJMI_TIME_EVENT_T *)this_v;
  return ser_jmi_time_event->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_TIME_EVENT_T *ser_jmi_time_event = (SerializableJMI_TIME_EVENT_T *)this_v;
  jmi_time_event_t *jmi_time_event = ser_jmi_time_event->jmi_time_event;
  const void *passport = ser_jmi_time_event->passport;
  Serializable *serializable = ser_jmi_time_event->serializable;
  MCLabFMILog_verbose("defined: %d", jmi_time_event->defined);
  Serializable_setField(serializable, FIELD_DEFINED, PROPERTIES_INT,
                       &jmi_time_event->defined, passport);
  MCLabFMILog_verbose("phase: %d", jmi_time_event->phase);
  Serializable_setField(serializable, FIELD_PHASE, PROPERTIES_INT,
                       &jmi_time_event->phase, passport);
  MCLabFMILog_verbose("time: %g", jmi_time_event->time);
  Serializable_setField(serializable, FIELD_TIME, PROPERTIES_DOUBLE,
                       &jmi_time_event->time, passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_TIME_EVENT_T *ser_jmi_time_event = (SerializableJMI_TIME_EVENT_T *)this_v;
  jmi_time_event_t *jmi_time_event = ser_jmi_time_event->jmi_time_event;
  const void *passport = ser_jmi_time_event->passport;
  Serializable *serializable = ser_jmi_time_event->serializable;
  // Deserializing simple fields via script
  jmi_time_event->defined =
    *(int *) Serializable_getField(serializable, FIELD_DEFINED, passport);
  MCLabFMILog_verbose("defined: %d", jmi_time_event->defined);
  jmi_time_event->phase =
    *(int *) Serializable_getField(serializable, FIELD_PHASE, passport);
  MCLabFMILog_verbose("phase: %d", jmi_time_event->phase);
  jmi_time_event->time =
    *(double *) Serializable_getField(serializable, FIELD_TIME, passport);
  MCLabFMILog_verbose("time: %g", jmi_time_event->time);
  MCLabFMILog_unnest();
}

SerializableJMI_TIME_EVENT_T *SerializableJMI_TIME_EVENT_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_time_event_t *jmi_time_event) {
  Debug_assert(DEBUG_ALWAYS, jmi_time_event != NULL, "jmi_time_event == NULL");
  SerializableJMI_TIME_EVENT_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_time_event)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_TIME_EVENT_T));
    new->jmi_time_event = jmi_time_event;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_time_event, new,
                    SerializableJMI_TIME_EVENT_T_free, new->passport);
    // Adding inner struct new
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_TIME_EVENT_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_TIME_EVENT_T *this_ = (SerializableJMI_TIME_EVENT_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_TIME_EVENT_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_TIME_EVENT_T *this_ = (SerializableJMI_TIME_EVENT_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
