/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi_callbacks.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_LOG_OPTIONS_T.h"

#define DEBUG "SerializableJMI_LOG_OPTIONS_T"

#define CLASS_NAME "jmi_log_options_t"

// defining constants for fields name
#define FIELD_LOGGING_ON_FLAG "logging_on_flag"
#define FIELD_LOG_LEVEL "log_level"
#define FIELD_COPY_LOG_TO_FILE_FLAG "copy_log_to_file_flag"

struct SerializableJMI_LOG_OPTIONS_T {
  jmi_log_options_t *jmi_log_options;
  // Adding inner struct
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_LOG_OPTIONS_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_LOG_OPTIONS_T *ser_jmi_log_options = (SerializableJMI_LOG_OPTIONS_T *)this_v;
  return ser_jmi_log_options->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_LOG_OPTIONS_T *ser_jmi_log_options = (SerializableJMI_LOG_OPTIONS_T *)this_v;
  jmi_log_options_t *jmi_log_options = ser_jmi_log_options->jmi_log_options;
  const void *passport = ser_jmi_log_options->passport;
  Serializable *serializable = ser_jmi_log_options->serializable;
  MCLabFMILog_verbose("logging_on_flag: %d", jmi_log_options->logging_on_flag);
  Serializable_setField(serializable, FIELD_LOGGING_ON_FLAG, PROPERTIES_BOOLEAN,
                       &jmi_log_options->logging_on_flag, passport);
  MCLabFMILog_verbose("log_level: %d", jmi_log_options->log_level);
  Serializable_setField(serializable, FIELD_LOG_LEVEL, PROPERTIES_INT,
                       &jmi_log_options->log_level, passport);
  MCLabFMILog_verbose("copy_log_to_file_flag: %d", jmi_log_options->copy_log_to_file_flag);
  Serializable_setField(serializable, FIELD_COPY_LOG_TO_FILE_FLAG, PROPERTIES_INT,
                       &jmi_log_options->copy_log_to_file_flag, passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_LOG_OPTIONS_T *ser_jmi_log_options = (SerializableJMI_LOG_OPTIONS_T *)this_v;
  jmi_log_options_t *jmi_log_options = ser_jmi_log_options->jmi_log_options;
  const void *passport = ser_jmi_log_options->passport;
  Serializable *serializable = ser_jmi_log_options->serializable;
  // Deserializing simple fields via script
  jmi_log_options->logging_on_flag =
    *(char *) Serializable_getField(serializable, FIELD_LOGGING_ON_FLAG, passport);
  MCLabFMILog_verbose("logging_on_flag: %c", jmi_log_options->logging_on_flag);
  jmi_log_options->log_level =
    *(int *) Serializable_getField(serializable, FIELD_LOG_LEVEL, passport);
  MCLabFMILog_verbose("log_level: %d", jmi_log_options->log_level);
  jmi_log_options->copy_log_to_file_flag =
    *(int *) Serializable_getField(serializable, FIELD_COPY_LOG_TO_FILE_FLAG, passport);
  MCLabFMILog_verbose("copy_log_to_file_flag: %d", jmi_log_options->copy_log_to_file_flag);
  MCLabFMILog_unnest();
}

SerializableJMI_LOG_OPTIONS_T *SerializableJMI_LOG_OPTIONS_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_log_options_t *jmi_log_options) {
  Debug_assert(DEBUG_ALWAYS, jmi_log_options != NULL, "jmi_log_options == NULL");
  SerializableJMI_LOG_OPTIONS_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_log_options)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_LOG_OPTIONS_T));
    new->jmi_log_options = jmi_log_options;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_log_options, new,
                    SerializableJMI_LOG_OPTIONS_T_free, new->passport);
    // Adding inner struct new
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_LOG_OPTIONS_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_LOG_OPTIONS_T *this_ = (SerializableJMI_LOG_OPTIONS_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_LOG_OPTIONS_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_LOG_OPTIONS_T *this_ = (SerializableJMI_LOG_OPTIONS_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
