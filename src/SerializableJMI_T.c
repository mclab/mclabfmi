/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "jmi_dynamic_state.h"
#include "jmi_delay_impl.h"
#include "jmi_block_residual.h"
#include "jmi_block_solver.h"
#include "jmi_block_solver_impl.h"
#include "SerializableJMI_CALLBACKS_T.h"
#include "SerializableJMI_BLOCK_RESIDUAL_T.h"
#include "SerializableJMI_BLOCK_RESIDUAL_T.h"
#include "SerializableJMI_DELAY_T.h"
#include "SerializableJMI_SPATIALDIST_T.h"
#include "SerializableJMI_DYNAMIC_STATE_SET_T.h"
#include "SerializableJMI_TIME_EVENT_T.h"
#include "SerializableJMI_OPTIONS_T.h"
#include "SerializableJMI_CHATTERING_T.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_T.h"

#define DEBUG "SerializableJMI_T"

#define CLASS_NAME "jmi_t"

// defining constants for fields name
#define FIELD_JMI_CALLBACKS "jmi_callbacks"




#define FIELD_N_REAL_CI "n_real_ci"
#define FIELD_N_REAL_CD "n_real_cd"
#define FIELD_N_REAL_PI "n_real_pi"
#define FIELD_N_REAL_PD "n_real_pd"
#define FIELD_N_INTEGER_CI "n_integer_ci"
#define FIELD_N_INTEGER_CD "n_integer_cd"
#define FIELD_N_INTEGER_PI "n_integer_pi"
#define FIELD_N_INTEGER_PD "n_integer_pd"
#define FIELD_N_BOOLEAN_CI "n_boolean_ci"
#define FIELD_N_BOOLEAN_CD "n_boolean_cd"
#define FIELD_N_BOOLEAN_PI "n_boolean_pi"
#define FIELD_N_BOOLEAN_PD "n_boolean_pd"
#define FIELD_N_REAL_DX "n_real_dx"
#define FIELD_N_REAL_X "n_real_x"
#define FIELD_N_REAL_U "n_real_u"
#define FIELD_N_REAL_W "n_real_w"
#define FIELD_N_REAL_D "n_real_d"
#define FIELD_N_INTEGER_D "n_integer_d"
#define FIELD_N_INTEGER_U "n_integer_u"
#define FIELD_N_BOOLEAN_D "n_boolean_d"
#define FIELD_N_BOOLEAN_U "n_boolean_u"
#define FIELD_N_SW "n_sw"
#define FIELD_N_SW_INIT "n_sw_init"
#define FIELD_N_TIME_SW "n_time_sw"
#define FIELD_N_STATE_SW "n_state_sw"
#define FIELD_N_GUARDS "n_guards"
#define FIELD_N_GUARDS_INIT "n_guards_init"
#define FIELD_N_V "n_v"
#define FIELD_N_Z "n_z"
#define FIELD_N_DAE_BLOCKS "n_dae_blocks"
#define FIELD_N_DAE_INIT_BLOCKS "n_dae_init_blocks"
#define FIELD_N_DELAYS "n_delays"
#define FIELD_N_SPATIALDISTS "n_spatialdists"
#define FIELD_OFFS_REAL_CI "offs_real_ci"
#define FIELD_OFFS_REAL_CD "offs_real_cd"
#define FIELD_OFFS_REAL_PI "offs_real_pi"
#define FIELD_OFFS_REAL_PI_S "offs_real_pi_s"
#define FIELD_OFFS_REAL_PI_F "offs_real_pi_f"
#define FIELD_OFFS_REAL_PI_E "offs_real_pi_e"
#define FIELD_OFFS_REAL_PD "offs_real_pd"
#define FIELD_OFFS_INTEGER_CI "offs_integer_ci"
#define FIELD_OFFS_INTEGER_CD "offs_integer_cd"
#define FIELD_OFFS_INTEGER_PI "offs_integer_pi"
#define FIELD_OFFS_INTEGER_PI_S "offs_integer_pi_s"
#define FIELD_OFFS_INTEGER_PI_F "offs_integer_pi_f"
#define FIELD_OFFS_INTEGER_PI_E "offs_integer_pi_e"
#define FIELD_OFFS_INTEGER_PD "offs_integer_pd"
#define FIELD_OFFS_BOOLEAN_CI "offs_boolean_ci"
#define FIELD_OFFS_BOOLEAN_CD "offs_boolean_cd"
#define FIELD_OFFS_BOOLEAN_PI "offs_boolean_pi"
#define FIELD_OFFS_BOOLEAN_PI_S "offs_boolean_pi_s"
#define FIELD_OFFS_BOOLEAN_PI_F "offs_boolean_pi_f"
#define FIELD_OFFS_BOOLEAN_PI_E "offs_boolean_pi_e"
#define FIELD_OFFS_BOOLEAN_PD "offs_boolean_pd"
#define FIELD_OFFS_REAL_DX "offs_real_dx"
#define FIELD_OFFS_REAL_X "offs_real_x"
#define FIELD_OFFS_REAL_U "offs_real_u"
#define FIELD_OFFS_REAL_W "offs_real_w"
#define FIELD_OFFS_T "offs_t"
#define FIELD_OFFS_HOMOTOPY_LAMBDA "offs_homotopy_lambda"
#define FIELD_OFFS_REAL_D "offs_real_d"
#define FIELD_OFFS_INTEGER_D "offs_integer_d"
#define FIELD_OFFS_INTEGER_U "offs_integer_u"
#define FIELD_OFFS_BOOLEAN_D "offs_boolean_d"
#define FIELD_OFFS_BOOLEAN_U "offs_boolean_u"
#define FIELD_OFFS_SW "offs_sw"
#define FIELD_OFFS_SW_INIT "offs_sw_init"
#define FIELD_OFFS_STATE_SW "offs_state_sw"
#define FIELD_OFFS_TIME_SW "offs_time_sw"
#define FIELD_OFFS_GUARDS "offs_guards"
#define FIELD_OFFS_GUARDS_INIT "offs_guards_init"
#define FIELD_OFFS_PRE_REAL_DX "offs_pre_real_dx"
#define FIELD_OFFS_PRE_REAL_X "offs_pre_real_x"
#define FIELD_OFFS_PRE_REAL_U "offs_pre_real_u"
#define FIELD_OFFS_PRE_REAL_W "offs_pre_real_w"
#define FIELD_OFFS_PRE_REAL_D "offs_pre_real_d"
#define FIELD_OFFS_PRE_INTEGER_D "offs_pre_integer_d"
#define FIELD_OFFS_PRE_INTEGER_U "offs_pre_integer_u"
#define FIELD_OFFS_PRE_BOOLEAN_D "offs_pre_boolean_d"
#define FIELD_OFFS_PRE_BOOLEAN_U "offs_pre_boolean_u"
#define FIELD_OFFS_PRE_SW "offs_pre_sw"
#define FIELD_OFFS_PRE_SW_INIT "offs_pre_sw_init"
#define FIELD_OFFS_PRE_GUARDS "offs_pre_guards"
#define FIELD_OFFS_PRE_GUARDS_INIT "offs_pre_guards_init"
#define FIELD_Z "z"
#define FIELD_Z_LAST "z_last"
#define FIELD_DZ "dz"
#define FIELD_DZ_ACTIVE_INDEX "dz_active_index"
#define FIELD_BLOCK_LEVEL "block_level"

#define FIELD_DZ_ACTIVE_VARIABLES_BUF "dz_active_variables_buf"

#define FIELD_NOMINALS "nominals"
#define FIELD_VARIABLE_SCALING_FACTORS "variable_scaling_factors"
#define FIELD_SCALING_METHOD "scaling_method"
#define FIELD_DAE_BLOCK_RESIDUALS "dae_block_residuals"
#define FIELD_DAE_INIT_BLOCK_RESIDUALS "dae_init_block_residuals"
#define FIELD_CACHED_BLOCK_JACOBIANS "cached_block_jacobians"
#define FIELD_DELAYS "delays"
#define FIELD_SPATIALDISTS "spatialdists"
#define FIELD_DELAY_EVENT_MODE "delay_event_mode"
#define FIELD_DYNAMIC_STATE_SETS "dynamic_state_sets"
#define FIELD_N_DYNAMIC_STATE_SETS "n_dynamic_state_sets"
#define FIELD_N_INITIAL_RELATIONS "n_initial_relations"
#define FIELD_INITIAL_RELATIONS "initial_relations"
#define FIELD_N_RELATIONS "n_relations"
#define FIELD_RELATIONS "relations"
#define FIELD_ATEVENT "atEvent"
#define FIELD_ATINITIAL "atInitial"
#define FIELD_ATTIMEEVENT "atTimeEvent"
#define FIELD_EVENTPHASE "eventPhase"
#define FIELD_SAVE_RESTORE_SOLVER_STATE_MODE "save_restore_solver_state_mode"
#define FIELD_NEXTTIMEEVENT "nextTimeEvent"
#define FIELD_IS_INITIALIZED "is_initialized"
#define FIELD_NBR_EVENT_ITER "nbr_event_iter"
#define FIELD_NBR_CONSEC_TIME_EVENTS "nbr_consec_time_events"
#define FIELD_LOG "log"//TO_BE_SERIALIZED
#define FIELD_OPTIONS "options"
#define FIELD_EVENTS_EPSILON "events_epsilon"
#define FIELD_TMP_EVENTS_EPSILON "tmp_events_epsilon"
#define FIELD_NEWTON_TOLERANCE "newton_tolerance"
#define FIELD_RECOMPUTEVARIABLES "recomputeVariables"
#define FIELD_UPDATED_STATES "updated_states"
#define FIELD_REAL_X_WORK "real_x_work"
#define FIELD_REAL_U_WORK "real_u_work"
#define FIELD_TRY_LOCATION "try_location"
#define FIELD_CURRENT_TRY_DEPTH "current_try_depth"
#define FIELD_MODEL_TERMINATE "model_terminate"
#define FIELD_USER_TERMINATE "user_terminate"
#define FIELD_REINIT_TRIGGERED "reinit_triggered"
#define FIELD_RESOURCE_LOCATION "resource_location"
#define FIELD_RESOURCE_LOCATION_VERIFIED "resource_location_verified"
#define FIELD_MODULES "modules"//TO_BE_SERIALIZED
#define FIELD_CHATTERING "chattering"




struct SerializableJMI_T {
  jmi_t *jmi;
  // Adding inner struct
  SerializableJMI_CALLBACKS_T *ser_jmi_callbacks;
  SerializableJMI_BLOCK_RESIDUAL_T **ser_dae_block_residuals;
  SerializableJMI_BLOCK_RESIDUAL_T **ser_dae_init_block_residuals;
  SerializableJMI_DELAY_T **ser_delays;
  SerializableJMI_SPATIALDIST_T **ser_spatialdists;
  SerializableJMI_DYNAMIC_STATE_SET_T **ser_dynamic_state_sets;
  SerializableJMI_TIME_EVENT_T *ser_nextTimeEvent;
  SerializableJMI_OPTIONS_T *ser_options;
  SerializableJMI_CHATTERING_T *ser_chattering;
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_T *ser_jmi = (SerializableJMI_T *)this_v;
  return ser_jmi->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_T *ser_jmi = (SerializableJMI_T *)this_v;
  jmi_t *jmi = ser_jmi->jmi;
  const void *passport = ser_jmi->passport;
  Serializable *serializable = ser_jmi->serializable;
  Serializable_setField(serializable, FIELD_JMI_CALLBACKS, PROPERTIES_POINTER,
                       SerializableJMI_CALLBACKS_T_asSerializable(ser_jmi->ser_jmi_callbacks), passport);
  //TO_BE_SERIALIZED FIELD_MODEL as type jmi_model_t*
  //TO_BE_SERIALIZED FIELD_INIT_DELAY as type jmi_generic_func_t
  //TO_BE_SERIALIZED FIELD_SAMPLE_DELAY as type jmi_generic_func_t
  MCLabFMILog_warning("Serialization of z_t is not implemented yet.");
  MCLabFMILog_verbose("n_real_ci: %d", jmi->n_real_ci);
  Serializable_setField(serializable, FIELD_N_REAL_CI, PROPERTIES_INT,
                       &jmi->n_real_ci, passport);
  MCLabFMILog_verbose("n_real_cd: %d", jmi->n_real_cd);
  Serializable_setField(serializable, FIELD_N_REAL_CD, PROPERTIES_INT,
                       &jmi->n_real_cd, passport);
  MCLabFMILog_verbose("n_real_pi: %d", jmi->n_real_pi);
  Serializable_setField(serializable, FIELD_N_REAL_PI, PROPERTIES_INT,
                       &jmi->n_real_pi, passport);
  MCLabFMILog_verbose("n_real_pd: %d", jmi->n_real_pd);
  Serializable_setField(serializable, FIELD_N_REAL_PD, PROPERTIES_INT,
                       &jmi->n_real_pd, passport);
  MCLabFMILog_verbose("n_integer_ci: %d", jmi->n_integer_ci);
  Serializable_setField(serializable, FIELD_N_INTEGER_CI, PROPERTIES_INT,
                       &jmi->n_integer_ci, passport);
  MCLabFMILog_verbose("n_integer_cd: %d", jmi->n_integer_cd);
  Serializable_setField(serializable, FIELD_N_INTEGER_CD, PROPERTIES_INT,
                       &jmi->n_integer_cd, passport);
  MCLabFMILog_verbose("n_integer_pi: %d", jmi->n_integer_pi);
  Serializable_setField(serializable, FIELD_N_INTEGER_PI, PROPERTIES_INT,
                       &jmi->n_integer_pi, passport);
  MCLabFMILog_verbose("n_integer_pd: %d", jmi->n_integer_pd);
  Serializable_setField(serializable, FIELD_N_INTEGER_PD, PROPERTIES_INT,
                       &jmi->n_integer_pd, passport);
  MCLabFMILog_verbose("n_boolean_ci: %d", jmi->n_boolean_ci);
  Serializable_setField(serializable, FIELD_N_BOOLEAN_CI, PROPERTIES_INT,
                       &jmi->n_boolean_ci, passport);
  MCLabFMILog_verbose("n_boolean_cd: %d", jmi->n_boolean_cd);
  Serializable_setField(serializable, FIELD_N_BOOLEAN_CD, PROPERTIES_INT,
                       &jmi->n_boolean_cd, passport);
  MCLabFMILog_verbose("n_boolean_pi: %d", jmi->n_boolean_pi);
  Serializable_setField(serializable, FIELD_N_BOOLEAN_PI, PROPERTIES_INT,
                       &jmi->n_boolean_pi, passport);
  MCLabFMILog_verbose("n_boolean_pd: %d", jmi->n_boolean_pd);
  Serializable_setField(serializable, FIELD_N_BOOLEAN_PD, PROPERTIES_INT,
                       &jmi->n_boolean_pd, passport);
  MCLabFMILog_verbose("n_real_dx: %d", jmi->n_real_dx);
  Serializable_setField(serializable, FIELD_N_REAL_DX, PROPERTIES_INT,
                       &jmi->n_real_dx, passport);
  MCLabFMILog_verbose("n_real_x: %d", jmi->n_real_x);
  Serializable_setField(serializable, FIELD_N_REAL_X, PROPERTIES_INT,
                       &jmi->n_real_x, passport);
  MCLabFMILog_verbose("n_real_u: %d", jmi->n_real_u);
  Serializable_setField(serializable, FIELD_N_REAL_U, PROPERTIES_INT,
                       &jmi->n_real_u, passport);
  MCLabFMILog_verbose("n_real_w: %d", jmi->n_real_w);
  Serializable_setField(serializable, FIELD_N_REAL_W, PROPERTIES_INT,
                       &jmi->n_real_w, passport);
  MCLabFMILog_verbose("n_real_d: %d", jmi->n_real_d);
  Serializable_setField(serializable, FIELD_N_REAL_D, PROPERTIES_INT,
                       &jmi->n_real_d, passport);
  MCLabFMILog_verbose("n_integer_d: %d", jmi->n_integer_d);
  Serializable_setField(serializable, FIELD_N_INTEGER_D, PROPERTIES_INT,
                       &jmi->n_integer_d, passport);
  MCLabFMILog_verbose("n_integer_u: %d", jmi->n_integer_u);
  Serializable_setField(serializable, FIELD_N_INTEGER_U, PROPERTIES_INT,
                       &jmi->n_integer_u, passport);
  MCLabFMILog_verbose("n_boolean_d: %d", jmi->n_boolean_d);
  Serializable_setField(serializable, FIELD_N_BOOLEAN_D, PROPERTIES_INT,
                       &jmi->n_boolean_d, passport);
  MCLabFMILog_verbose("n_boolean_u: %d", jmi->n_boolean_u);
  Serializable_setField(serializable, FIELD_N_BOOLEAN_U, PROPERTIES_INT,
                       &jmi->n_boolean_u, passport);
  MCLabFMILog_verbose("n_sw: %d", jmi->n_sw);
  Serializable_setField(serializable, FIELD_N_SW, PROPERTIES_INT,
                       &jmi->n_sw, passport);
  MCLabFMILog_verbose("n_sw_init: %d", jmi->n_sw_init);
  Serializable_setField(serializable, FIELD_N_SW_INIT, PROPERTIES_INT,
                       &jmi->n_sw_init, passport);
  MCLabFMILog_verbose("n_time_sw: %d", jmi->n_time_sw);
  Serializable_setField(serializable, FIELD_N_TIME_SW, PROPERTIES_INT,
                       &jmi->n_time_sw, passport);
  MCLabFMILog_verbose("n_state_sw: %d", jmi->n_state_sw);
  Serializable_setField(serializable, FIELD_N_STATE_SW, PROPERTIES_INT,
                       &jmi->n_state_sw, passport);
  MCLabFMILog_verbose("n_guards: %d", jmi->n_guards);
  Serializable_setField(serializable, FIELD_N_GUARDS, PROPERTIES_INT,
                       &jmi->n_guards, passport);
  MCLabFMILog_verbose("n_guards_init: %d", jmi->n_guards_init);
  Serializable_setField(serializable, FIELD_N_GUARDS_INIT, PROPERTIES_INT,
                       &jmi->n_guards_init, passport);
  MCLabFMILog_verbose("n_v: %d", jmi->n_v);
  Serializable_setField(serializable, FIELD_N_V, PROPERTIES_INT,
                       &jmi->n_v, passport);
  MCLabFMILog_verbose("n_z: %d", jmi->n_z);
  Serializable_setField(serializable, FIELD_N_Z, PROPERTIES_INT,
                       &jmi->n_z, passport);
  MCLabFMILog_verbose("n_dae_blocks: %d", jmi->n_dae_blocks);
  Serializable_setField(serializable, FIELD_N_DAE_BLOCKS, PROPERTIES_INT,
                       &jmi->n_dae_blocks, passport);
  MCLabFMILog_verbose("n_dae_init_blocks: %d", jmi->n_dae_init_blocks);
  Serializable_setField(serializable, FIELD_N_DAE_INIT_BLOCKS, PROPERTIES_INT,
                       &jmi->n_dae_init_blocks, passport);
  MCLabFMILog_verbose("n_delays: %d", jmi->n_delays);
  Serializable_setField(serializable, FIELD_N_DELAYS, PROPERTIES_INT,
                       &jmi->n_delays, passport);
  MCLabFMILog_verbose("n_spatialdists: %d", jmi->n_spatialdists);
  Serializable_setField(serializable, FIELD_N_SPATIALDISTS, PROPERTIES_INT,
                       &jmi->n_spatialdists, passport);
  MCLabFMILog_verbose("offs_real_ci: %d", jmi->offs_real_ci);
  Serializable_setField(serializable, FIELD_OFFS_REAL_CI, PROPERTIES_INT,
                       &jmi->offs_real_ci, passport);
  MCLabFMILog_verbose("offs_real_cd: %d", jmi->offs_real_cd);
  Serializable_setField(serializable, FIELD_OFFS_REAL_CD, PROPERTIES_INT,
                       &jmi->offs_real_cd, passport);
  MCLabFMILog_verbose("offs_real_pi: %d", jmi->offs_real_pi);
  Serializable_setField(serializable, FIELD_OFFS_REAL_PI, PROPERTIES_INT,
                       &jmi->offs_real_pi, passport);
  MCLabFMILog_verbose("offs_real_pi_s: %d", jmi->offs_real_pi_s);
  Serializable_setField(serializable, FIELD_OFFS_REAL_PI_S, PROPERTIES_INT,
                       &jmi->offs_real_pi_s, passport);
  MCLabFMILog_verbose("offs_real_pi_f: %d", jmi->offs_real_pi_f);
  Serializable_setField(serializable, FIELD_OFFS_REAL_PI_F, PROPERTIES_INT,
                       &jmi->offs_real_pi_f, passport);
  MCLabFMILog_verbose("offs_real_pi_e: %d", jmi->offs_real_pi_e);
  Serializable_setField(serializable, FIELD_OFFS_REAL_PI_E, PROPERTIES_INT,
                       &jmi->offs_real_pi_e, passport);
  MCLabFMILog_verbose("offs_real_pd: %d", jmi->offs_real_pd);
  Serializable_setField(serializable, FIELD_OFFS_REAL_PD, PROPERTIES_INT,
                       &jmi->offs_real_pd, passport);
  MCLabFMILog_verbose("offs_integer_ci: %d", jmi->offs_integer_ci);
  Serializable_setField(serializable, FIELD_OFFS_INTEGER_CI, PROPERTIES_INT,
                       &jmi->offs_integer_ci, passport);
  MCLabFMILog_verbose("offs_integer_cd: %d", jmi->offs_integer_cd);
  Serializable_setField(serializable, FIELD_OFFS_INTEGER_CD, PROPERTIES_INT,
                       &jmi->offs_integer_cd, passport);
  MCLabFMILog_verbose("offs_integer_pi: %d", jmi->offs_integer_pi);
  Serializable_setField(serializable, FIELD_OFFS_INTEGER_PI, PROPERTIES_INT,
                       &jmi->offs_integer_pi, passport);
  MCLabFMILog_verbose("offs_integer_pi_s: %d", jmi->offs_integer_pi_s);
  Serializable_setField(serializable, FIELD_OFFS_INTEGER_PI_S, PROPERTIES_INT,
                       &jmi->offs_integer_pi_s, passport);
  MCLabFMILog_verbose("offs_integer_pi_f: %d", jmi->offs_integer_pi_f);
  Serializable_setField(serializable, FIELD_OFFS_INTEGER_PI_F, PROPERTIES_INT,
                       &jmi->offs_integer_pi_f, passport);
  MCLabFMILog_verbose("offs_integer_pi_e: %d", jmi->offs_integer_pi_e);
  Serializable_setField(serializable, FIELD_OFFS_INTEGER_PI_E, PROPERTIES_INT,
                       &jmi->offs_integer_pi_e, passport);
  MCLabFMILog_verbose("offs_integer_pd: %d", jmi->offs_integer_pd);
  Serializable_setField(serializable, FIELD_OFFS_INTEGER_PD, PROPERTIES_INT,
                       &jmi->offs_integer_pd, passport);
  MCLabFMILog_verbose("offs_boolean_ci: %d", jmi->offs_boolean_ci);
  Serializable_setField(serializable, FIELD_OFFS_BOOLEAN_CI, PROPERTIES_INT,
                       &jmi->offs_boolean_ci, passport);
  MCLabFMILog_verbose("offs_boolean_cd: %d", jmi->offs_boolean_cd);
  Serializable_setField(serializable, FIELD_OFFS_BOOLEAN_CD, PROPERTIES_INT,
                       &jmi->offs_boolean_cd, passport);
  MCLabFMILog_verbose("offs_boolean_pi: %d", jmi->offs_boolean_pi);
  Serializable_setField(serializable, FIELD_OFFS_BOOLEAN_PI, PROPERTIES_INT,
                       &jmi->offs_boolean_pi, passport);
  MCLabFMILog_verbose("offs_boolean_pi_s: %d", jmi->offs_boolean_pi_s);
  Serializable_setField(serializable, FIELD_OFFS_BOOLEAN_PI_S, PROPERTIES_INT,
                       &jmi->offs_boolean_pi_s, passport);
  MCLabFMILog_verbose("offs_boolean_pi_f: %d", jmi->offs_boolean_pi_f);
  Serializable_setField(serializable, FIELD_OFFS_BOOLEAN_PI_F, PROPERTIES_INT,
                       &jmi->offs_boolean_pi_f, passport);
  MCLabFMILog_verbose("offs_boolean_pi_e: %d", jmi->offs_boolean_pi_e);
  Serializable_setField(serializable, FIELD_OFFS_BOOLEAN_PI_E, PROPERTIES_INT,
                       &jmi->offs_boolean_pi_e, passport);
  MCLabFMILog_verbose("offs_boolean_pd: %d", jmi->offs_boolean_pd);
  Serializable_setField(serializable, FIELD_OFFS_BOOLEAN_PD, PROPERTIES_INT,
                       &jmi->offs_boolean_pd, passport);
  MCLabFMILog_verbose("offs_real_dx: %d", jmi->offs_real_dx);
  Serializable_setField(serializable, FIELD_OFFS_REAL_DX, PROPERTIES_INT,
                       &jmi->offs_real_dx, passport);
  MCLabFMILog_verbose("offs_real_x: %d", jmi->offs_real_x);
  Serializable_setField(serializable, FIELD_OFFS_REAL_X, PROPERTIES_INT,
                       &jmi->offs_real_x, passport);
  MCLabFMILog_verbose("offs_real_u: %d", jmi->offs_real_u);
  Serializable_setField(serializable, FIELD_OFFS_REAL_U, PROPERTIES_INT,
                       &jmi->offs_real_u, passport);
  MCLabFMILog_verbose("offs_real_w: %d", jmi->offs_real_w);
  Serializable_setField(serializable, FIELD_OFFS_REAL_W, PROPERTIES_INT,
                       &jmi->offs_real_w, passport);
  MCLabFMILog_verbose("offs_t: %d", jmi->offs_t);
  Serializable_setField(serializable, FIELD_OFFS_T, PROPERTIES_INT,
                       &jmi->offs_t, passport);
  MCLabFMILog_verbose("offs_homotopy_lambda: %d", jmi->offs_homotopy_lambda);
  Serializable_setField(serializable, FIELD_OFFS_HOMOTOPY_LAMBDA, PROPERTIES_INT,
                       &jmi->offs_homotopy_lambda, passport);
  MCLabFMILog_verbose("offs_real_d: %d", jmi->offs_real_d);
  Serializable_setField(serializable, FIELD_OFFS_REAL_D, PROPERTIES_INT,
                       &jmi->offs_real_d, passport);
  MCLabFMILog_verbose("offs_integer_d: %d", jmi->offs_integer_d);
  Serializable_setField(serializable, FIELD_OFFS_INTEGER_D, PROPERTIES_INT,
                       &jmi->offs_integer_d, passport);
  MCLabFMILog_verbose("offs_integer_u: %d", jmi->offs_integer_u);
  Serializable_setField(serializable, FIELD_OFFS_INTEGER_U, PROPERTIES_INT,
                       &jmi->offs_integer_u, passport);
  MCLabFMILog_verbose("offs_boolean_d: %d", jmi->offs_boolean_d);
  Serializable_setField(serializable, FIELD_OFFS_BOOLEAN_D, PROPERTIES_INT,
                       &jmi->offs_boolean_d, passport);
  MCLabFMILog_verbose("offs_boolean_u: %d", jmi->offs_boolean_u);
  Serializable_setField(serializable, FIELD_OFFS_BOOLEAN_U, PROPERTIES_INT,
                       &jmi->offs_boolean_u, passport);
  MCLabFMILog_verbose("offs_sw: %d", jmi->offs_sw);
  Serializable_setField(serializable, FIELD_OFFS_SW, PROPERTIES_INT,
                       &jmi->offs_sw, passport);
  MCLabFMILog_verbose("offs_sw_init: %d", jmi->offs_sw_init);
  Serializable_setField(serializable, FIELD_OFFS_SW_INIT, PROPERTIES_INT,
                       &jmi->offs_sw_init, passport);
  MCLabFMILog_verbose("offs_state_sw: %d", jmi->offs_state_sw);
  Serializable_setField(serializable, FIELD_OFFS_STATE_SW, PROPERTIES_INT,
                       &jmi->offs_state_sw, passport);
  MCLabFMILog_verbose("offs_time_sw: %d", jmi->offs_time_sw);
  Serializable_setField(serializable, FIELD_OFFS_TIME_SW, PROPERTIES_INT,
                       &jmi->offs_time_sw, passport);
  MCLabFMILog_verbose("offs_guards: %d", jmi->offs_guards);
  Serializable_setField(serializable, FIELD_OFFS_GUARDS, PROPERTIES_INT,
                       &jmi->offs_guards, passport);
  MCLabFMILog_verbose("offs_guards_init: %d", jmi->offs_guards_init);
  Serializable_setField(serializable, FIELD_OFFS_GUARDS_INIT, PROPERTIES_INT,
                       &jmi->offs_guards_init, passport);
  MCLabFMILog_verbose("offs_pre_real_dx: %d", jmi->offs_pre_real_dx);
  Serializable_setField(serializable, FIELD_OFFS_PRE_REAL_DX, PROPERTIES_INT,
                       &jmi->offs_pre_real_dx, passport);
  MCLabFMILog_verbose("offs_pre_real_x: %d", jmi->offs_pre_real_x);
  Serializable_setField(serializable, FIELD_OFFS_PRE_REAL_X, PROPERTIES_INT,
                       &jmi->offs_pre_real_x, passport);
  MCLabFMILog_verbose("offs_pre_real_u: %d", jmi->offs_pre_real_u);
  Serializable_setField(serializable, FIELD_OFFS_PRE_REAL_U, PROPERTIES_INT,
                       &jmi->offs_pre_real_u, passport);
  MCLabFMILog_verbose("offs_pre_real_w: %d", jmi->offs_pre_real_w);
  Serializable_setField(serializable, FIELD_OFFS_PRE_REAL_W, PROPERTIES_INT,
                       &jmi->offs_pre_real_w, passport);
  MCLabFMILog_verbose("offs_pre_real_d: %d", jmi->offs_pre_real_d);
  Serializable_setField(serializable, FIELD_OFFS_PRE_REAL_D, PROPERTIES_INT,
                       &jmi->offs_pre_real_d, passport);
  MCLabFMILog_verbose("offs_pre_integer_d: %d", jmi->offs_pre_integer_d);
  Serializable_setField(serializable, FIELD_OFFS_PRE_INTEGER_D, PROPERTIES_INT,
                       &jmi->offs_pre_integer_d, passport);
  MCLabFMILog_verbose("offs_pre_integer_u: %d", jmi->offs_pre_integer_u);
  Serializable_setField(serializable, FIELD_OFFS_PRE_INTEGER_U, PROPERTIES_INT,
                       &jmi->offs_pre_integer_u, passport);
  MCLabFMILog_verbose("offs_pre_boolean_d: %d", jmi->offs_pre_boolean_d);
  Serializable_setField(serializable, FIELD_OFFS_PRE_BOOLEAN_D, PROPERTIES_INT,
                       &jmi->offs_pre_boolean_d, passport);
  MCLabFMILog_verbose("offs_pre_boolean_u: %d", jmi->offs_pre_boolean_u);
  Serializable_setField(serializable, FIELD_OFFS_PRE_BOOLEAN_U, PROPERTIES_INT,
                       &jmi->offs_pre_boolean_u, passport);
  MCLabFMILog_verbose("offs_pre_sw: %d", jmi->offs_pre_sw);
  Serializable_setField(serializable, FIELD_OFFS_PRE_SW, PROPERTIES_INT,
                       &jmi->offs_pre_sw, passport);
  MCLabFMILog_verbose("offs_pre_sw_init: %d", jmi->offs_pre_sw_init);
  Serializable_setField(serializable, FIELD_OFFS_PRE_SW_INIT, PROPERTIES_INT,
                       &jmi->offs_pre_sw_init, passport);
  MCLabFMILog_verbose("offs_pre_guards: %d", jmi->offs_pre_guards);
  Serializable_setField(serializable, FIELD_OFFS_PRE_GUARDS, PROPERTIES_INT,
                       &jmi->offs_pre_guards, passport);
  MCLabFMILog_verbose("offs_pre_guards_init: %d", jmi->offs_pre_guards_init);
  Serializable_setField(serializable, FIELD_OFFS_PRE_GUARDS_INIT, PROPERTIES_INT,
                       &jmi->offs_pre_guards_init, passport);
  MCLabFMILog_verbose("z[%d][%d]", 1, jmi->n_z);
  size_t z_sizes[2];
  z_sizes[0] = 1;
  z_sizes[1] = (size_t) jmi->n_z;
  Serializable_setArrayField(serializable, FIELD_Z, PROPERTIES_DOUBLE,
  	                        (void **)jmi->z, sizeof(double), z_sizes,
  	                        NULL, 1, false, NULL, passport);
  MCLabFMILog_verbose("z_last[%d][%d]", 1, jmi->n_z);
  size_t z_last_sizes[2];
  z_last_sizes[0] = 1;
  z_last_sizes[1] = (size_t) jmi->n_z;
  Serializable_setArrayField(serializable, FIELD_Z_LAST, PROPERTIES_DOUBLE,
  	                        (void **)jmi->z_last, sizeof(double), z_last_sizes,
  	                        NULL, 1, false, NULL, passport);
  MCLabFMILog_verbose("dz[%d][%d]", 1, jmi->n_v);
  size_t dz_sizes[2];
  dz_sizes[0] = 1;
  dz_sizes[1] = (size_t) jmi->n_v;
  Serializable_setArrayField(serializable, FIELD_DZ, PROPERTIES_DOUBLE,
  	                        (void **)jmi->dz, sizeof(double), dz_sizes,
  	                        NULL, 1, false, NULL, passport);
  MCLabFMILog_verbose("dz_active_index: %d", jmi->dz_active_index);
  Serializable_setField(serializable, FIELD_DZ_ACTIVE_INDEX, PROPERTIES_INT,
                       &jmi->dz_active_index, passport);
  MCLabFMILog_verbose("block_level: %d", jmi->block_level);
  Serializable_setField(serializable, FIELD_BLOCK_LEVEL, PROPERTIES_INT,
                       &jmi->block_level, passport);
  MCLabFMILog_warning("Serialization of dz_active_variables is not implemented yet.");
  MCLabFMILog_verbose("dz_active_variables_buf[%d][%d]", 3, jmi->n_v);
  size_t dz_active_variables_buf_sizes[2];
  dz_active_variables_buf_sizes[0] = 3;
  dz_active_variables_buf_sizes[1] = (size_t) jmi->n_v;
  Serializable_setArrayField(serializable, FIELD_DZ_ACTIVE_VARIABLES_BUF, PROPERTIES_DOUBLE,
  	                        (void **)jmi->dz_active_variables_buf, sizeof(double), dz_active_variables_buf_sizes,
  	                        NULL, 1, false, NULL, passport);
  MCLabFMILog_warning("Serialization of ext_objs is not implemented yet.");
  MCLabFMILog_verbose("nominals[%d]", jmi->n_real_x);
  size_t nominals_size_t = (size_t) jmi->n_real_x;
  Serializable_setArrayField(serializable, FIELD_NOMINALS, PROPERTIES_DOUBLE,
  	                        (void **)jmi->nominals, sizeof(double), &nominals_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("variable_scaling_factors[%d]", jmi->n_z);
  size_t variable_scaling_factors_size_t = (size_t) jmi->n_z;
  Serializable_setArrayField(serializable, FIELD_VARIABLE_SCALING_FACTORS, PROPERTIES_DOUBLE,
  	                        (void **)jmi->variable_scaling_factors, sizeof(double), &variable_scaling_factors_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("scaling_method: %d", jmi->scaling_method);
  Serializable_setField(serializable, FIELD_SCALING_METHOD, PROPERTIES_INT,
                       &jmi->scaling_method, passport);
  MCLabFMILog_verbose("dae_block_residuals[%d]", jmi->n_dae_blocks);
  size_t dae_block_residuals_size = (size_t) jmi->n_dae_blocks;
  Serializable_setArrayField(serializable, FIELD_DAE_BLOCK_RESIDUALS, PROPERTIES_POINTER, (void *)ser_jmi->ser_dae_block_residuals,
  	                        sizeof(SerializableJMI_BLOCK_RESIDUAL_T*), &dae_block_residuals_size,
  	                        SerializableJMI_BLOCK_RESIDUAL_T_asSerializable, 0, false, NULL, passport);
  MCLabFMILog_verbose("dae_init_block_residuals[%d]", jmi->n_dae_init_blocks);
  size_t dae_init_block_residuals_size = (size_t) jmi->n_dae_init_blocks;
  Serializable_setArrayField(serializable, FIELD_DAE_INIT_BLOCK_RESIDUALS, PROPERTIES_POINTER, (void *)ser_jmi->ser_dae_init_block_residuals,
  	                        sizeof(SerializableJMI_BLOCK_RESIDUAL_T*), &dae_init_block_residuals_size,
  	                        SerializableJMI_BLOCK_RESIDUAL_T_asSerializable, 0, false, NULL, passport);
  MCLabFMILog_verbose("cached_block_jacobians: %d", jmi->cached_block_jacobians);
  Serializable_setField(serializable, FIELD_CACHED_BLOCK_JACOBIANS, PROPERTIES_INT,
                       &jmi->cached_block_jacobians, passport);
  MCLabFMILog_verbose("delays[%d]", jmi->n_delays);
  size_t delays_size = (size_t) jmi->n_delays;
  Serializable_setArrayField(serializable, FIELD_DELAYS, PROPERTIES_POINTER, (void *)ser_jmi->ser_delays,
  	                        sizeof(SerializableJMI_DELAY_T*), &delays_size,
  	                        SerializableJMI_DELAY_T_asSerializable, 0, false, NULL, passport);
  MCLabFMILog_verbose("spatialdists[%d]", jmi->n_spatialdists);
  size_t spatialdists_size = (size_t) jmi->n_spatialdists;
  Serializable_setArrayField(serializable, FIELD_SPATIALDISTS, PROPERTIES_POINTER, (void *)ser_jmi->ser_spatialdists,
  	                        sizeof(SerializableJMI_SPATIALDIST_T*), &spatialdists_size,
  	                        SerializableJMI_SPATIALDIST_T_asSerializable, 0, false, NULL, passport);
  MCLabFMILog_verbose("delay_event_mode: %d", jmi->delay_event_mode);
  Serializable_setField(serializable, FIELD_DELAY_EVENT_MODE, PROPERTIES_BOOLEAN,
                       &jmi->delay_event_mode, passport);
  MCLabFMILog_verbose("dynamic_state_sets[%d]", jmi->n_dynamic_state_sets);
  size_t dynamic_state_sets_size = (size_t) jmi->n_dynamic_state_sets;
  Serializable_setArrayField(serializable, FIELD_DYNAMIC_STATE_SETS, PROPERTIES_POINTER, (void *)ser_jmi->ser_dynamic_state_sets,
  	                        sizeof(SerializableJMI_DYNAMIC_STATE_SET_T*), &dynamic_state_sets_size,
  	                        SerializableJMI_DYNAMIC_STATE_SET_T_asSerializable, 0, false, NULL, passport);
  MCLabFMILog_verbose("n_dynamic_state_sets: %d", jmi->n_dynamic_state_sets);
  Serializable_setField(serializable, FIELD_N_DYNAMIC_STATE_SETS, PROPERTIES_INT,
                       &jmi->n_dynamic_state_sets, passport);
  MCLabFMILog_verbose("n_initial_relations: %d", jmi->n_initial_relations);
  Serializable_setField(serializable, FIELD_N_INITIAL_RELATIONS, PROPERTIES_INT,
                       &jmi->n_initial_relations, passport);
  MCLabFMILog_verbose("initial_relations[%d]", jmi->n_initial_relations);
  size_t initial_relations_size_t = (size_t) jmi->n_initial_relations;
  Serializable_setArrayField(serializable, FIELD_INITIAL_RELATIONS, PROPERTIES_INT,
  	                        (void **)jmi->initial_relations, sizeof(int), &initial_relations_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("n_relations: %d", jmi->n_relations);
  Serializable_setField(serializable, FIELD_N_RELATIONS, PROPERTIES_INT,
                       &jmi->n_relations, passport);
  MCLabFMILog_verbose("relations[%d]", jmi->n_relations);
  size_t relations_size_t = (size_t) jmi->n_relations;
  Serializable_setArrayField(serializable, FIELD_RELATIONS, PROPERTIES_INT,
  	                        (void **)jmi->relations, sizeof(int), &relations_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("atEvent: %g", jmi->atEvent);
  Serializable_setField(serializable, FIELD_ATEVENT, PROPERTIES_DOUBLE,
                       &jmi->atEvent, passport);
  MCLabFMILog_verbose("atInitial: %g", jmi->atInitial);
  Serializable_setField(serializable, FIELD_ATINITIAL, PROPERTIES_DOUBLE,
                       &jmi->atInitial, passport);
  MCLabFMILog_verbose("atTimeEvent: %g", jmi->atTimeEvent);
  Serializable_setField(serializable, FIELD_ATTIMEEVENT, PROPERTIES_DOUBLE,
                       &jmi->atTimeEvent, passport);
  MCLabFMILog_verbose("eventPhase: %d", jmi->eventPhase);
  Serializable_setField(serializable, FIELD_EVENTPHASE, PROPERTIES_INT,
                       &jmi->eventPhase, passport);
  MCLabFMILog_verbose("save_restore_solver_state_mode: %d", jmi->save_restore_solver_state_mode);
  Serializable_setField(serializable, FIELD_SAVE_RESTORE_SOLVER_STATE_MODE, PROPERTIES_INT,
                       &jmi->save_restore_solver_state_mode, passport);
  Serializable_setField(serializable, FIELD_NEXTTIMEEVENT, PROPERTIES_POINTER,
                       SerializableJMI_TIME_EVENT_T_asSerializable(ser_jmi->ser_nextTimeEvent), passport);
  MCLabFMILog_verbose("is_initialized: %d", jmi->is_initialized);
  Serializable_setField(serializable, FIELD_IS_INITIALIZED, PROPERTIES_INT,
                       &jmi->is_initialized, passport);
  MCLabFMILog_verbose("nbr_event_iter: %d", jmi->nbr_event_iter);
  Serializable_setField(serializable, FIELD_NBR_EVENT_ITER, PROPERTIES_INT,
                       &jmi->nbr_event_iter, passport);
  MCLabFMILog_verbose("nbr_consec_time_events: %d", jmi->nbr_consec_time_events);
  Serializable_setField(serializable, FIELD_NBR_CONSEC_TIME_EVENTS, PROPERTIES_INT,
                       &jmi->nbr_consec_time_events, passport);
  //TO_BE_SERIALIZED FIELD_LOG as type jmi_log_t*
  Serializable_setField(serializable, FIELD_OPTIONS, PROPERTIES_POINTER,
                       SerializableJMI_OPTIONS_T_asSerializable(ser_jmi->ser_options), passport);
  MCLabFMILog_verbose("events_epsilon: %g", jmi->events_epsilon);
  Serializable_setField(serializable, FIELD_EVENTS_EPSILON, PROPERTIES_DOUBLE,
                       &jmi->events_epsilon, passport);
  MCLabFMILog_verbose("tmp_events_epsilon: %g", jmi->tmp_events_epsilon);
  Serializable_setField(serializable, FIELD_TMP_EVENTS_EPSILON, PROPERTIES_DOUBLE,
                       &jmi->tmp_events_epsilon, passport);
  MCLabFMILog_verbose("newton_tolerance: %g", jmi->newton_tolerance);
  Serializable_setField(serializable, FIELD_NEWTON_TOLERANCE, PROPERTIES_DOUBLE,
                       &jmi->newton_tolerance, passport);
  MCLabFMILog_verbose("recomputeVariables: %d", jmi->recomputeVariables);
  Serializable_setField(serializable, FIELD_RECOMPUTEVARIABLES, PROPERTIES_INT,
                       &jmi->recomputeVariables, passport);
  MCLabFMILog_verbose("updated_states: %d", jmi->updated_states);
  Serializable_setField(serializable, FIELD_UPDATED_STATES, PROPERTIES_INT,
                       &jmi->updated_states, passport);
  MCLabFMILog_verbose("real_x_work[%d]", jmi->n_real_x);
  size_t real_x_work_size_t = (size_t) jmi->n_real_x;
  Serializable_setArrayField(serializable, FIELD_REAL_X_WORK, PROPERTIES_DOUBLE,
  	                        (void **)jmi->real_x_work, sizeof(double), &real_x_work_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("real_u_work[%d]", jmi->n_real_u);
  size_t real_u_work_size_t = (size_t) jmi->n_real_u;
  Serializable_setArrayField(serializable, FIELD_REAL_U_WORK, PROPERTIES_DOUBLE,
  	                        (void **)jmi->real_u_work, sizeof(double), &real_u_work_size_t,
  	                        NULL, 0, false, NULL, passport);
  // MCLabFMILog_verbose("try_location[%d]", 11);
  // size_t try_location_size_t = (size_t) 11;
  // Serializable_setArrayField(serializable, FIELD_TRY_LOCATION, PROPERTIES_INT,
  // 	                        (void **)jmi->try_location, sizeof(int), &try_location_size_t,
  // 	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("current_try_depth: %d", jmi->current_try_depth);
  Serializable_setField(serializable, FIELD_CURRENT_TRY_DEPTH, PROPERTIES_INT,
                       &jmi->current_try_depth, passport);
  MCLabFMILog_verbose("model_terminate: %d", jmi->model_terminate);
  Serializable_setField(serializable, FIELD_MODEL_TERMINATE, PROPERTIES_INT,
                       &jmi->model_terminate, passport);
  MCLabFMILog_verbose("user_terminate: %d", jmi->user_terminate);
  Serializable_setField(serializable, FIELD_USER_TERMINATE, PROPERTIES_INT,
                       &jmi->user_terminate, passport);
  MCLabFMILog_verbose("reinit_triggered: %d", jmi->reinit_triggered);
  Serializable_setField(serializable, FIELD_REINIT_TRIGGERED, PROPERTIES_INT,
                       &jmi->reinit_triggered, passport);
  MCLabFMILog_verbose("resource_location: %s", jmi->resource_location);
  Serializable_setField(serializable, FIELD_RESOURCE_LOCATION, PROPERTIES_STRING,
                       (void *)jmi->resource_location, passport);
  MCLabFMILog_verbose("resource_location_verified: %d", jmi->resource_location_verified);
  Serializable_setField(serializable, FIELD_RESOURCE_LOCATION_VERIFIED, PROPERTIES_INT,
                       &jmi->resource_location_verified, passport);
  //TO_BE_SERIALIZED FIELD_MODULES as type jmi_modules_t
  Serializable_setField(serializable, FIELD_CHATTERING, PROPERTIES_POINTER,
                       SerializableJMI_CHATTERING_T_asSerializable(ser_jmi->ser_chattering), passport);
  MCLabFMILog_warning("Serialization of dyn_mem_head is not implemented yet.");
  MCLabFMILog_warning("Serialization of dyn_mem_last is not implemented yet.");
  MCLabFMILog_warning("Serialization of dyn_mem is not implemented yet.");
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_T *ser_jmi = (SerializableJMI_T *)this_v;
  jmi_t *jmi = ser_jmi->jmi;
  const void *passport = ser_jmi->passport;
  Serializable *serializable = ser_jmi->serializable;
  // Deserializing simple fields via script
  ser_jmi->ser_jmi_callbacks =
        *(SerializableJMI_CALLBACKS_T **) Serializable_getField(serializable, FIELD_JMI_CALLBACKS, passport);
  //TO_BE_SERIALIZED FIELD_MODEL as type jmi_model_t*
  //TO_BE_SERIALIZED FIELD_INIT_DELAY as type jmi_generic_func_t
  //TO_BE_SERIALIZED FIELD_SAMPLE_DELAY as type jmi_generic_func_t
  MCLabFMILog_warning("Deserialization of z_t is not implemented yet.");
  jmi->n_real_ci =
    *(int *) Serializable_getField(serializable, FIELD_N_REAL_CI, passport);
  MCLabFMILog_verbose("n_real_ci: %d", jmi->n_real_ci);
  jmi->n_real_cd =
    *(int *) Serializable_getField(serializable, FIELD_N_REAL_CD, passport);
  MCLabFMILog_verbose("n_real_cd: %d", jmi->n_real_cd);
  jmi->n_real_pi =
    *(int *) Serializable_getField(serializable, FIELD_N_REAL_PI, passport);
  MCLabFMILog_verbose("n_real_pi: %d", jmi->n_real_pi);
  jmi->n_real_pd =
    *(int *) Serializable_getField(serializable, FIELD_N_REAL_PD, passport);
  MCLabFMILog_verbose("n_real_pd: %d", jmi->n_real_pd);
  jmi->n_integer_ci =
    *(int *) Serializable_getField(serializable, FIELD_N_INTEGER_CI, passport);
  MCLabFMILog_verbose("n_integer_ci: %d", jmi->n_integer_ci);
  jmi->n_integer_cd =
    *(int *) Serializable_getField(serializable, FIELD_N_INTEGER_CD, passport);
  MCLabFMILog_verbose("n_integer_cd: %d", jmi->n_integer_cd);
  jmi->n_integer_pi =
    *(int *) Serializable_getField(serializable, FIELD_N_INTEGER_PI, passport);
  MCLabFMILog_verbose("n_integer_pi: %d", jmi->n_integer_pi);
  jmi->n_integer_pd =
    *(int *) Serializable_getField(serializable, FIELD_N_INTEGER_PD, passport);
  MCLabFMILog_verbose("n_integer_pd: %d", jmi->n_integer_pd);
  jmi->n_boolean_ci =
    *(int *) Serializable_getField(serializable, FIELD_N_BOOLEAN_CI, passport);
  MCLabFMILog_verbose("n_boolean_ci: %d", jmi->n_boolean_ci);
  jmi->n_boolean_cd =
    *(int *) Serializable_getField(serializable, FIELD_N_BOOLEAN_CD, passport);
  MCLabFMILog_verbose("n_boolean_cd: %d", jmi->n_boolean_cd);
  jmi->n_boolean_pi =
    *(int *) Serializable_getField(serializable, FIELD_N_BOOLEAN_PI, passport);
  MCLabFMILog_verbose("n_boolean_pi: %d", jmi->n_boolean_pi);
  jmi->n_boolean_pd =
    *(int *) Serializable_getField(serializable, FIELD_N_BOOLEAN_PD, passport);
  MCLabFMILog_verbose("n_boolean_pd: %d", jmi->n_boolean_pd);
  jmi->n_real_dx =
    *(int *) Serializable_getField(serializable, FIELD_N_REAL_DX, passport);
  MCLabFMILog_verbose("n_real_dx: %d", jmi->n_real_dx);
  jmi->n_real_x =
    *(int *) Serializable_getField(serializable, FIELD_N_REAL_X, passport);
  MCLabFMILog_verbose("n_real_x: %d", jmi->n_real_x);
  jmi->n_real_u =
    *(int *) Serializable_getField(serializable, FIELD_N_REAL_U, passport);
  MCLabFMILog_verbose("n_real_u: %d", jmi->n_real_u);
  jmi->n_real_w =
    *(int *) Serializable_getField(serializable, FIELD_N_REAL_W, passport);
  MCLabFMILog_verbose("n_real_w: %d", jmi->n_real_w);
  jmi->n_real_d =
    *(int *) Serializable_getField(serializable, FIELD_N_REAL_D, passport);
  MCLabFMILog_verbose("n_real_d: %d", jmi->n_real_d);
  jmi->n_integer_d =
    *(int *) Serializable_getField(serializable, FIELD_N_INTEGER_D, passport);
  MCLabFMILog_verbose("n_integer_d: %d", jmi->n_integer_d);
  jmi->n_integer_u =
    *(int *) Serializable_getField(serializable, FIELD_N_INTEGER_U, passport);
  MCLabFMILog_verbose("n_integer_u: %d", jmi->n_integer_u);
  jmi->n_boolean_d =
    *(int *) Serializable_getField(serializable, FIELD_N_BOOLEAN_D, passport);
  MCLabFMILog_verbose("n_boolean_d: %d", jmi->n_boolean_d);
  jmi->n_boolean_u =
    *(int *) Serializable_getField(serializable, FIELD_N_BOOLEAN_U, passport);
  MCLabFMILog_verbose("n_boolean_u: %d", jmi->n_boolean_u);
  jmi->n_sw =
    *(int *) Serializable_getField(serializable, FIELD_N_SW, passport);
  MCLabFMILog_verbose("n_sw: %d", jmi->n_sw);
  jmi->n_sw_init =
    *(int *) Serializable_getField(serializable, FIELD_N_SW_INIT, passport);
  MCLabFMILog_verbose("n_sw_init: %d", jmi->n_sw_init);
  jmi->n_time_sw =
    *(int *) Serializable_getField(serializable, FIELD_N_TIME_SW, passport);
  MCLabFMILog_verbose("n_time_sw: %d", jmi->n_time_sw);
  jmi->n_state_sw =
    *(int *) Serializable_getField(serializable, FIELD_N_STATE_SW, passport);
  MCLabFMILog_verbose("n_state_sw: %d", jmi->n_state_sw);
  jmi->n_guards =
    *(int *) Serializable_getField(serializable, FIELD_N_GUARDS, passport);
  MCLabFMILog_verbose("n_guards: %d", jmi->n_guards);
  jmi->n_guards_init =
    *(int *) Serializable_getField(serializable, FIELD_N_GUARDS_INIT, passport);
  MCLabFMILog_verbose("n_guards_init: %d", jmi->n_guards_init);
  jmi->n_v =
    *(int *) Serializable_getField(serializable, FIELD_N_V, passport);
  MCLabFMILog_verbose("n_v: %d", jmi->n_v);
  jmi->n_z =
    *(int *) Serializable_getField(serializable, FIELD_N_Z, passport);
  MCLabFMILog_verbose("n_z: %d", jmi->n_z);
  jmi->n_dae_blocks =
    *(int *) Serializable_getField(serializable, FIELD_N_DAE_BLOCKS, passport);
  MCLabFMILog_verbose("n_dae_blocks: %d", jmi->n_dae_blocks);
  jmi->n_dae_init_blocks =
    *(int *) Serializable_getField(serializable, FIELD_N_DAE_INIT_BLOCKS, passport);
  MCLabFMILog_verbose("n_dae_init_blocks: %d", jmi->n_dae_init_blocks);
  jmi->n_delays =
    *(int *) Serializable_getField(serializable, FIELD_N_DELAYS, passport);
  MCLabFMILog_verbose("n_delays: %d", jmi->n_delays);
  jmi->n_spatialdists =
    *(int *) Serializable_getField(serializable, FIELD_N_SPATIALDISTS, passport);
  MCLabFMILog_verbose("n_spatialdists: %d", jmi->n_spatialdists);
  jmi->offs_real_ci =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_REAL_CI, passport);
  MCLabFMILog_verbose("offs_real_ci: %d", jmi->offs_real_ci);
  jmi->offs_real_cd =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_REAL_CD, passport);
  MCLabFMILog_verbose("offs_real_cd: %d", jmi->offs_real_cd);
  jmi->offs_real_pi =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_REAL_PI, passport);
  MCLabFMILog_verbose("offs_real_pi: %d", jmi->offs_real_pi);
  jmi->offs_real_pi_s =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_REAL_PI_S, passport);
  MCLabFMILog_verbose("offs_real_pi_s: %d", jmi->offs_real_pi_s);
  jmi->offs_real_pi_f =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_REAL_PI_F, passport);
  MCLabFMILog_verbose("offs_real_pi_f: %d", jmi->offs_real_pi_f);
  jmi->offs_real_pi_e =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_REAL_PI_E, passport);
  MCLabFMILog_verbose("offs_real_pi_e: %d", jmi->offs_real_pi_e);
  jmi->offs_real_pd =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_REAL_PD, passport);
  MCLabFMILog_verbose("offs_real_pd: %d", jmi->offs_real_pd);
  jmi->offs_integer_ci =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_INTEGER_CI, passport);
  MCLabFMILog_verbose("offs_integer_ci: %d", jmi->offs_integer_ci);
  jmi->offs_integer_cd =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_INTEGER_CD, passport);
  MCLabFMILog_verbose("offs_integer_cd: %d", jmi->offs_integer_cd);
  jmi->offs_integer_pi =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_INTEGER_PI, passport);
  MCLabFMILog_verbose("offs_integer_pi: %d", jmi->offs_integer_pi);
  jmi->offs_integer_pi_s =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_INTEGER_PI_S, passport);
  MCLabFMILog_verbose("offs_integer_pi_s: %d", jmi->offs_integer_pi_s);
  jmi->offs_integer_pi_f =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_INTEGER_PI_F, passport);
  MCLabFMILog_verbose("offs_integer_pi_f: %d", jmi->offs_integer_pi_f);
  jmi->offs_integer_pi_e =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_INTEGER_PI_E, passport);
  MCLabFMILog_verbose("offs_integer_pi_e: %d", jmi->offs_integer_pi_e);
  jmi->offs_integer_pd =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_INTEGER_PD, passport);
  MCLabFMILog_verbose("offs_integer_pd: %d", jmi->offs_integer_pd);
  jmi->offs_boolean_ci =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_BOOLEAN_CI, passport);
  MCLabFMILog_verbose("offs_boolean_ci: %d", jmi->offs_boolean_ci);
  jmi->offs_boolean_cd =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_BOOLEAN_CD, passport);
  MCLabFMILog_verbose("offs_boolean_cd: %d", jmi->offs_boolean_cd);
  jmi->offs_boolean_pi =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_BOOLEAN_PI, passport);
  MCLabFMILog_verbose("offs_boolean_pi: %d", jmi->offs_boolean_pi);
  jmi->offs_boolean_pi_s =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_BOOLEAN_PI_S, passport);
  MCLabFMILog_verbose("offs_boolean_pi_s: %d", jmi->offs_boolean_pi_s);
  jmi->offs_boolean_pi_f =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_BOOLEAN_PI_F, passport);
  MCLabFMILog_verbose("offs_boolean_pi_f: %d", jmi->offs_boolean_pi_f);
  jmi->offs_boolean_pi_e =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_BOOLEAN_PI_E, passport);
  MCLabFMILog_verbose("offs_boolean_pi_e: %d", jmi->offs_boolean_pi_e);
  jmi->offs_boolean_pd =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_BOOLEAN_PD, passport);
  MCLabFMILog_verbose("offs_boolean_pd: %d", jmi->offs_boolean_pd);
  jmi->offs_real_dx =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_REAL_DX, passport);
  MCLabFMILog_verbose("offs_real_dx: %d", jmi->offs_real_dx);
  jmi->offs_real_x =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_REAL_X, passport);
  MCLabFMILog_verbose("offs_real_x: %d", jmi->offs_real_x);
  jmi->offs_real_u =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_REAL_U, passport);
  MCLabFMILog_verbose("offs_real_u: %d", jmi->offs_real_u);
  jmi->offs_real_w =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_REAL_W, passport);
  MCLabFMILog_verbose("offs_real_w: %d", jmi->offs_real_w);
  jmi->offs_t =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_T, passport);
  MCLabFMILog_verbose("offs_t: %d", jmi->offs_t);
  jmi->offs_homotopy_lambda =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_HOMOTOPY_LAMBDA, passport);
  MCLabFMILog_verbose("offs_homotopy_lambda: %d", jmi->offs_homotopy_lambda);
  jmi->offs_real_d =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_REAL_D, passport);
  MCLabFMILog_verbose("offs_real_d: %d", jmi->offs_real_d);
  jmi->offs_integer_d =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_INTEGER_D, passport);
  MCLabFMILog_verbose("offs_integer_d: %d", jmi->offs_integer_d);
  jmi->offs_integer_u =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_INTEGER_U, passport);
  MCLabFMILog_verbose("offs_integer_u: %d", jmi->offs_integer_u);
  jmi->offs_boolean_d =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_BOOLEAN_D, passport);
  MCLabFMILog_verbose("offs_boolean_d: %d", jmi->offs_boolean_d);
  jmi->offs_boolean_u =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_BOOLEAN_U, passport);
  MCLabFMILog_verbose("offs_boolean_u: %d", jmi->offs_boolean_u);
  jmi->offs_sw =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_SW, passport);
  MCLabFMILog_verbose("offs_sw: %d", jmi->offs_sw);
  jmi->offs_sw_init =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_SW_INIT, passport);
  MCLabFMILog_verbose("offs_sw_init: %d", jmi->offs_sw_init);
  jmi->offs_state_sw =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_STATE_SW, passport);
  MCLabFMILog_verbose("offs_state_sw: %d", jmi->offs_state_sw);
  jmi->offs_time_sw =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_TIME_SW, passport);
  MCLabFMILog_verbose("offs_time_sw: %d", jmi->offs_time_sw);
  jmi->offs_guards =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_GUARDS, passport);
  MCLabFMILog_verbose("offs_guards: %d", jmi->offs_guards);
  jmi->offs_guards_init =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_GUARDS_INIT, passport);
  MCLabFMILog_verbose("offs_guards_init: %d", jmi->offs_guards_init);
  jmi->offs_pre_real_dx =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_PRE_REAL_DX, passport);
  MCLabFMILog_verbose("offs_pre_real_dx: %d", jmi->offs_pre_real_dx);
  jmi->offs_pre_real_x =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_PRE_REAL_X, passport);
  MCLabFMILog_verbose("offs_pre_real_x: %d", jmi->offs_pre_real_x);
  jmi->offs_pre_real_u =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_PRE_REAL_U, passport);
  MCLabFMILog_verbose("offs_pre_real_u: %d", jmi->offs_pre_real_u);
  jmi->offs_pre_real_w =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_PRE_REAL_W, passport);
  MCLabFMILog_verbose("offs_pre_real_w: %d", jmi->offs_pre_real_w);
  jmi->offs_pre_real_d =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_PRE_REAL_D, passport);
  MCLabFMILog_verbose("offs_pre_real_d: %d", jmi->offs_pre_real_d);
  jmi->offs_pre_integer_d =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_PRE_INTEGER_D, passport);
  MCLabFMILog_verbose("offs_pre_integer_d: %d", jmi->offs_pre_integer_d);
  jmi->offs_pre_integer_u =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_PRE_INTEGER_U, passport);
  MCLabFMILog_verbose("offs_pre_integer_u: %d", jmi->offs_pre_integer_u);
  jmi->offs_pre_boolean_d =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_PRE_BOOLEAN_D, passport);
  MCLabFMILog_verbose("offs_pre_boolean_d: %d", jmi->offs_pre_boolean_d);
  jmi->offs_pre_boolean_u =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_PRE_BOOLEAN_U, passport);
  MCLabFMILog_verbose("offs_pre_boolean_u: %d", jmi->offs_pre_boolean_u);
  jmi->offs_pre_sw =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_PRE_SW, passport);
  MCLabFMILog_verbose("offs_pre_sw: %d", jmi->offs_pre_sw);
  jmi->offs_pre_sw_init =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_PRE_SW_INIT, passport);
  MCLabFMILog_verbose("offs_pre_sw_init: %d", jmi->offs_pre_sw_init);
  jmi->offs_pre_guards =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_PRE_GUARDS, passport);
  MCLabFMILog_verbose("offs_pre_guards: %d", jmi->offs_pre_guards);
  jmi->offs_pre_guards_init =
    *(int *) Serializable_getField(serializable, FIELD_OFFS_PRE_GUARDS_INIT, passport);
  MCLabFMILog_verbose("offs_pre_guards_init: %d", jmi->offs_pre_guards_init);
  size_t z_sizes[2];
  z_sizes[0] = 1;
  z_sizes[1] = (size_t) jmi->n_z;
  Serializable_getArrayField(serializable, FIELD_Z, (void **)jmi->z,
  	                        sizeof(double), z_sizes, 1, passport);
  MCLabFMILog_verbose("z[%d][%d]", 1, jmi->n_z);
  size_t z_last_sizes[2];
  z_last_sizes[0] = 1;
  z_last_sizes[1] = (size_t) jmi->n_z;
  Serializable_getArrayField(serializable, FIELD_Z_LAST, (void **)jmi->z_last,
  	                        sizeof(double), z_last_sizes, 1, passport);
  MCLabFMILog_verbose("z_last[%d][%d]", 1, jmi->n_z);
  size_t dz_sizes[2];
  dz_sizes[0] = 1;
  dz_sizes[1] = (size_t) jmi->n_v;
  Serializable_getArrayField(serializable, FIELD_DZ, (void **)jmi->dz,
  	                        sizeof(double), dz_sizes, 1, passport);
  MCLabFMILog_verbose("dz[%d][%d]", 1, jmi->n_v);
  jmi->dz_active_index =
    *(int *) Serializable_getField(serializable, FIELD_DZ_ACTIVE_INDEX, passport);
  MCLabFMILog_verbose("dz_active_index: %d", jmi->dz_active_index);
  jmi->block_level =
    *(int *) Serializable_getField(serializable, FIELD_BLOCK_LEVEL, passport);
  MCLabFMILog_verbose("block_level: %d", jmi->block_level);
  MCLabFMILog_warning("Deserialization of dz_active_variables is not implemented yet.");
  size_t dz_active_variables_buf_sizes[2];
  dz_active_variables_buf_sizes[0] = 3;
  dz_active_variables_buf_sizes[1] = (size_t) jmi->n_v;
  Serializable_getArrayField(serializable, FIELD_DZ_ACTIVE_VARIABLES_BUF, (void **)jmi->dz_active_variables_buf,
  	                        sizeof(double), dz_active_variables_buf_sizes, 1, passport);
  MCLabFMILog_verbose("dz_active_variables_buf[%d][%d]", 3, jmi->n_v);
  MCLabFMILog_warning("Deserialization of ext_objs is not implemented yet.");
  size_t nominals_size_t = (size_t) jmi->n_real_x;
  Serializable_getArrayField(serializable, FIELD_NOMINALS, (void **)jmi->nominals,
  	                        sizeof(double), &nominals_size_t, 0, passport);
  MCLabFMILog_verbose("nominals[%d]", jmi->n_real_x);
  size_t variable_scaling_factors_size_t = (size_t) jmi->n_z;
  Serializable_getArrayField(serializable, FIELD_VARIABLE_SCALING_FACTORS, (void **)jmi->variable_scaling_factors,
  	                        sizeof(double), &variable_scaling_factors_size_t, 0, passport);
  MCLabFMILog_verbose("variable_scaling_factors[%d]", jmi->n_z);
  jmi->scaling_method =
    *(int *) Serializable_getField(serializable, FIELD_SCALING_METHOD, passport);
  MCLabFMILog_verbose("scaling_method: %d", jmi->scaling_method);
  size_t dae_block_residuals_size = (size_t) jmi->n_dae_blocks;
  Serializable_getArrayField(serializable, FIELD_DAE_BLOCK_RESIDUALS, (void **)ser_jmi->ser_dae_block_residuals,
  	                        sizeof(SerializableJMI_BLOCK_RESIDUAL_T*), &dae_block_residuals_size, 0, passport);
  MCLabFMILog_verbose("dae_block_residuals[%d]", jmi->n_dae_blocks);
  size_t dae_init_block_residuals_size = (size_t) jmi->n_dae_init_blocks;
  Serializable_getArrayField(serializable, FIELD_DAE_INIT_BLOCK_RESIDUALS, (void **)ser_jmi->ser_dae_init_block_residuals,
  	                        sizeof(SerializableJMI_BLOCK_RESIDUAL_T*), &dae_init_block_residuals_size, 0, passport);
  MCLabFMILog_verbose("dae_init_block_residuals[%d]", jmi->n_dae_init_blocks);
  jmi->cached_block_jacobians =
    *(int *) Serializable_getField(serializable, FIELD_CACHED_BLOCK_JACOBIANS, passport);
  MCLabFMILog_verbose("cached_block_jacobians: %d", jmi->cached_block_jacobians);
  size_t delays_size = (size_t) jmi->n_delays;
  Serializable_getArrayField(serializable, FIELD_DELAYS, (void **)ser_jmi->ser_delays,
  	                        sizeof(SerializableJMI_DELAY_T*), &delays_size, 0, passport);
  MCLabFMILog_verbose("delays[%d]", jmi->n_delays);
  size_t spatialdists_size = (size_t) jmi->n_spatialdists;
  Serializable_getArrayField(serializable, FIELD_SPATIALDISTS, (void **)ser_jmi->ser_spatialdists,
  	                        sizeof(SerializableJMI_SPATIALDIST_T*), &spatialdists_size, 0, passport);
  MCLabFMILog_verbose("spatialdists[%d]", jmi->n_spatialdists);
  jmi->delay_event_mode =
    *(char *) Serializable_getField(serializable, FIELD_DELAY_EVENT_MODE, passport);
  MCLabFMILog_verbose("delay_event_mode: %c", jmi->delay_event_mode);
  size_t dynamic_state_sets_size = (size_t) jmi->n_dynamic_state_sets;
  Serializable_getArrayField(serializable, FIELD_DYNAMIC_STATE_SETS, (void **)ser_jmi->ser_dynamic_state_sets,
  	                        sizeof(SerializableJMI_DYNAMIC_STATE_SET_T*), &dynamic_state_sets_size, 0, passport);
  MCLabFMILog_verbose("dynamic_state_sets[%d]", jmi->n_dynamic_state_sets);
  jmi->n_dynamic_state_sets =
    *(int *) Serializable_getField(serializable, FIELD_N_DYNAMIC_STATE_SETS, passport);
  MCLabFMILog_verbose("n_dynamic_state_sets: %d", jmi->n_dynamic_state_sets);
  jmi->n_initial_relations =
    *(int *) Serializable_getField(serializable, FIELD_N_INITIAL_RELATIONS, passport);
  MCLabFMILog_verbose("n_initial_relations: %d", jmi->n_initial_relations);
  size_t initial_relations_size_t = (size_t) jmi->n_initial_relations;
  Serializable_getArrayField(serializable, FIELD_INITIAL_RELATIONS, (void **)jmi->initial_relations,
  	                        sizeof(int), &initial_relations_size_t, 0, passport);
  MCLabFMILog_verbose("initial_relations[%d]", jmi->n_initial_relations);
  jmi->n_relations =
    *(int *) Serializable_getField(serializable, FIELD_N_RELATIONS, passport);
  MCLabFMILog_verbose("n_relations: %d", jmi->n_relations);
  size_t relations_size_t = (size_t) jmi->n_relations;
  Serializable_getArrayField(serializable, FIELD_RELATIONS, (void **)jmi->relations,
  	                        sizeof(int), &relations_size_t, 0, passport);
  MCLabFMILog_verbose("relations[%d]", jmi->n_relations);
  jmi->atEvent =
    *(double *) Serializable_getField(serializable, FIELD_ATEVENT, passport);
  MCLabFMILog_verbose("atEvent: %g", jmi->atEvent);
  jmi->atInitial =
    *(double *) Serializable_getField(serializable, FIELD_ATINITIAL, passport);
  MCLabFMILog_verbose("atInitial: %g", jmi->atInitial);
  jmi->atTimeEvent =
    *(double *) Serializable_getField(serializable, FIELD_ATTIMEEVENT, passport);
  MCLabFMILog_verbose("atTimeEvent: %g", jmi->atTimeEvent);
  jmi->eventPhase =
    *(int *) Serializable_getField(serializable, FIELD_EVENTPHASE, passport);
  MCLabFMILog_verbose("eventPhase: %d", jmi->eventPhase);
  jmi->save_restore_solver_state_mode =
    *(int *) Serializable_getField(serializable, FIELD_SAVE_RESTORE_SOLVER_STATE_MODE, passport);
  MCLabFMILog_verbose("save_restore_solver_state_mode: %d", jmi->save_restore_solver_state_mode);
  ser_jmi->ser_nextTimeEvent =
        *(SerializableJMI_TIME_EVENT_T **) Serializable_getField(serializable, FIELD_NEXTTIMEEVENT, passport);
  jmi->is_initialized =
    *(int *) Serializable_getField(serializable, FIELD_IS_INITIALIZED, passport);
  MCLabFMILog_verbose("is_initialized: %d", jmi->is_initialized);
  jmi->nbr_event_iter =
    *(int *) Serializable_getField(serializable, FIELD_NBR_EVENT_ITER, passport);
  MCLabFMILog_verbose("nbr_event_iter: %d", jmi->nbr_event_iter);
  jmi->nbr_consec_time_events =
    *(int *) Serializable_getField(serializable, FIELD_NBR_CONSEC_TIME_EVENTS, passport);
  MCLabFMILog_verbose("nbr_consec_time_events: %d", jmi->nbr_consec_time_events);
  //TO_BE_SERIALIZED FIELD_LOG as type jmi_log_t*
  ser_jmi->ser_options =
        *(SerializableJMI_OPTIONS_T **) Serializable_getField(serializable, FIELD_OPTIONS, passport);
  jmi->events_epsilon =
    *(double *) Serializable_getField(serializable, FIELD_EVENTS_EPSILON, passport);
  MCLabFMILog_verbose("events_epsilon: %g", jmi->events_epsilon);
  jmi->tmp_events_epsilon =
    *(double *) Serializable_getField(serializable, FIELD_TMP_EVENTS_EPSILON, passport);
  MCLabFMILog_verbose("tmp_events_epsilon: %g", jmi->tmp_events_epsilon);
  jmi->newton_tolerance =
    *(double *) Serializable_getField(serializable, FIELD_NEWTON_TOLERANCE, passport);
  MCLabFMILog_verbose("newton_tolerance: %g", jmi->newton_tolerance);
  jmi->recomputeVariables =
    *(int *) Serializable_getField(serializable, FIELD_RECOMPUTEVARIABLES, passport);
  MCLabFMILog_verbose("recomputeVariables: %d", jmi->recomputeVariables);
  jmi->updated_states =
    *(int *) Serializable_getField(serializable, FIELD_UPDATED_STATES, passport);
  MCLabFMILog_verbose("updated_states: %d", jmi->updated_states);
  size_t real_x_work_size_t = (size_t) jmi->n_real_x;
  Serializable_getArrayField(serializable, FIELD_REAL_X_WORK, (void **)jmi->real_x_work,
  	                        sizeof(double), &real_x_work_size_t, 0, passport);
  MCLabFMILog_verbose("real_x_work[%d]", jmi->n_real_x);
  size_t real_u_work_size_t = (size_t) jmi->n_real_u;
  Serializable_getArrayField(serializable, FIELD_REAL_U_WORK, (void **)jmi->real_u_work,
  	                        sizeof(double), &real_u_work_size_t, 0, passport);
  MCLabFMILog_verbose("real_u_work[%d]", jmi->n_real_u);
  // size_t try_location_size_t = (size_t) 11;
  // Serializable_getArrayField(serializable, FIELD_TRY_LOCATION, (void **)jmi->try_location,
  // 	                        sizeof(int), &try_location_size_t, 0, passport);
  // MCLabFMILog_verbose("try_location[%d]", 11);
  jmi->current_try_depth =
    *(int *) Serializable_getField(serializable, FIELD_CURRENT_TRY_DEPTH, passport);
  MCLabFMILog_verbose("current_try_depth: %d", jmi->current_try_depth);
  jmi->model_terminate =
    *(int *) Serializable_getField(serializable, FIELD_MODEL_TERMINATE, passport);
  MCLabFMILog_verbose("model_terminate: %d", jmi->model_terminate);
  jmi->user_terminate =
    *(int *) Serializable_getField(serializable, FIELD_USER_TERMINATE, passport);
  MCLabFMILog_verbose("user_terminate: %d", jmi->user_terminate);
  jmi->reinit_triggered =
    *(int *) Serializable_getField(serializable, FIELD_REINIT_TRIGGERED, passport);
  MCLabFMILog_verbose("reinit_triggered: %d", jmi->reinit_triggered);
  SAFETY_CHECK_STRING(serializable, jmi->resource_location, FIELD_RESOURCE_LOCATION, passport);
  MCLabFMILog_verbose("resource_location: %s", jmi->resource_location);
  jmi->resource_location_verified =
    *(int *) Serializable_getField(serializable, FIELD_RESOURCE_LOCATION_VERIFIED, passport);
  MCLabFMILog_verbose("resource_location_verified: %d", jmi->resource_location_verified);
  //TO_BE_SERIALIZED FIELD_MODULES as type jmi_modules_t
  ser_jmi->ser_chattering =
        *(SerializableJMI_CHATTERING_T **) Serializable_getField(serializable, FIELD_CHATTERING, passport);
  MCLabFMILog_warning("Deserialization of dyn_mem_head is not implemented yet.");
  MCLabFMILog_warning("Deserialization of dyn_mem_last is not implemented yet.");
  MCLabFMILog_warning("Deserialization of dyn_mem is not implemented yet.");
  MCLabFMILog_unnest();
}

SerializableJMI_T *SerializableJMI_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_t *jmi) {
  Debug_assert(DEBUG_ALWAYS, jmi != NULL, "jmi == NULL");
  SerializableJMI_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_T));
    new->jmi = jmi;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi, new,
                    SerializableJMI_T_free, new->passport);
    // Adding inner struct new
    new->ser_jmi_callbacks = SerializableJMI_CALLBACKS_T_new(fmi_env, &jmi->jmi_callbacks);
    size_t dae_block_residuals_size = (size_t) jmi->n_dae_blocks;
    new->ser_dae_block_residuals = calloc(dae_block_residuals_size, sizeof(SerializableJMI_BLOCK_RESIDUAL_T *));
      for (long i = 0; i < dae_block_residuals_size; i++) {
        // fprintf(stderr, "SERIALIZE SOLVER KIND %d \n", jmi->dae_init_block_residuals[i]->options->solver);
        new->ser_dae_block_residuals[i] =
            SerializableJMI_BLOCK_RESIDUAL_T_new(fmi_env, jmi->dae_block_residuals[i]);
      }
    size_t dae_init_block_residuals_size = (size_t) jmi->n_dae_init_blocks;
    new->ser_dae_init_block_residuals = calloc(dae_init_block_residuals_size, sizeof(SerializableJMI_BLOCK_RESIDUAL_T *));
      for (long i = 0; i < dae_init_block_residuals_size; i++) {
        // fprintf(stderr, "SERIALIZE INIT SOLVER KIND %d\n", jmi->dae_init_block_residuals[i]->options->solver);
        new->ser_dae_init_block_residuals[i] =
            SerializableJMI_BLOCK_RESIDUAL_T_new(fmi_env, jmi->dae_init_block_residuals[i]);
      }
    size_t delays_size = (size_t) jmi->n_delays;
    new->ser_delays = calloc(delays_size, sizeof(SerializableJMI_DELAY_T *));
      for (long i = 0; i < delays_size; i++) {
        new->ser_delays[i] =
            SerializableJMI_DELAY_T_new(fmi_env, &jmi->delays[i]);
      }
    size_t spatialdists_size = (size_t) jmi->n_spatialdists;
    new->ser_spatialdists = calloc(spatialdists_size, sizeof(SerializableJMI_SPATIALDIST_T *));
      for (long i = 0; i < spatialdists_size; i++) {
        new->ser_spatialdists[i] =
            SerializableJMI_SPATIALDIST_T_new(fmi_env, &jmi->spatialdists[i]);
      }
    size_t dynamic_state_sets_size = (size_t) jmi->n_dynamic_state_sets;
    new->ser_dynamic_state_sets = calloc(dynamic_state_sets_size, sizeof(SerializableJMI_DYNAMIC_STATE_SET_T *));
      for (long i = 0; i < dynamic_state_sets_size; i++) {
        new->ser_dynamic_state_sets[i] =
            SerializableJMI_DYNAMIC_STATE_SET_T_new(fmi_env, &jmi->dynamic_state_sets[i]);
      }
    new->ser_nextTimeEvent = SerializableJMI_TIME_EVENT_T_new(fmi_env, &jmi->nextTimeEvent);
    new->ser_options = SerializableJMI_OPTIONS_T_new(fmi_env, &jmi->options);
    new->ser_chattering = SerializableJMI_CHATTERING_T_new(fmi_env, jmi->chattering, (size_t)jmi->n_sw);
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_T *this_ = (SerializableJMI_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->ser_dae_block_residuals);
  free(this_->ser_dae_init_block_residuals);
  free(this_->ser_delays);
  free(this_->ser_spatialdists);
  free(this_->ser_dynamic_state_sets);
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_T *this_ = (SerializableJMI_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
