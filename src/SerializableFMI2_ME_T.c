/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "fmi2_me.h"
#include "jmi_me.h"
#include "SerializableJMI_T.h"
#include "SerializableJMI_EVENT_INFO_T.h"

#include "MCLabFMIEnv.h"
#include "SerializableFMI2_ME_T.h"

#define DEBUG "SerializableFMI2_ME_T"

#define CLASS_NAME "fmi2_me_t"

// defining constants for fields name
#define FIELD_JMI "jmi"
#define FIELD_FMU_MODE "fmu_mode"
#define FIELD_FMU_TYPE "fmu_type"
#define FIELD_FMU_INSTANCE_NAME "fmu_instance_name"
#define FIELD_FMU_GUID "fmu_GUID"
#define FIELD_INITIAL_LOGGING_ON "initial_logging_on"

#define FIELD_EVENT_INFO "event_info"
#define FIELD_WORK_REAL_ARRAY "work_real_array"
#define FIELD_WORK_INT_ARRAY "work_int_array"

struct SerializableFMI2_ME_T {
  fmi2_me_t *fmi2_me;
  // Adding inner struct
  SerializableJMI_T *ser_jmi;
  SerializableJMI_EVENT_INFO_T *ser_event_info;
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableFMI2_ME_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableFMI2_ME_T *ser_fmi2_me = (SerializableFMI2_ME_T *)this_v;
  return ser_fmi2_me->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableFMI2_ME_T *ser_fmi2_me = (SerializableFMI2_ME_T *)this_v;
  fmi2_me_t *fmi2_me = ser_fmi2_me->fmi2_me;
  const void *passport = ser_fmi2_me->passport;
  Serializable *serializable = ser_fmi2_me->serializable;
  Serializable_setField(serializable, FIELD_JMI, PROPERTIES_POINTER,
                       SerializableJMI_T_asSerializable(ser_fmi2_me->ser_jmi), passport);
  MCLabFMILog_verbose("fmu_mode: %d", fmi2_me->fmu_mode);
  Serializable_setField(serializable, FIELD_FMU_MODE, PROPERTIES_INT,
                       &fmi2_me->fmu_mode, passport);
  MCLabFMILog_verbose("fmu_type: %d", fmi2_me->fmu_type);
  Serializable_setField(serializable, FIELD_FMU_TYPE, PROPERTIES_INT,
                       &fmi2_me->fmu_type, passport);
  MCLabFMILog_verbose("fmu_instance_name: %s", fmi2_me->fmu_instance_name);
  Serializable_setField(serializable, FIELD_FMU_INSTANCE_NAME, PROPERTIES_STRING,
                       (void *)fmi2_me->fmu_instance_name, passport);
  MCLabFMILog_verbose("fmu_GUID: %s", fmi2_me->fmu_GUID);
  Serializable_setField(serializable, FIELD_FMU_GUID, PROPERTIES_STRING,
                       (void *)fmi2_me->fmu_GUID, passport);
  MCLabFMILog_verbose("initial_logging_on: %d", fmi2_me->initial_logging_on);
  Serializable_setField(serializable, FIELD_INITIAL_LOGGING_ON, PROPERTIES_INT,
                       &fmi2_me->initial_logging_on, passport);
  //TO_BE_SERIALIZED FIELD_FMI_FUNCTIONS as type const fmi2CallbackFunctions*
  Serializable_setField(serializable, FIELD_EVENT_INFO, PROPERTIES_POINTER,
                       SerializableJMI_EVENT_INFO_T_asSerializable(ser_fmi2_me->ser_event_info), passport);
  MCLabFMILog_verbose("work_real_array[%d]", fmi2_me->jmi.n_z);
  size_t work_real_array_size_t = (size_t) fmi2_me->jmi.n_z;
  Serializable_setArrayField(serializable, FIELD_WORK_REAL_ARRAY, PROPERTIES_DOUBLE,
  	                        (void **)fmi2_me->work_real_array, sizeof(double), &work_real_array_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("work_int_array[%d]", fmi2_me->jmi.n_z);
  size_t work_int_array_size_t = (size_t) fmi2_me->jmi.n_z;
  Serializable_setArrayField(serializable, FIELD_WORK_INT_ARRAY, PROPERTIES_INT,
  	                        (void **)fmi2_me->work_int_array, sizeof(int), &work_int_array_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableFMI2_ME_T *ser_fmi2_me = (SerializableFMI2_ME_T *)this_v;
  fmi2_me_t *fmi2_me = ser_fmi2_me->fmi2_me;
  const void *passport = ser_fmi2_me->passport;
  Serializable *serializable = ser_fmi2_me->serializable;
  // Deserializing simple fields via script
  ser_fmi2_me->ser_jmi =
        *(SerializableJMI_T **) Serializable_getField(serializable, FIELD_JMI, passport);
  fmi2_me->fmu_mode =
    *(int *) Serializable_getField(serializable, FIELD_FMU_MODE, passport);
  MCLabFMILog_verbose("fmu_mode: %d", fmi2_me->fmu_mode);
  fmi2_me->fmu_type =
    *(int *) Serializable_getField(serializable, FIELD_FMU_TYPE, passport);
  MCLabFMILog_verbose("fmu_type: %d", fmi2_me->fmu_type);
  Serializable_deserializeString(
    (char *) fmi2_me->fmu_instance_name,
    (char *) Serializable_getField(serializable, FIELD_FMU_INSTANCE_NAME, passport));
  MCLabFMILog_verbose("fmu_instance_name: %s", fmi2_me->fmu_instance_name);
  Serializable_deserializeString(
    (char *) fmi2_me->fmu_GUID,
    (char *) Serializable_getField(serializable, FIELD_FMU_GUID, passport));
  MCLabFMILog_verbose("fmu_GUID: %s", fmi2_me->fmu_GUID);
  fmi2_me->initial_logging_on =
    *(int *) Serializable_getField(serializable, FIELD_INITIAL_LOGGING_ON, passport);
  MCLabFMILog_verbose("initial_logging_on: %d", fmi2_me->initial_logging_on);
  //TO_BE_SERIALIZED FIELD_FMI_FUNCTIONS as type const fmi2CallbackFunctions*
  ser_fmi2_me->ser_event_info =
        *(SerializableJMI_EVENT_INFO_T **) Serializable_getField(serializable, FIELD_EVENT_INFO, passport);
  size_t work_real_array_size_t = (size_t) fmi2_me->jmi.n_z;
  Serializable_getArrayField(serializable, FIELD_WORK_REAL_ARRAY, (void **)fmi2_me->work_real_array,
  	                        sizeof(double), &work_real_array_size_t, 0, passport);
  MCLabFMILog_verbose("work_real_array[%d]", fmi2_me->jmi.n_z);
  size_t work_int_array_size_t = (size_t) fmi2_me->jmi.n_z;
  Serializable_getArrayField(serializable, FIELD_WORK_INT_ARRAY, (void **)fmi2_me->work_int_array,
  	                        sizeof(int), &work_int_array_size_t, 0, passport);
  MCLabFMILog_verbose("work_int_array[%d]", fmi2_me->jmi.n_z);
  MCLabFMILog_unnest();
}

SerializableFMI2_ME_T *SerializableFMI2_ME_T_new(MCLabFMIEnv *fmi_env,
                                                 fmi2_me_t *fmi2_me) {
  Debug_assert(DEBUG_ALWAYS, fmi2_me != NULL, "fmi2_me == NULL");
  SerializableFMI2_ME_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, fmi2_me)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableFMI2_ME_T));
    new->fmi2_me = fmi2_me;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, fmi2_me, new,
                    SerializableFMI2_ME_T_free, new->passport);
    // Adding inner struct new
    new->ser_jmi = SerializableJMI_T_new(fmi_env, &fmi2_me->jmi);
    new->ser_event_info = SerializableJMI_EVENT_INFO_T_new(fmi_env, fmi2_me->event_info);
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableFMI2_ME_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableFMI2_ME_T *this_ = (SerializableFMI2_ME_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableFMI2_ME_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableFMI2_ME_T *this_ = (SerializableFMI2_ME_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
