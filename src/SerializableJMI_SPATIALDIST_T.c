/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "jmi_delay_impl.h"
#include "SerializableJMI_DELAYBUFFER_T.h"
#include "SerializableJMI_DELAY_POSITION_T.h"
#include "SerializableJMI_DELAY_POSITION_T.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_SPATIALDIST_T.h"

#define DEBUG "SerializableJMI_SPATIALDIST_T"

#define CLASS_NAME "jmi_spatialdist_t"

// defining constants for fields name
#define FIELD_BUFFER "buffer"
#define FIELD_NO_EVENT "no_event"
#define FIELD_LPOSITION "lposition"
#define FIELD_RPOSITION "rposition"
#define FIELD_LAST_X "last_x"

struct SerializableJMI_SPATIALDIST_T {
  jmi_spatialdist_t *jmi_spatialdist;
  // Adding inner struct
  SerializableJMI_DELAYBUFFER_T *ser_buffer;
  SerializableJMI_DELAY_POSITION_T *ser_lposition;
  SerializableJMI_DELAY_POSITION_T *ser_rposition;
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_SPATIALDIST_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_SPATIALDIST_T *ser_jmi_spatialdist = (SerializableJMI_SPATIALDIST_T *)this_v;
  return ser_jmi_spatialdist->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_SPATIALDIST_T *ser_jmi_spatialdist = (SerializableJMI_SPATIALDIST_T *)this_v;
  jmi_spatialdist_t *jmi_spatialdist = ser_jmi_spatialdist->jmi_spatialdist;
  const void *passport = ser_jmi_spatialdist->passport;
  Serializable *serializable = ser_jmi_spatialdist->serializable;
  Serializable_setField(serializable, FIELD_BUFFER, PROPERTIES_POINTER,
                       SerializableJMI_DELAYBUFFER_T_asSerializable(ser_jmi_spatialdist->ser_buffer), passport);
  MCLabFMILog_verbose("no_event: %d", jmi_spatialdist->no_event);
  Serializable_setField(serializable, FIELD_NO_EVENT, PROPERTIES_BOOLEAN,
                       &jmi_spatialdist->no_event, passport);
  Serializable_setField(serializable, FIELD_LPOSITION, PROPERTIES_POINTER,
                       SerializableJMI_DELAY_POSITION_T_asSerializable(ser_jmi_spatialdist->ser_lposition), passport);
  Serializable_setField(serializable, FIELD_RPOSITION, PROPERTIES_POINTER,
                       SerializableJMI_DELAY_POSITION_T_asSerializable(ser_jmi_spatialdist->ser_rposition), passport);
  MCLabFMILog_verbose("last_x: %g", jmi_spatialdist->last_x);
  Serializable_setField(serializable, FIELD_LAST_X, PROPERTIES_DOUBLE,
                       &jmi_spatialdist->last_x, passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_SPATIALDIST_T *ser_jmi_spatialdist = (SerializableJMI_SPATIALDIST_T *)this_v;
  jmi_spatialdist_t *jmi_spatialdist = ser_jmi_spatialdist->jmi_spatialdist;
  const void *passport = ser_jmi_spatialdist->passport;
  Serializable *serializable = ser_jmi_spatialdist->serializable;
  // Deserializing simple fields via script
  ser_jmi_spatialdist->ser_buffer =
        *(SerializableJMI_DELAYBUFFER_T **) Serializable_getField(serializable, FIELD_BUFFER, passport);
  jmi_spatialdist->no_event =
    *(char *) Serializable_getField(serializable, FIELD_NO_EVENT, passport);
  MCLabFMILog_verbose("no_event: %c", jmi_spatialdist->no_event);
  ser_jmi_spatialdist->ser_lposition =
        *(SerializableJMI_DELAY_POSITION_T **) Serializable_getField(serializable, FIELD_LPOSITION, passport);
  ser_jmi_spatialdist->ser_rposition =
        *(SerializableJMI_DELAY_POSITION_T **) Serializable_getField(serializable, FIELD_RPOSITION, passport);
  jmi_spatialdist->last_x =
    *(double *) Serializable_getField(serializable, FIELD_LAST_X, passport);
  MCLabFMILog_verbose("last_x: %g", jmi_spatialdist->last_x);
  MCLabFMILog_unnest();
}

SerializableJMI_SPATIALDIST_T *SerializableJMI_SPATIALDIST_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_spatialdist_t *jmi_spatialdist) {
  Debug_assert(DEBUG_ALWAYS, jmi_spatialdist != NULL, "jmi_spatialdist == NULL");
  SerializableJMI_SPATIALDIST_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_spatialdist)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_SPATIALDIST_T));
    new->jmi_spatialdist = jmi_spatialdist;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_spatialdist, new,
                    SerializableJMI_SPATIALDIST_T_free, new->passport);
    // Adding inner struct new
    new->ser_buffer = SerializableJMI_DELAYBUFFER_T_new(fmi_env, &jmi_spatialdist->buffer);
    new->ser_lposition = SerializableJMI_DELAY_POSITION_T_new(fmi_env, &jmi_spatialdist->lposition);
    new->ser_rposition = SerializableJMI_DELAY_POSITION_T_new(fmi_env, &jmi_spatialdist->rposition);
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_SPATIALDIST_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_SPATIALDIST_T *this_ = (SerializableJMI_SPATIALDIST_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_SPATIALDIST_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_SPATIALDIST_T *this_ = (SerializableJMI_SPATIALDIST_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
