/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include "fmi2_me.h"

#include <MCLabUtils.h>

#include "MCLabFMIUtils.h"
#include "MCLabFMIEnv.h"
#include "SerializableFMI2_ME_T.h"
#include "MCLabFMI.h"

#define KEY_LEN 128
#define DEBUG "MCLabFMI"
#define DEBUG_GET_VERBOSE "MCLabFMI Get Verbose"
#define DEBUG_SET_VERBOSE "MCLabFMI Set Verbose"
#define DEBUG_FREE_VERBOSE "MCLabFMI Free Verbose"


void MCLabFMILog_init(int log_level) {
  Debug_setDefaultOut(0);
  Debug_setIndentAmount(2);
  switch (log_level) {
    case 0:
      MCLabFMILog_enableError();
      break;
    case 1:
      MCLabFMILog_enableError();
      MCLabFMILog_enableWarning();
      break;
    case 2:
      MCLabFMILog_enableError();
      MCLabFMILog_enableWarning();
      MCLabFMILog_enableInfo();
      break;
    case 3:
      MCLabFMILog_enableError();
      MCLabFMILog_enableWarning();
      MCLabFMILog_enableInfo();
      MCLabFMILog_enableVerbose();
      break;
    case 4:
      Debug_setDefaultOut(1);
      break;
    default:
      MCLabFMILog_enableError();
      MCLabFMILog_error("Wrong log_level, default one is used");
  }
}

fmi2Status MCLabFMI_fmi2GetFMUstate(fmi2_me_t *fmi2_me,
                                    Properties **fmu_state) {
  Debug_assert(DEBUG_ALWAYS, fmi2_me != NULL, "fmi2_me == NULL\n");
  if (*fmu_state != NULL) {
    Properties_free(fmu_state);
  }
  MCLabFMILog_info("Start serialization of FMU state...");
  MCLabFMILog_nest();
  MCLabFMIEnv *fmi_env = MCLabFMIEnv_new();
  Serializable_init();
  SerializableFMI2_ME_T *ser_fmi2_me =
      SerializableFMI2_ME_T_new(fmi_env, fmi2_me);
  Serializable *serializable =
      SerializableFMI2_ME_T_asSerializable(ser_fmi2_me);
  *fmu_state = Properties_new();
  Serializable_serializeInto(serializable, *fmu_state);
  Serializable_free(&serializable);
  Serializable_finalize();
  MCLabFMIEnv_free(&fmi_env);
  MCLabFMILog_unnest();
  MCLabFMILog_info("done!");
  return fmi2OK;
}

fmi2Status MCLabFMI_fmi2SetFMUstate(fmi2_me_t *fmi2_me, Properties *fmu_state) {
  Debug_assert(DEBUG_ALWAYS, fmi2_me != NULL, "fmi2_me == NULL\n");
  Debug_assert(DEBUG_ALWAYS, fmu_state != NULL, "fmu_state == NULL\n");
  MCLabFMILog_info("Start deserialization of FMU state...");
  MCLabFMILog_nest();
  MCLabFMIEnv *fmi_env = MCLabFMIEnv_new();
  Serializable_init();
  SerializableFMI2_ME_T *ser_fmi2_me =
      SerializableFMI2_ME_T_new(fmi_env, fmi2_me);
  Serializable *serializable =
      SerializableFMI2_ME_T_asSerializable(ser_fmi2_me);
  Serializable_deserializeFrom(serializable, fmu_state);
  Serializable_free(&serializable);
  Serializable_finalize();
  MCLabFMIEnv_free(&fmi_env);
  MCLabFMILog_unnest();
  MCLabFMILog_info("done!");
  return fmi2OK;
}

fmi2Status MCLabFMI_fmi2FreeFMUstate(fmi2_me_t *fmi2_me,
                                     Properties **fmu_state) {
  Debug_assert(DEBUG_ALWAYS, fmi2_me != NULL, "fmi2_me == NULL\n");
  Debug_assert(DEBUG_ALWAYS, fmu_state != NULL, "fmu_state == NULL\n");
  if (*fmu_state == NULL) return fmi2OK;
  MCLabFMILog_info("Free memory of FMU state...");
  Properties_free(fmu_state);
  MCLabFMILog_info("done!");
  return fmi2OK;
}
