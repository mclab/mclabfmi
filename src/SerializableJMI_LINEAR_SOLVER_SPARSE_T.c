/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "jmi_linear_solver.h"
#include "SerializableJMI_LINEAR_SOLVER_SPARSE_T.h"
#include "SerializableJMI_MATRIX_SPARSE_CSC_T.h"

#include "MCLabFMIEnv.h"
#include "utils.h"

#define DEBUG "SerializableJMI_LINEAR_SOLVER_SPARSE_T"

#define CLASS_NAME "jmi_linear_solver_sparse"

// defining constants for fields name

#define FIELD_L "L"
#define FIELD_A12 "A12"
#define FIELD_A21 "A21"
#define FIELD_A22 "A22"
#define FIELD_M1 "M1"
#define FIELD_NZ_PATTERNS "nz_patterns"
#define FIELD_NZ_PATTERN_SIZES "nz_pattern_sizes"
#define FIELD_M1_PATTERNS "M1_patterns"
#define FIELD_NZ_SIZES "nz_sizes"
#define FIELD_M1_SIZES "M1_sizes"
#define FIELD_NZ_OFFSETS "nz_offsets"
#define FIELD_WORK_X "work_x"
#define FIELD_MAX_THREADS "max_threads"

struct SerializableJMI_LINEAR_SOLVER_SPARSE_T {
  jmi_linear_solver_sparse_t *solver;
  // Adding inner struct
  SerializableJMI_MATRIX_SPARSE_CSC_T *ser_L;
  SerializableJMI_MATRIX_SPARSE_CSC_T *ser_A12;
  SerializableJMI_MATRIX_SPARSE_CSC_T *ser_A21;
  SerializableJMI_MATRIX_SPARSE_CSC_T *ser_A22;
  SerializableJMI_MATRIX_SPARSE_CSC_T *ser_M1;
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_LINEAR_SOLVER_SPARSE_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_LINEAR_SOLVER_SPARSE_T *ser_solver = (SerializableJMI_LINEAR_SOLVER_SPARSE_T *)this_v;
  return ser_solver->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_LINEAR_SOLVER_SPARSE_T *ser_solver = (SerializableJMI_LINEAR_SOLVER_SPARSE_T *)this_v;
  jmi_linear_solver_sparse_t *solver = ser_solver->solver;
  const void *passport = ser_solver->passport;
  Serializable *serializable = ser_solver->serializable;

  if (solver->L != NULL) {
    Serializable_setField(serializable, FIELD_L, PROPERTIES_POINTER, SerializableJMI_MATRIX_SPARSE_CSC_T_asSerializable(ser_solver->ser_L), passport);
  }
  if (solver->A12 != NULL) {
    Serializable_setField(serializable, FIELD_A12, PROPERTIES_POINTER, SerializableJMI_MATRIX_SPARSE_CSC_T_asSerializable(ser_solver->ser_A12), passport);
  }
  if (solver->A21 != NULL) {
    Serializable_setField(serializable, FIELD_A21, PROPERTIES_POINTER, SerializableJMI_MATRIX_SPARSE_CSC_T_asSerializable(ser_solver->ser_A21), passport);
  }
  if (solver->A22 != NULL) {
    Serializable_setField(serializable, FIELD_A22, PROPERTIES_POINTER, SerializableJMI_MATRIX_SPARSE_CSC_T_asSerializable(ser_solver->ser_A22), passport);
  }
  if (solver->M1 != NULL) {
    Serializable_setField(serializable, FIELD_M1, PROPERTIES_POINTER, SerializableJMI_MATRIX_SPARSE_CSC_T_asSerializable(ser_solver->ser_M1), passport);
  }
  if (solver->nz_patterns != NULL) {
    size_t nz_patterns_size = (size_t) solver->A12->nbr_cols;
    MCLabFMILog_verbose("nz_patterns[%zu]", nz_patterns_size);
    for (size_t i = 0; i < (size_t) solver->A12->nbr_cols; ++i) {
      size_t nz_patterns_size_i = (size_t) solver->nz_sizes[i];
      MCLabFMILog_verbose("nz_patterns[%zu][%zu]", i, nz_patterns_size_i);
      char *field = NULL;
      sprintf_smart(&field, FIELD_NZ_PATTERNS "[%zu]", i);
      Serializable_setArrayField(serializable, field, PROPERTIES_INT, solver->nz_patterns[i], sizeof(int), &nz_patterns_size_i, NULL, 0, false, NULL, passport);
      free(field);
    }
  }
  if (solver->nz_pattern_sizes != NULL) {
    size_t nz_pattern_sizes_size = (size_t) solver->A12->nbr_cols;
    MCLabFMILog_verbose("nz_pattern_sizes[%zu]", nz_pattern_sizes_size);
    for (size_t i = 0; i < (size_t) solver->A12->nbr_cols; ++i) {
      size_t nz_pattern_sizes_size_i = (size_t) solver->nz_sizes[i];
      MCLabFMILog_verbose("nz_pattern_sizes[%zu][%zu]", i, nz_pattern_sizes_size_i);
      char *field = NULL;
      sprintf_smart(&field, FIELD_NZ_PATTERN_SIZES "[%zu]", i);
      Serializable_setArrayField(serializable, field, PROPERTIES_INT, solver->nz_pattern_sizes[i], sizeof(int), &nz_pattern_sizes_size_i, NULL, 0, false, NULL, passport);
      free(field);
    }
  }
  if (solver->M1_patterns != NULL) {
    size_t M1_patterns_size = (size_t) solver->A12->nbr_cols;
    MCLabFMILog_verbose("M1_patterns[%zu]", M1_patterns_size);
    for (size_t i = 0; i < (size_t) solver->A12->nbr_cols; ++i) {
      size_t M1_patterns_size_i = (size_t) solver->M1_sizes[i];
      MCLabFMILog_verbose("M1_patterns[%zu][%zu]", i, M1_patterns_size_i);
      char *field = NULL;
      sprintf_smart(&field, FIELD_M1_PATTERNS "[%zu]", i);
      Serializable_setArrayField(serializable, field, PROPERTIES_INT, solver->M1_patterns[i], sizeof(int), &M1_patterns_size_i, NULL, 0, false, NULL, passport);
      free(field);
    }
  }
  if (solver->nz_sizes != NULL) {
    size_t nz_sizes_size = (size_t) solver->A12->nbr_cols;
    MCLabFMILog_verbose("nz_sizes[%zu]", nz_sizes_size);
    Serializable_setArrayField(serializable, FIELD_NZ_SIZES, PROPERTIES_INT, solver->nz_sizes, sizeof(int), &nz_sizes_size, NULL, 0, false, NULL, passport);
  }
  if (solver->M1_sizes != NULL) {
    size_t M1_sizes_size = (size_t) solver->A12->nbr_cols;
    MCLabFMILog_verbose("M1_sizes[%zu]", M1_sizes_size);
    Serializable_setArrayField(serializable, FIELD_M1_SIZES, PROPERTIES_INT, solver->M1_sizes, sizeof(int), &M1_sizes_size, NULL, 0, false, NULL, passport);
  }
  if (solver->nz_offsets != NULL) {
    size_t nz_offsets_size = (size_t) solver->A12->nbr_cols;
    MCLabFMILog_verbose("nz_offsets[%zu]", nz_offsets_size);
    Serializable_setArrayField(serializable, FIELD_NZ_OFFSETS, PROPERTIES_INT, solver->nz_offsets, sizeof(int), &nz_offsets_size, NULL, 0, false, NULL, passport);
  }
  if (solver->work_x != NULL) {
    size_t max_dim = (size_t) (solver->L->nbr_cols > solver->A22->nbr_cols ? solver->L->nbr_cols : solver->A22->nbr_cols);
    size_t work_x_sizes[2] = {solver->max_threads, max_dim};
    MCLabFMILog_verbose("work_x[%zu][%zu]", work_x_sizes[0], work_x_sizes[1]);
    Serializable_setArrayField(serializable, FIELD_WORK_X, PROPERTIES_DOUBLE, solver->work_x, sizeof(double), work_x_sizes, NULL, 1, false, NULL, passport);
  }

  MCLabFMILog_verbose("max_threads = %d", solver->max_threads);
  Serializable_setField(serializable, FIELD_MAX_THREADS, PROPERTIES_INT, &solver->max_threads, passport);

  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_LINEAR_SOLVER_SPARSE_T *ser_solver = (SerializableJMI_LINEAR_SOLVER_SPARSE_T *)this_v;
  jmi_linear_solver_sparse_t *solver = ser_solver->solver;
  const void *passport = ser_solver->passport;
  Serializable *serializable = ser_solver->serializable;
  ser_solver->ser_L= *(SerializableJMI_MATRIX_SPARSE_CSC_T **) Serializable_getField(serializable, FIELD_L, passport);
  ser_solver->ser_A12= *(SerializableJMI_MATRIX_SPARSE_CSC_T **) Serializable_getField(serializable, FIELD_A12, passport);
  ser_solver->ser_A21= *(SerializableJMI_MATRIX_SPARSE_CSC_T **) Serializable_getField(serializable, FIELD_A21, passport);
  ser_solver->ser_A22= *(SerializableJMI_MATRIX_SPARSE_CSC_T **) Serializable_getField(serializable, FIELD_A22, passport);
  ser_solver->ser_M1= *(SerializableJMI_MATRIX_SPARSE_CSC_T **) Serializable_getField(serializable, FIELD_M1, passport);
  // Deserializing simple fields via script
  if (solver->nz_sizes != NULL) {
    size_t nz_sizes_size = (size_t) solver->A12->nbr_cols;
    MCLabFMILog_verbose("nz_sizes[%zu]", nz_sizes_size);
    Serializable_getArrayField(serializable, FIELD_NZ_SIZES, solver->nz_sizes, sizeof(int), &nz_sizes_size, 0, passport);
  }
  if (solver->nz_patterns != NULL) {
    size_t nz_patterns_size = (size_t) solver->A12->nbr_cols;
    MCLabFMILog_verbose("nz_patterns[%zu]", nz_patterns_size);
    for (size_t i = 0; i < (size_t) solver->A12->nbr_cols; ++i) {
      size_t nz_patterns_size_i = (size_t) solver->nz_sizes[i];
      MCLabFMILog_verbose("nz_patterns[%zu][%zu]", i, nz_patterns_size_i);
      char *field = NULL;
      sprintf_smart(&field, FIELD_NZ_PATTERNS "[%zu]", i);
      Serializable_getArrayField(serializable, field, solver->nz_patterns[i], sizeof(int), &nz_patterns_size_i, 0, passport);
      free(field);
    }
  }
  if (solver->nz_pattern_sizes != NULL) {
    size_t nz_pattern_sizes_size = (size_t) solver->A12->nbr_cols;
    MCLabFMILog_verbose("nz_pattern_sizes[%zu]", nz_pattern_sizes_size);
    for (size_t i = 0; i < (size_t) solver->A12->nbr_cols; ++i) {
      size_t nz_pattern_sizes_size_i = (size_t) solver->nz_sizes[i];
      MCLabFMILog_verbose("nz_pattern_sizes[%zu][%zu]", i, nz_pattern_sizes_size_i);
      char *field = NULL;
      sprintf_smart(&field, FIELD_NZ_PATTERN_SIZES "[%zu]", i);
      Serializable_getArrayField(serializable, field, solver->nz_pattern_sizes[i], sizeof(int), &nz_pattern_sizes_size_i, 0, passport);
      free(field);
    }
  }
  if (solver->M1_sizes != NULL) {
    size_t M1_sizes_size = (size_t) solver->A12->nbr_cols;
    MCLabFMILog_verbose("M1_sizes[%zu]", M1_sizes_size);
    Serializable_getArrayField(serializable, FIELD_M1_SIZES, solver->M1_sizes, sizeof(int), &M1_sizes_size, 0, passport);
  }
  if (solver->M1_patterns != NULL) {
    size_t M1_patterns_size = (size_t) solver->A12->nbr_cols;
    MCLabFMILog_verbose("M1_patterns[%zu]", M1_patterns_size);
    for (size_t i = 0; i < (size_t) solver->A12->nbr_cols; ++i) {
      size_t M1_patterns_size_i = (size_t) solver->M1_sizes[i];
      MCLabFMILog_verbose("M1_patterns[%zu][%zu]", i, M1_patterns_size_i);
      char *field = NULL;
      sprintf_smart(&field, FIELD_M1_PATTERNS "[%zu]", i);
      Serializable_getArrayField(serializable, field, solver->M1_patterns[i], sizeof(int), &M1_patterns_size_i, 0, passport);
      free(field);
    }
  }
  if (solver->nz_offsets != NULL) {
    size_t nz_offsets_size = (size_t) solver->A12->nbr_cols;
    MCLabFMILog_verbose("nz_offsets[%zu]", nz_offsets_size);
    Serializable_getArrayField(serializable, FIELD_NZ_OFFSETS, solver->nz_offsets, sizeof(int), &nz_offsets_size, 0, passport);
  }
  if (solver->work_x != NULL) {
    size_t max_dim = (size_t) (solver->L->nbr_cols > solver->A22->nbr_cols ? solver->L->nbr_cols : solver->A22->nbr_cols);
    size_t work_x_sizes[2] = {solver->max_threads, max_dim};
    MCLabFMILog_verbose("work_x[%zu][%zu]", work_x_sizes[0], work_x_sizes[1]);
    Serializable_getArrayField(serializable, FIELD_WORK_X, solver->work_x, sizeof(double), work_x_sizes, 1, passport);
  }

  solver->max_threads= *(int *) Serializable_getField(serializable, FIELD_MAX_THREADS, passport);
  MCLabFMILog_verbose("max_threads = %d", solver->max_threads);

  MCLabFMILog_unnest();
}

SerializableJMI_LINEAR_SOLVER_SPARSE_T *SerializableJMI_LINEAR_SOLVER_SPARSE_T_new(MCLabFMIEnv *fmi_env, jmi_linear_solver_sparse_t *solver) {
  Debug_assert(DEBUG_ALWAYS, solver != NULL, "solver == NULL");
  SerializableJMI_LINEAR_SOLVER_SPARSE_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, solver)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_LINEAR_SOLVER_SPARSE_T));
    new->solver = solver;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, solver, new,
                    SerializableJMI_LINEAR_SOLVER_SPARSE_T_free, new->passport);
    // Adding inner struct new
    if (solver->L != NULL) {
      new->ser_L = SerializableJMI_MATRIX_SPARSE_CSC_T_new(fmi_env, solver->L);
    }
    if (solver->A12 != NULL) {
      new->ser_A12 = SerializableJMI_MATRIX_SPARSE_CSC_T_new(fmi_env, solver->A12);
    }
    if (solver->A21 != NULL) {
      new->ser_A21 = SerializableJMI_MATRIX_SPARSE_CSC_T_new(fmi_env, solver->A21);
    }
    if (solver->A22 != NULL) {
      new->ser_A22 = SerializableJMI_MATRIX_SPARSE_CSC_T_new(fmi_env, solver->A22);
    }
    if (solver->M1 != NULL) {
      new->ser_M1 = SerializableJMI_MATRIX_SPARSE_CSC_T_new(fmi_env, solver->M1);
    }
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_LINEAR_SOLVER_SPARSE_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_LINEAR_SOLVER_SPARSE_T *this_ = (SerializableJMI_LINEAR_SOLVER_SPARSE_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_LINEAR_SOLVER_SPARSE_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_LINEAR_SOLVER_SPARSE_T *this_ = (SerializableJMI_LINEAR_SOLVER_SPARSE_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {
      .className = className,
      .passport = this_->passport,
      .serializeFields = serializeFields,
      .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
