/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi_block_solver.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_BLOCK_SOLVER_OPTIONS_T.h"

#define DEBUG "SerializableJMI_BLOCK_SOLVER_OPTIONS_T"

#define CLASS_NAME "jmi_block_solver_options_t"

// defining constants for fields name
#define FIELD_RES_TOL "res_tol"
#define FIELD_MIN_TOL "min_tol"
#define FIELD_STEP_LIMIT_FACTOR "step_limit_factor"
#define FIELD_REGULARIZATION_TOLERANCE "regularization_tolerance"
#define FIELD_MAX_ITER "max_iter"
#define FIELD_MAX_ITER_NO_JACOBIAN "max_iter_no_jacobian"
#define FIELD_SOLVER_EXIT_CRITERION_MODE "solver_exit_criterion_mode"
#define FIELD_ENFORCE_BOUNDS_FLAG "enforce_bounds_flag"
#define FIELD_USE_JACOBIAN_EQUILIBRATION_FLAG "use_jacobian_equilibration_flag"
#define FIELD_USE_BRENT_IN_1D_FLAG "use_Brent_in_1d_flag"
#define FIELD_BLOCK_JACOBIAN_CHECK "block_jacobian_check"
#define FIELD_BLOCK_JACOBIAN_CHECK_TOL "block_jacobian_check_tol"
#define FIELD_JACOBIAN_UPDATE_MODE "jacobian_update_mode"
#define FIELD_JACOBIAN_CALCULATION_MODE "jacobian_calculation_mode"
#define FIELD_RESIDUAL_EQUATION_SCALING_MODE "residual_equation_scaling_mode"
#define FIELD_ACTIVE_BOUNDS_MODE "active_bounds_mode"
#define FIELD_MAX_RESIDUAL_SCALING_FACTOR "max_residual_scaling_factor"
#define FIELD_MIN_RESIDUAL_SCALING_FACTOR "min_residual_scaling_factor"
#define FIELD_ITERATION_VARIABLE_SCALING_MODE "iteration_variable_scaling_mode"
#define FIELD_RESCALE_EACH_STEP_FLAG "rescale_each_step_flag"
#define FIELD_RESCALE_AFTER_SINGULAR_JAC_FLAG "rescale_after_singular_jac_flag"
#define FIELD_CHECK_JAC_COND_FLAG "check_jac_cond_flag"
#define FIELD_BRENT_IGNORE_ERROR_FLAG "brent_ignore_error_flag"
#define FIELD_EXPERIMENTAL_MODE "experimental_mode"
#define FIELD_EVENTS_EPSILON "events_epsilon"
#define FIELD_TIME_EVENTS_EPSILON "time_events_epsilon"
#define FIELD_USE_NEWTON_FOR_BRENT "use_newton_for_brent"
#define FIELD_ACTIVE_BOUNDS_THRESHOLD "active_bounds_threshold"
#define FIELD_USE_NOMINALS_AS_FALLBACK_IN_INIT "use_nominals_as_fallback_in_init"
#define FIELD_START_FROM_LAST_INTEGRATOR_STEP "start_from_last_integrator_step"
#define FIELD_JACOBIAN_FINITE_DIFFERENCE_DELTA "jacobian_finite_difference_delta"
#define FIELD_BLOCK_PROFILING "block_profiling"
#define FIELD_SOLVER "solver"
#define FIELD_JACOBIAN_VARIABILITY "jacobian_variability"
#define FIELD_LABEL "label"

struct SerializableJMI_BLOCK_SOLVER_OPTIONS_T {
  jmi_block_solver_options_t *jmi_block_solver_options;
  // Adding inner struct
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_BLOCK_SOLVER_OPTIONS_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_BLOCK_SOLVER_OPTIONS_T *ser_jmi_block_solver_options = (SerializableJMI_BLOCK_SOLVER_OPTIONS_T *)this_v;
  return ser_jmi_block_solver_options->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_BLOCK_SOLVER_OPTIONS_T *ser_jmi_block_solver_options = (SerializableJMI_BLOCK_SOLVER_OPTIONS_T *)this_v;
  jmi_block_solver_options_t *jmi_block_solver_options = ser_jmi_block_solver_options->jmi_block_solver_options;
  const void *passport = ser_jmi_block_solver_options->passport;
  Serializable *serializable = ser_jmi_block_solver_options->serializable;
  MCLabFMILog_verbose("res_tol: %g", jmi_block_solver_options->res_tol);
  Serializable_setField(serializable, FIELD_RES_TOL, PROPERTIES_DOUBLE,
                       &jmi_block_solver_options->res_tol, passport);
  MCLabFMILog_verbose("min_tol: %g", jmi_block_solver_options->min_tol);
  Serializable_setField(serializable, FIELD_MIN_TOL, PROPERTIES_DOUBLE,
                       &jmi_block_solver_options->min_tol, passport);
  MCLabFMILog_verbose("step_limit_factor: %g", jmi_block_solver_options->step_limit_factor);
  Serializable_setField(serializable, FIELD_STEP_LIMIT_FACTOR, PROPERTIES_DOUBLE,
                       &jmi_block_solver_options->step_limit_factor, passport);
  MCLabFMILog_verbose("regularization_tolerance: %g", jmi_block_solver_options->regularization_tolerance);
  Serializable_setField(serializable, FIELD_REGULARIZATION_TOLERANCE, PROPERTIES_DOUBLE,
                       &jmi_block_solver_options->regularization_tolerance, passport);
  MCLabFMILog_verbose("max_iter: %d", jmi_block_solver_options->max_iter);
  Serializable_setField(serializable, FIELD_MAX_ITER, PROPERTIES_INT,
                       &jmi_block_solver_options->max_iter, passport);
  MCLabFMILog_verbose("max_iter_no_jacobian: %d", jmi_block_solver_options->max_iter_no_jacobian);
  Serializable_setField(serializable, FIELD_MAX_ITER_NO_JACOBIAN, PROPERTIES_INT,
                       &jmi_block_solver_options->max_iter_no_jacobian, passport);
  MCLabFMILog_verbose("solver_exit_criterion_mode: %d", jmi_block_solver_options->solver_exit_criterion_mode);
  Serializable_setField(serializable, FIELD_SOLVER_EXIT_CRITERION_MODE, PROPERTIES_INT,
                       &jmi_block_solver_options->solver_exit_criterion_mode, passport);
  MCLabFMILog_verbose("enforce_bounds_flag: %d", jmi_block_solver_options->enforce_bounds_flag);
  Serializable_setField(serializable, FIELD_ENFORCE_BOUNDS_FLAG, PROPERTIES_INT,
                       &jmi_block_solver_options->enforce_bounds_flag, passport);
  MCLabFMILog_verbose("use_jacobian_equilibration_flag: %d", jmi_block_solver_options->use_jacobian_equilibration_flag);
  Serializable_setField(serializable, FIELD_USE_JACOBIAN_EQUILIBRATION_FLAG, PROPERTIES_INT,
                       &jmi_block_solver_options->use_jacobian_equilibration_flag, passport);
  MCLabFMILog_verbose("use_Brent_in_1d_flag: %d", jmi_block_solver_options->use_Brent_in_1d_flag);
  Serializable_setField(serializable, FIELD_USE_BRENT_IN_1D_FLAG, PROPERTIES_INT,
                       &jmi_block_solver_options->use_Brent_in_1d_flag, passport);
  MCLabFMILog_verbose("block_jacobian_check: %d", jmi_block_solver_options->block_jacobian_check);
  Serializable_setField(serializable, FIELD_BLOCK_JACOBIAN_CHECK, PROPERTIES_INT,
                       &jmi_block_solver_options->block_jacobian_check, passport);
  MCLabFMILog_verbose("block_jacobian_check_tol: %g", jmi_block_solver_options->block_jacobian_check_tol);
  Serializable_setField(serializable, FIELD_BLOCK_JACOBIAN_CHECK_TOL, PROPERTIES_DOUBLE,
                       &jmi_block_solver_options->block_jacobian_check_tol, passport);
  MCLabFMILog_verbose("jacobian_update_mode: %d", jmi_block_solver_options->jacobian_update_mode);
  Serializable_setField(serializable, FIELD_JACOBIAN_UPDATE_MODE, PROPERTIES_INT,
                       &jmi_block_solver_options->jacobian_update_mode, passport);
  MCLabFMILog_verbose("jacobian_calculation_mode: %d", jmi_block_solver_options->jacobian_calculation_mode);
  Serializable_setField(serializable, FIELD_JACOBIAN_CALCULATION_MODE, PROPERTIES_INT,
                       &jmi_block_solver_options->jacobian_calculation_mode, passport);
  MCLabFMILog_verbose("residual_equation_scaling_mode: %d", jmi_block_solver_options->residual_equation_scaling_mode);
  Serializable_setField(serializable, FIELD_RESIDUAL_EQUATION_SCALING_MODE, PROPERTIES_INT,
                       &jmi_block_solver_options->residual_equation_scaling_mode, passport);
  MCLabFMILog_verbose("active_bounds_mode: %d", jmi_block_solver_options->active_bounds_mode);
  Serializable_setField(serializable, FIELD_ACTIVE_BOUNDS_MODE, PROPERTIES_INT,
                       &jmi_block_solver_options->active_bounds_mode, passport);
  MCLabFMILog_verbose("max_residual_scaling_factor: %g", jmi_block_solver_options->max_residual_scaling_factor);
  Serializable_setField(serializable, FIELD_MAX_RESIDUAL_SCALING_FACTOR, PROPERTIES_DOUBLE,
                       &jmi_block_solver_options->max_residual_scaling_factor, passport);
  MCLabFMILog_verbose("min_residual_scaling_factor: %g", jmi_block_solver_options->min_residual_scaling_factor);
  Serializable_setField(serializable, FIELD_MIN_RESIDUAL_SCALING_FACTOR, PROPERTIES_DOUBLE,
                       &jmi_block_solver_options->min_residual_scaling_factor, passport);
  MCLabFMILog_verbose("iteration_variable_scaling_mode: %d", jmi_block_solver_options->iteration_variable_scaling_mode);
  Serializable_setField(serializable, FIELD_ITERATION_VARIABLE_SCALING_MODE, PROPERTIES_INT,
                       &jmi_block_solver_options->iteration_variable_scaling_mode, passport);
  MCLabFMILog_verbose("rescale_each_step_flag: %d", jmi_block_solver_options->rescale_each_step_flag);
  Serializable_setField(serializable, FIELD_RESCALE_EACH_STEP_FLAG, PROPERTIES_INT,
                       &jmi_block_solver_options->rescale_each_step_flag, passport);
  MCLabFMILog_verbose("rescale_after_singular_jac_flag: %d", jmi_block_solver_options->rescale_after_singular_jac_flag);
  Serializable_setField(serializable, FIELD_RESCALE_AFTER_SINGULAR_JAC_FLAG, PROPERTIES_INT,
                       &jmi_block_solver_options->rescale_after_singular_jac_flag, passport);
  MCLabFMILog_verbose("check_jac_cond_flag: %d", jmi_block_solver_options->check_jac_cond_flag);
  Serializable_setField(serializable, FIELD_CHECK_JAC_COND_FLAG, PROPERTIES_INT,
                       &jmi_block_solver_options->check_jac_cond_flag, passport);
  MCLabFMILog_verbose("brent_ignore_error_flag: %d", jmi_block_solver_options->brent_ignore_error_flag);
  Serializable_setField(serializable, FIELD_BRENT_IGNORE_ERROR_FLAG, PROPERTIES_INT,
                       &jmi_block_solver_options->brent_ignore_error_flag, passport);
  MCLabFMILog_verbose("experimental_mode: %d", jmi_block_solver_options->experimental_mode);
  Serializable_setField(serializable, FIELD_EXPERIMENTAL_MODE, PROPERTIES_INT,
                       &jmi_block_solver_options->experimental_mode, passport);
  MCLabFMILog_verbose("events_epsilon: %g", jmi_block_solver_options->events_epsilon);
  Serializable_setField(serializable, FIELD_EVENTS_EPSILON, PROPERTIES_DOUBLE,
                       &jmi_block_solver_options->events_epsilon, passport);
  MCLabFMILog_verbose("time_events_epsilon: %g", jmi_block_solver_options->time_events_epsilon);
  Serializable_setField(serializable, FIELD_TIME_EVENTS_EPSILON, PROPERTIES_DOUBLE,
                       &jmi_block_solver_options->time_events_epsilon, passport);
  MCLabFMILog_verbose("use_newton_for_brent: %d", jmi_block_solver_options->use_newton_for_brent);
  Serializable_setField(serializable, FIELD_USE_NEWTON_FOR_BRENT, PROPERTIES_INT,
                       &jmi_block_solver_options->use_newton_for_brent, passport);
  MCLabFMILog_verbose("active_bounds_threshold: %g", jmi_block_solver_options->active_bounds_threshold);
  Serializable_setField(serializable, FIELD_ACTIVE_BOUNDS_THRESHOLD, PROPERTIES_DOUBLE,
                       &jmi_block_solver_options->active_bounds_threshold, passport);
  MCLabFMILog_verbose("use_nominals_as_fallback_in_init: %d", jmi_block_solver_options->use_nominals_as_fallback_in_init);
  Serializable_setField(serializable, FIELD_USE_NOMINALS_AS_FALLBACK_IN_INIT, PROPERTIES_INT,
                       &jmi_block_solver_options->use_nominals_as_fallback_in_init, passport);
  MCLabFMILog_verbose("start_from_last_integrator_step: %d", jmi_block_solver_options->start_from_last_integrator_step);
  Serializable_setField(serializable, FIELD_START_FROM_LAST_INTEGRATOR_STEP, PROPERTIES_INT,
                       &jmi_block_solver_options->start_from_last_integrator_step, passport);
  MCLabFMILog_verbose("jacobian_finite_difference_delta: %g", jmi_block_solver_options->jacobian_finite_difference_delta);
  Serializable_setField(serializable, FIELD_JACOBIAN_FINITE_DIFFERENCE_DELTA, PROPERTIES_DOUBLE,
                       &jmi_block_solver_options->jacobian_finite_difference_delta, passport);
  MCLabFMILog_verbose("block_profiling: %d", jmi_block_solver_options->block_profiling);
  Serializable_setField(serializable, FIELD_BLOCK_PROFILING, PROPERTIES_INT,
                       &jmi_block_solver_options->block_profiling, passport);
  MCLabFMILog_verbose("solver: %d", jmi_block_solver_options->solver);
  Serializable_setField(serializable, FIELD_SOLVER, PROPERTIES_INT,
                       &jmi_block_solver_options->solver, passport);
  MCLabFMILog_verbose("jacobian_variability: %d", jmi_block_solver_options->jacobian_variability);
  Serializable_setField(serializable, FIELD_JACOBIAN_VARIABILITY, PROPERTIES_INT,
                       &jmi_block_solver_options->jacobian_variability, passport);
  MCLabFMILog_verbose("label: %s", jmi_block_solver_options->label);
  Serializable_setField(serializable, FIELD_LABEL, PROPERTIES_STRING,
                       (void *)jmi_block_solver_options->label, passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_BLOCK_SOLVER_OPTIONS_T *ser_jmi_block_solver_options = (SerializableJMI_BLOCK_SOLVER_OPTIONS_T *)this_v;
  jmi_block_solver_options_t *jmi_block_solver_options = ser_jmi_block_solver_options->jmi_block_solver_options;
  const void *passport = ser_jmi_block_solver_options->passport;
  Serializable *serializable = ser_jmi_block_solver_options->serializable;
  // Deserializing simple fields via script
  jmi_block_solver_options->res_tol =
    *(double *) Serializable_getField(serializable, FIELD_RES_TOL, passport);
  MCLabFMILog_verbose("res_tol: %g", jmi_block_solver_options->res_tol);
  jmi_block_solver_options->min_tol =
    *(double *) Serializable_getField(serializable, FIELD_MIN_TOL, passport);
  MCLabFMILog_verbose("min_tol: %g", jmi_block_solver_options->min_tol);
  jmi_block_solver_options->step_limit_factor =
    *(double *) Serializable_getField(serializable, FIELD_STEP_LIMIT_FACTOR, passport);
  MCLabFMILog_verbose("step_limit_factor: %g", jmi_block_solver_options->step_limit_factor);
  jmi_block_solver_options->regularization_tolerance =
    *(double *) Serializable_getField(serializable, FIELD_REGULARIZATION_TOLERANCE, passport);
  MCLabFMILog_verbose("regularization_tolerance: %g", jmi_block_solver_options->regularization_tolerance);
  jmi_block_solver_options->max_iter =
    *(int *) Serializable_getField(serializable, FIELD_MAX_ITER, passport);
  MCLabFMILog_verbose("max_iter: %d", jmi_block_solver_options->max_iter);
  jmi_block_solver_options->max_iter_no_jacobian =
    *(int *) Serializable_getField(serializable, FIELD_MAX_ITER_NO_JACOBIAN, passport);
  MCLabFMILog_verbose("max_iter_no_jacobian: %d", jmi_block_solver_options->max_iter_no_jacobian);
  jmi_block_solver_options->solver_exit_criterion_mode =
    *(int *) Serializable_getField(serializable, FIELD_SOLVER_EXIT_CRITERION_MODE, passport);
  MCLabFMILog_verbose("solver_exit_criterion_mode: %d", jmi_block_solver_options->solver_exit_criterion_mode);
  jmi_block_solver_options->enforce_bounds_flag =
    *(int *) Serializable_getField(serializable, FIELD_ENFORCE_BOUNDS_FLAG, passport);
  MCLabFMILog_verbose("enforce_bounds_flag: %d", jmi_block_solver_options->enforce_bounds_flag);
  jmi_block_solver_options->use_jacobian_equilibration_flag =
    *(int *) Serializable_getField(serializable, FIELD_USE_JACOBIAN_EQUILIBRATION_FLAG, passport);
  MCLabFMILog_verbose("use_jacobian_equilibration_flag: %d", jmi_block_solver_options->use_jacobian_equilibration_flag);
  jmi_block_solver_options->use_Brent_in_1d_flag =
    *(int *) Serializable_getField(serializable, FIELD_USE_BRENT_IN_1D_FLAG, passport);
  MCLabFMILog_verbose("use_Brent_in_1d_flag: %d", jmi_block_solver_options->use_Brent_in_1d_flag);
  jmi_block_solver_options->block_jacobian_check =
    *(int *) Serializable_getField(serializable, FIELD_BLOCK_JACOBIAN_CHECK, passport);
  MCLabFMILog_verbose("block_jacobian_check: %d", jmi_block_solver_options->block_jacobian_check);
  jmi_block_solver_options->block_jacobian_check_tol =
    *(double *) Serializable_getField(serializable, FIELD_BLOCK_JACOBIAN_CHECK_TOL, passport);
  MCLabFMILog_verbose("block_jacobian_check_tol: %g", jmi_block_solver_options->block_jacobian_check_tol);
  jmi_block_solver_options->jacobian_update_mode =
    *(int *) Serializable_getField(serializable, FIELD_JACOBIAN_UPDATE_MODE, passport);
  MCLabFMILog_verbose("jacobian_update_mode: %d", jmi_block_solver_options->jacobian_update_mode);
  jmi_block_solver_options->jacobian_calculation_mode =
    *(int *) Serializable_getField(serializable, FIELD_JACOBIAN_CALCULATION_MODE, passport);
  MCLabFMILog_verbose("jacobian_calculation_mode: %d", jmi_block_solver_options->jacobian_calculation_mode);
  jmi_block_solver_options->residual_equation_scaling_mode =
    *(int *) Serializable_getField(serializable, FIELD_RESIDUAL_EQUATION_SCALING_MODE, passport);
  MCLabFMILog_verbose("residual_equation_scaling_mode: %d", jmi_block_solver_options->residual_equation_scaling_mode);
  jmi_block_solver_options->active_bounds_mode =
    *(int *) Serializable_getField(serializable, FIELD_ACTIVE_BOUNDS_MODE, passport);
  MCLabFMILog_verbose("active_bounds_mode: %d", jmi_block_solver_options->active_bounds_mode);
  jmi_block_solver_options->max_residual_scaling_factor =
    *(double *) Serializable_getField(serializable, FIELD_MAX_RESIDUAL_SCALING_FACTOR, passport);
  MCLabFMILog_verbose("max_residual_scaling_factor: %g", jmi_block_solver_options->max_residual_scaling_factor);
  jmi_block_solver_options->min_residual_scaling_factor =
    *(double *) Serializable_getField(serializable, FIELD_MIN_RESIDUAL_SCALING_FACTOR, passport);
  MCLabFMILog_verbose("min_residual_scaling_factor: %g", jmi_block_solver_options->min_residual_scaling_factor);
  jmi_block_solver_options->iteration_variable_scaling_mode =
    *(int *) Serializable_getField(serializable, FIELD_ITERATION_VARIABLE_SCALING_MODE, passport);
  MCLabFMILog_verbose("iteration_variable_scaling_mode: %d", jmi_block_solver_options->iteration_variable_scaling_mode);
  jmi_block_solver_options->rescale_each_step_flag =
    *(int *) Serializable_getField(serializable, FIELD_RESCALE_EACH_STEP_FLAG, passport);
  MCLabFMILog_verbose("rescale_each_step_flag: %d", jmi_block_solver_options->rescale_each_step_flag);
  jmi_block_solver_options->rescale_after_singular_jac_flag =
    *(int *) Serializable_getField(serializable, FIELD_RESCALE_AFTER_SINGULAR_JAC_FLAG, passport);
  MCLabFMILog_verbose("rescale_after_singular_jac_flag: %d", jmi_block_solver_options->rescale_after_singular_jac_flag);
  jmi_block_solver_options->check_jac_cond_flag =
    *(int *) Serializable_getField(serializable, FIELD_CHECK_JAC_COND_FLAG, passport);
  MCLabFMILog_verbose("check_jac_cond_flag: %d", jmi_block_solver_options->check_jac_cond_flag);
  jmi_block_solver_options->brent_ignore_error_flag =
    *(int *) Serializable_getField(serializable, FIELD_BRENT_IGNORE_ERROR_FLAG, passport);
  MCLabFMILog_verbose("brent_ignore_error_flag: %d", jmi_block_solver_options->brent_ignore_error_flag);
  jmi_block_solver_options->experimental_mode =
    *(int *) Serializable_getField(serializable, FIELD_EXPERIMENTAL_MODE, passport);
  MCLabFMILog_verbose("experimental_mode: %d", jmi_block_solver_options->experimental_mode);
  jmi_block_solver_options->events_epsilon =
    *(double *) Serializable_getField(serializable, FIELD_EVENTS_EPSILON, passport);
  MCLabFMILog_verbose("events_epsilon: %g", jmi_block_solver_options->events_epsilon);
  jmi_block_solver_options->time_events_epsilon =
    *(double *) Serializable_getField(serializable, FIELD_TIME_EVENTS_EPSILON, passport);
  MCLabFMILog_verbose("time_events_epsilon: %g", jmi_block_solver_options->time_events_epsilon);
  jmi_block_solver_options->use_newton_for_brent =
    *(int *) Serializable_getField(serializable, FIELD_USE_NEWTON_FOR_BRENT, passport);
  MCLabFMILog_verbose("use_newton_for_brent: %d", jmi_block_solver_options->use_newton_for_brent);
  jmi_block_solver_options->active_bounds_threshold =
    *(double *) Serializable_getField(serializable, FIELD_ACTIVE_BOUNDS_THRESHOLD, passport);
  MCLabFMILog_verbose("active_bounds_threshold: %g", jmi_block_solver_options->active_bounds_threshold);
  jmi_block_solver_options->use_nominals_as_fallback_in_init =
    *(int *) Serializable_getField(serializable, FIELD_USE_NOMINALS_AS_FALLBACK_IN_INIT, passport);
  MCLabFMILog_verbose("use_nominals_as_fallback_in_init: %d", jmi_block_solver_options->use_nominals_as_fallback_in_init);
  jmi_block_solver_options->start_from_last_integrator_step =
    *(int *) Serializable_getField(serializable, FIELD_START_FROM_LAST_INTEGRATOR_STEP, passport);
  MCLabFMILog_verbose("start_from_last_integrator_step: %d", jmi_block_solver_options->start_from_last_integrator_step);
  jmi_block_solver_options->jacobian_finite_difference_delta =
    *(double *) Serializable_getField(serializable, FIELD_JACOBIAN_FINITE_DIFFERENCE_DELTA, passport);
  MCLabFMILog_verbose("jacobian_finite_difference_delta: %g", jmi_block_solver_options->jacobian_finite_difference_delta);
  jmi_block_solver_options->block_profiling =
    *(int *) Serializable_getField(serializable, FIELD_BLOCK_PROFILING, passport);
  MCLabFMILog_verbose("block_profiling: %d", jmi_block_solver_options->block_profiling);
  jmi_block_solver_options->solver =
    *(int *) Serializable_getField(serializable, FIELD_SOLVER, passport);
  MCLabFMILog_verbose("solver: %d", jmi_block_solver_options->solver);
  jmi_block_solver_options->jacobian_variability =
    *(int *) Serializable_getField(serializable, FIELD_JACOBIAN_VARIABILITY, passport);
  MCLabFMILog_verbose("jacobian_variability: %d", jmi_block_solver_options->jacobian_variability);
  SAFETY_CHECK_STRING(serializable, jmi_block_solver_options->label, FIELD_LABEL, passport);
  MCLabFMILog_verbose("label: %s", jmi_block_solver_options->label);
  MCLabFMILog_unnest();
}

SerializableJMI_BLOCK_SOLVER_OPTIONS_T *SerializableJMI_BLOCK_SOLVER_OPTIONS_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_block_solver_options_t *jmi_block_solver_options) {
  Debug_assert(DEBUG_ALWAYS, jmi_block_solver_options != NULL, "jmi_block_solver_options == NULL");
  SerializableJMI_BLOCK_SOLVER_OPTIONS_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_block_solver_options)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_BLOCK_SOLVER_OPTIONS_T));
    new->jmi_block_solver_options = jmi_block_solver_options;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_block_solver_options, new,
                    SerializableJMI_BLOCK_SOLVER_OPTIONS_T_free, new->passport);
    // Adding inner struct new
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_BLOCK_SOLVER_OPTIONS_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_BLOCK_SOLVER_OPTIONS_T *this_ = (SerializableJMI_BLOCK_SOLVER_OPTIONS_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_BLOCK_SOLVER_OPTIONS_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_BLOCK_SOLVER_OPTIONS_T *this_ = (SerializableJMI_BLOCK_SOLVER_OPTIONS_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
