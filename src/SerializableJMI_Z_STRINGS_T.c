/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "SerializableJMI_Z_OFFSETS_T.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_Z_STRINGS_T.h"

#define DEBUG "SerializableJMI_Z_STRINGS_T"

#define CLASS_NAME "jmi_z_strings_t"

// defining constants for fields name
#define FIELD_VALUES "values"
#define FIELD_OFFSETS "offsets"

struct SerializableJMI_Z_STRINGS_T {
  jmi_z_strings_t *jmi_z_strings;
  // Adding inner struct
  SerializableJMI_Z_OFFSETS_T *ser_offsets;
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_Z_STRINGS_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_Z_STRINGS_T *ser_jmi_z_strings = (SerializableJMI_Z_STRINGS_T *)this_v;
  return ser_jmi_z_strings->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_Z_STRINGS_T *ser_jmi_z_strings = (SerializableJMI_Z_STRINGS_T *)this_v;
  jmi_z_strings_t *jmi_z_strings = ser_jmi_z_strings->jmi_z_strings;
  const void *passport = ser_jmi_z_strings->passport;
  Serializable *serializable = ser_jmi_z_strings->serializable;
  MCLabFMILog_verbose("values[%d]", jmi_z_strings->offsets.n);
  size_t values_size = jmi_z_strings->offsets.n;
  Serializable_setArrayField(serializable, FIELD_VALUES, PROPERTIES_POINTER, (void *)jmi_z_strings->values,
  	                        sizeof(char *), &values_size, NULL, 0, false, NULL, passport);
  Serializable_setField(serializable, FIELD_OFFSETS, PROPERTIES_POINTER,
                       SerializableJMI_Z_OFFSETS_T_asSerializable(ser_jmi_z_strings->ser_offsets), passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_Z_STRINGS_T *ser_jmi_z_strings = (SerializableJMI_Z_STRINGS_T *)this_v;
  jmi_z_strings_t *jmi_z_strings = ser_jmi_z_strings->jmi_z_strings;
  const void *passport = ser_jmi_z_strings->passport;
  Serializable *serializable = ser_jmi_z_strings->serializable;
  // Deserializing simple fields via script
  size_t values_size = jmi_z_strings->offsets.n;
  Serializable_getArrayField(serializable, FIELD_VALUES, (void **)jmi_z_strings->values,
                             sizeof(char *), &values_size, 0, passport);
  MCLabFMILog_verbose("values[%d]", jmi_z_strings->offsets.n);
  ser_jmi_z_strings->ser_offsets =
        *(SerializableJMI_Z_OFFSETS_T **) Serializable_getField(serializable, FIELD_OFFSETS, passport);
  MCLabFMILog_unnest();
}

SerializableJMI_Z_STRINGS_T *SerializableJMI_Z_STRINGS_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_z_strings_t *jmi_z_strings) {
  Debug_assert(DEBUG_ALWAYS, jmi_z_strings != NULL, "jmi_z_strings == NULL");
  SerializableJMI_Z_STRINGS_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_z_strings)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_Z_STRINGS_T));
    new->jmi_z_strings = jmi_z_strings;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_z_strings, new,
                    SerializableJMI_Z_STRINGS_T_free, new->passport);
    // Adding inner struct new
    new->ser_offsets = SerializableJMI_Z_OFFSETS_T_new(fmi_env, &jmi_z_strings->offsets);
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_Z_STRINGS_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_Z_STRINGS_T *this_ = (SerializableJMI_Z_STRINGS_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_Z_STRINGS_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_Z_STRINGS_T *this_ = (SerializableJMI_Z_STRINGS_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
