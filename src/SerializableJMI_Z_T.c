/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_Z_T.h"

#define DEBUG "SerializableJMI_Z_T"

#define CLASS_NAME "jmi_z_t"

// defining constants for fields name


struct SerializableJMI_Z_T {
  jmi_z_t *jmi_z;
  // Adding inner struct
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_Z_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_Z_T *ser_jmi_z = (SerializableJMI_Z_T *)this_v;
  return ser_jmi_z->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  // SerializableJMI_Z_T *ser_jmi_z = (SerializableJMI_Z_T *)this_v;
  // jmi_z_t *jmi_z = ser_jmi_z->jmi_z;
  // const void *passport = ser_jmi_z->passport;
  // Serializable *serializable = ser_jmi_z->serializable;
  MCLabFMILog_warning("Serialization of strings is not implemented yet.");
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  // SerializableJMI_Z_T *ser_jmi_z = (SerializableJMI_Z_T *)this_v;
  // jmi_z_t *jmi_z = ser_jmi_z->jmi_z;
  // const void *passport = ser_jmi_z->passport;
  // Serializable *serializable = ser_jmi_z->serializable;
  // Deserializing simple fields via script
  MCLabFMILog_warning("Deserialization of strings is not implemented yet.");
  MCLabFMILog_unnest();
}

SerializableJMI_Z_T *SerializableJMI_Z_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_z_t *jmi_z) {
  Debug_assert(DEBUG_ALWAYS, jmi_z != NULL, "jmi_z == NULL");
  SerializableJMI_Z_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_z)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_Z_T));
    new->jmi_z = jmi_z;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_z, new,
                    SerializableJMI_Z_T_free, new->passport);
    // Adding inner struct new
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_Z_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_Z_T *this_ = (SerializableJMI_Z_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_Z_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_Z_T *this_ = (SerializableJMI_Z_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
