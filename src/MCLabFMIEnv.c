/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include "MCLabFMIUtils.h"
#include "MCLabUtils.h"

#include "MCLabFMIEnv.h"

#define DEBUG "MCLabFMIEnv"

#define MAX_LEN 1024
#define MAX_OBJ_POINTER_LEN 100

typedef struct MCLabFMIEnvKey {
  char class_name[MAX_LEN];
  void *objPtr;
} MCLabFMIEnvKey;

typedef struct MCLabFMIEnvObj {
  void *obj;
  serObj_free free;
} MCLabFMIEnvObj;

static MCLabFMIEnvObj *MCLabFMIEnvObj_new(void *serObj, serObj_free free);
static void MCLabFMIEnvObj_free(MCLabFMIEnvObj **thisP);

struct MCLabFMIEnv {
  HashTable *hash_table;
  Properties *passports;
};

MCLabFMIEnv *MCLabFMIEnv_new() {
  MCLabFMIEnv *new_ = calloc(1, sizeof(MCLabFMIEnv));
  new_->hash_table = HashTable_new();
  new_->passports = Properties_new(MAX_OBJ_POINTER_LEN);
  return new_;
}

void MCLabFMIEnv_free(MCLabFMIEnv **thisPtr) {
  if (thisPtr == NULL) return;
  MCLabFMIEnv *this_ = *thisPtr;
  HashTableIterator *it = HashTableIterator_new(this_->hash_table);
  const HashTable_Entry *e = NULL;
  while ((e = HashTableIterator_next(it)) != NULL) {
    MCLabFMIEnvObj *value = HashTable_Entry_value(e);
    MCLabFMIEnvKey envKey = *(MCLabFMIEnvKey *) HashTable_Entry_key(e);
    Debug_out(DEBUG,
              "Info: Free serObj %p with key = { class %s, objPtr %p }\n",
              value->obj, envKey.class_name, envKey.objPtr);
    MCLabFMIEnvObj_free(&value);
  }
  HashTableIterator_free(&it);
  HashTable_free(&this_->hash_table);
  Properties_free(&this_->passports);
  free(this_);
  thisPtr = NULL;
}

void MCLabFMIEnv_add(MCLabFMIEnv *this_, const char *class_name, void *objPtr,
                     void *serObj, serObj_free free, const void *passport) {
  Debug_assert(DEBUG_ALWAYS, this_ != NULL, "Error: this_ == NULL\n");
  Debug_assert(DEBUG_ALWAYS, class_name != NULL, "Error: class_name == NULL\n");
  Debug_assert(DEBUG_ALWAYS, strlen(class_name) < MAX_LEN,
               "Error: strlen(class_name) >= MAX_LEN\n");
  Debug_assert(DEBUG_ALWAYS, objPtr != NULL, "Error: objPtr == NULL\n");
  Debug_assert(DEBUG_ALWAYS, serObj != NULL, "Error: serObjPtr == NULL\n");
  Debug_assert(DEBUG_ALWAYS, passport != NULL, "Error: passport == NULL\n");
  Debug_out(DEBUG, "Info: Add serObj %p with key = { class %s, objPtr %p }\n",
            serObj, class_name, objPtr);
  MCLabFMIEnvKey envKey = {.class_name = {0}, .objPtr = objPtr};
  snprintf(envKey.class_name, MAX_LEN, "%s", class_name);
  MCLabFMIEnvObj *envSerObj = MCLabFMIEnvObj_new(serObj, free);
  char propertiesKey[MAX_OBJ_POINTER_LEN] = {0};
  sprintf(propertiesKey, "%p", objPtr);
  Properties_setPointer(this_->passports, propertiesKey, passport);
  MCLabFMIEnvObj *oldEnvSerObj = NULL;
  if ((oldEnvSerObj = HashTable_put(this_->hash_table, &envKey, sizeof(MCLabFMIEnvKey), envSerObj)) ==
      NULL)
    return;
  MCLabFMILog_warning(
      "objPtr %p already exists, an oldSerObj %p is "
      "replaced with serObj %p. This could cause memory error.\n",
      objPtr, oldEnvSerObj->obj, envSerObj->obj);
  MCLabFMIEnvObj_free(&oldEnvSerObj);
}

void *MCLabFMIEnv_get(MCLabFMIEnv *this_, const char *class_name,
                      void *objPtr) {
  Debug_assert(DEBUG_ALWAYS, this_ != NULL, "Error: this_ == NULL\n");
  Debug_assert(DEBUG_ALWAYS, class_name != NULL, "Error: class_name == NULL\n");
  Debug_assert(DEBUG_ALWAYS, strlen(class_name) < MAX_LEN,
               "Error: strlen(class_name) >= MAX_LEN\n");
  Debug_assert(DEBUG_ALWAYS, objPtr != NULL, "Error: objPtr == NULL\n");
  Debug_out(DEBUG, "Info: Get key = { class %s, objPtr %p }\n", class_name,
            objPtr);
  MCLabFMIEnvKey envKey = {.class_name = {0}, .objPtr = objPtr};
  snprintf(envKey.class_name, MAX_LEN, "%s", class_name);
  MCLabFMIEnvObj *envSerObj = NULL;
  if ((envSerObj = HashTable_get(this_->hash_table, &envKey, sizeof(MCLabFMIEnvKey),
                                 (void *) &envSerObj)) == NULL)
    return NULL;
  return envSerObj->obj;
}

void MCLabFMIEnv_replace(MCLabFMIEnv *this_, const char *class_name,
                         void *oldObjPtr, void *newObjPtr, void *newSerObj,
                         const void *passport) {
  Debug_assert(DEBUG_ALWAYS, this_ != NULL, "Error: this_ == NULL\n");
  Debug_assert(DEBUG_ALWAYS, class_name != NULL, "Error: class_name == NULL\n");
  Debug_assert(DEBUG_ALWAYS, strlen(class_name) < MAX_LEN,
               "Error: strlen(class_name) >= MAX_LEN\n");
  Debug_assert(DEBUG_ALWAYS, oldObjPtr != NULL, "Error: oldObjPtr == NULL\n");
  Debug_assert(DEBUG_ALWAYS, newObjPtr != NULL, "Error: newObjPtr == NULL\n");
  Debug_assert(DEBUG_ALWAYS, newSerObj != NULL, "Error: newSerObj == NULL\n");
  Debug_assert(DEBUG_ALWAYS, passport != NULL, "Error: passport == NULL\n");
  if (oldObjPtr == newObjPtr) return;
  Debug_out(DEBUG,
            "Info: Replace key = { class %s, oldObjPtr %p } with key "
            "= { class %s, newObjPtr %p }\n",
            class_name, oldObjPtr, class_name, newObjPtr);
  MCLabFMIEnvKey envKey = {.class_name = {0}, .objPtr = oldObjPtr};
  snprintf(envKey.class_name, MAX_LEN, "%s", class_name);
  MCLabFMIEnvObj *envSerObj =
      HashTable_get(this_->hash_table, &envKey, sizeof(MCLabFMIEnvKey), (void *) &envSerObj);
  Debug_assert(DEBUG_ALWAYS, envSerObj != NULL,
               "Error: envSerObj == NULL, key = %p", oldObjPtr);
  char propertiesKey[MAX_OBJ_POINTER_LEN] = {0};
  sprintf(propertiesKey, "%p", oldObjPtr);
  const void *savedPassport = NULL;
  Properties_getPointer(this_->passports, propertiesKey, &savedPassport);
  Debug_assert(DEBUG_ALWAYS, passport == savedPassport,
               "Error: saved passport %p is different from %p\n", savedPassport,
               passport);
  char newPropertiesKey[MAX_OBJ_POINTER_LEN] = {0};
  sprintf(newPropertiesKey, "%p", newObjPtr);
  Properties_remove(this_->passports, propertiesKey);
  Properties_setPointer(this_->passports, newPropertiesKey, passport);
  HashTable_remove(this_->hash_table, &envKey, sizeof(MCLabFMIEnvKey));
  envSerObj->obj = newSerObj;
  MCLabFMIEnvKey newEnvKey = {.class_name = {0}, .objPtr = newObjPtr};
  MCLabFMIEnvObj *oldEnvSerObj = NULL;
  if ((oldEnvSerObj =
           HashTable_put(this_->hash_table, &newEnvKey, sizeof(MCLabFMIEnvKey), envSerObj)) == NULL)
    return;
  MCLabFMILog_warning(
      "objPtr %p already exists, an oldSerObj %p is "
      "replaced with serObj %p. This could cause memory error.\n",
      newObjPtr, oldEnvSerObj->obj, envSerObj->obj);
  MCLabFMIEnvObj_free(&oldEnvSerObj);
}

static MCLabFMIEnvObj *MCLabFMIEnvObj_new(void *serObj, serObj_free free) {
  MCLabFMIEnvObj *new_ = calloc(1, sizeof(MCLabFMIEnvObj));
  new_->obj = serObj;
  new_->free = free;
  return new_;
}
static void MCLabFMIEnvObj_free(MCLabFMIEnvObj **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  MCLabFMIEnvObj *this_ = *thisP;
  this_->free(&this_->obj);
  free(this_);
  thisP = NULL;
}
