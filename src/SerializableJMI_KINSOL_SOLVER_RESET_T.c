/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "jmi_block_solver.h"
#include "jmi_block_solver_impl.h"
#include "SerializableJMI_KINSOL_SOLVER_RESET_T.h"
#include "SerializableDLSMAT.h"

#include "MCLabFMIEnv.h"

#define DEBUG "SerializableJMI_KINSOL_SOLVER_RESET_T"

#define CLASS_NAME "jmi_kinsol_solver"

#define FIELD_J "J"
#define FIELD_J_modified "J_modified"
#define FIELD_LAPACK_IPIV "lapack_ipiv"
#define FIELD_KIN_Y_SCALE "kin_y_scale"
#define FIELD_KIN_F_SCALE "kin_f_scale"
#define FIELD_J_IS_SINGULAR_FLAG "J_is_singular_flag"
#define FIELD_HANDLING_OF_SINGULAR_JACOBIAN_FLAG " handling_of_singular_jacobian_flag"
#define FIELD_MBSET "mbset"
#define FIELD_FORCE_NEW_J_FLAG "force_new_J_flag"
#define FIELD_KIN_SCALE_UPDATING_TIME "kin_scale_update_time"
#define FIELD_FORCE_RESCALING "force_rescaling"


struct SerializableJMI_KINSOL_SOLVER_RESET_T {
  jmi_kinsol_solver_reset_t *solver;
  jmi_block_solver_t *block_solver;
  // Adding inner struct
  SerializableDLSMAT *ser_j;
  SerializableDLSMAT *ser_j_modified;
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_KINSOL_SOLVER_RESET_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_KINSOL_SOLVER_RESET_T *ser_solver = (SerializableJMI_KINSOL_SOLVER_RESET_T *)this_v;
  return ser_solver->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_KINSOL_SOLVER_RESET_T *ser_solver = (SerializableJMI_KINSOL_SOLVER_RESET_T *)this_v;
  jmi_kinsol_solver_reset_t *solver = ser_solver->solver;
  jmi_block_solver_t *block_solver = ser_solver->block_solver;
  const void *passport = ser_solver->passport;
  Serializable *serializable = ser_solver->serializable;
  Serializable_setField(serializable, FIELD_J, PROPERTIES_POINTER, SerializableDLSMAT_asSerializable(ser_solver->ser_j), passport);
  Serializable_setField(serializable, FIELD_J_modified, PROPERTIES_POINTER, SerializableDLSMAT_asSerializable(ser_solver->ser_j_modified), passport);
  size_t lapack_ipiv_size = (size_t) (block_solver->n + 2);
  MCLabFMILog_verbose("lapack_ipiv_size[%zu]", lapack_ipiv_size);
  Serializable_setArrayField(serializable, FIELD_LAPACK_IPIV, PROPERTIES_INT, solver->lapack_ipiv, sizeof(int), &lapack_ipiv_size, NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("kin_y_scale[%d]", block_solver->n);
  size_t kin_y_scale_size = (size_t) block_solver->n;
  Serializable_setArrayField(serializable, FIELD_KIN_Y_SCALE, PROPERTIES_DOUBLE, N_VGetArrayPointer(solver->kin_y_scale), sizeof(double), &kin_y_scale_size, NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("kin_f_scale[%d]", block_solver->n);
  size_t kin_f_scale_size = (size_t) block_solver->n;
  Serializable_setArrayField(serializable, FIELD_KIN_F_SCALE, PROPERTIES_DOUBLE, N_VGetArrayPointer(solver->kin_f_scale), sizeof(double), &kin_f_scale_size, NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("J_is_singular_flag = %d", solver->J_is_singular_flag);
  Serializable_setField(serializable, FIELD_J_IS_SINGULAR_FLAG, PROPERTIES_INT, &(solver->J_is_singular_flag), passport);
  MCLabFMILog_verbose("handling_of_singular_jacobian_flag= %d", solver->handling_of_singular_jacobian_flag);
  Serializable_setField(serializable, FIELD_HANDLING_OF_SINGULAR_JACOBIAN_FLAG, PROPERTIES_INT, &(solver->handling_of_singular_jacobian_flag), passport);
  MCLabFMILog_verbose("mbset= %d", solver->mbset);
  Serializable_setField(serializable, FIELD_MBSET, PROPERTIES_INT, &(solver->mbset), passport);
  MCLabFMILog_verbose("force_new_J_flag= %d", solver->force_new_J_flag);
  Serializable_setField(serializable, FIELD_FORCE_NEW_J_FLAG, PROPERTIES_INT, &(solver->force_new_J_flag), passport);
  MCLabFMILog_verbose("force_rescaling= %d", solver->force_rescaling);
  Serializable_setField(serializable, FIELD_FORCE_RESCALING, PROPERTIES_INT, &(solver->force_rescaling), passport);
  MCLabFMILog_verbose("kin_scale_update_time= %g", solver->kin_scale_update_time);
  Serializable_setField(serializable, FIELD_KIN_SCALE_UPDATING_TIME, PROPERTIES_DOUBLE, &(solver->kin_scale_update_time), passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_KINSOL_SOLVER_RESET_T *ser_solver = (SerializableJMI_KINSOL_SOLVER_RESET_T *)this_v;
  jmi_kinsol_solver_reset_t *solver = ser_solver->solver;
  jmi_block_solver_t *block_solver = ser_solver->block_solver;
  const void *passport = ser_solver->passport;
  Serializable *serializable = ser_solver->serializable;
  ser_solver->ser_j = *(SerializableDLSMAT **) Serializable_getField(serializable, FIELD_J, passport);
  ser_solver->ser_j_modified = *(SerializableDLSMAT **) Serializable_getField(serializable, FIELD_J_modified, passport);
  size_t lapack_ipiv_size = (size_t) (block_solver->n + 2);
  Serializable_getArrayField(serializable, FIELD_LAPACK_IPIV, solver->lapack_ipiv, sizeof(int), &lapack_ipiv_size, 0, passport);
  MCLabFMILog_verbose("lapack_ipiv_size[%zu]", lapack_ipiv_size);
  size_t kin_y_scale_size = (size_t) block_solver->n;
  Serializable_getArrayField(serializable, FIELD_KIN_Y_SCALE, N_VGetArrayPointer(solver->kin_y_scale), sizeof(double), &kin_y_scale_size, 0, passport);
  MCLabFMILog_verbose("kin_y_scale[%d]", block_solver->n);
  size_t kin_f_scale_size = (size_t) block_solver->n;
  Serializable_getArrayField(serializable, FIELD_KIN_F_SCALE, N_VGetArrayPointer(solver->kin_f_scale), sizeof(double), &kin_f_scale_size, 0, passport);
  MCLabFMILog_verbose("kin_f_scale[%d]", block_solver->n);
  solver->J_is_singular_flag = *(int *) Serializable_getField(serializable, FIELD_J_IS_SINGULAR_FLAG, passport);
  MCLabFMILog_verbose("J_is_singular_flag = %d", solver->J_is_singular_flag);
  solver->handling_of_singular_jacobian_flag = *(int *) Serializable_getField(serializable, FIELD_HANDLING_OF_SINGULAR_JACOBIAN_FLAG, passport);
  MCLabFMILog_verbose("handling_of_singular_jacobian_flag= %d", solver->handling_of_singular_jacobian_flag);
  solver->mbset = *(int *) Serializable_getField(serializable, FIELD_MBSET,passport);
  MCLabFMILog_verbose("mbset= %d", solver->mbset);
  solver->force_new_J_flag = *(int *) Serializable_getField(serializable, FIELD_FORCE_NEW_J_FLAG, passport);
  MCLabFMILog_verbose("force_new_J_flag= %d", solver->force_new_J_flag);
  solver->force_rescaling = *(int *) Serializable_getField(serializable, FIELD_FORCE_RESCALING, passport);
  MCLabFMILog_verbose("force_rescaling= %d", solver->force_rescaling);
  solver->kin_scale_update_time = *(double *) Serializable_getField(serializable, FIELD_KIN_SCALE_UPDATING_TIME, passport);
  MCLabFMILog_verbose("kin_scale_update_time= %g", solver->kin_scale_update_time);
  // Deserializing simple fields via script
  MCLabFMILog_unnest();
}

SerializableJMI_KINSOL_SOLVER_RESET_T *SerializableJMI_KINSOL_SOLVER_RESET_T_new(MCLabFMIEnv *fmi_env, jmi_kinsol_solver_reset_t *solver, jmi_block_solver_t *block_solver) {
  Debug_assert(DEBUG_ALWAYS, solver != NULL, "solver == NULL");
  SerializableJMI_KINSOL_SOLVER_RESET_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, solver)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_KINSOL_SOLVER_RESET_T));
    new->solver = solver;
    new->block_solver = block_solver;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, solver, new,
                    SerializableJMI_KINSOL_SOLVER_RESET_T_free, new->passport);
    // Adding inner struct new
    new->ser_j = SerializableDLSMAT_new(fmi_env, solver->J);
    new->ser_j_modified = SerializableDLSMAT_new(fmi_env, solver->J_modified);
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_KINSOL_SOLVER_RESET_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_KINSOL_SOLVER_RESET_T *this_ = (SerializableJMI_KINSOL_SOLVER_RESET_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_KINSOL_SOLVER_RESET_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_KINSOL_SOLVER_RESET_T *this_ = (SerializableJMI_KINSOL_SOLVER_RESET_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {
      .className = className,
      .passport = this_->passport,
      .serializeFields = serializeFields,
      .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
