/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "jmi_util.h"
#include "jmi_block_residual.h"
#include "SerializableJMI_T.h"
#include "SerializableJMI_BLOCK_SOLVER_OPTIONS_T.h"
#include "SerializableJMI_BLOCK_SOLVER_T.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_BLOCK_RESIDUAL_T.h"

#define DEBUG "SerializableJMI_BLOCK_RESIDUAL_T"

#define CLASS_NAME "jmi_block_residual_t"

// defining constants for fields name
#define FIELD_JMI "jmi"
#define FIELD_OPTIONS "options"




#define FIELD_X "x"
#define FIELD_N "n"
#define FIELD_N_DR "n_dr"
#define FIELD_N_NR "n_nr"
#define FIELD_N_NRT "n_nrt"
#define FIELD_N_SR "n_sr"
#define FIELD_N_DIRECT_NR "n_direct_nr"
#define FIELD_N_DIRECT_BOOL "n_direct_bool"
#define FIELD_N_SW "n_sw"
#define FIELD_N_DIRECT_SW "n_direct_sw"
#define FIELD_EVENT_ITER "event_iter"
#define FIELD_SW_OLD "sw_old"
#define FIELD_NR_OLD "nr_old"
#define FIELD_X_OLD "x_old"
#define FIELD_DR_OLD "dr_old"
#define FIELD_SW_INDEX "sw_index"
#define FIELD_SR_VREF "sr_vref"
#define FIELD_SW_DIRECT_INDEX "sw_direct_index"
#define FIELD_NR_INDEX "nr_index"
#define FIELD_NR_PRE_INDEX "nr_pre_index"
#define FIELD_NR_DIRECT_INDEX "nr_direct_index"
#define FIELD_BOOL_DIRECT_INDEX "bool_direct_index"
#define FIELD_NR_VREF "nr_vref"
#define FIELD_DR_INDEX "dr_index"
#define FIELD_DR_PRE_INDEX "dr_pre_index"
#define FIELD_DR_VREF "dr_vref"
#define FIELD_WORK_SWITCHES "work_switches"
#define FIELD_WORK_NON_REALS "work_non_reals"
#define FIELD_WORK_DISCRETE_REALS "work_discrete_reals"
#define FIELD_WORK_IVS "work_ivs"
#define FIELD_DX "dx"
#define FIELD_DV "dv"
#define FIELD_INDEX "index"

#define FIELD_PARENT_INDEX "parent_index"
#define FIELD_IS_INIT_BLOCK "is_init_block"

#define FIELD_LABEL "label"
#define FIELD_RES "res"
#define FIELD_DRES "dres"
#define FIELD_JAC "jac"
#define FIELD_IPIV "ipiv"
#define FIELD_MIN "min"
#define FIELD_MAX "max"
#define FIELD_NOMINAL "nominal"
#define FIELD_INITIAL "initial"
#define FIELD_DISCRETE_NOMINALS "discrete_nominals"
#define FIELD_JACOBIAN_VARIABILITY "jacobian_variability"
#define FIELD_VALUE_REFERENCES "value_references"
#define FIELD_BLOCK_SOLVER "block_solver"


#define FIELD_INIT "init"
#define FIELD_AT_EVENT "at_event"
#define FIELD_NB_CALLS "nb_calls"
#define FIELD_NB_ITERS "nb_iters"
#define FIELD_NB_JEVALS "nb_jevals"
#define FIELD_NB_FEVALS "nb_fevals"
#define FIELD_TIME_SPENT "time_spent"
#define FIELD_MESSAGE_BUFFER "message_buffer"//TO_BE_SERIALIZED

struct SerializableJMI_BLOCK_RESIDUAL_T {
  jmi_block_residual_t *jmi_block_residual;
  // Adding inner struct
  SerializableJMI_T *ser_jmi;
  SerializableJMI_BLOCK_SOLVER_OPTIONS_T *ser_options;
  SerializableJMI_BLOCK_SOLVER_T *ser_block_solver;
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_BLOCK_RESIDUAL_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_BLOCK_RESIDUAL_T *ser_jmi_block_residual = (SerializableJMI_BLOCK_RESIDUAL_T *)this_v;
  return ser_jmi_block_residual->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_BLOCK_RESIDUAL_T *ser_jmi_block_residual = (SerializableJMI_BLOCK_RESIDUAL_T *)this_v;
  jmi_block_residual_t *jmi_block_residual = ser_jmi_block_residual->jmi_block_residual;
  const void *passport = ser_jmi_block_residual->passport;
  Serializable *serializable = ser_jmi_block_residual->serializable;
  Serializable_setField(serializable, FIELD_JMI, PROPERTIES_POINTER,
                       SerializableJMI_T_asSerializable(ser_jmi_block_residual->ser_jmi), passport);
  Serializable_setField(serializable, FIELD_OPTIONS, PROPERTIES_POINTER,
                       SerializableJMI_BLOCK_SOLVER_OPTIONS_T_asSerializable(ser_jmi_block_residual->ser_options), passport);
  //TO_BE_SERIALIZED FIELD_F as type jmi_block_residual_func_t
  //TO_BE_SERIALIZED FIELD_J as type jmi_block_jacobian_func_t
  //TO_BE_SERIALIZED FIELD_J_STRUCTURE as type jmi_block_jacobian_structure_func_t
  //TO_BE_SERIALIZED FIELD_DF as type jmi_block_dir_der_func_t
  MCLabFMILog_verbose("x[%d]", jmi_block_residual->n);
  size_t x_size_t = (size_t) jmi_block_residual->n;
  Serializable_setArrayField(serializable, FIELD_X, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->x, sizeof(double), &x_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("n: %d", jmi_block_residual->n);
  Serializable_setField(serializable, FIELD_N, PROPERTIES_INT,
                       &jmi_block_residual->n, passport);
  MCLabFMILog_verbose("n_dr: %d", jmi_block_residual->n_dr);
  Serializable_setField(serializable, FIELD_N_DR, PROPERTIES_INT,
                       &jmi_block_residual->n_dr, passport);
  MCLabFMILog_verbose("n_nr: %d", jmi_block_residual->n_nr);
  Serializable_setField(serializable, FIELD_N_NR, PROPERTIES_INT,
                       &jmi_block_residual->n_nr, passport);
  MCLabFMILog_verbose("n_nrt: %d", jmi_block_residual->n_nrt);
  Serializable_setField(serializable, FIELD_N_NRT, PROPERTIES_INT,
                       &jmi_block_residual->n_nrt, passport);
  MCLabFMILog_verbose("n_sr: %d", jmi_block_residual->n_sr);
  Serializable_setField(serializable, FIELD_N_SR, PROPERTIES_INT,
                       &jmi_block_residual->n_sr, passport);
  MCLabFMILog_verbose("n_direct_nr: %d", jmi_block_residual->n_direct_nr);
  Serializable_setField(serializable, FIELD_N_DIRECT_NR, PROPERTIES_INT,
                       &jmi_block_residual->n_direct_nr, passport);
  MCLabFMILog_verbose("n_direct_bool: %d", jmi_block_residual->n_direct_bool);
  Serializable_setField(serializable, FIELD_N_DIRECT_BOOL, PROPERTIES_INT,
                       &jmi_block_residual->n_direct_bool, passport);
  MCLabFMILog_verbose("n_sw: %d", jmi_block_residual->n_sw);
  Serializable_setField(serializable, FIELD_N_SW, PROPERTIES_INT,
                       &jmi_block_residual->n_sw, passport);
  MCLabFMILog_verbose("n_direct_sw: %d", jmi_block_residual->n_direct_sw);
  Serializable_setField(serializable, FIELD_N_DIRECT_SW, PROPERTIES_INT,
                       &jmi_block_residual->n_direct_sw, passport);
  MCLabFMILog_verbose("event_iter: %d", jmi_block_residual->event_iter);
  Serializable_setField(serializable, FIELD_EVENT_ITER, PROPERTIES_INT,
                       &jmi_block_residual->event_iter, passport);
  size_t sw_old_size = (size_t) ((30+2)*jmi_block_residual->n_sw);
  MCLabFMILog_verbose("sw_old[%zu]", sw_old_size);
  Serializable_setArrayField(serializable, FIELD_SW_OLD, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->sw_old, sizeof(double), &sw_old_size,
  	                        NULL, 0, false, NULL, passport);
  size_t nr_old_size = (size_t) ((30+2)*jmi_block_residual->n_nr);
  MCLabFMILog_verbose("nr_old[%zu]", nr_old_size);
  Serializable_setArrayField(serializable, FIELD_NR_OLD, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->nr_old, sizeof(double), &nr_old_size,
  	                        NULL, 0, false, NULL, passport);
  size_t x_old_size = (size_t) ((30+2)*jmi_block_residual->n);
  MCLabFMILog_verbose("x_old[%zu]", x_old_size);
  Serializable_setArrayField(serializable, FIELD_X_OLD, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->x_old, sizeof(double), &x_old_size,
  	                        NULL, 0, false, NULL, passport);
  size_t dr_old_size = (size_t) ((30+2)*jmi_block_residual->n_dr);
  MCLabFMILog_verbose("dr_old[%zu]", dr_old_size);
  Serializable_setArrayField(serializable, FIELD_DR_OLD, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->dr_old, sizeof(double), &dr_old_size,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("sw_index[%d]", jmi_block_residual->n_sw);
  size_t sw_index_size_t = (size_t) jmi_block_residual->n_sw;
  Serializable_setArrayField(serializable, FIELD_SW_INDEX, PROPERTIES_INT,
  	                        (void **)jmi_block_residual->sw_index, sizeof(int), &sw_index_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("sr_vref[%d]", jmi_block_residual->n_sr);
  size_t sr_vref_size_t = (size_t) jmi_block_residual->n_sr;
  Serializable_setArrayField(serializable, FIELD_SR_VREF, PROPERTIES_INT,
  	                        (void **)jmi_block_residual->sr_vref, sizeof(int), &sr_vref_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("sw_direct_index[%d]", jmi_block_residual->n_direct_sw);
  size_t sw_direct_index_size_t = (size_t) jmi_block_residual->n_direct_sw;
  Serializable_setArrayField(serializable, FIELD_SW_DIRECT_INDEX, PROPERTIES_INT,
  	                        (void **)jmi_block_residual->sw_direct_index, sizeof(int), &sw_direct_index_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("nr_index[%d]", jmi_block_residual->n_nr);
  size_t nr_index_size_t = (size_t) jmi_block_residual->n_nr;
  Serializable_setArrayField(serializable, FIELD_NR_INDEX, PROPERTIES_INT,
  	                        (void **)jmi_block_residual->nr_index, sizeof(int), &nr_index_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("nr_pre_index[%d]", jmi_block_residual->n_nr);
  size_t nr_pre_index_size_t = (size_t) jmi_block_residual->n_nr;
  Serializable_setArrayField(serializable, FIELD_NR_PRE_INDEX, PROPERTIES_INT,
  	                        (void **)jmi_block_residual->nr_pre_index, sizeof(int), &nr_pre_index_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("nr_direct_index[%d]", jmi_block_residual->n_direct_nr);
  size_t nr_direct_index_size_t = (size_t) jmi_block_residual->n_direct_nr;
  Serializable_setArrayField(serializable, FIELD_NR_DIRECT_INDEX, PROPERTIES_INT,
  	                        (void **)jmi_block_residual->nr_direct_index, sizeof(int), &nr_direct_index_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("bool_direct_index[%d]", jmi_block_residual->n_direct_nr);
  size_t bool_direct_index_size_t = (size_t) jmi_block_residual->n_direct_nr;
  Serializable_setArrayField(serializable, FIELD_BOOL_DIRECT_INDEX, PROPERTIES_INT,
  	                        (void **)jmi_block_residual->bool_direct_index, sizeof(int), &bool_direct_index_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("nr_vref[%d]", jmi_block_residual->n_nr);
  size_t nr_vref_size_t = (size_t) jmi_block_residual->n_nr;
  Serializable_setArrayField(serializable, FIELD_NR_VREF, PROPERTIES_INT,
  	                        (void **)jmi_block_residual->nr_vref, sizeof(int), &nr_vref_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("dr_index[%d]", jmi_block_residual->n_dr);
  size_t dr_index_size_t = (size_t) jmi_block_residual->n_dr;
  Serializable_setArrayField(serializable, FIELD_DR_INDEX, PROPERTIES_INT,
  	                        (void **)jmi_block_residual->dr_index, sizeof(int), &dr_index_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("dr_pre_index[%d]", jmi_block_residual->n_dr);
  size_t dr_pre_index_size_t = (size_t) jmi_block_residual->n_dr;
  Serializable_setArrayField(serializable, FIELD_DR_PRE_INDEX, PROPERTIES_INT,
  	                        (void **)jmi_block_residual->dr_pre_index, sizeof(int), &dr_pre_index_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("dr_vref[%d]", jmi_block_residual->n_dr);
  size_t dr_vref_size_t = (size_t) jmi_block_residual->n_dr;
  Serializable_setArrayField(serializable, FIELD_DR_VREF, PROPERTIES_INT,
  	                        (void **)jmi_block_residual->dr_vref, sizeof(int), &dr_vref_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("work_switches[%d]", jmi_block_residual->n_sw);
  size_t work_switches_size_t = (size_t) jmi_block_residual->n_sw;
  Serializable_setArrayField(serializable, FIELD_WORK_SWITCHES, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->work_switches, sizeof(double), &work_switches_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("work_non_reals[%d]", jmi_block_residual->n_nr);
  size_t work_non_reals_size_t = (size_t) jmi_block_residual->n_nr;
  Serializable_setArrayField(serializable, FIELD_WORK_NON_REALS, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->work_non_reals, sizeof(double), &work_non_reals_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("work_discrete_reals[%d]", jmi_block_residual->n_dr);
  size_t work_discrete_reals_size_t = (size_t) jmi_block_residual->n_dr;
  Serializable_setArrayField(serializable, FIELD_WORK_DISCRETE_REALS, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->work_discrete_reals, sizeof(double), &work_discrete_reals_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("work_ivs[%d]", jmi_block_residual->n);
  size_t work_ivs_size_t = (size_t) jmi_block_residual->n;
  Serializable_setArrayField(serializable, FIELD_WORK_IVS, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->work_ivs, sizeof(double), &work_ivs_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("dx[%d]", jmi_block_residual->n);
  size_t dx_size_t = (size_t) jmi_block_residual->n;
  Serializable_setArrayField(serializable, FIELD_DX, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->dx, sizeof(double), &dx_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("dv[%d]", jmi_block_residual->n);
  size_t dv_size_t = (size_t) jmi_block_residual->n;
  Serializable_setArrayField(serializable, FIELD_DV, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->dv, sizeof(double), &dv_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("index: %d", jmi_block_residual->index);
  Serializable_setField(serializable, FIELD_INDEX, PROPERTIES_INT,
                       &jmi_block_residual->index, passport);
#ifdef JMI_PROFILE_RUNTIME
  MCLabFMILog_verbose("parent_index: %d", jmi_block_residual->parent_index);
  Serializable_setField(serializable, FIELD_PARENT_INDEX, PROPERTIES_INT,
                       &jmi_block_residual->parent_index, passport);
  MCLabFMILog_verbose("is_init_block: %d", jmi_block_residual->is_init_block);
  Serializable_setField(serializable, FIELD_IS_INIT_BLOCK, PROPERTIES_INT,
                       &jmi_block_residual->is_init_block, passport);
#endif
  MCLabFMILog_verbose("label: %s", jmi_block_residual->label);
  Serializable_setField(serializable, FIELD_LABEL, PROPERTIES_STRING,
                       (void *)jmi_block_residual->label, passport);
  MCLabFMILog_verbose("res[%d]", jmi_block_residual->n);
  size_t res_size_t = (size_t) jmi_block_residual->n;
  Serializable_setArrayField(serializable, FIELD_RES, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->res, sizeof(double), &res_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("dres[%d]", jmi_block_residual->n);
  size_t dres_size_t = (size_t) jmi_block_residual->n;
  Serializable_setArrayField(serializable, FIELD_DRES, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->dres, sizeof(double), &dres_size_t,
  	                        NULL, 0, false, NULL, passport);
  size_t jac_size = (size_t) (jmi_block_residual->n*jmi_block_residual->n);
  MCLabFMILog_verbose("jac[%zu]", jac_size);
  Serializable_setArrayField(serializable, FIELD_JAC, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->jac, sizeof(double), &jac_size,
  	                        NULL, 0, false, NULL, passport);
  size_t ipiv_size = (size_t) (2*jmi_block_residual->n+1);
  MCLabFMILog_verbose("ipiv[%zu]", ipiv_size);
  Serializable_setArrayField(serializable, FIELD_IPIV, PROPERTIES_INT,
  	                        (void **)jmi_block_residual->ipiv, sizeof(int), &ipiv_size,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("min[%d]", jmi_block_residual->n);
  size_t min_size_t = (size_t) jmi_block_residual->n;
  Serializable_setArrayField(serializable, FIELD_MIN, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->min, sizeof(double), &min_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("max[%d]", jmi_block_residual->n);
  size_t max_size_t = (size_t) jmi_block_residual->n;
  Serializable_setArrayField(serializable, FIELD_MAX, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->max, sizeof(double), &max_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("nominal[%d]", jmi_block_residual->n);
  size_t nominal_size_t = (size_t) jmi_block_residual->n;
  Serializable_setArrayField(serializable, FIELD_NOMINAL, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->nominal, sizeof(double), &nominal_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("initial[%d]", jmi_block_residual->n);
  size_t initial_size_t = (size_t) jmi_block_residual->n;
  Serializable_setArrayField(serializable, FIELD_INITIAL, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->initial, sizeof(double), &initial_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("discrete_nominals[%d]", jmi_block_residual->n_dr);
  size_t discrete_nominals_size_t = (size_t) jmi_block_residual->n_dr;
  Serializable_setArrayField(serializable, FIELD_DISCRETE_NOMINALS, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_residual->discrete_nominals, sizeof(double), &discrete_nominals_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("jacobian_variability: %d", jmi_block_residual->jacobian_variability);
  Serializable_setField(serializable, FIELD_JACOBIAN_VARIABILITY, PROPERTIES_INT,
                       &jmi_block_residual->jacobian_variability, passport);
  MCLabFMILog_verbose("value_references[%d]", jmi_block_residual->n);
  size_t value_references_size_t = (size_t) jmi_block_residual->n;
  Serializable_setArrayField(serializable, FIELD_VALUE_REFERENCES, PROPERTIES_INT,
  	                        (void **)jmi_block_residual->value_references, sizeof(int), &value_references_size_t,
  	                        NULL, 0, false, NULL, passport);
  Serializable_setField(serializable, FIELD_BLOCK_SOLVER, PROPERTIES_POINTER,
                       SerializableJMI_BLOCK_SOLVER_T_asSerializable(ser_jmi_block_residual->ser_block_solver), passport);
  //TO_BE_SERIALIZED FIELD_SOLVE as type jmi_block_residual_solve_func_t
  //TO_BE_SERIALIZED FIELD_DELETE_SOLVER as type jmi_block_residual_delete_func_t
  //TO_BE_SERIALIZED FIELD_EVALUATE_JACOBIAN as type jmi_block_residual_jacobian_func_t
  MCLabFMILog_verbose("init: %d", jmi_block_residual->init);
  Serializable_setField(serializable, FIELD_INIT, PROPERTIES_INT,
                       &jmi_block_residual->init, passport);
  MCLabFMILog_verbose("at_event: %d", jmi_block_residual->at_event);
  Serializable_setField(serializable, FIELD_AT_EVENT, PROPERTIES_INT,
                       &jmi_block_residual->at_event, passport);
  MCLabFMILog_verbose("nb_calls: %lu", jmi_block_residual->nb_calls);
  Serializable_setField(serializable, FIELD_NB_CALLS, PROPERTIES_ULONG,
                       &jmi_block_residual->nb_calls, passport);
  MCLabFMILog_verbose("nb_iters: %lu", jmi_block_residual->nb_iters);
  Serializable_setField(serializable, FIELD_NB_ITERS, PROPERTIES_ULONG,
                       &jmi_block_residual->nb_iters, passport);
  MCLabFMILog_verbose("nb_jevals: %lu", jmi_block_residual->nb_jevals);
  Serializable_setField(serializable, FIELD_NB_JEVALS, PROPERTIES_ULONG,
                       &jmi_block_residual->nb_jevals, passport);
  MCLabFMILog_verbose("nb_fevals: %lu", jmi_block_residual->nb_fevals);
  Serializable_setField(serializable, FIELD_NB_FEVALS, PROPERTIES_ULONG,
                       &jmi_block_residual->nb_fevals, passport);
  MCLabFMILog_verbose("time_spent: %g", jmi_block_residual->time_spent);
  Serializable_setField(serializable, FIELD_TIME_SPENT, PROPERTIES_DOUBLE,
                       &jmi_block_residual->time_spent, passport);
  //TO_BE_SERIALIZED FIELD_MESSAGE_BUFFER as type char*
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_BLOCK_RESIDUAL_T *ser_jmi_block_residual = (SerializableJMI_BLOCK_RESIDUAL_T *)this_v;
  jmi_block_residual_t *jmi_block_residual = ser_jmi_block_residual->jmi_block_residual;
  const void *passport = ser_jmi_block_residual->passport;
  Serializable *serializable = ser_jmi_block_residual->serializable;
  // Deserializing simple fields via script
  ser_jmi_block_residual->ser_jmi =
        *(SerializableJMI_T **) Serializable_getField(serializable, FIELD_JMI, passport);
  ser_jmi_block_residual->ser_options =
        *(SerializableJMI_BLOCK_SOLVER_OPTIONS_T **) Serializable_getField(serializable, FIELD_OPTIONS, passport);
  //TO_BE_SERIALIZED FIELD_F as type jmi_block_residual_func_t
  //TO_BE_SERIALIZED FIELD_J as type jmi_block_jacobian_func_t
  //TO_BE_SERIALIZED FIELD_J_STRUCTURE as type jmi_block_jacobian_structure_func_t
  //TO_BE_SERIALIZED FIELD_DF as type jmi_block_dir_der_func_t
  size_t x_size_t = (size_t) jmi_block_residual->n;
  Serializable_getArrayField(serializable, FIELD_X, (void **)jmi_block_residual->x,
  	                        sizeof(double), &x_size_t, 0, passport);
  MCLabFMILog_verbose("x[%d]", jmi_block_residual->n);
  jmi_block_residual->n =
    *(int *) Serializable_getField(serializable, FIELD_N, passport);
  MCLabFMILog_verbose("n: %d", jmi_block_residual->n);
  jmi_block_residual->n_dr =
    *(int *) Serializable_getField(serializable, FIELD_N_DR, passport);
  MCLabFMILog_verbose("n_dr: %d", jmi_block_residual->n_dr);
  jmi_block_residual->n_nr =
    *(int *) Serializable_getField(serializable, FIELD_N_NR, passport);
  MCLabFMILog_verbose("n_nr: %d", jmi_block_residual->n_nr);
  jmi_block_residual->n_nrt =
    *(int *) Serializable_getField(serializable, FIELD_N_NRT, passport);
  MCLabFMILog_verbose("n_nrt: %d", jmi_block_residual->n_nrt);
  jmi_block_residual->n_sr =
    *(int *) Serializable_getField(serializable, FIELD_N_SR, passport);
  MCLabFMILog_verbose("n_sr: %d", jmi_block_residual->n_sr);
  jmi_block_residual->n_direct_nr =
    *(int *) Serializable_getField(serializable, FIELD_N_DIRECT_NR, passport);
  MCLabFMILog_verbose("n_direct_nr: %d", jmi_block_residual->n_direct_nr);
  jmi_block_residual->n_direct_bool =
    *(int *) Serializable_getField(serializable, FIELD_N_DIRECT_BOOL, passport);
  MCLabFMILog_verbose("n_direct_bool: %d", jmi_block_residual->n_direct_bool);
  jmi_block_residual->n_sw =
    *(int *) Serializable_getField(serializable, FIELD_N_SW, passport);
  MCLabFMILog_verbose("n_sw: %d", jmi_block_residual->n_sw);
  jmi_block_residual->n_direct_sw =
    *(int *) Serializable_getField(serializable, FIELD_N_DIRECT_SW, passport);
  MCLabFMILog_verbose("n_direct_sw: %d", jmi_block_residual->n_direct_sw);
  jmi_block_residual->event_iter =
    *(int *) Serializable_getField(serializable, FIELD_EVENT_ITER, passport);
  MCLabFMILog_verbose("event_iter: %d", jmi_block_residual->event_iter);
  size_t sw_old_size = (size_t) ((30+2)*jmi_block_residual->n_sw);
  Serializable_getArrayField(serializable, FIELD_SW_OLD, (void **)jmi_block_residual->sw_old,
  	                        sizeof(double), &sw_old_size, 0, passport);
  MCLabFMILog_verbose("sw_old[%zu]", sw_old_size);
  size_t nr_old_size = (size_t) ((30+2)*jmi_block_residual->n_nr);
  Serializable_getArrayField(serializable, FIELD_NR_OLD, (void **)jmi_block_residual->nr_old,
  	                        sizeof(double), &nr_old_size, 0, passport);
  MCLabFMILog_verbose("nr_old[%zu]", nr_old_size);
  size_t x_old_size = (size_t) ((30+2)*jmi_block_residual->n);
  Serializable_getArrayField(serializable, FIELD_X_OLD, (void **)jmi_block_residual->x_old,
  	                        sizeof(double), &x_old_size, 0, passport);
  MCLabFMILog_verbose("x_old[%zu]", x_old_size);
  size_t dr_old_size = (size_t) ((30+2)*jmi_block_residual->n_dr);
  Serializable_getArrayField(serializable, FIELD_DR_OLD, (void **)jmi_block_residual->dr_old,
  	                        sizeof(double), &dr_old_size, 0, passport);
  MCLabFMILog_verbose("dr_old[%zu]", dr_old_size);
  size_t sw_index_size_t = (size_t) jmi_block_residual->n_sw;
  Serializable_getArrayField(serializable, FIELD_SW_INDEX, (void **)jmi_block_residual->sw_index,
  	                        sizeof(int), &sw_index_size_t, 0, passport);
  MCLabFMILog_verbose("sw_index[%d]", jmi_block_residual->n_sw);
  size_t sr_vref_size_t = (size_t) jmi_block_residual->n_sr;
  Serializable_getArrayField(serializable, FIELD_SR_VREF, (void **)jmi_block_residual->sr_vref,
  	                        sizeof(int), &sr_vref_size_t, 0, passport);
  MCLabFMILog_verbose("sr_vref[%d]", jmi_block_residual->n_sr);
  size_t sw_direct_index_size_t = (size_t) jmi_block_residual->n_direct_sw;
  Serializable_getArrayField(serializable, FIELD_SW_DIRECT_INDEX, (void **)jmi_block_residual->sw_direct_index,
  	                        sizeof(int), &sw_direct_index_size_t, 0, passport);
  MCLabFMILog_verbose("sw_direct_index[%d]", jmi_block_residual->n_direct_sw);
  size_t nr_index_size_t = (size_t) jmi_block_residual->n_nr;
  Serializable_getArrayField(serializable, FIELD_NR_INDEX, (void **)jmi_block_residual->nr_index,
  	                        sizeof(int), &nr_index_size_t, 0, passport);
  MCLabFMILog_verbose("nr_index[%d]", jmi_block_residual->n_nr);
  size_t nr_pre_index_size_t = (size_t) jmi_block_residual->n_nr;
  Serializable_getArrayField(serializable, FIELD_NR_PRE_INDEX, (void **)jmi_block_residual->nr_pre_index,
  	                        sizeof(int), &nr_pre_index_size_t, 0, passport);
  MCLabFMILog_verbose("nr_pre_index[%d]", jmi_block_residual->n_nr);
  size_t nr_direct_index_size_t = (size_t) jmi_block_residual->n_direct_nr;
  Serializable_getArrayField(serializable, FIELD_NR_DIRECT_INDEX, (void **)jmi_block_residual->nr_direct_index,
  	                        sizeof(int), &nr_direct_index_size_t, 0, passport);
  MCLabFMILog_verbose("nr_direct_index[%d]", jmi_block_residual->n_direct_nr);
  size_t bool_direct_index_size_t = (size_t) jmi_block_residual->n_direct_nr;
  Serializable_getArrayField(serializable, FIELD_BOOL_DIRECT_INDEX, (void **)jmi_block_residual->bool_direct_index,
  	                        sizeof(int), &bool_direct_index_size_t, 0, passport);
  MCLabFMILog_verbose("bool_direct_index[%d]", jmi_block_residual->n_direct_nr);
  size_t nr_vref_size_t = (size_t) jmi_block_residual->n_nr;
  Serializable_getArrayField(serializable, FIELD_NR_VREF, (void **)jmi_block_residual->nr_vref,
  	                        sizeof(int), &nr_vref_size_t, 0, passport);
  MCLabFMILog_verbose("nr_vref[%d]", jmi_block_residual->n_nr);
  size_t dr_index_size_t = (size_t) jmi_block_residual->n_dr;
  Serializable_getArrayField(serializable, FIELD_DR_INDEX, (void **)jmi_block_residual->dr_index,
  	                        sizeof(int), &dr_index_size_t, 0, passport);
  MCLabFMILog_verbose("dr_index[%d]", jmi_block_residual->n_dr);
  size_t dr_pre_index_size_t = (size_t) jmi_block_residual->n_dr;
  Serializable_getArrayField(serializable, FIELD_DR_PRE_INDEX, (void **)jmi_block_residual->dr_pre_index,
  	                        sizeof(int), &dr_pre_index_size_t, 0, passport);
  MCLabFMILog_verbose("dr_pre_index[%d]", jmi_block_residual->n_dr);
  size_t dr_vref_size_t = (size_t) jmi_block_residual->n_dr;
  Serializable_getArrayField(serializable, FIELD_DR_VREF, (void **)jmi_block_residual->dr_vref,
  	                        sizeof(int), &dr_vref_size_t, 0, passport);
  MCLabFMILog_verbose("dr_vref[%d]", jmi_block_residual->n_dr);
  size_t work_switches_size_t = (size_t) jmi_block_residual->n_sw;
  Serializable_getArrayField(serializable, FIELD_WORK_SWITCHES, (void **)jmi_block_residual->work_switches,
  	                        sizeof(double), &work_switches_size_t, 0, passport);
  MCLabFMILog_verbose("work_switches[%d]", jmi_block_residual->n_sw);
  size_t work_non_reals_size_t = (size_t) jmi_block_residual->n_nr;
  Serializable_getArrayField(serializable, FIELD_WORK_NON_REALS, (void **)jmi_block_residual->work_non_reals,
  	                        sizeof(double), &work_non_reals_size_t, 0, passport);
  MCLabFMILog_verbose("work_non_reals[%d]", jmi_block_residual->n_nr);
  size_t work_discrete_reals_size_t = (size_t) jmi_block_residual->n_dr;
  Serializable_getArrayField(serializable, FIELD_WORK_DISCRETE_REALS, (void **)jmi_block_residual->work_discrete_reals,
  	                        sizeof(double), &work_discrete_reals_size_t, 0, passport);
  MCLabFMILog_verbose("work_discrete_reals[%d]", jmi_block_residual->n_dr);
  size_t work_ivs_size_t = (size_t) jmi_block_residual->n;
  Serializable_getArrayField(serializable, FIELD_WORK_IVS, (void **)jmi_block_residual->work_ivs,
  	                        sizeof(double), &work_ivs_size_t, 0, passport);
  MCLabFMILog_verbose("work_ivs[%d]", jmi_block_residual->n);
  size_t dx_size_t = (size_t) jmi_block_residual->n;
  Serializable_getArrayField(serializable, FIELD_DX, (void **)jmi_block_residual->dx,
  	                        sizeof(double), &dx_size_t, 0, passport);
  MCLabFMILog_verbose("dx[%d]", jmi_block_residual->n);
  size_t dv_size_t = (size_t) jmi_block_residual->n;
  Serializable_getArrayField(serializable, FIELD_DV, (void **)jmi_block_residual->dv,
  	                        sizeof(double), &dv_size_t, 0, passport);
  MCLabFMILog_verbose("dv[%d]", jmi_block_residual->n);
  jmi_block_residual->index =
    *(int *) Serializable_getField(serializable, FIELD_INDEX, passport);
  MCLabFMILog_verbose("index: %d", jmi_block_residual->index);
#ifdef JMI_PROFILE_RUNTIME
  jmi_block_residual->parent_index =
    *(int *) Serializable_getField(serializable, FIELD_PARENT_INDEX, passport);
  MCLabFMILog_verbose("parent_index: %d", jmi_block_residual->parent_index);
  jmi_block_residual->is_init_block =
    *(int *) Serializable_getField(serializable, FIELD_IS_INIT_BLOCK, passport);
  MCLabFMILog_verbose("is_init_block: %d", jmi_block_residual->is_init_block);
#endif
  SAFETY_CHECK_STRING(serializable, jmi_block_residual->label, FIELD_LABEL, passport);
  MCLabFMILog_verbose("label: %s", jmi_block_residual->label);
  size_t res_size_t = (size_t) jmi_block_residual->n;
  Serializable_getArrayField(serializable, FIELD_RES, (void **)jmi_block_residual->res,
  	                        sizeof(double), &res_size_t, 0, passport);
  MCLabFMILog_verbose("res[%d]", jmi_block_residual->n);
  size_t dres_size_t = (size_t) jmi_block_residual->n;
  Serializable_getArrayField(serializable, FIELD_DRES, (void **)jmi_block_residual->dres,
  	                        sizeof(double), &dres_size_t, 0, passport);
  MCLabFMILog_verbose("dres[%d]", jmi_block_residual->n);
  size_t jac_size = (size_t) (jmi_block_residual->n*jmi_block_residual->n);
  Serializable_getArrayField(serializable, FIELD_JAC, (void **)jmi_block_residual->jac,
  	                        sizeof(double), &jac_size, 0, passport);
  MCLabFMILog_verbose("jac[%zu]", jac_size);
  size_t ipiv_size = (size_t) (2*jmi_block_residual->n+1);
  Serializable_getArrayField(serializable, FIELD_IPIV, (void **)jmi_block_residual->ipiv,
  	                        sizeof(int), &ipiv_size, 0, passport);
  MCLabFMILog_verbose("ipiv[%zu]", ipiv_size);
  size_t min_size_t = (size_t) jmi_block_residual->n;
  Serializable_getArrayField(serializable, FIELD_MIN, (void **)jmi_block_residual->min,
  	                        sizeof(double), &min_size_t, 0, passport);
  MCLabFMILog_verbose("min[%d]", jmi_block_residual->n);
  size_t max_size_t = (size_t) jmi_block_residual->n;
  Serializable_getArrayField(serializable, FIELD_MAX, (void **)jmi_block_residual->max,
  	                        sizeof(double), &max_size_t, 0, passport);
  MCLabFMILog_verbose("max[%d]", jmi_block_residual->n);
  size_t nominal_size_t = (size_t) jmi_block_residual->n;
  Serializable_getArrayField(serializable, FIELD_NOMINAL, (void **)jmi_block_residual->nominal,
  	                        sizeof(double), &nominal_size_t, 0, passport);
  MCLabFMILog_verbose("nominal[%d]", jmi_block_residual->n);
  size_t initial_size_t = (size_t) jmi_block_residual->n;
  Serializable_getArrayField(serializable, FIELD_INITIAL, (void **)jmi_block_residual->initial,
  	                        sizeof(double), &initial_size_t, 0, passport);
  MCLabFMILog_verbose("initial[%d]", jmi_block_residual->n);
  size_t discrete_nominals_size_t = (size_t) jmi_block_residual->n_dr;
  Serializable_getArrayField(serializable, FIELD_DISCRETE_NOMINALS, (void **)jmi_block_residual->discrete_nominals,
  	                        sizeof(double), &discrete_nominals_size_t, 0, passport);
  MCLabFMILog_verbose("discrete_nominals[%d]", jmi_block_residual->n_dr);
  jmi_block_residual->jacobian_variability =
    *(int *) Serializable_getField(serializable, FIELD_JACOBIAN_VARIABILITY, passport);
  MCLabFMILog_verbose("jacobian_variability: %d", jmi_block_residual->jacobian_variability);
  size_t value_references_size_t = (size_t) jmi_block_residual->n;
  Serializable_getArrayField(serializable, FIELD_VALUE_REFERENCES, (void **)jmi_block_residual->value_references,
  	                        sizeof(int), &value_references_size_t, 0, passport);
  MCLabFMILog_verbose("value_references[%d]", jmi_block_residual->n);
  ser_jmi_block_residual->ser_block_solver =
        *(SerializableJMI_BLOCK_SOLVER_T **) Serializable_getField(serializable, FIELD_BLOCK_SOLVER, passport);
  //TO_BE_SERIALIZED FIELD_SOLVE as type jmi_block_residual_solve_func_t
  //TO_BE_SERIALIZED FIELD_DELETE_SOLVER as type jmi_block_residual_delete_func_t
  //TO_BE_SERIALIZED FIELD_EVALUATE_JACOBIAN as type jmi_block_residual_jacobian_func_t
  jmi_block_residual->init =
    *(int *) Serializable_getField(serializable, FIELD_INIT, passport);
  MCLabFMILog_verbose("init: %d", jmi_block_residual->init);
  jmi_block_residual->at_event =
    *(int *) Serializable_getField(serializable, FIELD_AT_EVENT, passport);
  MCLabFMILog_verbose("at_event: %d", jmi_block_residual->at_event);
  jmi_block_residual->nb_calls =
    *(unsigned long *) Serializable_getField(serializable, FIELD_NB_CALLS, passport);
  MCLabFMILog_verbose("nb_calls: %lu", jmi_block_residual->nb_calls);
  jmi_block_residual->nb_iters =
    *(unsigned long *) Serializable_getField(serializable, FIELD_NB_ITERS, passport);
  MCLabFMILog_verbose("nb_iters: %lu", jmi_block_residual->nb_iters);
  jmi_block_residual->nb_jevals =
    *(unsigned long *) Serializable_getField(serializable, FIELD_NB_JEVALS, passport);
  MCLabFMILog_verbose("nb_jevals: %lu", jmi_block_residual->nb_jevals);
  jmi_block_residual->nb_fevals =
    *(unsigned long *) Serializable_getField(serializable, FIELD_NB_FEVALS, passport);
  MCLabFMILog_verbose("nb_fevals: %lu", jmi_block_residual->nb_fevals);
  jmi_block_residual->time_spent =
    *(double *) Serializable_getField(serializable, FIELD_TIME_SPENT, passport);
  MCLabFMILog_verbose("time_spent: %g", jmi_block_residual->time_spent);
  //TO_BE_SERIALIZED FIELD_MESSAGE_BUFFER as type char*
  MCLabFMILog_unnest();
}

SerializableJMI_BLOCK_RESIDUAL_T *SerializableJMI_BLOCK_RESIDUAL_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_block_residual_t *jmi_block_residual) {
  Debug_assert(DEBUG_ALWAYS, jmi_block_residual != NULL, "jmi_block_residual == NULL");
  SerializableJMI_BLOCK_RESIDUAL_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_block_residual)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_BLOCK_RESIDUAL_T));
    new->jmi_block_residual = jmi_block_residual;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_block_residual, new,
                    SerializableJMI_BLOCK_RESIDUAL_T_free, new->passport);
    // Adding inner struct new
    new->ser_jmi = SerializableJMI_T_new(fmi_env, jmi_block_residual->jmi);
    new->ser_options = SerializableJMI_BLOCK_SOLVER_OPTIONS_T_new(fmi_env, jmi_block_residual->options);
    new->ser_block_solver = SerializableJMI_BLOCK_SOLVER_T_new(fmi_env, jmi_block_residual->block_solver);
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_BLOCK_RESIDUAL_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_BLOCK_RESIDUAL_T *this_ = (SerializableJMI_BLOCK_RESIDUAL_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_BLOCK_RESIDUAL_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_BLOCK_RESIDUAL_T *this_ = (SerializableJMI_BLOCK_RESIDUAL_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
