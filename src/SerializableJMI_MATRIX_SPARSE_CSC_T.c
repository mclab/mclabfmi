/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "jmi_block_solver.h"
#include "jmi_block_solver_impl.h"
#include "SerializableJMI_MATRIX_SPARSE_CSC_T.h"

#include "MCLabFMIEnv.h"

#define DEBUG "SerializableJMI_MATRIX_SPARSE_CSC_T"

#define CLASS_NAME "jmi_matrix_sparse_csc_t"

// defining constants for fields name

#define FIELD_TYPE "type"
#define FIELD_NBR_COLS "nbr_cols"
#define FIELD_NBR_ROWS "nbr_rows"
#define FIELD_NNZ "nnz"
#define FIELD_COL_PTRS "col_ptrs"
#define FIELD_ROW_IND "row_ind"
#define FIELD_X "x"

struct SerializableJMI_MATRIX_SPARSE_CSC_T {
  jmi_matrix_sparse_csc_t *matrix;
  // Adding inner struct
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_MATRIX_SPARSE_CSC_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_MATRIX_SPARSE_CSC_T *ser_matrix = (SerializableJMI_MATRIX_SPARSE_CSC_T *)this_v;
  return ser_matrix->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_MATRIX_SPARSE_CSC_T *ser_matrix = (SerializableJMI_MATRIX_SPARSE_CSC_T *)this_v;
  jmi_matrix_sparse_csc_t *matrix = ser_matrix->matrix;
  const void *passport = ser_matrix->passport;
  Serializable *serializable = ser_matrix->serializable;

  int type = (int) matrix->type.type;
  MCLabFMILog_verbose("type = %d", type);
  Serializable_setField(serializable, FIELD_TYPE, PROPERTIES_INT, &type, passport);

  MCLabFMILog_verbose("nbr_cols = %d", matrix->nbr_cols);
  Serializable_setField(serializable, FIELD_NBR_COLS, PROPERTIES_INT, &matrix->nbr_cols, passport);

  MCLabFMILog_verbose("nbr_rows = %d", matrix->nbr_rows);
  Serializable_setField(serializable, FIELD_NBR_ROWS, PROPERTIES_INT, &matrix->nbr_rows, passport);

  MCLabFMILog_verbose("nnz = %d", matrix->nnz);
  Serializable_setField(serializable, FIELD_NNZ, PROPERTIES_INT, &matrix->nnz, passport);

  size_t col_ptrs_size = (size_t) matrix->nbr_cols + 1;
  MCLabFMILog_verbose("col_ptrs[%zu]", col_ptrs_size);
  Serializable_setArrayField(serializable, FIELD_COL_PTRS, PROPERTIES_INT, matrix->col_ptrs, sizeof(int), &col_ptrs_size, NULL, 0, false, NULL, passport);

  size_t row_ind_size = (size_t) matrix->nnz;
  MCLabFMILog_verbose("row_ind[%zu]", row_ind_size);
  Serializable_setArrayField(serializable, FIELD_ROW_IND, PROPERTIES_INT, matrix->row_ind, sizeof(int), &row_ind_size, NULL, 0, false, NULL, passport);

  size_t x_size = (size_t) matrix->nnz;
  MCLabFMILog_verbose("x[%zu]", x_size);
  Serializable_setArrayField(serializable, FIELD_X, PROPERTIES_DOUBLE, matrix->x, sizeof(double), &x_size, NULL, 0, false, NULL, passport);

  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_MATRIX_SPARSE_CSC_T *ser_matrix = (SerializableJMI_MATRIX_SPARSE_CSC_T *)this_v;
  jmi_matrix_sparse_csc_t *matrix = ser_matrix->matrix;
  const void *passport = ser_matrix->passport;
  Serializable *serializable = ser_matrix->serializable;
  // Deserializing simple fields via script

  int type = *((int *) Serializable_getField(serializable, FIELD_TYPE, passport));
  matrix->type.type = (jmi_matrix_type_t) type;
  MCLabFMILog_verbose("type = %d", type);

  matrix->nbr_cols = *((int *) Serializable_getField(serializable, FIELD_NBR_COLS, passport));
  MCLabFMILog_verbose("nbr_cols = %d", matrix->nbr_cols);

  matrix->nbr_rows = *((int *) Serializable_getField(serializable, FIELD_NBR_ROWS, passport));
  MCLabFMILog_verbose("nbr_rows = %d", matrix->nbr_rows);

  matrix->nnz = *((int *) Serializable_getField(serializable, FIELD_NNZ, passport));
  MCLabFMILog_verbose("nnz = %d", matrix->nnz);

  size_t col_ptrs_size = (size_t) matrix->nbr_cols + 1;
  MCLabFMILog_verbose("col_ptrs[%zu]", col_ptrs_size);
  Serializable_getArrayField(serializable, FIELD_COL_PTRS, matrix->col_ptrs, sizeof(int), &col_ptrs_size, 0, passport);

  size_t row_ind_size = (size_t) matrix->nnz;
  MCLabFMILog_verbose("row_ind[%zu]", row_ind_size);
  Serializable_getArrayField(serializable, FIELD_ROW_IND, matrix->row_ind, sizeof(int), &row_ind_size, 0, passport);

  size_t x_size = (size_t) matrix->nnz;
  MCLabFMILog_verbose("x[%zu]", x_size);
  Serializable_getArrayField(serializable, FIELD_X, matrix->x, sizeof(double), &x_size, 0, passport);

  MCLabFMILog_unnest();
}

SerializableJMI_MATRIX_SPARSE_CSC_T *SerializableJMI_MATRIX_SPARSE_CSC_T_new(MCLabFMIEnv *fmi_env, jmi_matrix_sparse_csc_t *matrix) {
  Debug_assert(DEBUG_ALWAYS, matrix != NULL, "matrix == NULL");
  SerializableJMI_MATRIX_SPARSE_CSC_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, matrix)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_MATRIX_SPARSE_CSC_T));
    new->matrix = matrix;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, matrix, new,
                    SerializableJMI_MATRIX_SPARSE_CSC_T_free, new->passport);
    // Adding inner struct new
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_MATRIX_SPARSE_CSC_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_MATRIX_SPARSE_CSC_T *this_ = (SerializableJMI_MATRIX_SPARSE_CSC_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_MATRIX_SPARSE_CSC_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_MATRIX_SPARSE_CSC_T *this_ = (SerializableJMI_MATRIX_SPARSE_CSC_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {
      .className = className,
      .passport = this_->passport,
      .serializeFields = serializeFields,
      .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
