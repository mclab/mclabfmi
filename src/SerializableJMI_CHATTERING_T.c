/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi_chattering.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_CHATTERING_T.h"

#define DEBUG "SerializableJMI_CHATTERING_T"

#define CLASS_NAME "jmi_chattering_t"

// defining constants for fields name
#define FIELD_CHATTERING_DETECTION_MODE "chattering_detection_mode"
#define FIELD_CLEAR_COUNTER "clear_counter"
#define FIELD_LOGGING_COUNTER "logging_counter"
#define FIELD_PRE_SWITCHES "pre_switches"
#define FIELD_CHATTERING "chattering"
#define FIELD_MAX_CHATTERING "max_chattering"

struct SerializableJMI_CHATTERING_T {
  jmi_chattering_t *jmi_chattering;
  // Adding inner struct
 size_t n_sw;
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_CHATTERING_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_CHATTERING_T *ser_jmi_chattering = (SerializableJMI_CHATTERING_T *)this_v;
  return ser_jmi_chattering->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_CHATTERING_T *ser_jmi_chattering = (SerializableJMI_CHATTERING_T *)this_v;
  jmi_chattering_t *jmi_chattering = ser_jmi_chattering->jmi_chattering;
  const void *passport = ser_jmi_chattering->passport;
  Serializable *serializable = ser_jmi_chattering->serializable;
  MCLabFMILog_verbose("chattering_detection_mode: %d", jmi_chattering->chattering_detection_mode);
  Serializable_setField(serializable, FIELD_CHATTERING_DETECTION_MODE, PROPERTIES_INT,
                       &jmi_chattering->chattering_detection_mode, passport);
  MCLabFMILog_verbose("clear_counter: %d", jmi_chattering->clear_counter);
  Serializable_setField(serializable, FIELD_CLEAR_COUNTER, PROPERTIES_INT,
                       &jmi_chattering->clear_counter, passport);
  MCLabFMILog_verbose("logging_counter: %d", jmi_chattering->logging_counter);
  Serializable_setField(serializable, FIELD_LOGGING_COUNTER, PROPERTIES_INT,
                       &jmi_chattering->logging_counter, passport);
  size_t pre_switches_size = (size_t) (ser_jmi_chattering->n_sw);
  MCLabFMILog_verbose("pre_switches[%zu]", pre_switches_size);
  Serializable_setArrayField(serializable, FIELD_PRE_SWITCHES, PROPERTIES_DOUBLE,
  	                        (void **)jmi_chattering->pre_switches, sizeof(double), &pre_switches_size,
  	                        NULL, 0, false, NULL, passport);
  size_t chattering_size = (size_t) (ser_jmi_chattering->n_sw);
  MCLabFMILog_verbose("chattering[%zu]", chattering_size);
  Serializable_setArrayField(serializable, FIELD_CHATTERING, PROPERTIES_INT,
  	                        (void **)jmi_chattering->chattering, sizeof(int), &chattering_size,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("max_chattering: %d", jmi_chattering->max_chattering);
  Serializable_setField(serializable, FIELD_MAX_CHATTERING, PROPERTIES_INT,
                       &jmi_chattering->max_chattering, passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_CHATTERING_T *ser_jmi_chattering = (SerializableJMI_CHATTERING_T *)this_v;
  jmi_chattering_t *jmi_chattering = ser_jmi_chattering->jmi_chattering;
  const void *passport = ser_jmi_chattering->passport;
  Serializable *serializable = ser_jmi_chattering->serializable;
  // Deserializing simple fields via script
  jmi_chattering->chattering_detection_mode =
    *(int *) Serializable_getField(serializable, FIELD_CHATTERING_DETECTION_MODE, passport);
  MCLabFMILog_verbose("chattering_detection_mode: %d", jmi_chattering->chattering_detection_mode);
  jmi_chattering->clear_counter =
    *(int *) Serializable_getField(serializable, FIELD_CLEAR_COUNTER, passport);
  MCLabFMILog_verbose("clear_counter: %d", jmi_chattering->clear_counter);
  jmi_chattering->logging_counter =
    *(int *) Serializable_getField(serializable, FIELD_LOGGING_COUNTER, passport);
  MCLabFMILog_verbose("logging_counter: %d", jmi_chattering->logging_counter);
  size_t pre_switches_size = (size_t) (ser_jmi_chattering->n_sw);
  Serializable_getArrayField(serializable, FIELD_PRE_SWITCHES, (void **)jmi_chattering->pre_switches,
  	                        sizeof(double), &pre_switches_size, 0, passport);
  MCLabFMILog_verbose("pre_switches[%zu]", pre_switches_size);
  size_t chattering_size = (size_t) (ser_jmi_chattering->n_sw);
  Serializable_getArrayField(serializable, FIELD_CHATTERING, (void **)jmi_chattering->chattering,
  	                        sizeof(int), &chattering_size, 0, passport);
  MCLabFMILog_verbose("chattering[%zu]", chattering_size);
  jmi_chattering->max_chattering =
    *(int *) Serializable_getField(serializable, FIELD_MAX_CHATTERING, passport);
  MCLabFMILog_verbose("max_chattering: %d", jmi_chattering->max_chattering);
  MCLabFMILog_unnest();
}

SerializableJMI_CHATTERING_T *SerializableJMI_CHATTERING_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_chattering_t *jmi_chattering, size_t n_sw) {
  Debug_assert(DEBUG_ALWAYS, jmi_chattering != NULL, "jmi_chattering == NULL");
  SerializableJMI_CHATTERING_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_chattering)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_CHATTERING_T));
    new->jmi_chattering = jmi_chattering;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_chattering, new,
                    SerializableJMI_CHATTERING_T_free, new->passport);
    // Adding inner struct new
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_CHATTERING_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_CHATTERING_T *this_ = (SerializableJMI_CHATTERING_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_CHATTERING_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_CHATTERING_T *this_ = (SerializableJMI_CHATTERING_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
