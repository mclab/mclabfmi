/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "jmi_block_solver.h"
#include "SerializableJMI_MINPACK_SOLVER_T.h"

#include "MCLabFMIEnv.h"

#define DEBUG "SerializableJMI_MINPACK_SOLVER_T"

#define CLASS_NAME "jmi_minpack_solver"

// defining constants for fields name



struct SerializableJMI_MINPACK_SOLVER_T {
  jmi_minpack_solver_t *solver;
  // Adding inner struct
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_MINPACK_SOLVER_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_MINPACK_SOLVER_T *ser_solver = (SerializableJMI_MINPACK_SOLVER_T *)this_v;
  return ser_solver->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  // SerializableJMI_MINPACK_SOLVER_T *ser_solver = (SerializableJMI_MINPACK_SOLVER_T *)this_v;
  // jmi_minpack_solver_t *solver = ser_solver->solver;
  // const void *passport = ser_solver->passport;
  // Serializable *serializable = ser_solver->serializable;

  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  // SerializableJMI_MINPACK_SOLVER_T *ser_solver = (SerializableJMI_MINPACK_SOLVER_T *)this_v;
  // jmi_minpack_solver_t *solver = ser_solver->solver;
  // const void *passport = ser_solver->passport;
  // Serializable *serializable = ser_solver->serializable;
  // Deserializing simple fields via script
  MCLabFMILog_unnest();
}

SerializableJMI_MINPACK_SOLVER_T *SerializableJMI_MINPACK_SOLVER_T_new(MCLabFMIEnv *fmi_env, jmi_minpack_solver_t *solver, jmi_block_solver_t *block_solver) {
  Debug_assert(DEBUG_ALWAYS, solver != NULL, "solver == NULL");
  SerializableJMI_MINPACK_SOLVER_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, solver)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_MINPACK_SOLVER_T));
    new->solver = solver;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, solver, new,
                    SerializableJMI_MINPACK_SOLVER_T_free, new->passport);
    // Adding inner struct new
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_MINPACK_SOLVER_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_MINPACK_SOLVER_T *this_ = (SerializableJMI_MINPACK_SOLVER_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_MINPACK_SOLVER_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_MINPACK_SOLVER_T *this_ = (SerializableJMI_MINPACK_SOLVER_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {
      .className = className,
      .passport = this_->passport,
      .serializeFields = serializeFields,
      .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
