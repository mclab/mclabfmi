/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "jmi_block_residual.h"
#include "jmi_block_solver.h"
#include "jmi_block_solver_impl.h"
#include "SerializableJMI_BLOCK_RESIDUAL_T.h"
#include "SerializableJMI_BLOCK_SOLVER_OPTIONS_T.h"
#include "SerializableJMI_CALLBACKS_T.h"
#include "SerializableJMI_BLOCK_SOLVER_T.h"
#include "SerializableDLSMAT.h"
#include "SerializableJMI_KINSOL_SOLVER_T.h"
#include "SerializableJMI_LINEAR_SOLVER_T.h"
#ifdef JMI_INCLUDE_MINPACK
#include "SerializableJMI_MINPACK_SOLVER_T.h"
#endif

#include "MCLabFMIEnv.h"
#include "SerializableJMI_BLOCK_SOLVER_T.h"

#define DEBUG "SerializableJMI_BLOCK_SOLVER_T"

#define CLASS_NAME "jmi_block_solver_t"

// defining constants for fields name
#define FIELD_PROBLEM_DATA "problem_data"
#define FIELD_OPTIONS "options"
#define FIELD_CALLBACKS "callbacks"
#define FIELD_LOG "log"//TO_BE_SERIALIZED
#define FIELD_LABEL "label"
#define FIELD_F_SCALE "f_scale"
#define FIELD_SCALE_UPDATE_TIME "scale_update_time"
#define FIELD_N "n"
#define FIELD_N_SR "n_sr"
#define FIELD_X "x"
#define FIELD_LAST_ACCEPTED_X "last_accepted_x"
#define FIELD_J "J"
#define FIELD_J_SCALE "J_scale"


#define FIELD_USING_MAX_MIN_SCALING_FLAG "using_max_min_scaling_flag"
#define FIELD_DX "dx"
#define FIELD_RES "res"
#define FIELD_DRES "dres"
#define FIELD_JAC "jac"
#define FIELD_IPIV "ipiv"

#define FIELD_PARENT_BLOCK "parent_block"
#define FIELD_IS_INIT_BLOCK "is_init_block"
#define FIELD_TIME_IN_BRENT "time_in_brent"
#define FIELD_TIME_F "time_f"
#define FIELD_TIME_DF "time_df"

#define FIELD_FUNC_EVAL_TIME "func_eval_time"
#define FIELD_JAC_EVAL_TIME "jac_eval_time"
#define FIELD_BROYDEN_UPDATE_TIME "broyden_update_time"
#define FIELD_STEP_CALC_TIME "step_calc_time"
#define FIELD_FACTORIZATION_TIME "factorization_time"
#define FIELD_BOUNDS_HANDLING_TIME "bounds_handling_time"
#define FIELD_LOGGING_TIME "logging_time"
#define FIELD_MIN "min"
#define FIELD_MAX "max"
#define FIELD_NOMINAL "nominal"
#define FIELD_RESIDUAL_NOMINAL "residual_nominal"
#define FIELD_RESIDUAL_HEURISTIC_NOMINAL "residual_heuristic_nominal"
#define FIELD_INITIAL "initial"
#define FIELD_START_SET "start_set"
#define FIELD_JACOBIAN_VARIABILITY "jacobian_variability"
#define FIELD_VALUE_REFERENCES "value_references"
#define FIELD_CUR_TIME "cur_time"
#define FIELD_FORCE_RESCALING "force_rescaling"
#define FIELD_SOLVER "solver"
#define FIELD_INIT "init"
#define FIELD_AT_EVENT "at_event"
#define FIELD_NB_CALLS "nb_calls"
#define FIELD_NB_ITERS "nb_iters"
#define FIELD_NB_JEVALS "nb_jevals"
#define FIELD_NB_FEVALS "nb_fevals"
#define FIELD_TIME_SPENT "time_spent"
#define FIELD_MESSAGE_BUFFER "message_buffer"//TO_BE_SERIALIZED
#define FIELD_CANARI "canari"
#define FIELD_RESIDUAL_ERROR_INDICATOR "residual_error_indicator"

struct SerializableJMI_BLOCK_SOLVER_T {
  jmi_block_solver_t *jmi_block_solver;
  // Adding inner struct
  SerializableJMI_BLOCK_RESIDUAL_T *ser_problem_data;
  SerializableJMI_BLOCK_SOLVER_OPTIONS_T *ser_options;
  SerializableJMI_CALLBACKS_T *ser_callbacks;
  SerializableJMI_BLOCK_SOLVER_T *ser_parent_block;
  SerializableDLSMAT *ser_J;
  SerializableDLSMAT *ser_J_scale;
  void *ser_solver; // SerializableXXX_SOLVER
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_BLOCK_SOLVER_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_BLOCK_SOLVER_T *ser_jmi_block_solver = (SerializableJMI_BLOCK_SOLVER_T *)this_v;
  return ser_jmi_block_solver->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_BLOCK_SOLVER_T *ser_jmi_block_solver = (SerializableJMI_BLOCK_SOLVER_T *)this_v;
  jmi_block_solver_t *jmi_block_solver = ser_jmi_block_solver->jmi_block_solver;
  const void *passport = ser_jmi_block_solver->passport;
  Serializable *serializable = ser_jmi_block_solver->serializable;
  Serializable_setField(serializable, FIELD_PROBLEM_DATA, PROPERTIES_POINTER,
                       SerializableJMI_BLOCK_RESIDUAL_T_asSerializable(ser_jmi_block_solver->ser_problem_data), passport);
  Serializable_setField(serializable, FIELD_OPTIONS, PROPERTIES_POINTER,
                       SerializableJMI_BLOCK_SOLVER_OPTIONS_T_asSerializable(ser_jmi_block_solver->ser_options), passport);
  Serializable_setField(serializable, FIELD_CALLBACKS, PROPERTIES_POINTER,
                       SerializableJMI_CALLBACKS_T_asSerializable(ser_jmi_block_solver->ser_callbacks), passport);
  //TO_BE_SERIALIZED FIELD_LOG as type jmi_log_t*
  MCLabFMILog_verbose("label: %s", jmi_block_solver->label);
  Serializable_setField(serializable, FIELD_LABEL, PROPERTIES_STRING,
                       (void *)jmi_block_solver->label, passport);
  MCLabFMILog_verbose("f_scale[%d]", jmi_block_solver->n);
  size_t f_scale_size_t = (size_t) jmi_block_solver->n;
  Serializable_setArrayField(serializable, FIELD_F_SCALE, PROPERTIES_DOUBLE,
                            N_VGetArrayPointer(jmi_block_solver->f_scale), sizeof(double), &f_scale_size_t,
                            NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("scale_update_time: %g", jmi_block_solver->scale_update_time);
  Serializable_setField(serializable, FIELD_SCALE_UPDATE_TIME, PROPERTIES_DOUBLE,
                       &jmi_block_solver->scale_update_time, passport);
  MCLabFMILog_verbose("n: %d", jmi_block_solver->n);
  Serializable_setField(serializable, FIELD_N, PROPERTIES_INT,
                       &jmi_block_solver->n, passport);
  MCLabFMILog_verbose("n_sr: %d", jmi_block_solver->n_sr);
  Serializable_setField(serializable, FIELD_N_SR, PROPERTIES_INT,
                       &jmi_block_solver->n_sr, passport);
  MCLabFMILog_verbose("x[%d]", jmi_block_solver->n);
  size_t x_size_t = (size_t) jmi_block_solver->n;
  Serializable_setArrayField(serializable, FIELD_X, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_solver->x, sizeof(double), &x_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("last_accepted_x[%d]", jmi_block_solver->n);
  size_t last_accepted_x_size_t = (size_t) jmi_block_solver->n;
  Serializable_setArrayField(serializable, FIELD_LAST_ACCEPTED_X, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_solver->last_accepted_x, sizeof(double), &last_accepted_x_size_t,
  	                        NULL, 0, false, NULL, passport);
  if (jmi_block_solver->n > 0) {
    Serializable_setField(serializable, FIELD_J, PROPERTIES_POINTER, SerializableDLSMAT_asSerializable(ser_jmi_block_solver->ser_J), passport);
    Serializable_setField(serializable, FIELD_J_SCALE, PROPERTIES_POINTER, SerializableDLSMAT_asSerializable(ser_jmi_block_solver->ser_J_scale), passport);
  }
  MCLabFMILog_verbose("using_max_min_scaling_flag: %d", jmi_block_solver->using_max_min_scaling_flag);
  Serializable_setField(serializable, FIELD_USING_MAX_MIN_SCALING_FLAG, PROPERTIES_INT,
                       &jmi_block_solver->using_max_min_scaling_flag, passport);
  MCLabFMILog_verbose("dx[%d]", jmi_block_solver->n);
  size_t dx_size_t = (size_t) jmi_block_solver->n;
  Serializable_setArrayField(serializable, FIELD_DX, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_solver->dx, sizeof(double), &dx_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("res[%d]", jmi_block_solver->n);
  size_t res_size_t = (size_t) jmi_block_solver->n;
  Serializable_setArrayField(serializable, FIELD_RES, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_solver->res, sizeof(double), &res_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("dres[%d]", jmi_block_solver->n);
  size_t dres_size_t = (size_t) jmi_block_solver->n;
  Serializable_setArrayField(serializable, FIELD_DRES, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_solver->dres, sizeof(double), &dres_size_t,
  	                        NULL, 0, false, NULL, passport);
  size_t jac_size = (size_t) (jmi_block_solver->n*jmi_block_solver->n);
  MCLabFMILog_verbose("jac[%zu]", jac_size);
  Serializable_setArrayField(serializable, FIELD_JAC, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_solver->jac, sizeof(double), &jac_size,
  	                        NULL, 0, false, NULL, passport);
  size_t ipiv_size = (size_t) (2*jmi_block_solver->n+1);
  MCLabFMILog_verbose("ipiv[%zu]", ipiv_size);
  Serializable_setArrayField(serializable, FIELD_IPIV, PROPERTIES_INT,
  	                        (void **)jmi_block_solver->ipiv, sizeof(int), &ipiv_size,
  	                        NULL, 0, false, NULL, passport);
#ifdef JMI_PROFILE_RUNTIME
  Serializable_setField(serializable, FIELD_PARENT_BLOCK, PROPERTIES_POINTER,
                       SerializableJMI_BLOCK_SOLVER_T_asSerializable(ser_jmi_block_solver->ser_parent_block), passport);
  MCLabFMILog_verbose("is_init_block: %d", jmi_block_solver->is_init_block);
  Serializable_setField(serializable, FIELD_IS_INIT_BLOCK, PROPERTIES_INT,
                       &jmi_block_solver->is_init_block, passport);
  MCLabFMILog_verbose("time_in_brent: %g", jmi_block_solver->time_in_brent);
  Serializable_setField(serializable, FIELD_TIME_IN_BRENT, PROPERTIES_DOUBLE,
                       &jmi_block_solver->time_in_brent, passport);
  MCLabFMILog_verbose("time_f: %g", jmi_block_solver->time_f);
  Serializable_setField(serializable, FIELD_TIME_F, PROPERTIES_DOUBLE,
                       &jmi_block_solver->time_f, passport);
  MCLabFMILog_verbose("time_df: %g", jmi_block_solver->time_df);
  Serializable_setField(serializable, FIELD_TIME_DF, PROPERTIES_DOUBLE,
                       &jmi_block_solver->time_df, passport);
#endif
  MCLabFMILog_verbose("func_eval_time: %g", jmi_block_solver->func_eval_time);
  Serializable_setField(serializable, FIELD_FUNC_EVAL_TIME, PROPERTIES_DOUBLE,
                       &jmi_block_solver->func_eval_time, passport);
  MCLabFMILog_verbose("jac_eval_time: %g", jmi_block_solver->jac_eval_time);
  Serializable_setField(serializable, FIELD_JAC_EVAL_TIME, PROPERTIES_DOUBLE,
                       &jmi_block_solver->jac_eval_time, passport);
  MCLabFMILog_verbose("broyden_update_time: %g", jmi_block_solver->broyden_update_time);
  Serializable_setField(serializable, FIELD_BROYDEN_UPDATE_TIME, PROPERTIES_DOUBLE,
                       &jmi_block_solver->broyden_update_time, passport);
  MCLabFMILog_verbose("step_calc_time: %g", jmi_block_solver->step_calc_time);
  Serializable_setField(serializable, FIELD_STEP_CALC_TIME, PROPERTIES_DOUBLE,
                       &jmi_block_solver->step_calc_time, passport);
  MCLabFMILog_verbose("factorization_time: %g", jmi_block_solver->factorization_time);
  Serializable_setField(serializable, FIELD_FACTORIZATION_TIME, PROPERTIES_DOUBLE,
                       &jmi_block_solver->factorization_time, passport);
  MCLabFMILog_verbose("bounds_handling_time: %g", jmi_block_solver->bounds_handling_time);
  Serializable_setField(serializable, FIELD_BOUNDS_HANDLING_TIME, PROPERTIES_DOUBLE,
                       &jmi_block_solver->bounds_handling_time, passport);
  MCLabFMILog_verbose("logging_time: %g", jmi_block_solver->logging_time);
  Serializable_setField(serializable, FIELD_LOGGING_TIME, PROPERTIES_DOUBLE,
                       &jmi_block_solver->logging_time, passport);
  MCLabFMILog_verbose("min[%d]", jmi_block_solver->n);
  size_t min_size_t = (size_t) jmi_block_solver->n;
  Serializable_setArrayField(serializable, FIELD_MIN, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_solver->min, sizeof(double), &min_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("max[%d]", jmi_block_solver->n);
  size_t max_size_t = (size_t) jmi_block_solver->n;
  Serializable_setArrayField(serializable, FIELD_MAX, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_solver->max, sizeof(double), &max_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("nominal[%d]", jmi_block_solver->n);
  size_t nominal_size_t = (size_t) jmi_block_solver->n;
  Serializable_setArrayField(serializable, FIELD_NOMINAL, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_solver->nominal, sizeof(double), &nominal_size_t,
  	                        NULL, 0, false, NULL, passport);
  size_t residual_nominal_size = (size_t) (jmi_block_solver->n+1);
  MCLabFMILog_verbose("residual_nominal[%zu]", residual_nominal_size);
  Serializable_setArrayField(serializable, FIELD_RESIDUAL_NOMINAL, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_solver->residual_nominal, sizeof(double), &residual_nominal_size,
  	                        NULL, 0, false, NULL, passport);
  size_t residual_heuristic_nominal_size = (size_t) (jmi_block_solver->n+1);
  MCLabFMILog_verbose("residual_heuristic_nominal[%zu]", residual_heuristic_nominal_size);
  Serializable_setArrayField(serializable, FIELD_RESIDUAL_HEURISTIC_NOMINAL, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_solver->residual_heuristic_nominal, sizeof(double), &residual_heuristic_nominal_size,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("initial[%d]", jmi_block_solver->n);
  size_t initial_size_t = (size_t) jmi_block_solver->n;
  Serializable_setArrayField(serializable, FIELD_INITIAL, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_solver->initial, sizeof(double), &initial_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("start_set[%d]", jmi_block_solver->n);
  size_t start_set_size_t = (size_t) jmi_block_solver->n;
  Serializable_setArrayField(serializable, FIELD_START_SET, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_solver->start_set, sizeof(double), &start_set_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("jacobian_variability: %d", jmi_block_solver->jacobian_variability);
  Serializable_setField(serializable, FIELD_JACOBIAN_VARIABILITY, PROPERTIES_INT,
                       &jmi_block_solver->jacobian_variability, passport);
  MCLabFMILog_verbose("value_references[%d]", jmi_block_solver->n);
  size_t value_references_size_t = (size_t) jmi_block_solver->n;
  Serializable_setArrayField(serializable, FIELD_VALUE_REFERENCES, PROPERTIES_DOUBLE,
  	                        (void **)jmi_block_solver->value_references, sizeof(double), &value_references_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_verbose("cur_time: %g", jmi_block_solver->cur_time);
  Serializable_setField(serializable, FIELD_CUR_TIME, PROPERTIES_DOUBLE,
                       &jmi_block_solver->cur_time, passport);
  MCLabFMILog_verbose("force_rescaling: %d", jmi_block_solver->force_rescaling);
  Serializable_setField(serializable, FIELD_FORCE_RESCALING, PROPERTIES_INT,
                       &jmi_block_solver->force_rescaling, passport);
  switch(jmi_block_solver->options->solver) {
    case JMI_KINSOL_SOLVER:
      Serializable_setField(serializable, FIELD_SOLVER, PROPERTIES_POINTER, SerializableJMI_KINSOL_SOLVER_T_asSerializable(ser_jmi_block_solver->ser_solver), passport);
      break;
    case JMI_MINPACK_SOLVER:
#ifdef JMI_INCLUDE_MINPACK
      Serializable_setField(serializable, FIELD_SOLVER, PROPERTIES_POINTER, SerializableJMI_MINPACK_SOLVER_T_asSerializable(ser_jmi_block_solver->ser_solver), passport);
      break;
#else
      assert(0);
#endif
    case JMI_SIMPLE_NEWTON_SOLVER:
      break;
    case JMI_LINEAR_SOLVER:
      Serializable_setField(serializable, FIELD_SOLVER, PROPERTIES_POINTER, SerializableJMI_LINEAR_SOLVER_T_asSerializable(ser_jmi_block_solver->ser_solver), passport);
      break;
    default:
      assert(0);
  }

  MCLabFMILog_verbose("init: %d", jmi_block_solver->init);
  Serializable_setField(serializable, FIELD_INIT, PROPERTIES_INT,
                       &jmi_block_solver->init, passport);
  MCLabFMILog_verbose("at_event: %d", jmi_block_solver->at_event);
  Serializable_setField(serializable, FIELD_AT_EVENT, PROPERTIES_INT,
                       &jmi_block_solver->at_event, passport);
  MCLabFMILog_verbose("nb_calls: %lu", jmi_block_solver->nb_calls);
  Serializable_setField(serializable, FIELD_NB_CALLS, PROPERTIES_ULONG,
                       &jmi_block_solver->nb_calls, passport);
  MCLabFMILog_verbose("nb_iters: %lu", jmi_block_solver->nb_iters);
  Serializable_setField(serializable, FIELD_NB_ITERS, PROPERTIES_ULONG,
                       &jmi_block_solver->nb_iters, passport);
  MCLabFMILog_verbose("nb_jevals: %lu", jmi_block_solver->nb_jevals);
  Serializable_setField(serializable, FIELD_NB_JEVALS, PROPERTIES_ULONG,
                       &jmi_block_solver->nb_jevals, passport);
  MCLabFMILog_verbose("nb_fevals: %lu", jmi_block_solver->nb_fevals);
  Serializable_setField(serializable, FIELD_NB_FEVALS, PROPERTIES_ULONG,
                       &jmi_block_solver->nb_fevals, passport);
  MCLabFMILog_verbose("time_spent: %g", jmi_block_solver->time_spent);
  Serializable_setField(serializable, FIELD_TIME_SPENT, PROPERTIES_DOUBLE,
                       &jmi_block_solver->time_spent, passport);
  //TO_BE_SERIALIZED FIELD_MESSAGE_BUFFER as type char*
  MCLabFMILog_verbose("canari: %g", jmi_block_solver->canari);
  Serializable_setField(serializable, FIELD_CANARI, PROPERTIES_DOUBLE,
                       &jmi_block_solver->canari, passport);
  MCLabFMILog_verbose("residual_error_indicator[%d]", jmi_block_solver->n);
  size_t residual_error_indicator_size_t = (size_t) jmi_block_solver->n;
  Serializable_setArrayField(serializable, FIELD_RESIDUAL_ERROR_INDICATOR, PROPERTIES_INT,
  	                        (void **)jmi_block_solver->residual_error_indicator, sizeof(int), &residual_error_indicator_size_t,
  	                        NULL, 0, false, NULL, passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_BLOCK_SOLVER_T *ser_jmi_block_solver = (SerializableJMI_BLOCK_SOLVER_T *)this_v;
  jmi_block_solver_t *jmi_block_solver = ser_jmi_block_solver->jmi_block_solver;
  const void *passport = ser_jmi_block_solver->passport;
  Serializable *serializable = ser_jmi_block_solver->serializable;
  // Deserializing simple fields via script
  ser_jmi_block_solver->ser_problem_data =
        *(SerializableJMI_BLOCK_RESIDUAL_T **) Serializable_getField(serializable, FIELD_PROBLEM_DATA, passport);
  ser_jmi_block_solver->ser_options =
        *(SerializableJMI_BLOCK_SOLVER_OPTIONS_T **) Serializable_getField(serializable, FIELD_OPTIONS, passport);
  ser_jmi_block_solver->ser_callbacks =
        *(SerializableJMI_CALLBACKS_T **) Serializable_getField(serializable, FIELD_CALLBACKS, passport);
  //TO_BE_SERIALIZED FIELD_LOG as type jmi_log_t*
  SAFETY_CHECK_STRING(serializable, jmi_block_solver->label, FIELD_LABEL, passport);
  MCLabFMILog_verbose("label: %s", jmi_block_solver->label);
  size_t f_scale_size_t = (size_t) jmi_block_solver->n;
  double *f_scale_ptr = N_VGetArrayPointer(jmi_block_solver->f_scale);
  Serializable_getArrayField(serializable, FIELD_F_SCALE, f_scale_ptr,
  	                        sizeof(double), &f_scale_size_t, 0, passport);
  MCLabFMILog_verbose("f_scale[%d]", jmi_block_solver->n);
  jmi_block_solver->scale_update_time =
    *(double *) Serializable_getField(serializable, FIELD_SCALE_UPDATE_TIME, passport);
  MCLabFMILog_verbose("scale_update_time: %g", jmi_block_solver->scale_update_time);
  jmi_block_solver->n =
    *(int *) Serializable_getField(serializable, FIELD_N, passport);
  MCLabFMILog_verbose("n: %d", jmi_block_solver->n);
  jmi_block_solver->n_sr =
    *(int *) Serializable_getField(serializable, FIELD_N_SR, passport);
  MCLabFMILog_verbose("n_sr: %d", jmi_block_solver->n_sr);
  size_t x_size_t = (size_t) jmi_block_solver->n;
  Serializable_getArrayField(serializable, FIELD_X, (void **)jmi_block_solver->x,
  	                        sizeof(double), &x_size_t, 0, passport);
  MCLabFMILog_verbose("x[%d]", jmi_block_solver->n);
  size_t last_accepted_x_size_t = (size_t) jmi_block_solver->n;
  Serializable_getArrayField(serializable, FIELD_LAST_ACCEPTED_X, (void **)jmi_block_solver->last_accepted_x,
  	                        sizeof(double), &last_accepted_x_size_t, 0, passport);
  MCLabFMILog_verbose("last_accepted_x[%d]", jmi_block_solver->n);
  if (jmi_block_solver->n > 0) {
    ser_jmi_block_solver->ser_J = *(SerializableDLSMAT **) Serializable_getField(serializable, FIELD_J, passport);
    ser_jmi_block_solver->ser_J_scale = *(SerializableDLSMAT **) Serializable_getField(serializable, FIELD_J_SCALE, passport);
  }
  jmi_block_solver->using_max_min_scaling_flag =
    *(int *) Serializable_getField(serializable, FIELD_USING_MAX_MIN_SCALING_FLAG, passport);
  MCLabFMILog_verbose("using_max_min_scaling_flag: %d", jmi_block_solver->using_max_min_scaling_flag);
  size_t dx_size_t = (size_t) jmi_block_solver->n;
  Serializable_getArrayField(serializable, FIELD_DX, (void **)jmi_block_solver->dx,
  	                        sizeof(double), &dx_size_t, 0, passport);
  MCLabFMILog_verbose("dx[%d]", jmi_block_solver->n);
  size_t res_size_t = (size_t) jmi_block_solver->n;
  Serializable_getArrayField(serializable, FIELD_RES, (void **)jmi_block_solver->res,
  	                        sizeof(double), &res_size_t, 0, passport);
  MCLabFMILog_verbose("res[%d]", jmi_block_solver->n);
  size_t dres_size_t = (size_t) jmi_block_solver->n;
  Serializable_getArrayField(serializable, FIELD_DRES, (void **)jmi_block_solver->dres,
  	                        sizeof(double), &dres_size_t, 0, passport);
  MCLabFMILog_verbose("dres[%d]", jmi_block_solver->n);
  size_t jac_size = (size_t) (jmi_block_solver->n*jmi_block_solver->n);
  Serializable_getArrayField(serializable, FIELD_JAC, (void **)jmi_block_solver->jac,
  	                        sizeof(double), &jac_size, 0, passport);
  MCLabFMILog_verbose("jac[%zu]", jac_size);
  size_t ipiv_size = (size_t) (2*jmi_block_solver->n+1);
  Serializable_getArrayField(serializable, FIELD_IPIV, (void **)jmi_block_solver->ipiv,
  	                        sizeof(int), &ipiv_size, 0, passport);
  MCLabFMILog_verbose("ipiv[%zu]", ipiv_size);
#ifdef JMI_PROFILE_RUNTIME
  ser_jmi_block_solver->ser_parent_block =
        *(SerializableJMI_BLOCK_SOLVER_T **) Serializable_getField(serializable, FIELD_PARENT_BLOCK, passport);
  jmi_block_solver->is_init_block =
    *(int *) Serializable_getField(serializable, FIELD_IS_INIT_BLOCK, passport);
  MCLabFMILog_verbose("is_init_block: %d", jmi_block_solver->is_init_block);
  jmi_block_solver->time_in_brent =
    *(double *) Serializable_getField(serializable, FIELD_TIME_IN_BRENT, passport);
  MCLabFMILog_verbose("time_in_brent: %g", jmi_block_solver->time_in_brent);
  jmi_block_solver->time_f =
    *(double *) Serializable_getField(serializable, FIELD_TIME_F, passport);
  MCLabFMILog_verbose("time_f: %g", jmi_block_solver->time_f);
  jmi_block_solver->time_df =
    *(double *) Serializable_getField(serializable, FIELD_TIME_DF, passport);
  MCLabFMILog_verbose("time_df: %g", jmi_block_solver->time_df);
#endif
  jmi_block_solver->func_eval_time =
    *(double *) Serializable_getField(serializable, FIELD_FUNC_EVAL_TIME, passport);
  MCLabFMILog_verbose("func_eval_time: %g", jmi_block_solver->func_eval_time);
  jmi_block_solver->jac_eval_time =
    *(double *) Serializable_getField(serializable, FIELD_JAC_EVAL_TIME, passport);
  MCLabFMILog_verbose("jac_eval_time: %g", jmi_block_solver->jac_eval_time);
  jmi_block_solver->broyden_update_time =
    *(double *) Serializable_getField(serializable, FIELD_BROYDEN_UPDATE_TIME, passport);
  MCLabFMILog_verbose("broyden_update_time: %g", jmi_block_solver->broyden_update_time);
  jmi_block_solver->step_calc_time =
    *(double *) Serializable_getField(serializable, FIELD_STEP_CALC_TIME, passport);
  MCLabFMILog_verbose("step_calc_time: %g", jmi_block_solver->step_calc_time);
  jmi_block_solver->factorization_time =
    *(double *) Serializable_getField(serializable, FIELD_FACTORIZATION_TIME, passport);
  MCLabFMILog_verbose("factorization_time: %g", jmi_block_solver->factorization_time);
  jmi_block_solver->bounds_handling_time =
    *(double *) Serializable_getField(serializable, FIELD_BOUNDS_HANDLING_TIME, passport);
  MCLabFMILog_verbose("bounds_handling_time: %g", jmi_block_solver->bounds_handling_time);
  jmi_block_solver->logging_time =
    *(double *) Serializable_getField(serializable, FIELD_LOGGING_TIME, passport);
  MCLabFMILog_verbose("logging_time: %g", jmi_block_solver->logging_time);
  size_t min_size_t = (size_t) jmi_block_solver->n;
  Serializable_getArrayField(serializable, FIELD_MIN, (void **)jmi_block_solver->min,
  	                        sizeof(double), &min_size_t, 0, passport);
  MCLabFMILog_verbose("min[%d]", jmi_block_solver->n);
  size_t max_size_t = (size_t) jmi_block_solver->n;
  Serializable_getArrayField(serializable, FIELD_MAX, (void **)jmi_block_solver->max,
  	                        sizeof(double), &max_size_t, 0, passport);
  MCLabFMILog_verbose("max[%d]", jmi_block_solver->n);
  size_t nominal_size_t = (size_t) jmi_block_solver->n;
  Serializable_getArrayField(serializable, FIELD_NOMINAL, (void **)jmi_block_solver->nominal,
  	                        sizeof(double), &nominal_size_t, 0, passport);
  MCLabFMILog_verbose("nominal[%d]", jmi_block_solver->n);
  size_t residual_nominal_size = (size_t) (jmi_block_solver->n+1);
  Serializable_getArrayField(serializable, FIELD_RESIDUAL_NOMINAL, (void **)jmi_block_solver->residual_nominal,
  	                        sizeof(double), &residual_nominal_size, 0, passport);
  MCLabFMILog_verbose("residual_nominal[%zu]", residual_nominal_size);
  size_t residual_heuristic_nominal_size = (size_t) (jmi_block_solver->n+1);
  Serializable_getArrayField(serializable, FIELD_RESIDUAL_HEURISTIC_NOMINAL, (void **)jmi_block_solver->residual_heuristic_nominal,
  	                        sizeof(double), &residual_heuristic_nominal_size, 0, passport);
  MCLabFMILog_verbose("residual_heuristic_nominal[%zu]", residual_heuristic_nominal_size);
  size_t initial_size_t = (size_t) jmi_block_solver->n;
  Serializable_getArrayField(serializable, FIELD_INITIAL, (void **)jmi_block_solver->initial,
  	                        sizeof(double), &initial_size_t, 0, passport);
  MCLabFMILog_verbose("initial[%d]", jmi_block_solver->n);
  size_t start_set_size_t = (size_t) jmi_block_solver->n;
  Serializable_getArrayField(serializable, FIELD_START_SET, (void **)jmi_block_solver->start_set,
  	                        sizeof(double), &start_set_size_t, 0, passport);
  MCLabFMILog_verbose("start_set[%d]", jmi_block_solver->n);
  jmi_block_solver->jacobian_variability =
    *(int *) Serializable_getField(serializable, FIELD_JACOBIAN_VARIABILITY, passport);
  MCLabFMILog_verbose("jacobian_variability: %d", jmi_block_solver->jacobian_variability);
  size_t value_references_size_t = (size_t) jmi_block_solver->n;
  Serializable_getArrayField(serializable, FIELD_VALUE_REFERENCES, (void **)jmi_block_solver->value_references,
  	                        sizeof(double), &value_references_size_t, 0, passport);
  MCLabFMILog_verbose("value_references[%d]", jmi_block_solver->n);
  jmi_block_solver->cur_time =
    *(double *) Serializable_getField(serializable, FIELD_CUR_TIME, passport);
  MCLabFMILog_verbose("cur_time: %g", jmi_block_solver->cur_time);
  jmi_block_solver->force_rescaling =
    *(int *) Serializable_getField(serializable, FIELD_FORCE_RESCALING, passport);
  MCLabFMILog_verbose("force_rescaling: %d", jmi_block_solver->force_rescaling);
  switch(jmi_block_solver->options->solver) {
    case JMI_KINSOL_SOLVER:
      ser_jmi_block_solver->ser_solver = (void *) *(SerializableJMI_KINSOL_SOLVER_T **) Serializable_getField(serializable, FIELD_SOLVER, passport);
      break;
    case JMI_MINPACK_SOLVER:
#ifdef JMI_INCLUDE_MINPACK
      ser_jmi_block_solver->ser_solver = (void *) *(SerializableJMI_MINPACK_SOLVER_T **) Serializable_getField(serializable, FIELD_SOLVER, passport);
      break;
#else
      assert(0);
#endif
    case JMI_SIMPLE_NEWTON_SOLVER:
      break;
    case JMI_LINEAR_SOLVER:
      ser_jmi_block_solver->ser_solver = (void *) *(SerializableJMI_LINEAR_SOLVER_T **) Serializable_getField(serializable, FIELD_SOLVER, passport);
      break;
    default:
      assert(0);
  }
  jmi_block_solver->init =
    *(int *) Serializable_getField(serializable, FIELD_INIT, passport);
  MCLabFMILog_verbose("init: %d", jmi_block_solver->init);
  jmi_block_solver->at_event =
    *(int *) Serializable_getField(serializable, FIELD_AT_EVENT, passport);
  MCLabFMILog_verbose("at_event: %d", jmi_block_solver->at_event);
  jmi_block_solver->nb_calls =
    *(unsigned long *) Serializable_getField(serializable, FIELD_NB_CALLS, passport);
  MCLabFMILog_verbose("nb_calls: %lu", jmi_block_solver->nb_calls);
  jmi_block_solver->nb_iters =
    *(unsigned long *) Serializable_getField(serializable, FIELD_NB_ITERS, passport);
  MCLabFMILog_verbose("nb_iters: %lu", jmi_block_solver->nb_iters);
  jmi_block_solver->nb_jevals =
    *(unsigned long *) Serializable_getField(serializable, FIELD_NB_JEVALS, passport);
  MCLabFMILog_verbose("nb_jevals: %lu", jmi_block_solver->nb_jevals);
  jmi_block_solver->nb_fevals =
    *(unsigned long *) Serializable_getField(serializable, FIELD_NB_FEVALS, passport);
  MCLabFMILog_verbose("nb_fevals: %lu", jmi_block_solver->nb_fevals);
  jmi_block_solver->time_spent =
    *(double *) Serializable_getField(serializable, FIELD_TIME_SPENT, passport);
  MCLabFMILog_verbose("time_spent: %g", jmi_block_solver->time_spent);
  //TO_BE_SERIALIZED FIELD_MESSAGE_BUFFER as type char*
  jmi_block_solver->canari =
    *(double *) Serializable_getField(serializable, FIELD_CANARI, passport);
  MCLabFMILog_verbose("canari: %g", jmi_block_solver->canari);
  size_t residual_error_indicator_size_t = (size_t) jmi_block_solver->n;
  Serializable_getArrayField(serializable, FIELD_RESIDUAL_ERROR_INDICATOR, (void **)jmi_block_solver->residual_error_indicator,
  	                        sizeof(int), &residual_error_indicator_size_t, 0, passport);
  MCLabFMILog_verbose("residual_error_indicator[%d]", jmi_block_solver->n);
  MCLabFMILog_unnest();
}

SerializableJMI_BLOCK_SOLVER_T *SerializableJMI_BLOCK_SOLVER_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_block_solver_t *jmi_block_solver) {
  Debug_assert(DEBUG_ALWAYS, jmi_block_solver != NULL, "jmi_block_solver == NULL");
  SerializableJMI_BLOCK_SOLVER_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_block_solver)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_BLOCK_SOLVER_T));
    new->jmi_block_solver = jmi_block_solver;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_block_solver, new,
                    SerializableJMI_BLOCK_SOLVER_T_free, new->passport);
    // Adding inner struct new
    new->ser_problem_data = SerializableJMI_BLOCK_RESIDUAL_T_new(fmi_env, jmi_block_solver->problem_data);
    new->ser_options = SerializableJMI_BLOCK_SOLVER_OPTIONS_T_new(fmi_env, jmi_block_solver->options);
    new->ser_callbacks = SerializableJMI_CALLBACKS_T_new(fmi_env, jmi_block_solver->callbacks);
#ifdef JMI_PROFILE_RUNTIME
    new->ser_parent_block = SerializableJMI_BLOCK_SOLVER_T_new(fmi_env, jmi_block_solver->parent_block);
#endif
    if (jmi_block_solver->n > 0) {
      new->ser_J = SerializableDLSMAT_new(fmi_env, jmi_block_solver->J);
      new->ser_J_scale = SerializableDLSMAT_new(fmi_env, jmi_block_solver->J_scale);
    }
    switch(jmi_block_solver->options->solver) {
      case JMI_KINSOL_SOLVER:
        new->ser_solver = SerializableJMI_KINSOL_SOLVER_T_new(fmi_env, jmi_block_solver->solver, jmi_block_solver);
        break;
      case JMI_MINPACK_SOLVER:
#ifdef JMI_INCLUDE_MINPACK
        new->ser_solver = SerializableJMI_MINPACK_SOLVER_T_new(fmi_env, jmi_block_solver->solver, jmi_block_solver);
        break;
#else
        assert(0);
#endif
      case JMI_SIMPLE_NEWTON_SOLVER:
        new->ser_solver = NULL;
        break;
      case JMI_LINEAR_SOLVER:
        new->ser_solver = SerializableJMI_LINEAR_SOLVER_T_new(fmi_env, jmi_block_solver->solver, jmi_block_solver);
        break;
      default:
        assert(0);
    }
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_BLOCK_SOLVER_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_BLOCK_SOLVER_T *this_ = (SerializableJMI_BLOCK_SOLVER_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_BLOCK_SOLVER_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_BLOCK_SOLVER_T *this_ = (SerializableJMI_BLOCK_SOLVER_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
