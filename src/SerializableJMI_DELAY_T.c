/*
#    This file is part of MCLabFMI
#    Copyright (C) 2020  Agostina Calabrese, Vadim Alimguzhin, Stefano Sinisi, Alessandro Steri
#
#    MCLabFMI is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabFMI is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabFMI.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <MCLabUtils.h>
#include "MCLabFMIUtils.h"

//Adding needed include
#include "jmi.h"
#include "jmi_delay_impl.h"
#include "SerializableJMI_DELAYBUFFER_T.h"
#include "SerializableJMI_DELAY_POSITION_T.h"

#include "MCLabFMIEnv.h"
#include "SerializableJMI_DELAY_T.h"

#define DEBUG "SerializableJMI_DELAY_T"

#define CLASS_NAME "jmi_delay_t"

// defining constants for fields name
#define FIELD_BUFFER "buffer"
#define FIELD_FIXED "fixed"
#define FIELD_NO_EVENT "no_event"
#define FIELD_POSITION "position"

struct SerializableJMI_DELAY_T {
  jmi_delay_t *jmi_delay;
  // Adding inner struct
  SerializableJMI_DELAYBUFFER_T *ser_buffer;
  SerializableJMI_DELAY_POSITION_T *ser_position;
  const char *class_name;
  void *passport;
  Serializable *serializable;
};

static void SerializableJMI_DELAY_T_free(void **thisP);

static const char *className(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL");
  SerializableJMI_DELAY_T *ser_jmi_delay = (SerializableJMI_DELAY_T *)this_v;
  return ser_jmi_delay->class_name;
}

static void serializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Serialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_DELAY_T *ser_jmi_delay = (SerializableJMI_DELAY_T *)this_v;
  jmi_delay_t *jmi_delay = ser_jmi_delay->jmi_delay;
  const void *passport = ser_jmi_delay->passport;
  Serializable *serializable = ser_jmi_delay->serializable;
  Serializable_setField(serializable, FIELD_BUFFER, PROPERTIES_POINTER,
                       SerializableJMI_DELAYBUFFER_T_asSerializable(ser_jmi_delay->ser_buffer), passport);
  MCLabFMILog_verbose("fixed: %d", jmi_delay->fixed);
  Serializable_setField(serializable, FIELD_FIXED, PROPERTIES_BOOLEAN,
                       &jmi_delay->fixed, passport);
  MCLabFMILog_verbose("no_event: %d", jmi_delay->no_event);
  Serializable_setField(serializable, FIELD_NO_EVENT, PROPERTIES_BOOLEAN,
                       &jmi_delay->no_event, passport);
  Serializable_setField(serializable, FIELD_POSITION, PROPERTIES_POINTER,
                       SerializableJMI_DELAY_POSITION_T_asSerializable(ser_jmi_delay->ser_position), passport);
  MCLabFMILog_unnest();
}

static void deserializeFields(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "Error: this_v == NULL\n");
  MCLabFMILog_info("Deserialize " CLASS_NAME " fields");
  MCLabFMILog_nest();
  SerializableJMI_DELAY_T *ser_jmi_delay = (SerializableJMI_DELAY_T *)this_v;
  jmi_delay_t *jmi_delay = ser_jmi_delay->jmi_delay;
  const void *passport = ser_jmi_delay->passport;
  Serializable *serializable = ser_jmi_delay->serializable;
  // Deserializing simple fields via script
  ser_jmi_delay->ser_buffer =
        *(SerializableJMI_DELAYBUFFER_T **) Serializable_getField(serializable, FIELD_BUFFER, passport);
  jmi_delay->fixed =
    *(char *) Serializable_getField(serializable, FIELD_FIXED, passport);
  MCLabFMILog_verbose("fixed: %c", jmi_delay->fixed);
  jmi_delay->no_event =
    *(char *) Serializable_getField(serializable, FIELD_NO_EVENT, passport);
  MCLabFMILog_verbose("no_event: %c", jmi_delay->no_event);
  ser_jmi_delay->ser_position =
        *(SerializableJMI_DELAY_POSITION_T **) Serializable_getField(serializable, FIELD_POSITION, passport);
  MCLabFMILog_unnest();
}

SerializableJMI_DELAY_T *SerializableJMI_DELAY_T_new(MCLabFMIEnv *fmi_env,
                                                 jmi_delay_t *jmi_delay) {
  Debug_assert(DEBUG_ALWAYS, jmi_delay != NULL, "jmi_delay == NULL");
  SerializableJMI_DELAY_T *new = NULL;
  if ((new = MCLabFMIEnv_get(fmi_env, CLASS_NAME, jmi_delay)) == NULL) {
    MCLabFMILog_nest();
    new = calloc(1, sizeof(SerializableJMI_DELAY_T));
    new->jmi_delay = jmi_delay;
    new->class_name = CLASS_NAME;
    new->passport = malloc(sizeof(char));
    new->serializable = NULL;
    MCLabFMIEnv_add(fmi_env, CLASS_NAME, jmi_delay, new,
                    SerializableJMI_DELAY_T_free, new->passport);
    // Adding inner struct new
    new->ser_buffer = SerializableJMI_DELAYBUFFER_T_new(fmi_env, &jmi_delay->buffer);
    new->ser_position = SerializableJMI_DELAY_POSITION_T_new(fmi_env, &jmi_delay->position);
    MCLabFMILog_unnest();
  }
  return new;
}
static void SerializableJMI_DELAY_T_free(void **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  SerializableJMI_DELAY_T *this_ = (SerializableJMI_DELAY_T *)*thisP;
  // TODO free of calloc-ated stuff and free of inner structure
  free(this_->passport);
  free(*thisP);
}
Serializable *SerializableJMI_DELAY_T_asSerializable(void *this_v) {
  Debug_assert(DEBUG_ALWAYS, this_v != NULL, "this_v == NULL\n");
  SerializableJMI_DELAY_T *this_ = (SerializableJMI_DELAY_T *)this_v;
  if (this_->serializable == NULL) {
    SerializableMethods methods = {.className = className,
                                   .passport = this_->passport,
                                   .serializeFields = serializeFields,
                                   .deserializeFields = deserializeFields};
    this_->serializable = Serializable_new(this_, methods);
  }
  return this_->serializable;
}
